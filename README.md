# Supplememntary Information for the manuscript 'Sequential Therapy in the lab and the patient' and it's supplementary information

Author: Christin Nyhoegen 
Contact: nyhoegen@evolbio.mpg.de

Scripts were run on MATLAB version 2022a/b

**Data availability**  All data generated for the manuscript can be found here https://doi.org/10.5281/zenodo.6653294 (The description of the data sets is given below)

Folder Deterministic_Model: Code to generate the data for the deterministic model 

Folder Figures:        Scripts to generate the figures, data files need to be available for these scripts (some scripts need access to function in folder Deterministic_Model - noted in the respective scripts) 

Folder Hybrid_Model:        Code to generate the data for the hybrid model 



## Description of functions in Deterministic Model


**Two_drug_Cycling_CC_u.m**: Deterministic model for sequential therapy in the patient 
 Input: struct array entries of variable P (which can be loaded from 'Cip.mat')

	    G    - struct array for initial population and mutation rates 

	    INT  - struct array for the interaction parameters 

	    PD   - struct array for the pharmacodynamic parameters 

	    PK   - struct array for the pharmacokinetic parameters 

	    CC   - struct array for the carrying capacity 

	    M    - struct array for some implementational changes 

	    -> The data can be generated with the Parameter_Script (there you find a detailed description on the different entries)
Output: dyn,data 

	    dyn - struct array with the time points and cell numbers per time, as well as overall cell number 
	    data - additional information such as growth curves, treatment etc. 

	    -> The data can be plotted with the function Plot_Cycling_Data(dyn,data,fig_number,'PAT' or 'LAB')

This function uses: 

- Psi_with_Int_CC_and_u: function to calculate the growth rate of a specific type, given current time point and population size 

-   Drug concentration: Function to calculate the drug concentration at a certain time point during a sequential regimen 

	The pharmacodynamic function was implemented in three ways 
	1. Exp_cycle: exponential decline of drug concentration (given the half life of the drug) - This was used in the publication
	2. Bolus_Cycle: two-compartment model with administration of dose at once 
	3. Infusion_Cycle: two-compartment model with administration of dose at continuously over a certain time 

- Mutation_Rate: function that calculate the rate of new mutants evolving from a given type (based on carrying capacity, mutation and replication rate)

- MyEvents: defines the event for the ode-system - this is only used for the case of P.M.cut = 1


**Lab_Cycling_NumCC_u.m**: Deterministic model for sequential therapy in the lab

 Input: struct array entries of variable P (wich can be loaded from 'Cip.mat')

	    G    - struct array for initial population and mutation rates 

	    PD   - struct array for the pharmacodynamic parameters 

	    Lab  - struct array for drug dosing in lab  

	    CC   - struct array for the carrying capacity 

	    M    - struct array for some implementational changes 

	    -> The data can be generated with the Parameter_Script (there you find a detailed description on the different entries)

Output: dyn,data 

	    dyn - struct array with the time points and cell numbers per time, as well as overall cell number 

	    data - additional information such as growth curves, treatment etc. 

	    -> The data can be plotted with the function Plot_Cycling_Data(dyn,data,fig_number,'PAT' or 'LAB')


This function uses: 

- Psi_of_c_CC: function to calculate the growth rate of a specific type, given current time point  (no drug-drug-interactions)

- Mutation_Rate: function that calculate the rate of new mutants evolving from a given type (based on carrying capacity, mutation and replication rate)

- MyEvents: defines the event for the ode-system - this is only used for the case of P.M.cut = 1


Data Generation and processing:

- Treatment_Comparison_Drug_Dose_Data: function to generate the data for a treatment given a wide range of drug concentration, measure for each treatment a variety of values such as time until threshold N_c (t_erad)

- Evaluate_Treatment: function used by Treatment_Comparison_.. to measure specific values 

- Calculate_Effective_Concnetration_Examples: Example script on how to calculate the first effective concentration 

- Example_Deterministic_Model: Test script on how to run the code  


## Description of functions in Hybrid Model


**Gillespie_Thinning_Hybrid_Full**: Hybrid algorithm for sequential therapy 

  Input: P         (Structarray containing all pharmacodynamic and pharmacokinetic information and information about drug-drug-interaction. An example can be loaded 
                    from 'Cip.m' (structarray with fields P (Patient) and L (Lab)).

        model       set to either 'LAB' or 'PAT' depending on the data in P 


        N_det       threshold above which the calculations are deterministic (I usually choose 1000)

	    eps         an range parameter for the choice of lambda bar around the time point at which drugs are administered (eps = 0.9 works well) 

	    seed        seed for random generator 

 Output: Sym        struct array with cell numbers for each type over time course of the treatment, timepoints, lambda_bar and lambda_t

 Function uses: 

- Deterministic_step: function that solves the ode system for all types with population sizes above the threshold for one step 

- Drug_Concentration: calculates the drug concentration depending on the PK settings (needs function Exp_cycle) 

- Lambda_i: gives out function handles for c_i (vector of probabilities for each event) depending on the state of the system and gives back lambda_i

- mu_of_c: calculates the death rate with the pharmacodynamic function given a certain drug concentration without drug-drug-interactions

- mu_with_Int.: calculates the death rate with the pharmacodynamic function based on the drug concnetration at the current time point in the treatment with drug-drug-interactions

- Reaction: depending on what reaction is chosen given a certain state of the system, the function gives back the updated cell numbers 

- Ci_max: Function to calculate the upper bound for the rates already multiplied with the cell numbers 



Other Functions (Described in detail above)

- Lab_Cycling_NumCC_u (needs Psi_of_c_CC_u)
- Two_Drugs_Cycling_CC_u (needs Psi_with_Int_CC_and_u)
(both need Mutation_Rate and Results can be plotted with Plot_Cycling_Data) 

- Example_Hybrid_Model: Test script on how to run the Algorithm 

-> Data Genration: The scripts were run on a HCP Cluster. For each drug concentration and run (100 runs) only the time until extinction was saved not the whole trajectory. 

## Description of Datasets

Each folder containing data is denoted as follows: 

TreatmentsXh_Drug_addionalInfo

- Xh: stands for the time between switches (default is 12h but we also looked at 8h and 24h cycling for one drug)

- Drug: denotes the paramter set used 

1. Cip = the parameter values according to Table 1 of the publication for slow replication and kappa=1

2. Fab1 = same parameters as Cip, except for psi_min which was set to -8.8 

3. Fab2 = same parameters as Cip, except for kappa which was set to 2

4. CipFab2 = mixed drug set, in which the first drug has the characteristics of Cip and the second of Fab2

- additionalInfo: Any additional variation for the drug set, inital population etc.

1. Bolus stand for a 2 compartment PK model with Bolus administration 

2. sgv stand for standing genetic variation and denotes the pre-existence of 92 single mutants per drug 

3. sgv stand for standing genetic variation and denotes the pre-existence of 1000 single mutants per drug 

4. Types used population in which resistance evolution is not possible (W) or the whole population is fully resistant (MAB)

5. fast means that the cells are replicating rapidly

The dataset itself will denote with: 

- P or L if it is data from a lab or patient model 

- sCS or wCS if collateral sensitivity is present (w = weak or s = strong)

- DDIa or DDIs if drug-drug interactions are present (a = antagonism and s = synergism)

- C denotes the concentration range which was usually 0-0.6 microgram/milliliter (0-35.5 times MIC)

- W or MAB denote in the dataset for additionalInfo = Types, if the population is fully susceptible without mutation (W) or fully resistant (MAB)

The data set C_star_.. or Effective_C_.. include the first effective concetration for the dataset (.._Types) and the full model respectively, for the denoted drug sets in the Lab (denoted by Lab) or Patient (no additional denotation)

**Deterministic Datasets**

Each data set contains for 16 different treatments (dimension of struct array) and 399 different drug concentrations (vector entry of fields) the following fields: 

- N_end, NI_end (where I denotes the type): cell numbers at the end of the treatment (1x399 vector)
- N_cyc, NI_cyc (where I denotes the type): cell numbers at the end of each administration (one hour before the next drug is applied - 399x80 vector)
- PI_end (where I denotes the type): relative frequency of a type at the end of the treatment (1x399 vector)
- PI_cyc (where I denotes the type): relative frequency of a type at the end of each administration (one hour before the next drug is applied - 399x80 vector)
- t_erad: Time until the threshold N_c is reached (1x399 vector)
- t_m_50: Time until mutants make 50% of the population (1x399 vector)
- t_mAB: Time until double mutant appears (1x399 vector)
- erad_mAB: Number of double mutants (which is below one) at t_erad (1x399 vector)
- t_erad_W: Time until the Wildtype is eradicated (1x399 vector)
- Nm_t_erad_W: Number of mutants at t_erad_W (1x399 vector)

The drug concentrations used are given in the dataset D_times_zMIC (to generate the data the concentration needs to be given independent of the zMIC -> divide values in D by 0.017 (the used zMIC-value))

**Simulation data**

Each Sim_TX_Y dataset cotain for the denotes treatments (X,Y) for each concentration (cell array entries) (careful: different treatments needed different initial concentrations -> data for the concentration is given in the Hybrid_Simulation folder) a vector of 100 (number of simulation runs) times until eradication. Dyn_TX_Y contains for every treatment one struct array entry with a vector of times until the threshold N_c is reached (one entry per concentration). 

	


	
