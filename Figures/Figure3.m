% Skript to generate Figure 4
% Initialize Matlab:
clc
clear
close all
%% Load Data: 

% Kappa = 1: 
%---Kappa = 1---------------------------------------------------------------
% Define Data Path
dataPath = 'Data/Treatments12h_Cip';
P_C = load(strcat(dataPath,'/Data_P_C0_06.mat')); 
P_C =P_C.P_data;
L_C = load(strcat(dataPath,'/Data_L_C0_06.mat')); 
L_C = L_C.P_data;

%---Kappa = 2---------------------------------------------------------------
dataPath = 'Data/Treatments12h_Fab2';
P_F2 = load(strcat(dataPath,'/Data_P_C0_06.mat')); 
P_F2 =P_F2.P_data;
L_F2 = load(strcat(dataPath,'/Data_L_C0_06.mat')); 
L_F2 = L_F2.P_data;

n = 200;
D1 = [linspace(0,0.2,n);linspace(0,0.2,n)];
D2 = [linspace(0.2,0.6,n);linspace(0.2,0.6,n)]; 
D = [D1(:,1:end-1),D2];
% Scale Drug Concnetration in fold change of MIC
D = D./0.017; 

%---Simulation Data: kappa = 1 ------------------------------------------- 
Sim1_C = load('Data/Hybrid_Simulations/Treatments12h_Cip/Sim_T1_2.mat'); 
Sim1_C = Sim1_C.Sim;
Sim1_C_L = load('Data/Hybrid_Simulations/Treatments12h_Cip/Sim_Lab_T1_2.mat'); 
Sim1_C_L = Sim1_C_L.Sim;

%T3 and T4
Sim2_C = load('Data/Hybrid_Simulations/Treatments12h_Cip/Sim_T3_4.mat'); 
Sim2_C = Sim2_C.Sim;
Sim2_C_L = load('Data/Hybrid_Simulations/Treatments12h_Cip/Sim_Lab_T3_4.mat'); 
Sim2_C_L = Sim2_C_L.Sim;

%T5 
Sim3_C = load('Data/Hybrid_Simulations/Treatments12h_Cip/Sim_T5.mat'); 
Sim3_C = Sim3_C.Sim;
Sim3_C_L = load('Data/Hybrid_Simulations/Treatments12h_Cip/Sim_Lab_T5.mat'); 
Sim3_C_L = Sim3_C_L.Sim;

% Combine:  
Sim_C = [Sim1_C;Sim2_C;Sim3_C]; 
Sim_C_L = [Sim1_C_L;Sim2_C_L;Sim3_C_L]; 
D_C = (0.08:0.01:0.4)/0.017;
D_C_L = (0.05:0.01:0.4)/0.017;

%---Simulation Data: kappa = 2 ------------------------------------------- 
Sim1_F2 = load('Data/Hybrid_Simulations/Treatments12h_Fab2/Sim_T1_2.mat'); 
Sim1_F2 = Sim1_F2.Sim;
Sim1_F2_L = load('Data/Hybrid_Simulations/Treatments12h_Fab2/Sim_Lab_T1_2.mat'); 
Sim1_F2_L = Sim1_F2_L.Sim;

%T3 and T4
Sim2_F2 = load('Data/Hybrid_Simulations/Treatments12h_Fab2/Sim_T3_4.mat'); 
Sim2_F2 = Sim2_F2.Sim;
Sim2_F2_L = load('Data/Hybrid_Simulations/Treatments12h_Fab2/Sim_Lab_T3_4.mat'); 
Sim2_F2_L = Sim2_F2_L.Sim;

%T5 
Sim3_F2 = load('Data/Hybrid_Simulations/Treatments12h_Fab2/Sim_T5.mat'); 
Sim3_F2 = Sim3_F2.Sim;
Sim3_F2_L = load('Data/Hybrid_Simulations/Treatments12h_Fab2/Sim_Lab_T5.mat'); 
Sim3_F2_L = Sim3_F2_L.Sim;

% Compbine:  
Sim_F2 = [Sim1_F2;Sim2_F2;Sim3_F2]; 
Sim_F2_L = [Sim1_F2_L;Sim2_F2_L;Sim3_F2_L]; 
D_F2 = (0.054:0.01:0.4)/0.017;
D_F2_L = (0.03:0.01:0.4)/0.017;

% Calculate mean values 
n_sim = 100; 
for i = 1:5
    for j = 1:length(D_C)
        Sim_t_erad_C(1,j) = mean(Sim_C{i,j},'omitnan'); 
        Sim_t_erad_std_C(1,j) = std(Sim_C{i,j},'omitnan'); 
    end
    Sim_mean_C{i} = Sim_t_erad_C; 
    Sim_std_C{i} = Sim_t_erad_std_C;
    Sim_sem_C{i} = Sim_t_erad_std_C/sqrt(n_sim);
end 

for i = 1:5
    for j = 1:length(D_C_L)
        Sim_t_erad_C_L(1,j) = mean(Sim_C_L{i,j},'omitnan'); 
        Sim_t_erad_std_C_L(1,j) = std(Sim_C_L{i,j},'omitnan'); 
    end
    Sim_mean_C_L{i} = Sim_t_erad_C_L; 
    Sim_std_C_L{i} = Sim_t_erad_std_C_L;
    Sim_sem_C_L{i} = Sim_t_erad_std_C_L/sqrt(n_sim);
end 

for i = 1:5
    for j = 1:length(D_F2)
        Sim_t_erad_F2(1,j) = mean(Sim_F2{i,j},'omitnan'); 
        Sim_t_erad_std_F2(1,j) = std(Sim_F2{i,j},'omitnan'); 
    end
    Sim_mean_F2{i} = Sim_t_erad_F2; 
    Sim_std_F2{i} = Sim_t_erad_std_F2;
    Sim_sem_F2{i} = Sim_t_erad_std_F2/sqrt(n_sim);
end

for i = 1:5
    for j = 1:length(D_F2_L)
        Sim_t_erad_F2_L(1,j) = mean(Sim_F2_L{i,j},'omitnan'); 
        Sim_t_erad_std_F2_L(1,j) = std(Sim_F2_L{i,j},'omitnan'); 
    end
    Sim_mean_F2_L{i} = Sim_t_erad_F2_L; 
    Sim_std_F2_L{i} = Sim_t_erad_std_F2_L;
    Sim_sem_F2_L{i} = Sim_t_erad_std_F2_L/sqrt(n_sim);
end
%% Plot Settings 

% Label for Legend 
treatments = {'12h','24h','1.5d','2d','2.5d','3d','3.5d','4d','5d','6d','7d','8d','9d'};

% Set the positions of the different subplots 
% length and height of sub-plots: 
sp_length = 0.35; 
sp_height = 0.16;
% position of subplots in x-direction 
x_pos1 = 0.08; 
x_pos2 = 0.5; 

% Position of the Subplots 
Position = [x_pos1,0.795,sp_length,sp_height;...
            x_pos2,0.795,sp_length,sp_height;...
            x_pos1,0.55,sp_length,sp_height;...
            x_pos2,0.55,sp_length,sp_height;...
            x_pos1,0.315,sp_length,sp_height;...
            x_pos2,0.315,sp_length,sp_height;...
            x_pos1,0.075,sp_length,sp_height;...
            x_pos2,0.075,sp_length,sp_height];

%Colors
dark = [2 93 114]/255;
pink = [178 93 141]/255;
light = [121 182 185]/255; 
occa = [215,200,100]/255;
black = [0 0 0];
% Define a color vector 
col = [dark;pink;light;occa];
% Define a colormap
col_n=4;
mymap1 = [linspace(black(1),pink(1),6)', linspace(black(2),pink(2),6)',linspace(black(3),pink(3),6)'];
mymap2 = [linspace(pink(1),occa(1),col_n)', linspace(pink(2),occa(2),col_n)',linspace(pink(3),occa(3),col_n)'];
mymap3 = [linspace(occa(1),dark(1),col_n)', linspace(occa(2),dark(2),col_n)',linspace(occa(3),dark(3),col_n)'];
mymap4 = [linspace(dark(1),light(1),col_n)', linspace(dark(2),light(2),col_n)',linspace(dark(3),light(3),col_n)'];
mymap5 = [linspace(light(1),light(1),col_n)', linspace(light(2),light(2),col_n)',linspace(light(3),light(3),col_n)'];
mymap = [mymap1;mymap2;mymap3;mymap4;mymap5];

% Linewidth
set(0, 'DefaultLineLineWidth', 1.5);
% Fontsizes
font = 10; 
font2 = 8;
font3 = 12;

% Selection of Colors
ind_col = [3,6,9,12,16];

%% Plot Figure sortet by Lab/Pat

% New color vector: 
grey_mat = flipud(gray(14)); 

% Plot figure 
fig2=figure(2);
% Set size of the figure 
fig2.Units = 'centimeters';
fig2.Position=[10,2,15.5,18]; 

h1 = subplot('Position',Position(1,:)); 
for j = 13:-1:6
    % Plot t_erad 
    plots(j) = plot(D(1,:),L_C(j).t_erad,'--','Color', grey_mat(j-3,:),'Linewidth',1);
    hold on 
end
for j = 5:-1:1
    % Plot t_erad 
    plots(j) = plot(D(1,:),L_C(j).t_erad,'Color', mymap(ind_col(j),:),'Linewidth',1.5);
    hold on 
end


% Set fontsize for axis
set(gca,'FontSize',font2);
% set axis propeties 
xlim([0,23.53])
ylim([0,300])
yticks([0,50,150,250]);
title('Lab','FontSize',font);

h2 = subplot('Position',Position(2,:)); 
for j = 13:-1:6
    % Plot t_erad 
    plots(j) = plot(D(1,:),P_C(j).t_erad,'--','Color',grey_mat(j-3,:),'Linewidth',1);
    hold on 
end
for j = 5:-1:1
    % Plot t_erad 
    plots(j) = plot(D(1,:),P_C(j).t_erad,'Color', mymap(ind_col(j),:),'Linewidth',1.5);
    hold on 
end
% Set fontsize for axis
set(gca,'FontSize',font2);
% set axis propeties 
xlim([0,23.53])
ylim([0,300])
yticks([0,50,150,250]);
title('Patient','FontSize',font);

h5 = subplot('Position',Position(5,:)); 
for j = 13:-1:6
    % Plot t_erad 
    plots(j) = plot(D(1,:),L_F2(j).t_erad,'--','Color', grey_mat(j-3,:),'Linewidth',1);
    hold on 
end
for j = 5:-1:1
    % Plot t_erad 
    plots(j) = plot(D(1,:),L_F2(j).t_erad,'Color', mymap(ind_col(j),:),'Linewidth',1.5);
    hold on 
end
% Set fontsize for axis
set(gca,'FontSize',font2);
% set axis propeties 
xlim([0,23.53])
ylim([0,300])
yticks([0,50,150,250,350]);
yticks([0,50,150,250]);
l1=legend(plots(13:-1:1),treatments(13:-1:1),'FontSize',font2,'Location','best');
l1.Position = [0.875,0.762,0.1,0.1];
l1.ItemTokenSize = [15 18];
title(l1,'Switch after');


h6= subplot('Position',Position(6,:)); 
for j = 13:-1:6
    % Plot t_erad 
    plots(j) = plot(D(1,:),P_F2(j).t_erad,'--','Color', grey_mat(j-3,:),'Linewidth',1);
    hold on 
end
for j = 5:-1:1
    % Plot t_erad 
    plots(j) = plot(D(1,:),P_F2(j).t_erad,'Color', mymap(ind_col(j),:),'Linewidth',1.5);
    hold on 
end
% Set fontsize for axis
set(gca,'FontSize',font2);
% set axis propeties 
xlim([0,23.53])
ylim([0,300])
yticks([0,50,150,250,350]);
%Inlcude Zoomin 
axes('position',[0.68 0.384 0.16 0.08])
box on
for i = 13:-1:6
    plot(D(1,:),P_F2(i).t_erad,'--','Color',grey_mat(i-3,:),'linewidth',1)
    hold on    
end
for i = 5:-1:1
    plot(D(1,:),P_F2(i).t_erad,'Color',mymap(ind_col(i),:),'linewidth',1)
    hold on    
end
%axis properties 
        ylim([20 200])
        xlim([3.5 7])
       yticks([60,120,180]);
        xticks([3,4,5,6,7]);


h3=subplot('Position',Position(3,:));
for i = 5:-1:1
    plot(D_C_L,Sim_mean_C_L{i},'Color',mymap(ind_col(i),:),'linewidth',1.5)
    hold on  
end
xlim([0 23.53]);
ylim([0,300])
set(gca,'FontSize',font2); 
yticks([0,50,150,250,350,450]);
%Inlcude Zoomin 
axes('position',[0.26 0.62 0.16 0.08])
box on
for i = 5:-1:1
    plot(D_C_L,Sim_mean_C_L{i},'Color',mymap(ind_col(i),:),'linewidth',1)
    hold on    
    %axis properties 
        ylim([15 90])
        xlim([4 18])
        yticks([20,40,60,80]);
        xticks([5,10,15]);
end


h7=subplot('Position',Position(7,:));
for i = 5:-1:1
    plots_sim(i) = plot(D_F2_L,Sim_mean_F2_L{i},'Color',mymap(ind_col(i),:),'linewidth',1.5);
    hold on  
end
xlim([0 23.53]);
ylim([0,300])
set(gca,'FontSize',font2); 
yticks([0,50,150,250,350]);
%Inlcude Zoomin 
axes('position',[0.26 0.145 0.16 0.08])
box on
for i = 5:-1:1
    plot(D_F2_L,Sim_mean_F2_L{i},'Color',mymap(ind_col(i),:),'linewidth',1)
    hold on  
    % axis properties 
        ylim([0 90])
        xlim([2 10])
        yticks([0,25,50,75]);
        xticks([3,5,7,9]);
end


h4=subplot('Position',Position(4,:));
for i = 5:-1:1
    plot(D_C(2:end),Sim_mean_C{i}(2:end),'Color',mymap(ind_col(i),:),'linewidth',1.5)
    hold on  
end
xlim([0 23.53]);
ylim([0,300])
yticks([0,50,150,250,350]);
set(gca,'FontSize',font2); 
%Inlcude Zoomin 
axes('position',[0.68 0.62 0.16 0.08])
box on
for i = 5:-1:1
    plot(D_C(2:end),Sim_mean_C{i}(2:end),'Color',mymap(ind_col(i),:),'linewidth',1)
    hold on  
    % axis properties 
        ylim([30 90])
        xlim([10 23.53])
        yticks([40,60,80]);
        xticks([11,15,19,23]);
end

h8=subplot('Position',Position(8,:));
for i = 5:-1:1
    plots_sim(i) = plot(D_F2(2:end),Sim_mean_F2{i}(2:end),'Color',mymap(ind_col(i),:),'linewidth',1.5);
    hold on  
end
xlim([0 23.53]);
ylim([0,300])
set(gca,'FontSize',font2); 
yticks([0,50,150,250,350]);
%Inlcude Zoomin 
axes('position',[0.68 0.145 0.16 0.08])
box on
for i = 5:-1:1
    plot(D_F2(2:end),Sim_mean_F2{i}(2:end),'Color',mymap(ind_col(i),:),'linewidth',1)
    hold on  
    % axis properties 
        ylim([20 200])
        xlim([3.75 7])
        yticks([60,120,180]);
        xticks([3,4,5,6,7]);
end


% % Commen Y label
p1=get(h3,'position');
p2=get(h5,'position');
height=p1(2)+p1(4)-p2(2);
h15=axes('position',[p1(1) p2(2) p2(3) height],'visible','off');
ylabel('Time until threshold $N_c$ in hours','visible','on','Fontsize',font,'Interpreter','latex');

% Commen X label 
p3=get(h7,'position');
p4=get(h8,'position');
width = p4(1)+p4(3)-p3(1); 
axes('position',[p3(1) p4(2) width p3(4)],'visible','off');
xlabel('Drug dose $D$ in multiples of zMIC$_W$','FontSize',font','visible','on','Fontsize',font,'Interpreter','latex');

   

%Annotate 'A' and 'B' 
dimA = [0.07,0.99,0,0];
annotation('textbox',dimA,'String','A','FitBoxToText','on','EdgeColor','none','Fontsize',font3);
dimC = [0.07,0.7425,0,0];
annotation('textbox',dimC,'String','B','FitBoxToText','on','EdgeColor','none','Fontsize',font3);
dimE = [0.07,0.51,0,0];
annotation('textbox',dimE,'String','C','FitBoxToText','on','EdgeColor','none','Fontsize',font3);
dimG = [0.07,0.27,0,0];
annotation('textbox',dimG,'String','D','FitBoxToText','on','EdgeColor','none','Fontsize',font3);

dimB = [0.81,0.99,0,0];
annotation('textbox',dimB,'String','E','FitBoxToText','on','EdgeColor','none','Fontsize',font3);
dimD = [0.81,0.7425,0,0];
annotation('textbox',dimD,'String','F','FitBoxToText','on','EdgeColor','none','Fontsize',font3);
dimF = [0.81,0.51,0,0];
annotation('textbox',dimF,'String','G','FitBoxToText','on','EdgeColor','none','Fontsize',font3);
dimH = [0.81,0.27,0,0];
annotation('textbox',dimH,'String','H','FitBoxToText','on','EdgeColor','none','Fontsize',font3);

dim5 = [0.327016129032258,0.961782408108314,0.304515402236154,0.038217591891686];
str5 = '$\kappa_A = \kappa_B = 1$, deterministic';
annotation('textbox',dim5,'String',str5,'FitBoxToText','on','EdgeColor','none','Fontsize',font,'fontweight','bold');

dim6 = [0.327016129032258,0.481782408108314,0.304515402236154,0.038217591891686];
str6 = '$\kappa_A = \kappa_B = 2$, deterministic';
annotation('textbox',dim6,'String',str6,'FitBoxToText','on','EdgeColor','none','Fontsize',font,'fontweight','bold');

dim8 = [0.32,0.716782408108314,0.272967618473599,0.038217591891686];
str8 = '$\kappa_A = \kappa_B = 1$, semi-stochastic';
annotation('textbox',dim8,'String',str8,'FitBoxToText','on','EdgeColor','none','Fontsize',font,'fontweight','bold');

dim7 = [0.32,0.241782408108314,0.272967618473599,0.038217591891686];
str7 = '$\kappa_A = \kappa_B = 2$, semi-stochastic';
annotation('textbox',dim7,'String',str7,'FitBoxToText','on','EdgeColor','none','Fontsize',font,'fontweight','bold');


set(findall(fig2,'-property','Interpreter'),'Interpreter','latex');
set(findall(fig2,'-property','TickLabelInterpreter'),'TickLabelInterpreter','latex');
