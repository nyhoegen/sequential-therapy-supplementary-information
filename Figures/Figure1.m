% Skript to generate Figure 1
% 02.09.22
% 

% Initialize Matlab:
clc
clear
close all

%% Generate Data 

% Define the example treatment (3xA,3xB,3xA,3xB)
treatment = [1,1,1,0,0,0,1,1,1,0,0,0]; 
% Define the time between treatments 
TA = 12; 
TB = 12; 

% Load dataset Cip.mat as example 
Cip = load('Cip_growth.mat'); 
P = Cip.P; 
L = Cip.L; 

% Set TA and TB: 
P.PK(1).Tthis = TA; 
P.PK(1).Tother = TB; 
P.PK(2).Tthis = TB; 
P.PK(2).Tother = TA;

% Set Drug Concentration 
c = 0.119;
P.PK(1).D = c; 
P.PK(2).D = c; 
L.Lab.cA = c; 
L.Lab.cB = c; 

% Set Treatment: 
P.PK(1).cycle = repmat([1 1 1 0 0 0],[1 4]);
P.PK(2).cycle = ~repmat([1 1 1 0 0 0],[1 4]);
L.Lab.cycle = repmat([1 1 1 0 0 0],[1 4]);
% Subplot 1: Treatment Schedule: 
% length of xaxis
cyc_A = TA*treatment;
cyc_B = TB*~treatment;
t_end = sum([cyc_A,cyc_B]);
cycle_img = repelem(treatment,TA);
% Xticks:
tick_val = 12*(1:12); 
cyc = {'A','A','A','B','B','B','A','A','A','B','B','B'};

% Subplot 2: Drug Concentration over time in the Patient 
% Subplot 3: Growth rate of the wildtype over time 
time = linspace(0,t_end+11,1000); 
cA_t = zeros(1,length(time)); 
cB_t = zeros(1,length(time)); 
psi_W_t = zeros(1,length(time)); 
for i = 1:length(time)
    cA_t(1,i) = Drug_Concentration(P.PK(1),time(i)); 
    cB_t(1,i) = Drug_Concentration(P.PK(2),time(i));
    psi_W_t(1,i) = Psi_with_Int_CC_and_u(P.INT,P.CC,P.PD(1),P.PK,0,time(i),0); 
end

% Subplot 4: Drug Concentration over time in the lab 
% Subplot 5: Growth rate of the wildtype over time lab
time = linspace(0,t_end+11,1000); 
cA_L = zeros(1,length(time)); 
cB_L = zeros(1,length(time)); 
psi_W_L = zeros(1,length(time)); 
for i = 1:length(time)
    if ~mod(floor(time(i)/36),2)
        cA_L(1,i) = c; 
        cB_L(1,i) = 0;
    else 
        cA_L(1,i) = 0; 
        cB_L(1,i) = c;
    end
    psi_W_L(1,i) = Psi_of_c_CC_u(L.PD(1),L.Lab,time(i),0,L.CC,0);
end

% Subplot 6: Transition Diagramm
% Included later via InkScape

% Subplot 7-9: Pharmacodynamics 
% Function handle for the pharmacodynamic function:  
% Kill rate 
mu = @(c,zMIC,p_max,k)  (p_max+6.5)*(c./zMIC).^k./((c./zMIC).^k+6.5/p_max);
% PD-function: 
psi = @(c,zMIC,p_max,CC,k) CC*p_max - mu(c,zMIC,p_max,k);

% DDI term: 
INT = @(c,I) 1 + (I*c./(0.05+c));
% Kill rate 
mu_int = @(cA,cB,zMIC,p_max,I,k)  ((p_max+6.5)*(cA./(INT(cB,I)*zMIC)).^k./((cA./(INT(cB,I)*zMIC)).^k+6.5/p_max));
% PD-function with DDI 
psi_int = @(cA,cB,zMICA,zMICB,pmax,I,k) pmax-mu_int(cA,cB,zMICA,pmax,I,k);

% define a vector of concnetrations 
c_vec = linspace(0,0.3,1000);

%Subplot 10: Example Dynamics 
[dyn,~] = Two_Drugs_Cycling_CC_u(P.G,P.INT,P.PD,P.PK,P.CC,P.M); 
%% Figure Settings

% Set the positions of the different subplots 
% length and height of sub-plots: 
sp_length = 0.4; 
sp_height1 = 0.175; 
sp_height2 = 0.065; 
% position of subplots in x-direction 
x_pos1 = 0.05; 
x_pos2 = 0.55; 

Pos = [x_pos1, 0.875,sp_length,sp_height2-0.035;...
       x_pos1, 0.66,sp_length,sp_height2;...
       x_pos1, 0.54,sp_length,sp_height2;...
       x_pos1, 0.42,sp_length,sp_height2;...
       x_pos1, 0.3,sp_length,sp_height2;...
       x_pos1, 0.05,sp_length,sp_height1;...
       x_pos2, 0.8,sp_length,sp_height1;...
       x_pos2, 0.55,sp_length,sp_height1;...
       x_pos2, 0.3,sp_length,sp_height1;...
       x_pos2, 0.05,sp_length,sp_height1]; 

%Colors
dark = [2 93 114]/255;
pink = [178 93 141]/255;
light = [121 182 185]/255; 
occa = [215,200,100]/255;
black = [0 0 0];
% Define a color vector 
col = [dark;pink;light;occa];
% Define a colormap
col_n=4;
mymap1 = [linspace(black(1),pink(1),6)', linspace(black(2),pink(2),6)',linspace(black(3),pink(3),6)'];
mymap2 = [linspace(pink(1),occa(1),col_n)', linspace(pink(2),occa(2),col_n)',linspace(pink(3),occa(3),col_n)'];
mymap3 = [linspace(occa(1),dark(1),col_n)', linspace(occa(2),dark(2),col_n)',linspace(occa(3),dark(3),col_n)'];
mymap4 = [linspace(dark(1),light(1),col_n)', linspace(dark(2),light(2),col_n)',linspace(dark(3),light(3),col_n)'];
mymap5 = [linspace(light(1),light(1),col_n)', linspace(light(2),light(2),col_n)',linspace(light(3),light(3),col_n)'];
mymap6 = [linspace(light(1),pink(1),20)', linspace(light(2),pink(2),20)',linspace(light(3),pink(3),20)'];
mymap = [mymap1;mymap2;mymap3;mymap4;mymap5];

% Linewidth
set(0, 'DefaultLineLineWidth', 1.5);
% Fontsizes
font = 10; 
font2 = 8;
font3 = 12;

%% Plot Data 
close all 

% Set the positions of the different subplots 
% length and height of sub-plots: 
sp_length = 0.4; 
sp_height1 = 0.165; 
sp_height2 = 0.05; 
% position of subplots in x-direction 
x_pos1 = 0.075; 
x_pos2 = 0.575; 

Pos = [x_pos1, 0.85,sp_length,sp_height2-0.025;...
       x_pos1, 0.655,sp_length,sp_height2;...
       x_pos1, 0.54,sp_length,sp_height2;...
       x_pos1, 0.41,sp_length,sp_height2;...
       x_pos1, 0.295,sp_length,sp_height2;...
       x_pos1, 0.05,sp_length,sp_height1;...
       x_pos2, 0.785,sp_length,sp_height1;...
       x_pos2, 0.54,sp_length,sp_height1;...
       x_pos2, 0.295,sp_length,sp_height1;...
       x_pos2, 0.05,sp_length,sp_height1]; 

% Plot second version 
fig1 = figure(1); 
fig1.Units = 'centimeter';
fig1.Position = [10,2,15.5,18]; 

subplot('Position',Pos(1,:));
% Plot image 
imagesc(cycle_img)
hold on 
title('Drug schedule', 'FontSize', font)
set(gca,'Fontsize', font2);
% Set ticks and color
set(gca,'YTickLabel',[]);
xticks(tick_val-6);
xticklabels(cyc);
colormap(mymap6)
% Add lines to seperate the administrations 
for i = 1:11
    line([i*12+0.5,i*12+0.5],[1.5,0.5],'Color',black,'LineWidth',0.5)
    hold on 
end
set(get(gca,'title'),'Position',[70 -2.4],'FontSize',font)
% Inlcude the description as text: 
annotation('textbox',[0.105,0.825,0.1,0.1],'String','T','FitBoxToText','on','EdgeColor','none','Fontsize',font);
annotation('textbox',[0.065,0.71,0.1,0.1],'String','time between switches','FitBoxToText','on','EdgeColor','none','Fontsize',font);

subplot('Position',Pos(4,:))
plot(time,cA_t,'Color',pink)
hold on 
plot(time,cB_t,'Color', light)
set(gca,'Fontsize', font2);
% xlabel('Time t')
ylabel('$c_A(t),c_B(t)$','Interpreter','latex','FontSize',font)
title('Drug concentration over time: Patient', 'FontSize', font)
ylim([0,0.15])
xlim([0,144])
set(gca,'YTick',[0,c],'YTickLabels',{'0','c'})


h5=subplot('Position',Pos(5,:));
plot(time,psi_W_t,'Color',dark)
set(gca,'Fontsize', font2);
% xlabel('Time t')
ylabel('$\psi_W$','Interpreter','latex','FontSize',font)
title('Growth rate of wild type over time: Patient', 'FontSize', font)
set(gca,'YTick',0);
xlim([0,144])
% p5=get(h5,'position');
% axes('position',[p5(1) p5(2)+0.02 p5(3) p5(4)],'visible','off');
% xlabel('Time $t$','visible','on','Interpreter','latex','FontSize',font)

subplot('Position',Pos(2,:))
plot(time,cA_L,'.','Color',pink)
hold on 
plot(time,cB_L,'.','Color', light)
set(gca,'Fontsize', font2);
% xlabel('Time t')
ylabel('$c_A(t),c_B(t)$','Interpreter','latex','FontSize',font)
title('Drug concentration over time: Lab', 'FontSize', font)
ylim([0,0.15])
xlim([0,144])
set(gca,'YTick',[0,c],'YTickLabels',{'0','c'})

subplot('Position',Pos(3,:))
plot(time,psi_W_L,'Color',dark)
set(gca,'Fontsize', font2);
ylabel('$\psi_W$','Interpreter','latex','FontSize',font)
title('Growth rate of wild type over time: Lab', 'FontSize', font)
xlim([0,144])
set(gca,'YTick',0);

h6=subplot('Position',Pos(6,:));
plot(dyn.time,dyn.W,'Color',dark)
hold on 
plot(dyn.time,dyn.MA,'Color',pink)
hold on 
plot(dyn.time,dyn.MB,'Color',light)
hold on 
plot(dyn.time,dyn.MAB,'Color',occa)
hold on 
set(gca,'Fontsize', font2);
ylabel('Cell numbers','Interpreter','latex','FontSize',font)
title('Example dynamics', 'FontSize', font)
leg6=legend('$W$','$M_A$','$M_B$','$M_{AB}$','Interpreter','latex');
leg6.ItemTokenSize = [15,18];
xlim([0,144]); 
set(gca,'YScale','log')
p6=get(h6,'position');
axes('position',[p6(1) p6(2)+0.005 p6(3) p6(4)],'visible','off');
xlabel('Time $t$','visible','on','Interpreter','latex','FontSize',font)


h7 = subplot('Position',Pos(7,:)); 
plot(c_vec/0.017,psi(c_vec,0.017,0.88,1,2),'Color',dark);
hold on 
plot(c_vec/0.017,psi(c_vec,0.017,0.88,1,1),'Color',pink);
hold on 
plot(nan,nan,'--','Color',[0,0,0,0.4]); 
hold on 
plot([0,15],[0,0],'--k','LineWidth',0.5)
hold on 
set(gca,'Fontsize', font2);
ylabel('$\psi_W(c_A,c_B=0,N=0)$','Interpreter','Latex','FontSize',font)
title('Pharmacodynamics: Hill coefficient', 'FontSize', font)
xlim([0,15]);
ylim([-7,2])
leg7 =legend('$\kappa_A = 2$','$\kappa_A = 1$','Interpreter','latex');
leg7.ItemTokenSize = [15,18];
p7=get(h7,'position');
axes('position',[p7(1) p7(2)+0.0075 p7(3) p7(4)],'visible','off');
xlabel('$c_A$ in multiples of zMIC$_W$','Interpreter','Latex','FontSize',font','visible','on','Fontsize',font);

h8 = subplot('Position',Pos(8,:)); 
plot([0,15],[0,0],'--k','LineWidth',0.5)
hold on 
plots(1) = plot(c_vec/0.017,psi(c_vec,0.017,0.88,1,2),'Color',dark);
hold on 
plots(2) = plot(c_vec/0.017,psi(c_vec,2*0.017,0.98*0.6-0.1,1,2),'Color',pink);
hold on 
plot(c_vec/0.017,psi(c_vec,0.017,0.88,0.2,2),'--','Color',[dark,0.4])
hold on 
plot(c_vec/0.017,psi(c_vec,2*0.017,0.98*0.6-0.1,0.2,2),'--','Color',[pink,0.4])
hold on 
plots(3) = plot(nan,nan,'Color',[0,0,0]); 
hold on 
set(gca,'Fontsize', font2);
ylabel('$\psi_X(c_A,c_B=0,N)$','Interpreter','Latex','FontSize',font)
title('Resistance \& Population density', 'FontSize', font)
xlim([0,15]);
ylim([-7,2])
leg8 =legend(plots,{'Wild type','Resistant to drug A'},'Interpreter','latex');
leg8.ItemTokenSize = [15,18];
p8=get(h8,'position');
axes('position',[p8(1) p8(2)+0.0075 p8(3) p8(4)],'visible','off');
xlabel('$c_A$ in multiples of zMIC$_W$','Interpreter','Latex','FontSize',font','visible','on','Fontsize',font);
% Annotate Arrow: 
x = [0.68 0.73];
y = [0.56 0.56];
annotation('textarrow',x,y,'String','$N= 0.8 \cdot K$','LineWidth',0.5,'HeadWidth',5,'HeadLength',5,'FontSize',font2,'Color',[0.3,0.3,0.3],'HeadStyle','vback3')
x = [0.68 0.73];
y = [0.56 0.62];
annotation('arrow',x,y,'LineWidth',0.5,'HeadWidth',5,'HeadLength',5,'Color',[0.3,0.3,0.3],'HeadStyle','vback3')
% Annotate Arrow: 
x = [0.9 0.85];
y = [0.6 0.595];
annotation('textarrow',x,y,'String','$N= 0$','LineWidth',0.5,'HeadWidth',5,'HeadLength',5,'FontSize',font2,'Color',[0.3,0.3,0.3],'HeadStyle','vback3')
x = [0.9 0.85];
y = [0.6 0.56];
annotation('arrow',x,y,'LineWidth',0.5,'HeadWidth',5,'HeadLength',5,'Color',[0.3,0.3,0.3],'HeadStyle','vback3')
% Annotate legend: 
% annotation('textbox',[0.65,0.86,0.1,0.1],'String','Mutant','FitBoxToText','on','EdgeColor','none','Fontsize',font2);
% annotation('textbox',[0.56,0.74,0.1,0.1],'String','Wildtype','FitBoxToText','on','EdgeColor','none','Fontsize',font2);
% annotation('textbox',[0.815,0.82,0.1,0.1],'String','$N=0$','Interpreter','latex','FitBoxToText','on','EdgeColor','none','Fontsize',font2);
% annotation('textbox',[0.815,0.8,0.1,0.1],'String','$N = 0.8 \cdot N_{max}$','Interpreter','latex','FitBoxToText','on','EdgeColor','none','Fontsize',font2);

h9=subplot('Position',Pos(10,:));
plot(c_vec/0.017,psi_int(c_vec,0.05,0.017,0.017,0.88,5,2),'Color',pink)
hold on 
plot(c_vec/0.017,psi_int(c_vec,0.05,0.017,0.017,0.88,0,2),'Color',dark)
hold on 
plot(c_vec/0.017,psi_int(c_vec,0.05,0.017,0.017,0.88,-0.9,2),'Color',light)
hold on 
plot([0,15],[0,0],'--k','LineWidth',0.5)
hold on 
set(gca,'Fontsize', font2);
ylabel('$\psi_W(c_A,c_B=0.05,N = 0)$','Interpreter','Latex','FontSize',font)
title('Drug-drug interactions', 'FontSize', font)
xlim([0,15]);
ylim([-7,2])
leg9=legend('Antagonism','Bliss independence','Synergism','Interpreter','latex');
leg9.ItemTokenSize = [15,18];
p9=get(h9,'position');
axes('position',[p9(1) p9(2)+0.0075 p9(3) p9(4)],'visible','off');
xlabel('$c_A$ in multiples of zMIC$_W$','Interpreter','Latex','FontSize',font','visible','on','Fontsize',font);


h10=subplot('Position',Pos(9,:));
plot(c_vec/0.017,psi(c_vec,0.017,0.98*0.6-0.1,1,2),'Color',pink)
hold on 
plot(c_vec/0.017,psi(c_vec,0.007,0.98*0.6-0.1,1,2),'-.','Color',pink)
hold on
plot([0,15],[0,0],'--k','LineWidth',0.5)
hold on 
set(gca,'Fontsize', font2);
ylabel('$\psi_A(c_A = 0,c_B,N=0)$','Interpreter','Latex','FontSize',font)
title('Collateral Sensitivity (CS)', 'FontSize', font)
xlim([0,15]);
ylim([-7,2]);
leg10=legend('Without CS','With CS','Interpreter','latex');
leg10.ItemTokenSize = [15,18];
p10=get(h10,'position');
axes('position',[p10(1) p10(2)+0.0075 p10(3) p10(4)],'visible','off');
xlabel('$c_B$ in multiples of zMIC$_W$','Interpreter','Latex','FontSize',font','visible','on','Fontsize',font);



%Annotate 'A' and 'B' 
dimA = [0.06,0.99,0,0];
annotation('textbox',dimA,'String','A','FitBoxToText','on','EdgeColor','none','Fontsize',font3);
dimB = [0,0.745,0,0];
annotation('textbox',dimB,'String','B','FitBoxToText','on','EdgeColor','none','Fontsize',font3);
dimC = [0,0.5,0,0];
annotation('textbox',dimC,'String','C','FitBoxToText','on','EdgeColor','none','Fontsize',font3);
dimD = [0.06,0.255,0,0];
annotation('textbox',dimD,'String','D','FitBoxToText','on','EdgeColor','none','Fontsize',font3);

dimE = [0.56,0.99,0,0];
annotation('textbox',dimE,'String','E','FitBoxToText','on','EdgeColor','none','Fontsize',font3);
dimF = [0.56,0.745,0,0];
annotation('textbox',dimF,'String','F','FitBoxToText','on','EdgeColor','none','Fontsize',font3);
dimG = [0.56,0.5,0,0];
annotation('textbox',dimG,'String','G','FitBoxToText','on','EdgeColor','none','Fontsize',font3);
dimH = [0.56,0.255,0,0];
annotation('textbox',dimH,'String','H','FitBoxToText','on','EdgeColor','none','Fontsize',font3);

set(findall(fig1,'-property','Interpreter'),'Interpreter','latex');
set(findall(fig1,'-property','TickLabelInterpreter'),'TickLabelInterpreter','latex');


