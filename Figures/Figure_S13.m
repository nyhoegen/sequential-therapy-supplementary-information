%% SI Figure for previous figure 8 
% Initialize Matlab 
close all 
clear  

%% Plot Settings 

% Label for Legend 
treatments = {'12h','24h','1.5d','2d','2.5d','3d','3.5d','4d','5d','6d','7d','8d','9d'};
% Set the positions of the different subplots 
% length and height of sub-plots: 
sp_length = 0.35; 
sp_height = 0.2;
% position of subplots in x-direction 
x_pos1 = 0.08; 
x_pos2 = 0.5; 

% Position of the Subplots 
Position = [x_pos1,0.7,sp_length,sp_height;...
            x_pos2,0.7,sp_length,sp_height;...
            x_pos1,0.4,sp_length,sp_height;...
            x_pos2,0.4,sp_length,sp_height;...
            x_pos1,0.1,sp_length,sp_height;...
            x_pos2,0.1,sp_length,sp_height];

        
%Colors
dark = [2 93 114]/255;
pink = [178 93 141]/255;
light = [121 182 185]/255; 
occa = [215,200,100]/255;
black = [0 0 0];
% Define a color vector 
col = [dark;pink;light;occa];
% Define a colormap
col_n=4;
mymap1 = [linspace(black(1),pink(1),6)', linspace(black(2),pink(2),6)',linspace(black(3),pink(3),6)'];
mymap2 = [linspace(pink(1),occa(1),col_n)', linspace(pink(2),occa(2),col_n)',linspace(pink(3),occa(3),col_n)'];
mymap3 = [linspace(occa(1),dark(1),col_n)', linspace(occa(2),dark(2),col_n)',linspace(occa(3),dark(3),col_n)'];
mymap4 = [linspace(dark(1),light(1),col_n)', linspace(dark(2),light(2),col_n)',linspace(dark(3),light(3),col_n)'];
mymap5 = [linspace(light(1),light(1),col_n)', linspace(light(2),light(2),col_n)',linspace(light(3),light(3),col_n)'];
mymap = [mymap1;mymap2;mymap3;mymap4;mymap5];

% Linewidth
set(0, 'DefaultLineLineWidth', 1.5);
% Fontsizes
font = 10; 
font2 = 8;
font3 = 12;

% Selection of Colors
ind_col = [3,6,9,12,16];
grey_mat = flipud(gray(14)); 
%% Load Data 
dec = 2; 
%---Kappa = 1---------------------------------------------------------------
% Define Data Path
dataPath = 'Data/Treatments12h_Cip';
P = load(strcat(dataPath,'/Data_P_C0_06.mat')); 
P = P.P_data;
L = load(strcat(dataPath,'/Data_L_C0_06.mat')); 
L  = L .P_data;

% CS
P_wCS = load(strcat(dataPath,'/Data_P_wCS_C0_06.mat')); 
L_wCS = load(strcat(dataPath,'/Data_L_wCS_C0_06.mat'));
P_sCS = load(strcat(dataPath,'/Data_P_sCS_C0_06.mat')); 
L_sCS = load(strcat(dataPath,'/Data_L_sCS_C0_06.mat')); 
P_wCS = P_wCS.P_data;
L_wCS = L_wCS.P_data;
P_sCS = P_sCS.P_data;
L_sCS = L_sCS.P_data;

% DDI
P_DDIa = load(strcat(dataPath,'/Data_P_DDIa_C0_06.mat')); 
P_DDIs = load(strcat(dataPath,'/Data_P_DDIs_C0_06.mat')); 
P_DDIa = P_DDIa.P_data;
P_DDIs = P_DDIs.P_data;

% DDI and sCS
P_sCS_DDIa = load(strcat(dataPath,'/Data_P_sCS_DDIa_C0_06.mat')); 
P_sCS_DDIs = load(strcat(dataPath,'/Data_P_sCS_DDIs_C0_06.mat'));  
P_sCS_DDIa = P_sCS_DDIa.P_data;
P_sCS_DDIs = P_sCS_DDIs.P_data;



%---Kappa = 2---------------------------------------------------------------
% Define Data Path
dataPath = 'Data/Treatments12h_Fab2';
P_F = load(strcat(dataPath,'/Data_P_C0_06.mat')); 
P_F = P_F.P_data;
L_F = load(strcat(dataPath,'/Data_L_C0_06.mat')); 
L_F  = L_F.P_data;

% CS
P_wCS_F = load(strcat(dataPath,'/Data_P_wCS_C0_06.mat')); 
L_wCS_F = load(strcat(dataPath,'/Data_L_wCS_C0_06.mat'));
P_sCS_F = load(strcat(dataPath,'/Data_P_sCS_C0_06.mat')); 
L_sCS_F = load(strcat(dataPath,'/Data_L_sCS_C0_06.mat')); 
P_wCS_F = P_wCS_F.P_data;
L_wCS_F = L_wCS_F.P_data;
P_sCS_F = P_sCS_F.P_data;
L_sCS_F= L_sCS_F.P_data;

% DDI
P_DDIa_F = load(strcat(dataPath,'/Data_P_DDIa_C0_06.mat')); 
P_DDIs_F = load(strcat(dataPath,'/Data_P_DDIs_C0_06.mat')); 
P_DDIa_F = P_DDIa_F.P_data;
P_DDIs_F= P_DDIs_F.P_data;

% DDI and sCS
P_sCS_DDIa_F = load(strcat(dataPath,'/Data_P_sCS_DDIa_C0_06.mat')); 
P_sCS_DDIs_F = load(strcat(dataPath,'/Data_P_sCS_DDIs_C0_06.mat'));  
P_sCS_DDIa_F = P_sCS_DDIa_F.P_data;
P_sCS_DDIs_F = P_sCS_DDIs_F.P_data;


%--SGV---------------------------------------------------------------------

%---Kappa = 1---------------------------------------------------------------
% Define Data Path
dataPath = 'Data/Treatments12h_Cip_sgv';
P_sgv = load(strcat(dataPath,'/Data_P_C0_06.mat')); 
P_sgv = P_sgv.P_data;
L_sgv = load(strcat(dataPath,'/Data_L_C0_06.mat')); 
L_sgv  = L_sgv .P_data;

% CS
P_wCS_sgv = load(strcat(dataPath,'/Data_P_wCS_C0_06.mat')); 
L_wCS_sgv = load(strcat(dataPath,'/Data_L_wCS_C0_06.mat'));
P_sCS_sgv = load(strcat(dataPath,'/Data_P_sCS_C0_06.mat')); 
L_sCS_sgv = load(strcat(dataPath,'/Data_L_sCS_C0_06.mat')); 
P_wCS_sgv = P_wCS_sgv.P_data;
L_wCS_sgv = L_wCS_sgv.P_data;
P_sCS_sgv = P_sCS_sgv.P_data;
L_sCS_sgv = L_sCS_sgv.P_data;

% DDI
P_DDIa_sgv = load(strcat(dataPath,'/Data_P_DDIa_C0_06.mat')); 
P_DDIs_sgv = load(strcat(dataPath,'/Data_P_DDIs_C0_06.mat')); 
P_DDIa_sgv = P_DDIa_sgv.P_data;
P_DDIs_sgv = P_DDIs_sgv.P_data;



% SGV2---------------------------------------------------------------------
%---Kappa = 1---------------------------------------------------------------
% Define Data Path
dataPath = 'Data/Treatments12h_Cip_sgv2';
P_sgv2 = load(strcat(dataPath,'/Data_P_C0_06.mat')); 
P_sgv2 = P_sgv2.P_data;
L_sgv2 = load(strcat(dataPath,'/Data_L_C0_06.mat')); 
L_sgv2  = L_sgv2.P_data;

% CS
P_wCS_sgv2 = load(strcat(dataPath,'/Data_P_wCS_C0_06.mat')); 
L_wCS_sgv2 = load(strcat(dataPath,'/Data_L_wCS_C0_06.mat'));
P_sCS_sgv2 = load(strcat(dataPath,'/Data_P_sCS_C0_06.mat')); 
L_sCS_sgv2 = load(strcat(dataPath,'/Data_L_sCS_C0_06.mat')); 
P_wCS_sgv2 = P_wCS_sgv2.P_data;
L_wCS_sgv2 = L_wCS_sgv2.P_data;
P_sCS_sgv2 = P_sCS_sgv2.P_data;
L_sCS_sgv2 = L_sCS_sgv2.P_data;

% DDI
P_DDIa_sgv2 = load(strcat(dataPath,'/Data_P_DDIa_C0_06.mat')); 
P_DDIs_sgv2 = load(strcat(dataPath,'/Data_P_DDIs_C0_06.mat')); 
P_DDIa_sgv2 = P_DDIa_sgv2.P_data;
P_DDIs_sgv2 = P_DDIs_sgv2.P_data;

%---Kappa = 2---------------------------------------------------------------
% Define Data Path
dataPath = 'Data/Treatments12h_Fab2_sgv';
P_F_sgv = load(strcat(dataPath,'/Data_P_C0_06.mat')); 
P_F_sgv = P_F_sgv.P_data;
L_F_sgv = load(strcat(dataPath,'/Data_L_C0_06.mat')); 
L_F_sgv  = L_F_sgv.P_data;

% CS
P_wCS_F_sgv = load(strcat(dataPath,'/Data_P_wCS_C0_06.mat')); 
L_wCS_F_sgv = load(strcat(dataPath,'/Data_L_wCS_C0_06.mat'));
P_sCS_F_sgv = load(strcat(dataPath,'/Data_P_sCS_C0_06.mat')); 
L_sCS_F_sgv= load(strcat(dataPath,'/Data_L_sCS_C0_06.mat')); 
P_wCS_F_sgv = P_wCS_F_sgv.P_data;
L_wCS_F_sgv = L_wCS_F_sgv.P_data;
P_sCS_F_sgv = P_sCS_F_sgv.P_data;
L_sCS_F_sgv= L_sCS_F_sgv.P_data;

% DDI
P_DDIa_F_sgv = load(strcat(dataPath,'/Data_P_DDIa_C0_06.mat')); 
P_DDIs_F_sgv = load(strcat(dataPath,'/Data_P_DDIs_C0_06.mat')); 
P_DDIa_F_sgv = P_DDIa_F_sgv.P_data;
P_DDIs_F_sgv= P_DDIs_F_sgv.P_data;

%SGV2----------------------------------------------------------------------
% Define Data Path
dataPath = 'Data/Treatments12h_Fab2_sgv2';
P_F_sgv2 = load(strcat(dataPath,'/Data_P_C0_06.mat')); 
P_F_sgv2 = P_F_sgv2.P_data;
L_F_sgv2 = load(strcat(dataPath,'/Data_L_C0_06.mat')); 
L_F_sgv2  = L_F_sgv2.P_data;

% CS
P_wCS_F_sgv2 = load(strcat(dataPath,'/Data_P_wCS_C0_06.mat')); 
L_wCS_F_sgv2 = load(strcat(dataPath,'/Data_L_wCS_C0_06.mat'));
P_sCS_F_sgv2 = load(strcat(dataPath,'/Data_P_sCS_C0_06.mat')); 
L_sCS_F_sgv2= load(strcat(dataPath,'/Data_L_sCS_C0_06.mat')); 
P_wCS_F_sgv2 = P_wCS_F_sgv2.P_data;
L_wCS_F_sgv2 = L_wCS_F_sgv2.P_data;
P_sCS_F_sgv2 = P_sCS_F_sgv2.P_data;
L_sCS_F_sgv2= L_sCS_F_sgv2.P_data;

% DDI
P_DDIa_F_sgv2 = load(strcat(dataPath,'/Data_P_DDIa_C0_06.mat')); 
P_DDIs_F_sgv2 = load(strcat(dataPath,'/Data_P_DDIs_C0_06.mat')); 
P_DDIa_F_sgv2 = P_DDIa_F_sgv2.P_data;
P_DDIs_F_sgv2= P_DDIs_F_sgv2.P_data;




n = 200;
D1 = [linspace(0,0.2,n);linspace(0,0.2,n)];
D2 = [linspace(0.2,0.6,n);linspace(0.2,0.6,n)]; 
D = [D1(:,1:end-1),D2];
% Scale Drug Concnetration in fold change of MIC
D = D./0.017; 

%% Figure 1 - with right kappa 2

% Set the positions of the different subplots 
% length and height of sub-plots: 
sp_length = 0.35; 
sp_height = 0.2;
% position of subplots in x-direction 
x_pos1 = 0.3; 
x_pos2 = 0.5; 

% Position of the Subplots 
Position = [x_pos1,0.7,sp_length,sp_height;...
            x_pos1,0.4,sp_length,sp_height;...
            x_pos1,0.1,sp_length,sp_height];

% Concentartions: 
ind_sgv = 93;
c2_ind = ind_sgv; 
c4_ind = ind_sgv; 
c6_ind = ind_sgv; 
c7_ind = 115;
c8_ind = 190;

fig1=figure(1); 
% Set size of the figure 
fig1.Units = 'centimeters';
fig1.Position=[10,2,15.5,15]; 


    % Subplot 1 --------------------------------------------------------------
for i = 1:13
    y(1,i) = P_F(i).t_erad(c2_ind); 
    y(2,i) = P_sCS_F(i).t_erad(c2_ind); 
    y(3,i) = P_DDIa_F(i).t_erad(c2_ind); 
    y(4,i) = P_DDIs_F(i).t_erad(c2_ind); 
end
s2 =subplot('Position',Position(1,:)); 
b=bar(y,'EdgeColor',0.35*ones(1,3));
hold on 
for i = 1:5 
    b(i).FaceColor = mymap(ind_col(i),:);
end   
for i = 6:13 
    b(i).FaceColor = grey_mat(i-3,:);
    b(i).FaceAlpha = 0.7;
end
set(gca,'FontSize',font2)
set(gca,'xTickLabels',{'none','CS','Ant','Syn'});
    for i = 1:13 
        for k = 1:4 
            if isnan(y(k,i))
                plot(b(i).XEndPoints(k),10,'xk','MarkerSize',2.5,'LineWidth',0.5)

            end
        end
    end
title('No pre-existing resistance','Interpreter','latex')
l1=legend(b(1:13),treatments(1:13),'FontSize',font2,'Location','best');
l1.Position = [0.7,0.675,0.1,0.1];
l1.ItemTokenSize = [15 18];
title(l1,'Switch after');
% Subplot 2 --------------------------------------------------------------
for i = 1:13
    y(1,i) = P_F_sgv(i).t_erad(c4_ind); 
    y(2,i) = P_sCS_F_sgv(i).t_erad(c4_ind); 
    y(3,i) = P_DDIa_F_sgv(i).t_erad(c4_ind); 
    y(4,i) = P_DDIs_F_sgv(i).t_erad(c4_ind);  
end
s4 = subplot('Position',Position(2,:));
b=bar(y,'EdgeColor',0.35*ones(1,3));
hold on 
for i = 1:5 
    b(i).FaceColor = mymap(ind_col(i),:);
end   
for i = 6:13 
    b(i).FaceColor = grey_mat(i-3,:);
    b(i).FaceAlpha = 0.7;
end
set(gca,'FontSize',font2)
set(gca,'xTickLabels',{'none','CS','Ant','Syn'});
    for i = 1:13 
        for k = 1:4 
            if isnan(y(k,i))
                plot(b(i).XEndPoints(k),10,'xk','MarkerSize',2.5,'LineWidth',0.5)
            end
        end
    end
title('$M_A(0)=M_B(0)=92$','Interpreter','latex')
ylabel('Time until threshold $N_c$ in hours');   

% Subplot 3 --------------------------------------------------------------
for i = 1:13
    y(1,i) = P_F_sgv2(i).t_erad(c6_ind); 
    y(2,i) = P_sCS_F_sgv2(i).t_erad(c6_ind); 
    y(3,i) = P_DDIa_F_sgv2(i).t_erad(c6_ind); 
    y(4,i) = P_DDIs_F_sgv2(i).t_erad(c6_ind);  
end
s6 = subplot('Position',Position(3,:));
b=bar(y,'EdgeColor',0.35*ones(1,3));
hold on 
for i = 1:5 
    b(i).FaceColor = mymap(ind_col(i),:);
end   
for i = 6:13 
    b(i).FaceColor = grey_mat(i-3,:);
    b(i).FaceAlpha = 0.7;
end
set(gca,'FontSize',font2)
set(gca,'xTickLabels',{'none','CS','Ant','Syn'});
    for i = 1:13 
        for k = 1:4 
            if isnan(y(k,i))
               plot_x = plot(b(i).XEndPoints(k),10,'xk','MarkerSize',2.5,'LineWidth',0.5);

            end
        end
    end
title('$M_A(0)=M_B(0)=1000$','Interpreter','latex');
leg_x = legend(plot_x,'Threshold $N_c$ was not reached','Position',[0.42,0.05,0,0],'EdgeColor','none');
leg_x.ItemTokenSize = [6 8];



%Annotate 'A' and 'B' 
annotation('textbox',[0.29,0.95,0,0],'String','A','FitBoxToText','on','EdgeColor','none','Fontsize',font3);
annotation('textbox',[0.29,0.65,0,0],'String','B','FitBoxToText','on','EdgeColor','none','Fontsize',font3);
annotation('textbox',[0.29,0.35,0,0],'String','C','FitBoxToText','on','EdgeColor','none','Fontsize',font3);


set(findall(fig1,'-property','Interpreter'),'Interpreter','latex');
set(findall(fig1,'-property','TickLabelInterpreter'),'TickLabelInterpreter','latex');
set(findall(fig1,'-property','YLim'),'YLim',[0,300]);
set(findall(fig1,'-property','YTick'),'YTick',[50,150,250]);


