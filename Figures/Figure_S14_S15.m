% Skript to generate Figures for dynamics for low cocentrations (S11 and
% S12)

close all 
clear 

%% Plot Settings 

%Colors
dark = [2 93 114]/255;
pink = [178 93 141]/255;
light = [121 182 185]/255; 
occa = [215,200,100]/255;
black = [0 0 0];
% Define a color vector 
col = [dark;pink;light;occa];
% Define a colormap
col_n=4;
mymap1 = [linspace(black(1),pink(1),6)', linspace(black(2),pink(2),6)',linspace(black(3),pink(3),6)'];
mymap2 = [linspace(pink(1),occa(1),col_n)', linspace(pink(2),occa(2),col_n)',linspace(pink(3),occa(3),col_n)'];
mymap3 = [linspace(occa(1),dark(1),col_n)', linspace(occa(2),dark(2),col_n)',linspace(occa(3),dark(3),col_n)'];
mymap4 = [linspace(dark(1),light(1),col_n)', linspace(dark(2),light(2),col_n)',linspace(dark(3),light(3),col_n)'];
mymap5 = [linspace(light(1),light(1),col_n)', linspace(light(2),light(2),col_n)',linspace(light(3),light(3),col_n)'];
mymap = [mymap1;mymap2;mymap3;mymap4;mymap5];

% Define a marker array for the Treatment Comparison 
marker ={'-','--',':','-.','-','--',':','-.','-','--',':','-.','-','--',':','-.'};
% Linewidth
set(0, 'DefaultLineLineWidth', 1.5);
% Fontsizes
font = 10; 
font2 = 8;
font3 = 16;

% Treatment Name 
T ={'12h','24h','1.5D','2D','2.5D','3D','3.5D','4D','5D','6D','7D','8D','9D','14D','20D','never'};
% Size of the Marker 
m_Size = 5;
mymap1 = [[200,200,200]/255;mymap(12,:);mymap(4,:)];
mymap2 = [mymap(12,:);[200,200,200]/255;mymap(4,:)];
%% Load Data 
dataPath = 'Data/Treatments12h_Cip';

P_C = load(strcat(dataPath,'/Data_P_C0_06.mat')); 
P_C =P_C.P_data;
L_C = load(strcat(dataPath,'/Data_L_C0_06.mat')); 
L_C = L_C.P_data;

P_C_DDIa = load(strcat(dataPath,'/Data_P_DDIa_C0_06.mat')); 
P_C_DDIs = load(strcat(dataPath,'/Data_P_DDIs_C0_06.mat')); 
P_C_DDIa = P_C_DDIa.P_data;
P_C_DDIs = P_C_DDIs.P_data;

dataPath = 'Data/Treatments12h_Fab2';

P_F2 = load(strcat(dataPath,'/Data_P_C0_06.mat')); 
P_F2 =P_F2.P_data;
L_F2 = load(strcat(dataPath,'/Data_L_C0_06.mat')); 
L_F2 = L_F2.P_data;

P_F2_DDIa = load(strcat(dataPath,'/Data_P_DDIa_C0_06.mat')); 
P_F2_DDIs = load(strcat(dataPath,'/Data_P_DDIs_C0_06.mat')); 
P_F2_DDIa = P_F2_DDIa.P_data;
P_F2_DDIs = P_F2_DDIs.P_data;

n = 200;
D1 = [linspace(0,0.2,n);linspace(0,0.2,n)];
D2 = [linspace(0.2,0.6,n);linspace(0.2,0.6,n)]; 
D = [D1(:,1:end-1),D2];

D = D./0.017; 
T ={'12h','24h','1.5D','2D','2.5D','3D','3.5D','4D','5D','6D','7D','8D','9D','14D','20D','never'};

%% Figure 1: Kappa 

% Position of the Subplots 
Position = [0.2,0.7,0.6,0.25;...
            0.2,0.39,0.6,0.25;
            0.2,0.08,0.6,0.25];
d1 =1; 
d2 = 100; 
D_set = D(1,d1:d2);

fig1 = figure(1); 
% Set size of the figure 
fig1.Units = 'centimeters';
fig1.Position=[5,5,15.5,15];


s1=subplot('Position',Position(1,:));
im1 = P_C(1).NmAB_cyc(d1:d2,:) > 0.9*10^10;
im2 = P_C(1).NmAB_cyc(d1:d2,:) > 0;
im = im1+im2;
contourf(im,[0,1,2]);
%imagesc(varargin{2}(1).N_cyc(d1:d2,:));
set(gca,'XTickLabel',[])
set(gca,'FontSize',font2)
% colorbar
% colorbar('XTickLabel',{'0','>0','>0.9*10^{10}'}, ...
%                'XTick', [0,1,2])
colormap(s1,mymap1)
% set(gca,'ColorScale','log')
ax = gca; 
ax.YTick = 10:20:90;
ax.YTickLabels = round(D_set(1,10:20:90),2);
title('Number of double mutants for a drug pair with $\kappa_A = \kappa_B = 1$','Fontsize',font); 


s2=subplot('Position',Position(2,:));
im1 = P_F2(1).NmAB_cyc(d1:d2,:) > 0.9*10^10;
im2 = P_F2(1).NmAB_cyc(d1:d2,:) > 0;
im = im1+im2;
contourf(im,[0,1,2]);
set(gca,'FontSize',font2)
%imagesc(varargin{2}(1).N_cyc(d1:d2,:));
% colorbar
% colorbar('XTickLabel',{'0','>0','>0.9*10^{10}'}, ...
%                'XTick', [0,1,2])
colormap(s2,mymap1)
ax = gca; 
ax.YTick = 10:20:90;
ax.YTickLabels = round(D_set(1,10:20:90),2);
title('Number of double mutants for a drug pair with $\kappa_A = \kappa_B = 2$','Fontsize',font); 
set(gca,'XTickLabel',[])

s3 = subplot('Position',Position(3,:));
im1 = (P_C(1).NmAB_cyc(d1:d2,:)-P_F2(1).NmAB_cyc(d1:d2,:))>0;
im2 = (P_C(1).NmAB_cyc(d1:d2,:)-P_F2(1).NmAB_cyc(d1:d2,:))<0;
im = im1-im2;
contourf(im,[-1,0,1]);
set(gca,'FontSize',font2)
%imagesc(varargin{2}(1).N_cyc(d1:d2,:));
% colorbar
% colorbar('XTickLabel',{'\kappa_2>\kappa_1','\kappa_2=\kappa_1','\kappa_2<\kappa_1'}, ...
%                'XTick', [-0.5,0,0.5])
colormap(s3,mymap2)
ax = gca; 
ax.YTick = 10:20:90;
ax.YTickLabels = round(D_set(1,10:20:90),2);
xlabel('Time in administrations','FontSize',font); 
title('Difference in the number of double mutants','Fontsize',font); 

 % Commen Y label
p1=get(s2,'position');
p2=get(s2,'position');
height=p1(2)+p1(4)-p2(2);
h15=axes('position',[p1(1)-0.05 p2(2) p2(3) height],'visible','off');
ylabel('Drug dose $D$ in multiples of zMIC$_W$','visible','on','Fontsize',font,'Interpreter','latex');   


% Annotate 'A' and 'B' 
annotation('textbox',[0.15,1,0,0],'String','A','FitBoxToText','on','EdgeColor','none','Fontsize',font3);
annotation('textbox',[0.15,0.69,0,0],'String','B','FitBoxToText','on','EdgeColor','none','Fontsize',font3);
annotation('textbox',[0.15,0.38,0,0],'String','C','FitBoxToText','on','EdgeColor','none','Fontsize',font3);

str1 = {'Fewer than $N_c=1$'; 'double mutants for'; 'both drug pairs'};
annotation('textbox',[0.2,0.29,0,0],'String',str1,'FitBoxToText','on','EdgeColor','none','Fontsize',font2);
str2 = {'More double mutants for the drug pair'; 'with $\kappa_A = \kappa_B = 1$'};
annotation('textbox',[0.46,0.29,0,0],'String',str2,'FitBoxToText','on','EdgeColor','none','Fontsize',font2,'Color',[1,1,1]);
str3 = {'More double mutants for the drug pair'; 'with $\kappa_A = \kappa_B = 2$'};
annotation('textbox',[0.46,0.215,0,0],'String',str3,'FitBoxToText','on','EdgeColor','none','Fontsize',font2);

str1 = {'Fewer than $N_c=1$'; 'double mutants'};
annotation('textbox',[0.2,0.875,0,0],'String',str1,'FitBoxToText','on','EdgeColor','none','Fontsize',font2);
str2 = {'More than $N_c=1$'; 'but less than $0.9 \cdot 10^{10}$'; 'double mutants'};
annotation('textbox',[0.38,0.875,0,0],'String',str2,'FitBoxToText','on','EdgeColor','none','Fontsize',font2);
str3 = {'More than $0.9 \cdot 10^{10}$'; 'double mutants'};
annotation('textbox',[0.6,0.875,0,0],'String',str3,'FitBoxToText','on','EdgeColor','none','Fontsize',font2,'Color',[1,1,1]);

str1 = {'Fewer than $N_c=1$'; 'double mutants'};
annotation('textbox',[0.2,0.525,0,0],'String',str1,'FitBoxToText','on','EdgeColor','none','Fontsize',font2);
str2 = {'More than $N_c=1$'; 'but less than $0.9 \cdot 10^{10}$'; 'double mutants'};
annotation('textbox',[0.36,0.525,0,0],'String',str2,'FitBoxToText','on','EdgeColor','none','Fontsize',font2);
str3 = {'More than $0.9 \cdot 10^{10}$'; 'double mutants'};
annotation('textbox',[0.6,0.525,0,0],'String',str3,'FitBoxToText','on','EdgeColor','none','Fontsize',font2,'Color',[1,1,1]);

set(findall(fig1,'-property','Interpreter'),'Interpreter','latex');
set(findall(fig1,'-property','TickLabelInterpreter'),'TickLabelInterpreter','latex');
%% Figure 2: DDI
mymap1 = [mymap(4,:);[200,200,200]/255;mymap(12,:)];
mymap2 = [mymap(16,:);[200,200,200]/255;mymap(12,:)];
mymap3 = [mymap(16,:);[200,200,200]/255;mymap(4,:)];

% Position of the Subplots 
Position = [0.2,0.7,0.6,0.25;...
            0.2,0.39,0.6,0.25;
            0.2,0.08,0.6,0.25];
d1 =1; 
d2 = 150; 
D_set = D(1,d1:d2);
fig1 = figure(1); 
% Set size of the figure 
fig1.Units = 'centimeters';
fig1.Position=[5,5,15.5,15];

s1 = subplot('Position',Position(1,:));
im1 = (P_C(1).NmAB_cyc(d1:d2,:)-P_C_DDIs(1).NmAB_cyc(d1:d2,:))>0;
im2 = (P_C(1).NmAB_cyc(d1:d2,:)-P_C_DDIs(1).NmAB_cyc(d1:d2,:))<0;
im = im1-im2;
contourf(im,[-1,0,1]);
set(gca,'FontSize',font2)
colormap(gca,mymap1)
ax = gca; 
ax.YTick = 10:20:130;
ax.YTickLabels = round(D_set(1,10:20:130),2);
set(gca,'XTickLabel',[])
title('Comparison between Bliss independence and synergism','Fontsize',font); 

s2 = subplot('Position',Position(2,:));
im1 = (P_C(1).NmAB_cyc(d1:d2,:)-P_C_DDIa(1).NmAB_cyc(d1:d2,:))>0;
im2 = (P_C(1).NmAB_cyc(d1:d2,:)-P_C_DDIa(1).NmAB_cyc(d1:d2,:))<0;
im = im1-im2;
contourf(im,[-1,0,1]);
set(gca,'FontSize',font2)
colormap(gca,mymap2)
ax = gca; 
ax.YTick = 10:20:130;
ax.YTickLabels = round(D_set(1,10:20:130),2);
set(gca,'XTickLabel',[])
title('Comparison between Bliss independence and antagonism','Fontsize',font); 

s3 = subplot('Position',Position(3,:));
im1 = (P_C_DDIs(1).NmAB_cyc(d1:d2,:)-P_C_DDIa(1).NmAB_cyc(d1:d2,:))>0;
im2 = (P_C_DDIs(1).NmAB_cyc(d1:d2,:)-P_C_DDIa(1).NmAB_cyc(d1:d2,:))<0;
im = im1-im2;
contourf(im,[-1,0,1]);
set(gca,'FontSize',font2)
colormap(gca,mymap3)
ax = gca; 
ax.YTick = 10:20:130;
ax.YTickLabels = round(D_set(1,10:20:130),2);
xlabel('Time in administrations','FontSize',font); 
title('Comparison between antagonism and synergism','Fontsize',font); 

 % Commen Y label
p1=get(s2,'position');
p2=get(s2,'position');
height=p1(2)+p1(4)-p2(2);
h15=axes('position',[p1(1)-0.05 p2(2) p2(3) height],'visible','off');
ylabel('Drug dose $D$ in multiples of zMIC$_W$','visible','on','Fontsize',font,'Interpreter','latex');   


% Annotate 'A' and 'B' 
annotation('textbox',[0.15,1,0,0],'String','A','FitBoxToText','on','EdgeColor','none','Fontsize',font3);
annotation('textbox',[0.15,0.69,0,0],'String','B','FitBoxToText','on','EdgeColor','none','Fontsize',font3);
annotation('textbox',[0.15,0.38,0,0],'String','C','FitBoxToText','on','EdgeColor','none','Fontsize',font3);

str1 = {'Fewer than'; '$N_c=1$ double'; 'mutants for'; 'both cases'};
annotation('textbox',[0.2,0.32,0,0],'String',str1,'FitBoxToText','on','EdgeColor','none','Fontsize',font2);
str2 = {'More double mutants with antagonism'};
annotation('textbox',[0.4,0.24,0,0],'String',str2,'FitBoxToText','on','EdgeColor','none','Fontsize',font2,'Color',[1,1,1]);
str3 = {'More double mutants with synergism'};
annotation('textbox',[0.4,0.13,0,0],'String',str3,'FitBoxToText','on','EdgeColor','none','Fontsize',font2,'Color',[1,1,1]);

str1 = {'Fewer than $N_c = 1$ double'; 'mutants for both cases'};
annotation('textbox',[0.4,0.92,0,0],'String',str1,'FitBoxToText','on','EdgeColor','none','Fontsize',font2);
str2 = {'More double mutants with Bliss independence'};
annotation('textbox',[0.4,0.82,0,0],'String',str2,'FitBoxToText','on','EdgeColor','none','Fontsize',font2);
str3 = {'More double mutants with synergism'};
annotation('textbox',[0.4,0.75,0,0],'String',str3,'FitBoxToText','on','EdgeColor','none','Fontsize',font2,'Color',[1,1,1]);

str1 = {'Fewer than'; '$N_c=1$ double'; 'mutants for'; 'both cases'};
annotation('textbox',[0.2,0.635,0,0],'String',str1,'FitBoxToText','on','EdgeColor','none','Fontsize',font2);
str2 = {'More double mutants with Bliss independence'};
annotation('textbox',[0.4,0.44,0,0],'String',str2,'FitBoxToText','on','EdgeColor','none','Fontsize',font2);
str3 = {'More double mutants with antagonism'};
annotation('textbox',[0.4,0.55,0,0],'String',str3,'FitBoxToText','on','EdgeColor','none','Fontsize',font2,'Color',[1,1,1]);

set(findall(fig1,'-property','Interpreter'),'Interpreter','latex');
set(findall(fig1,'-property','TickLabelInterpreter'),'TickLabelInterpreter','latex');
