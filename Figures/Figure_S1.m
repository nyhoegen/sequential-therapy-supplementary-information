% SI Figure for the Pharmacodynamic Surfaces (S1): 
% This Skript can only be run with the access to the functions in the
% folder: Determinsitic Model 
%
% Initialize Matlab 
clc 
clear 
close all 
%% Plot Settings 

%Colors
dark = [2 93 114]/255;
pink = [178 93 141]/255;
light = [121 182 185]/255; 
occa = [215,200,100]/255;
black = [0 0 0];
% Define a color vector 
col = [dark;pink;light;occa];
% Define a colormap
col_n=10;
mymap1 = [linspace(black(1),pink(1),6)', linspace(black(2),pink(2),6)',linspace(black(3),pink(3),6)'];
mymap2 = [linspace(pink(1),occa(1),col_n)', linspace(pink(2),occa(2),col_n)',linspace(pink(3),occa(3),col_n)'];
mymap3 = [linspace(occa(1),dark(1),col_n)', linspace(occa(2),dark(2),col_n)',linspace(occa(3),dark(3),col_n)'];
mymap4 = [linspace(dark(1),light(1),col_n)', linspace(dark(2),light(2),col_n)',linspace(dark(3),light(3),col_n)'];
mymap5 = [linspace(light(1),light(1),col_n)', linspace(light(2),light(2),col_n)',linspace(light(3),light(3),col_n)'];
mymap = [mymap1;mymap2;mymap3;mymap4;mymap5];

% Define a marker array for the Treatment Comparison 
marker ={'-','--',':','-.','-','--',':','-.','-','--',':','-.','-','--',':','-.'};
% Linewidth
set(0, 'DefaultLineLineWidth', 1.5);
% Fontsizes
font = 10; 
font2 = 8;
font3 = 12;

% Treatment Name 
T ={'12h','24h','1.5D','2D','2.5D','3D','3.5D','4D','5D','6D','7D','8D','9D','14D','20D','never'};
% Size of the Marker 
m_Size = 5;

%%  Load Data 
data = load('Cip_growth.mat');

% save PD and INT struct arrays 
PD_data = data.P.PD; 
INT_data = data.P.INT; 

%% Generate Data 

INT_data(1).I = 5; 
INT_data(1).EC50 = 0.05; 
INT_data(2).I = 5; 
INT_data(2).EC50 = 0.05;

x=linspace(0,1,1000);
y=linspace(0,1,1000);
[X,Y]=meshgrid(x,y);

Z_W_diff =Psi_with_Int_c(INT_data,PD_data(1),X,Y)-Psi_with_Int_c(INT_data,PD_data(1),X,0); 
Z_MA_diff = Psi_with_Int_c(INT_data,PD_data(2),X,Y)-Psi_with_Int_c(INT_data,PD_data(1),X,Y); 
Z_MB_diff = Psi_with_Int_c(INT_data,PD_data(3),X,Y)-Psi_with_Int_c(INT_data,PD_data(1),X,Y); 
Z_MAB_diff = Psi_with_Int_c(INT_data,PD_data(4),X,Y)-Psi_with_Int_c(INT_data,PD_data(1),X,Y); 

for i = 1:4
    PD_data(i).kA = 2;
    PD_data(i).kB = 2; 
end

Z_W_diff2 =Psi_with_Int_c(INT_data,PD_data(1),X,Y)-Psi_with_Int_c(INT_data,PD_data(1),X,0); 
Z_MA_diff2 = Psi_with_Int_c(INT_data,PD_data(2),X,Y)-Psi_with_Int_c(INT_data,PD_data(1),X,Y); 
Z_MB_diff2 = Psi_with_Int_c(INT_data,PD_data(3),X,Y)-Psi_with_Int_c(INT_data,PD_data(1),X,Y); 
Z_MAB_diff2 = Psi_with_Int_c(INT_data,PD_data(4),X,Y)-Psi_with_Int_c(INT_data,PD_data(1),X,Y); 

%% Plot Data 

% Define colors for the countourf plot 
mymap_small = [0.95,0.95,0.95;mymap(4,:)];

% IF x and y until 10:
mymap_small = [mymap2;mymap4];
n_con = [-8:1:0,0.5:1:8];
cmin = -8; 
cmax = 8;

% IF x and y until 1:
% mymap_small = [mymap2;mymap4];
% n_con = [-5:0.2:0,0.2:0.2:5];
% cmin = -5; 
% cmax = 5;

% Position of the Su6bplots 
Position = [0.1,0.575,0.2,0.25;...
            0.325,0.575,0.2,0.25;...
            0.55,0.575,0.2,0.25;...
            0.775,0.575,0.2,0.25;...
            0.1,0.1,0.2,0.25;...
            0.325,0.1,0.2,0.25;...
            0.55,0.1,0.2,0.25;...
            0.775,0.1,0.2,0.25];

fig1 = figure(1);
fig1.Units = 'centimeters'; 
fig1.Position = [5,2,15.5,8];

s1=subplot('Position',Position(1,:));
contourf(X/0.017,Y/0.017,Z_W_diff,n_con)
colorbar
colormap(mymap_small)
set(gca,'FontSize',font2);
title('$\mu_W(c_A,c_B)-\mu_W(c_A,c_B=0)$','FontSize',font2)
view(2)
caxis([cmin,cmax])
% xticks(100:100:500);
% yticks(100:100:500);
xticks(10:20:50);
yticks(10:20:50);


s2=subplot('Position',Position(2,:));
contourf(X/0.017,Y/0.017,Z_MA_diff,n_con)
colorbar
colormap(mymap_small)
set(gca,'FontSize',font2);
title('$\mu_{A}(c_A,c_B)-\mu_W(c_A,c_B)$','FontSize',font2)
view(2)
caxis([cmin,cmax])
% xticks(100:100:500);
% yticks(100:100:500);
xticks(10:20:50);
yticks(10:20:50);

s3 = subplot('Position',Position(3,:));
contourf(X/0.017,Y/0.017,Z_MB_diff,n_con)
colorbar
colormap(mymap_small)
set(gca,'FontSize',font2); 
title('$\mu_{B}(c_A,c_B)-\mu_W(c_A,c_B)$','FontSize',font2)
view(2)
caxis([cmin,cmax])
% xticks(100:100:500);
% yticks(100:100:500);
xticks(10:20:50);
yticks(10:20:50);

s4 = subplot('Position',Position(4,:));
contourf(X/0.017,Y/0.017,Z_MAB_diff,n_con)
colorbar
colormap(mymap_small)
set(gca,'FontSize',font2);
title('$\mu_{AB}(c_A,c_B)-\mu_W(c_A,c_B)$','FontSize',font2)
view(2)
caxis([cmin,cmax]) 
% xticks(100:100:500);
% yticks(100:100:500);
xticks(10:20:50);
yticks(10:20:50);

s5=subplot('Position',Position(5,:));
contourf(X/0.017,Y/0.017,Z_W_diff2,n_con)
colorbar
colormap(mymap_small)
set(gca,'FontSize',font2);
title('$\mu_W(c_A,c_B)-\mu_W(c_A,c_B=0)$','FontSize',font2)
view(2)
caxis([cmin,cmax])
% xticks(100:100:500);
% yticks(100:100:500);
xticks(10:20:50);
yticks(10:20:50);


s6=subplot('Position',Position(6,:));
contourf(X/0.017,Y/0.017,Z_MA_diff2,n_con)
colorbar
colormap(mymap_small)
set(gca,'FontSize',font2);
title('$\mu_{A}(c_A,c_B)-\mu_W(c_A,c_B)$','FontSize',font2)
view(2)
caxis([cmin,cmax])
% xticks(100:100:500);
% yticks(100:100:500);
xticks(10:20:50);
yticks(10:20:50);

s7 = subplot('Position',Position(7,:));
contourf(X/0.017,Y/0.017,Z_MB_diff2,n_con)
colorbar
colormap(mymap_small)
set(gca,'FontSize',font2); 
title('$\mu_{B}(c_A,c_B)-\mu_W(c_A,c_B)$','FontSize',font2)
view(2)
caxis([cmin,cmax])
% xticks(100:100:500);
% yticks(100:100:500);
xticks(10:20:50);
yticks(10:20:50);

s8 = subplot('Position',Position(8,:));
contourf(X/0.017,Y/0.017,Z_MAB_diff2,n_con)
colorbar
colormap(mymap_small)
set(gca,'FontSize',font2);
title('$\mu_{AB}(c_A,c_B)-\mu_W(c_A,c_B)$','FontSize',font2)
view(2)
caxis([cmin,cmax]) 
% xticks(100:100:500);
% yticks(100:100:500);
xticks(10:20:50);
yticks(10:20:50);

% Commen X label 
p3=get(s6,'position');
p4=get(s7,'position');
width = p4(1)+p4(3)-p3(1); 
axes('position',[p3(1) p4(2) width p3(4)],'visible','off');
xlabel('$c_A$ in multiples of zMIC$_W$','FontSize',font','visible','on','Fontsize',font);

 % Commen Y label
p1=get(s1,'position');
p2=get(s5,'position');
height=p1(2)+p1(4)-p2(2);
h15=axes('position',[p1(1)-0.03 p2(2) p2(3) height],'visible','off');
ylabel('$c_B$ in multiples of zMIC$_W$','visible','on','Fontsize',font,'Interpreter','latex');   

annotation('textbox',[0.4,1,0,0],'String','$\kappa_A = \kappa_B = 1$','FitBoxToText','on','EdgeColor','none','Fontsize',font3);
annotation('textbox',[0.4,0.525,0,0],'String','$\kappa_A = \kappa_B = 2$','FitBoxToText','on','EdgeColor','none','Fontsize',font3);

% Annotate 'A' and 'B' 
annotation('textbox',[0.06,0.945,0,0],'String','A','FitBoxToText','on','EdgeColor','none','Fontsize',font3);
annotation('textbox',[0.285,0.945,0,0],'String','B','FitBoxToText','on','EdgeColor','none','Fontsize',font3);
annotation('textbox',[0.51,0.945,0,0],'String','C','FitBoxToText','on','EdgeColor','none','Fontsize',font3);
annotation('textbox',[0.735,0.945,0,0],'String','D','FitBoxToText','on','EdgeColor','none','Fontsize',font3);

annotation('textbox',[0.06,0.47,0,0],'String','E','FitBoxToText','on','EdgeColor','none','Fontsize',font3);
annotation('textbox',[0.285,0.47,0,0],'String','G','FitBoxToText','on','EdgeColor','none','Fontsize',font3);
annotation('textbox',[0.51,0.47,0,0],'String','F','FitBoxToText','on','EdgeColor','none','Fontsize',font3);
annotation('textbox',[0.735,0.47,0,0],'String','H','FitBoxToText','on','EdgeColor','none','Fontsize',font3);
set(findall(fig1,'-property','Interpreter'),'Interpreter','latex');
set(findall(fig1,'-property','TickLabelInterpreter'),'TickLabelInterpreter','latex');


