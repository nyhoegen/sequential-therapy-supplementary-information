% SI Figures: Sudden drop for Kappa = 2
% This Skript can only be run with the access to the functions in the
% folder: Determinsitic Model 
%
% Initialize MATLAB 
clc 
clear 
close all 
%% Plot Settings 

%Colors
dark = [2 93 114]/255;
pink = [178 93 141]/255;
light = [121 182 185]/255; 
occa = [215,200,100]/255;
black = [0 0 0];
% Define a color vector 
col = [dark;pink;light;occa];
% Define a colormap
col_n=4;
mymap1 = [linspace(black(1),pink(1),6)', linspace(black(2),pink(2),6)',linspace(black(3),pink(3),6)'];
mymap2 = [linspace(pink(1),occa(1),col_n)', linspace(pink(2),occa(2),col_n)',linspace(pink(3),occa(3),col_n)'];
mymap3 = [linspace(occa(1),dark(1),col_n)', linspace(occa(2),dark(2),col_n)',linspace(occa(3),dark(3),col_n)'];
mymap4 = [linspace(dark(1),light(1),col_n)', linspace(dark(2),light(2),col_n)',linspace(dark(3),light(3),col_n)'];
mymap5 = [linspace(light(1),light(1),col_n)', linspace(light(2),light(2),col_n)',linspace(light(3),light(3),col_n)'];
mymap = [mymap1;mymap2;mymap3;mymap4;mymap5];

% Define a marker array for the Treatment Comparison 
marker ={'-','--',':','-.','-','--',':','-.','-','--',':','-.','-','--',':','-.'};
% Linewidth
set(0, 'DefaultLineLineWidth', 1.5);
% Fontsizes
font = 10; 
font2 = 8;
font3 = 12;

% Treatment Name 
T ={'12h','24h','1.5D','2D','2.5D','3D','3.5D','4D','5D','6D','7D','8D','9D','14D','20D','never'};
% Size of the Marker 
m_Size = 5;


%% Calculate Data
close all
% Load Data: 
Cip = load('Cip.mat'); 
P = Cip.P;

for i=1:4
    P.PD(i).kA = 2; 
    P.PD(i).kB = 2; 
end

% set correct time point 
t_val = 12;
% set TA, TB: 12
P.PK(1).Tthis = t_val; 
P.PK(2).Tthis = t_val; 
P.PK(1).Tother = t_val; 
P.PK(2).Tother = t_val; 

n = 200;
D1 = [linspace(0,0.2,n);linspace(0,0.2,n)];
D2 = [linspace(0.2,0.6,n);linspace(0.2,0.6,n)]; 
D = [D1(:,1:end-1),D2];

t8 = repmat([ones(1,8),zeros(1,8)], [1 5]);
% Set the correct Treatment: 
P.PK(1).cycle = t8(1:10); 
P.PK(2).cycle = ~t8(1:10); 

P.PK(1).D = D(1,161);
P.PK(2).D = D(1,161);
[dyn1,data1] = Two_Drugs_Cycling_CC_u(P.G,P.INT,P.PD,P.PK,P.CC,P.M);

P.PK(1).D = D(1,162);
P.PK(2).D = D(1,162);
[dyn2,data2] = Two_Drugs_Cycling_CC_u(P.G,P.INT,P.PD,P.PK,P.CC,P.M);

% Dynamics below zero: 
P.M.show = 1; 
P.PK(1).D = D(1,161);
P.PK(2).D = D(1,161);
[dyn3,data3] = Two_Drugs_Cycling_CC_u(P.G,P.INT,P.PD,P.PK,P.CC,P.M);

P.PK(1).D = D(1,162);
P.PK(2).D = D(1,162);
[dyn4,data4] = Two_Drugs_Cycling_CC_u(P.G,P.INT,P.PD,P.PK,P.CC,P.M);

%% 
% Positions for the subplots 
Position = [0.1,0.2,0.38,0.7;...
            0.56,0.2,0.38,0.7];


set(0, 'DefaultLineLineWidth', 2);
fig1 = figure(1); 
fig1.Units = 'centimeters';
fig1.Position=[5,5,15.5,6]; 

s1 = subplot('Position',Position(1,:)); 
for i = 1:7
    plot(i*12*ones(1,2),[0.3,10^10],':','Color',pink,'LineWidth',1)
    hold on 
end
plot(8*12*ones(1,2),[0.3,10^10],':','Color',light,'LineWidth',1)
hold on 
plot(9*12*ones(1,2),[0.3,10^10],':','Color',light,'LineWidth',1)
hold on 
plot([1,110],[1,1],'--','Color',[0.2,0.2,0.2,0.2]);
hold on
plot(dyn3.time,dyn3.W,'Color',[occa,0.4]); 
hold on 
plot(dyn3.time,dyn3.MA,'Color',[dark,0.4]); 
hold on 
plot(dyn1.time,dyn1.W,'Color',occa); 
hold on 
plot(dyn1.time,dyn1.MA,'Color',dark); 
hold on 
set(gca,'FontSize',font2);
set(gca,'YScale','log'); 
ylabel('Cell numbers','Fontsize',font); 
title('Drug dose $D = 9.46 \cdot \mathrm{zMIC}_W$');
ylim([0.3,10^10]); 
ax = gca; 
ax.YTick = [1,10^5,10^10];
ax.YTickLabels = {'1','$10^5$','$10^{10}$'};
xlim([0,110]);

%Inlcude Zoomin 
axes('position',[0.22 0.45 0.15 0.2])
box on
plot([1,110],[1,1],'--','Color',[0.2,0.2,0.2,0.2]);
hold on 
plot(dyn3.time,dyn3.W,'Color',[occa,0.4]); 
hold on 
plot(dyn3.time(1:2990),dyn3.MA(1:2990),'Color',[dark,0.4]); 
hold on 
plot(dyn3.time,dyn3.W,'Color',occa); 
hold on 
plot(dyn1.time,dyn1.MA,'Color',dark); 
hold on 
set(gca,'FontSize',font2);
set(gca,'YScale','log'); 
xlim([16.2,16.8]);
ylim([0.95,1.05]);
ax = gca;
ax.YTick = 1;
ax.YTickLabel = '1';

s2 = subplot('Position',Position(2,:)); 
for i = 1:7
    plot(i*12*ones(1,2),[0.3,10^10],':','Color',pink,'LineWidth',1)
    hold on 
end
plot(8*12*ones(1,2),[0.3,10^10],':','Color',light,'LineWidth',1)
hold on 
plot(9*12*ones(1,2),[0.3,10^10],':','Color',light,'LineWidth',1)
hold on 
plot([1,110],[1,1],'--','Color',[0.2,0.2,0.2,0.2]);
hold on
plot(dyn4.time,dyn4.W,'Color',[occa,0.4]); 
hold on 
plot(dyn4.time(1:2990),dyn4.MA(1:2990),'Color',[dark,0.4]); 
hold on 
plots(1) = plot(dyn2.time,dyn2.W,'Color',occa); 
hold on 
plots(2) = plot(dyn2.time,dyn2.MA,'Color',dark); 
hold on  
set(gca,'FontSize',font2);
set(gca,'YScale','log'); 
l1=legend(plots,{'$W$','$M_A$'});
l1.ItemTokenSize = [15 18]; 
title('Drug dose $D = 9.52 \cdot \mathrm{zMIC}_W$');
ylim([0.3,10^10]); 
ax = gca; 
ax.YTick = [1,10^5,10^10];
ax.YTickLabels = {'1','$10^5$','$10^{10}$'};
xlim([0,110]);

%Inlcude Zoomin 
axes('position',[0.68 0.45 0.15 0.2])
box on
plot([1,110],[1,1],'--','Color',[0.2,0.2,0.2,0.2]);
hold on 
plot(dyn4.time,dyn4.W,'Color',[occa,0.4]); 
hold on 
plot(dyn4.time(1:2990),dyn4.MA(1:2990),'Color',[dark,0.4]); 
hold on 
plot(dyn4.time,dyn4.W,'Color',occa); 
hold on 
plot(dyn2.time,dyn2.MA,'Color',dark); 
hold on 
set(gca,'FontSize',font2);
set(gca,'YScale','log'); 
xlim([16.45,16.55]);
ylim([0.99,1.01]);
ax = gca;
ax.YTick = 1;
ax.YTickLabel = '1';

p11=get(s1,'position');
p12=get(s2,'position');

% Commen X label 
width = p12(1)+p12(3)-p11(1); 
h6 = axes('position',[p11(1) p12(2) width p11(4)],'visible','off');
h_label=xlabel('Time in hours','visible','on','Fontsize',font);

dim3 = [0.1,1,0,0];
str3 = 'A';
annotation('textbox',dim3,'String',str3,'FitBoxToText','on','EdgeColor','none','Fontsize',font3);
dim4 = [0.56,1,0,0];
str4= 'B';
annotation('textbox',dim4,'String',str4,'FitBoxToText','on','EdgeColor','none','Fontsize',font3);
annotation('textbox',[0.8,0.3,0,0],'String','Threshold $N_c$','FitBoxToText','on','EdgeColor','none','Fontsize',font2,'Color',[0.6,0.6,0.6],'BackgroundColor','white','Margin',1);
annotation('textbox',[0.11,0.88,0,0],'String','Drug A','FitBoxToText','on','EdgeColor','none','Fontsize',font2,'Color',pink,'BackgroundColor','white','Margin',1);
annotation('textbox',[0.4,0.88,0,0],'String','Drug B','FitBoxToText','on','EdgeColor','none','Fontsize',font2,'Color',light,'BackgroundColor','white','Margin',1);

set(findall(fig1,'-property','Interpreter'),'Interpreter','latex');
set(findall(fig1,'-property','TickLabelInterpreter'),'TickLabelInterpreter','latex');
% set(findall(fig1,'-property','XLim'),'Xlim',[0,110]);