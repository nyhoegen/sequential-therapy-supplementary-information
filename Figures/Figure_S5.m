%% Skript to generate figure for the effct of psi_min (S2): 

% Initialize matlab 
close all 
clear 

%% Plot Settings 

%Colors
dark = [2 93 114]/255;
pink = [178 93 141]/255;
light = [121 182 185]/255; 
occa = [215,200,100]/255;
black = [0 0 0];
% Define a color vector 
col = [dark;pink;light;occa];
% Define a colormap
col_n=4;
mymap1 = [linspace(black(1),pink(1),6)', linspace(black(2),pink(2),6)',linspace(black(3),pink(3),6)'];
mymap2 = [linspace(pink(1),occa(1),col_n)', linspace(pink(2),occa(2),col_n)',linspace(pink(3),occa(3),col_n)'];
mymap3 = [linspace(occa(1),dark(1),col_n)', linspace(occa(2),dark(2),col_n)',linspace(occa(3),dark(3),col_n)'];
mymap4 = [linspace(dark(1),light(1),col_n)', linspace(dark(2),light(2),col_n)',linspace(dark(3),light(3),col_n)'];
mymap5 = [linspace(light(1),light(1),col_n)', linspace(light(2),light(2),col_n)',linspace(light(3),light(3),col_n)'];
mymap = [mymap1;mymap2;mymap3;mymap4;mymap5];

% Define a marker array for the Treatment Comparison 
marker ={'-','--',':','-.','-','--',':','-.','-','--',':','-.','-','--',':','-.'};
% Linewidth
set(0, 'DefaultLineLineWidth', 1.5);
% Fontsizes
font = 10; 
font2 = 8;
font3 = 16;

% Treatment Name 
T ={'12h','24h','1.5D','2D','2.5D','3D','3.5D','4D','5D','6D','7D','8D','9D','14D','20D','never'};
% Size of the Marker 
m_Size = 5;
%% Load Data 

%---Kappa = 1---------------------------------------------------------------
% Define Data Path
dataPath = 'Data/Treatments12h_Cip';
P_C = load(strcat(dataPath,'/Data_P_C0_06.mat')); 
P_C =P_C.P_data;
L_C = load(strcat(dataPath,'/Data_L_C0_06.mat')); 
L_C = L_C.P_data;

%---Kappa = 2---------------------------------------------------------------
dataPath = 'Data/Treatments12h_Fab1';
P_F1 = load(strcat(dataPath,'/Data_P_C0_06.mat')); 
P_F1 =P_F1.P_data;
L_F1 = load(strcat(dataPath,'/Data_L_C0_06.mat')); 
L_F1 = L_F1.P_data;


% First Effective Concentration for all cases 
EffectiveC_Cip = load('Data/First_Effective_Concentration/EffectiveC_Cip.mat');
EffectiveC_Cip = EffectiveC_Cip.Effective_C;
EffectiveC_lab_Cip = load('Data/First_Effective_Concentration/EffectiveC_Lab_Cip.mat');
EffectiveC_Lab_Cip = EffectiveC_lab_Cip.Effective_C_Lab;
EffectiveC_Fab1 = load('Data/First_Effective_Concentration/EffectiveC_Fab1.mat');
EffectiveC_Fab1 = EffectiveC_Fab1.Effective_C;
EffectiveC_lab_Fab1 = load('Data/First_Effective_Concentration/EffectiveC_Lab_Fab1.mat');
EffectiveC_Lab_Fab1 = EffectiveC_lab_Fab1.Effective_C_Lab;

n = 200;
D1 = [linspace(0,0.2,n);linspace(0,0.2,n)];
D2 = [linspace(0.2,0.6,n);linspace(0.2,0.6,n)]; 
D = [D1(:,1:end-1),D2];
% Scale Drug Concnetration in fold change of MIC
D = D./0.017; 
%% FIGURE 1 
% Treatment settings for x-axis
treatment = [0.5,1,1.5,2,2.5,3,3.5,4,5,6,7,8,9,14,20,25];
T_sub ={'','1','','2','','3','','4','5','6','7','8','9','14','20','mono-therapy'};

% function handles for the psi_functions  
mu_fab =@(c,zMIC)  ((0.088+8.8)*(c/zMIC).^1./((c/zMIC).^1+8.8/0.088));
mu_cip =@(c,zMIC)  ((0.088+6.5)*(c/zMIC).^1./((c/zMIC).^1+6.5/0.088));

mu_fab_W =@(c) mu_fab(c,0.017);
mu_fab_M =@(c) mu_fab(c,28*0.017);

% Drug Range 
c_vec = linspace(0.001,100,10000);

% Position of the Subplots 
Position = [0.1,0.2,0.375,0.65;...
            0.58,0.2,0.375,0.65];

% Plot figure 

fig1=figure(1); 
subplot('Position',Position(1,:))
plot([35,35],[-10,2],'--','Color',[0.6,0.6,0.6,0.5]);
hold on
plots2(1)=plot(c_vec/0.017,0.088-mu_fab(c_vec,0.017),'Color',mymap(5,:));
hold on
plots2(2)=plot(c_vec/0.017,0.088-mu_cip(c_vec,0.017),'Color',dark);
hold on 
set(gca,'FontSize',font2)
set(gca,'XScale','log');
ylim([-9,2]); 
xlim([0,10^3]);
% yticks([-3,-2,-1,0,1]);
legend(plots2,'$\psi_{\mathrm{min}} = -8.8$','$\psi_{\mathrm{min}} = -6.5$','Location','southwest');  
xlabel('Drug concentration $c_A$ in multiples of zMIC$_W$','FontSize',font); 
ylabel({'Growth rate'; '$\psi_W(c_A,c_B=0,N=0)$'},'FontSize',font);


subplot('Position',Position(2,:))
p_sub(2) = plot(treatment(1:15),EffectiveC_Cip.Basic(1:15)/0.017,'-o','Color',mymap(15,:),'MarkerSize',m_Size); 
hold on
plot(25,EffectiveC_Cip.Basic(16)/0.017,'o','Color',mymap(15,:),'MarkerSize',m_Size); 
hold on
p_sub(1) = plot(treatment(1:15),EffectiveC_Fab1.Basic(1:15)/0.017,'-x','Color',mymap(5,:),'MarkerSize',m_Size); 
hold on
plot(25,EffectiveC_Fab1.Basic(16)/0.017,'x','Color',mymap(5,:),'MarkerSize',m_Size); 
hold on
plot(treatment(1:15),EffectiveC_Lab_Cip.Basic(1:15)/0.017,'--o','Color',mymap(15,:),'MarkerSize',m_Size); 
hold on
plot(25,EffectiveC_Lab_Cip.Basic(16)/0.017,'o','Color',mymap(15,:),'MarkerSize',m_Size); 
hold on
plot(treatment(1:15),EffectiveC_Lab_Fab1.Basic(1:15)/0.017,'--x','Color',mymap(5,:),'MarkerSize',m_Size); 
hold on
plot(25,EffectiveC_Lab_Fab1.Basic(16)/0.017,'x','Color',mymap(5,:),'MarkerSize',m_Size); 
hold on
set(gca,'Fontsize',font2); 
legend(p_sub,'$\psi_{\mathrm{min}} = -8.8$','$\psi_{\mathrm{min}} = -6.5$','FontSize',font2,'Location','northwest');
xlim([0,26]);
ax = gca;
ax.XTick = treatment;
ax.XTickLabel=T_sub;
set(gca,'YScale','log')
ylim([1,4*10^2]);
ylabel('First effective concentration','FontSize',font)
xlabel('Time between the switches in days','Fontsize',font)

% Annotate Arrow: 
x = [0.85 0.82];
y = [0.7 0.6];
annotation('textarrow',x,y,'String','Patient','LineWidth',0.5,'HeadWidth',5,'HeadLength',5,'FontSize',font2,'Color',[0.3,0.3,0.3],'HeadStyle','vback3')
% Annotate Arrow: 
x = [0.85 0.82];
y = [0.35 0.45];
annotation('textarrow',x,y,'String','Lab','LineWidth',0.5,'HeadWidth',5,'HeadLength',5,'FontSize',font2,'Color',[0.3,0.3,0.3],'HeadStyle','vback3')

% Set size of the figure 
fig1.Units = 'centimeters';
fig1.Position=[10,10,15.5,6.5];

% Annotate 'A' and 'B' 
dim3 = [0.09,1,0,0];
str3 = 'A';
annotation('textbox',dim3,'String',str3,'FitBoxToText','on','EdgeColor','none','Fontsize',font3);
dim4 = [0.57,1,0,0];
str4= 'B';
annotation('textbox',dim4,'String',str4,'FitBoxToText','on','EdgeColor','none','Fontsize',font3);

% Include axis break 
annotation('textbox', [0.9, 0.2, 0.01, 0.01], 'String', "[...]",'EdgeColor',...
    'white','BackgroundColor','white','FitBoxToText','on','Margin',0.001,...
    'VerticalAlignment','middle','HorizontalAlignment','center','FontSize',6)

set(findall(fig1,'-property','Interpreter'),'Interpreter','latex');
set(findall(fig1,'-property','TickLabelInterpreter'),'TickLabelInterpreter','latex');
