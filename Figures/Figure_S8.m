% Figures for SI section: Collateral resistenz 

% Initialize Matlab 
clc
clear all 
close all 

%% Plot Settings 

%Colors
dark = [2 93 114]/255;
pink = [178 93 141]/255;
light = [121 182 185]/255; 
occa = [215,200,100]/255;
black = [0 0 0];
% Define a color vector 
col = [dark;pink;light;occa];
% Define a colormap
col_n=4;
mymap1 = [linspace(black(1),pink(1),6)', linspace(black(2),pink(2),6)',linspace(black(3),pink(3),6)'];
mymap2 = [linspace(pink(1),occa(1),col_n)', linspace(pink(2),occa(2),col_n)',linspace(pink(3),occa(3),col_n)'];
mymap3 = [linspace(occa(1),dark(1),col_n)', linspace(occa(2),dark(2),col_n)',linspace(occa(3),dark(3),col_n)'];
mymap4 = [linspace(dark(1),light(1),col_n)', linspace(dark(2),light(2),col_n)',linspace(dark(3),light(3),col_n)'];
mymap5 = [linspace(light(1),light(1),col_n)', linspace(light(2),light(2),col_n)',linspace(light(3),light(3),col_n)'];
mymap = [mymap1;mymap2;mymap3;mymap4;mymap5];

% Linewidth
set(0, 'DefaultLineLineWidth', 1.5);
% Fontsizes
font = 10; 
font2 = 8;
font3 = 12;

% Treatment Name 
T ={'12 hours','24h','1.5D','2 days','2.5D','3D','3.5D','4 days','5D','6D','7D','8 days','9D','14D','20D','never'};


%% Load Data 

% Define Data Path
dataPath = 'Data_Cluster/Final_Data/Treatments12h_Cip';
P_Cip = load(strcat(dataPath,'/Data_P_C0_06.mat')); 
P_Cip = P_Cip.P_data;
L_Cip = load(strcat(dataPath,'/Data_L_C0_06.mat')); 
L_Cip  = L_Cip .P_data;

% CR: 
P_Cip_wCR = load(strcat(dataPath,'/Data_P_wCR_C0_06.mat')); 
L_Cip_wCR = load(strcat(dataPath,'/Data_L_wCR_C0_06.mat'));
P_Cip_sCR = load(strcat(dataPath,'/Data_P_sCR_C0_06.mat')); 
L_Cip_sCR = load(strcat(dataPath,'/Data_L_sCR_C0_06.mat')); 
P_Cip_wCR = P_Cip_wCR.P_data;
L_Cip_wCR = L_Cip_wCR.P_data;
P_Cip_sCR = P_Cip_sCR.P_data;
L_Cip_sCR = L_Cip_sCR.P_data;


% Drug Concentration
n = 200;
D1 = [linspace(0,0.2,n);linspace(0,0.2,n)];
D2 = [linspace(0.2,0.6,n);linspace(0.2,0.6,n)]; 
D = [D1(:,1:end-1),D2];
% Scale Drug Concnetration in fold change of MIC
D = D./0.017; 

%% Load Simulation 

dataPath = 'Data_Cluster/Final_Data/Hybrid_Simulations_new/Treatments12h_Cip';
Sim_Cip_Basic = load(strcat(dataPath,'/Sim_T1_2.mat')); 
Sim_Cip_Basic_T1 = Sim_Cip_Basic.Sim;

Sim_Cip_Basic_T1_HC = load(strcat(dataPath,'/Sim_T1_2_HC.mat'));
Sim_Cip_Basic_T1_HC = Sim_Cip_Basic_T1_HC.Sim;
Sim_Cip_Basic = [Sim_Cip_Basic_T1,Sim_Cip_Basic_T1_HC];


dataPath = 'Data_Cluster/Final_Data/Hybrid_Simulations_new/Treatments12h_Cip_CS_DDI';
Sim_Cip_CR = load(strcat(dataPath,'/Sim_T1_CR.mat')); 
Sim_Cip_CR = Sim_Cip_CR.Sim;


D_Basic = 0.08:0.01:0.6; 


for j = 1:53

        Sim_t_erad_C_Basic(1,j) = mean(Sim_Cip_Basic{1,j},'omitnan'); 
        Sim_t_erad_std_C_Basic(1,j) = std(Sim_Cip_Basic{1,j},'omitnan'); 
        
        Sim_t_erad_C_CR(1,j) = mean(Sim_Cip_CR{1,j},'omitnan'); 
        Sim_t_erad_std_C_CR(1,j) = std(Sim_Cip_CR{1,j},'omitnan');      
end

Sim_mean_C_CR = Sim_t_erad_C_CR; 
Sim_std_C_CR = Sim_t_erad_std_C_CR;
Sim_sem_C_CR = Sim_t_erad_std_C_CR/sqrt(100);


Sim_mean_C_Basic = Sim_t_erad_C_Basic; 
Sim_std_C_Basic = Sim_t_erad_std_C_Basic;
Sim_sem_C_Basic = Sim_t_erad_std_C_Basic/sqrt(100);
        

%% Figure with simulation 


x_length = 0.22;
y_height = 0.3;
set_ind = [1,8,12];
n_err = 3;


Pos = [0.26,0.55, x_length,y_height;
            0.49,0.55, x_length,y_height;
            0.72,0.55, x_length,y_height;...
            0.26,0.1, x_length,y_height]; 
        
fig1 = figure(1); 
fig1.Units = 'centimeters';
fig1.Position=[10,2,15.5,10];
for i=1:3
    p_sub(i) = subplot('Position', Pos(i,:));
    % Plot Basic, wCS and sCS for the four different Treatments 
    p_CS(2)=plot(D(1,:),P_Cip_wCR(set_ind(i)).t_erad,'Color', mymap(13,:),'Linewidth',2);
    hold on 
    p_CS(1)=plot(D(1,:),P_Cip_sCR(set_ind(i)).t_erad,'Color', mymap(12,:),'Linewidth',2);
    hold on    
    p_CS(3)=plot(D(1,:),P_Cip(set_ind(i)).t_erad,'Color', mymap(15,:),'Linewidth',2);
    hold on
    set(gca,'FontSize',font2)
    str = strcat('Switch after',{' '},T(set_ind(i)));
    title(str,'FontSize',font)
    xlim([0 36]); 
    ylim([0,400]);
    ax1 = gca;
    if i == 1
        set(gca,'YTick',[50,150,250,350])
        set(gca,'YTickLabels',[50,150,250,350])
       
    else 
        set(gca,'YTickLabels',[])
        set(gca,'YTick',[50,150,250,350])
    end
end
l1=legend(p_CS,{'Strong CR','Weak CR','Without CR'},'Position',[0.020058110222014,0.661185407433938,0.152657973104359,0.188814592566062],'FontSize',font2);
title(l1,{'Collateral'; 'Resistance (CR)'})
l1.ItemTokenSize = [15,18];

p_sub(4)=subplot('Position',Pos(4,:));
plot(D_Basic./0.017,Sim_mean_C_Basic,'Color',mymap(15,:));
hold on 
plot(D_Basic./0.017,Sim_mean_C_CR,'Color',mymap(13,:));
hold on 
set(gca,'YTick',[50,150,250,350])
   xlim([0 36]); 
    ylim([0,400]); 
    title({'Semi-stochastic'; 'simulation'},'FontSize',font)


% % Commen Y label
p1=get(p_sub(1),'position');
p2=get(p_sub(4),'position');
height=p1(2)+p1(4)-p2(2);
h15=axes('position',[p1(1)-0.02 p2(2) p2(3) height],'visible','off');
ylabel('Time until threshold $N_c$ in hours','visible','on','Fontsize',font,'Interpreter','latex');


% Commen X label 
p3=get(p_sub(2),'position');
p4=get(p_sub(4),'position');
width = p4(1)+p4(3)-p3(1); 
axes('position',[p3(1) p4(2) p3(3) p3(4)],'visible','off');
xlabel('Drug dose $D$ in multiples of zMIC$_W$','FontSize',font','visible','on','Fontsize',font,'Interpreter','latex');

annotation('textbox',[0.255,0.855,0,0],'String','A','FitBoxToText','on','EdgeColor','none','Fontsize',font3);
annotation('textbox',[0.485,0.855,0,0],'String','B','FitBoxToText','on','EdgeColor','none','Fontsize',font3);
annotation('textbox',[0.715,0.855,0,0],'String','C','FitBoxToText','on','EdgeColor','none','Fontsize',font3);
annotation('textbox',[0.255,0.405,0,0],'String','D','FitBoxToText','on','EdgeColor','none','Fontsize',font3);


set(findall(fig1,'-property','Interpreter'),'Interpreter','latex');
set(findall(fig1,'-property','TickLabelInterpreter'),'TickLabelInterpreter','latex');


