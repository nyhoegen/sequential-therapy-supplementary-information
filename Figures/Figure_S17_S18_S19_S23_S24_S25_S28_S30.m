% Analysis of the new data with fast growth rates 
% Folders: treatments12h_Cip_fast and treatments12h_Fab2_fast

% Initialize Matlab 
clc
clear all 
close all
%%
treatments = {'12h','24h','1.5d','2d','2.5d','3d','3.5d','4d','5d'};

%Colors
dark = [2 93 114]/255;
pink = [178 93 141]/255;
light = [121 182 185]/255; 
occa = [215,200,100]/255;
black = [0 0 0];
% Define a color vector 
col = [dark;pink;light;occa];
% Define a colormap
col_n=4;
mymap1 = [linspace(black(1),pink(1),6)', linspace(black(2),pink(2),6)',linspace(black(3),pink(3),6)'];
mymap2 = [linspace(pink(1),occa(1),col_n)', linspace(pink(2),occa(2),col_n)',linspace(pink(3),occa(3),col_n)'];
mymap3 = [linspace(occa(1),dark(1),col_n)', linspace(occa(2),dark(2),col_n)',linspace(occa(3),dark(3),col_n)'];
mymap4 = [linspace(dark(1),light(1),col_n)', linspace(dark(2),light(2),col_n)',linspace(dark(3),light(3),col_n)'];
mymap5 = [linspace(light(1),light(1),col_n)', linspace(light(2),light(2),col_n)',linspace(light(3),light(3),col_n)'];
mymap = [mymap1;mymap2;mymap3;mymap4;mymap5];

% Linewidth
set(0, 'DefaultLineLineWidth', 1.5);
% Fontsizes
font = 10; 
font2 = 8;
font3 = 12;

% Selection of Colors
ind_col = [3,6,9,12,16];

%% Load Data: 

% Kappa = 1: 
%---Kappa = 1---------------------------------------------------------------
% Define Data Path
dataPath = 'Data/Treatments12h_Cip_fast';
P_C = load(strcat(dataPath,'/Data_P_C0_1.mat')); 
P_C =P_C.P_data;
L_C = load(strcat(dataPath,'/Data_L_C0_1.mat')); 
L_C = L_C.P_data;
L_C_Bot = load(strcat(dataPath,'/Data_L_Bot_C0_1.mat')); 
L_C_Bot = L_C_Bot.P_data;

P_C_sCS = load(strcat(dataPath,'/Data_P_sCS_C0_1.mat')); 
P_C_sCS =P_C_sCS.P_data;
L_C_sCS = load(strcat(dataPath,'/Data_L_sCS_C0_1.mat')); 
L_C_sCS = L_C_sCS.P_data;
L_C_Bot_sCS = load(strcat(dataPath,'/Data_L_Bot_sCS_C0_1.mat')); 
L_C_Bot_sCS = L_C_Bot_sCS.P_data;

P_C_DDIa = load(strcat(dataPath,'/Data_P_DDIa_C0_1.mat')); 
P_C_DDIa =P_C_DDIa.P_data;
P_C_DDIs = load(strcat(dataPath,'/Data_P_DDIs_C0_1.mat')); 
P_C_DDIs =P_C_DDIs.P_data;

%---Kappa = 2---------------------------------------------------------------
% Define Data Path
dataPath = 'Data/Treatments12h_Fab2_fast';
P_F2 = load(strcat(dataPath,'/Data_P_C0_1.mat')); 
P_F2 =P_F2.P_data;
L_F2 = load(strcat(dataPath,'/Data_L_C0_1.mat')); 
L_F2 = L_F2.P_data;
L_F2_Bot = load(strcat(dataPath,'/Data_L_Bot_C0_1.mat')); 
L_F2_Bot = L_F2_Bot.P_data;

P_F2_sCS = load(strcat(dataPath,'/Data_P_sCS_C0_1.mat')); 
P_F2_sCS =P_F2_sCS.P_data;
L_F2_sCS = load(strcat(dataPath,'/Data_L_sCS_C0_1.mat')); 
L_F2_sCS = L_F2_sCS.P_data;
L_F2_Bot_sCS = load(strcat(dataPath,'/Data_L_Bot_sCS_C0_1.mat')); 
L_F2_Bot_sCS = L_F2_Bot_sCS.P_data;

P_F2_DDIa = load(strcat(dataPath,'/Data_P_DDIa_C0_1.mat')); 
P_F2_DDIa =P_F2_DDIa.P_data;
P_F2_DDIs = load(strcat(dataPath,'/Data_P_DDIs_C0_1.mat')); 
P_F2_DDIs =P_F2_DDIs.P_data;


D = [0:0.01:1;0:0.01:1]; 
% Scale Drug Concnetration in fold change of MIC
D = D./0.017; 

%% Simulation data Patient 

Sim1_C = load('Data/Hybrid_Simulations_new/Treatments12h_Cip_fast/Sim_Pat_T1.mat'); 
Sim1_C = Sim1_C.Sim;
Sim2_C = load('Data/Hybrid_Simulations_new/Treatments12h_Cip_fast/Sim_Pat_T2.mat'); 
Sim2_C = Sim2_C.Sim;
Sim3_C = load('Data/Hybrid_Simulations_new/Treatments12h_Cip_fast/Sim_Pat_T3.mat'); 
Sim3_C = Sim3_C.Sim;
Sim4_C = load('Data/Hybrid_Simulations_new/Treatments12h_Cip_fast/Sim_Pat_T4.mat'); 
Sim4_C = Sim4_C.Sim;
Sim5_C = load('Data/Hybrid_Simulations_new/Treatments12h_Cip_fast/Sim_Pat_T5.mat'); 
Sim5_C = Sim5_C.Sim;

Sim_C = [Sim1_C;Sim2_C;Sim3_C;Sim4_C;Sim5_C];

Sim1_F2 = load('Data/Hybrid_Simulations_new/Treatments12h_Fab2_fast/Sim_Pat_T1.mat'); 
Sim1_F2 = Sim1_F2.Sim;
Sim2_F2 = load('Data/Hybrid_Simulations_new/Treatments12h_Fab2_fast/Sim_Pat_T2.mat'); 
Sim2_F2 = Sim2_F2.Sim;
Sim3_F2 = load('Data/Hybrid_Simulations_new/Treatments12h_Fab2_fast/Sim_Pat_T3.mat'); 
Sim3_F2 = Sim3_F2.Sim;
Sim4_F2 = load('Data/Hybrid_Simulations_new/Treatments12h_Fab2_fast/Sim_Pat_T4.mat'); 
Sim4_F2 = Sim4_F2.Sim;
Sim5_F2 = load('Data/Hybrid_Simulations_new/Treatments12h_Fab2_fast/Sim_Pat_T5.mat'); 
Sim5_F2 = Sim5_F2.Sim;

Sim_F2 = [Sim1_F2;Sim2_F2;Sim3_F2;Sim4_F2;Sim5_F2];


%% Simulation data Bottleneck 

dataPath = 'Data/Hybrid_Simulations_new/Treatments12h_Cip_fast';
Sim_C_T1_2 = load(strcat(dataPath,'/Sim_Lab_T1_BN.mat')); 
Sim_C_T1_2 = Sim_C_T1_2.Sim; 

Sim_C_T2 = load(strcat(dataPath,'/Sim_Lab_T2_BN.mat')); 
Sim_C_T2 = Sim_C_T2.Sim; 

Sim_C_T3_4 = load(strcat(dataPath,'/Sim_Lab_T3_BN.mat')); 
Sim_C_T3_4 = Sim_C_T3_4.Sim; 

Sim_C_T4 = load(strcat(dataPath,'/Sim_Lab_T4_BN.mat')); 
Sim_C_T4 = Sim_C_T4.Sim; 

Sim_C_T5_6 = load(strcat(dataPath,'/Sim_Lab_T5_BN.mat')); 
Sim_C_T5_6 = Sim_C_T5_6.Sim; 

Sim_C_T6 = load(strcat(dataPath,'/Sim_Lab_T6_BN.mat')); 
Sim_C_T6 = Sim_C_T6.Sim; 


Sim_C_BN = [Sim_C_T1_2(1,:);Sim_C_T2(2,:);Sim_C_T3_4(1,:);Sim_C_T4(2,:);Sim_C_T5_6(1,:);Sim_C_T6(2,:)]; 


dataPath = 'Data/Hybrid_Simulations_new/Treatments12h_Fab2_fast';
Sim_F2_T1 = load(strcat(dataPath,'/Sim_Lab_T1_BN.mat')); 
Sim_F2_T1 = Sim_F2_T1.Sim; 

Sim_F2_T2 = load(strcat(dataPath,'/Sim_Lab_T2_BN.mat')); 
Sim_F2_T2 = Sim_F2_T2.Sim; 

Sim_F2_T3 = load(strcat(dataPath,'/Sim_Lab_T3_BN.mat')); 
Sim_F2_T3 = Sim_F2_T3.Sim; 

Sim_F2_T4 = load(strcat(dataPath,'/Sim_Lab_T4_BN.mat')); 
Sim_F2_T4 = Sim_F2_T4.Sim; 

Sim_F2_T5 = load(strcat(dataPath,'/Sim_Lab_T5_BN.mat')); 
Sim_F2_T5 = Sim_F2_T5.Sim; 

Sim_F2_T6 = load(strcat(dataPath,'/Sim_Lab_T6_BN.mat')); 
Sim_F2_T6 = Sim_F2_T6.Sim; 

Sim_F2_BN = [Sim_F2_T1;Sim_F2_T2;Sim_F2_T3;Sim_F2_T4;Sim_F2_T5;Sim_F2_T6];

%% Simulations CS and DDI 
Sim1_C_CS = load('Data/Hybrid_Simulations_new/Treatments12h_Cip_CS_DDI_fast/Sim_T1_CS.mat'); 
Sim1_C_CS = Sim1_C_CS.Sim;
Sim5_C_CS = load('Data/Hybrid_Simulations_new/Treatments12h_Cip_CS_DDI_fast/Sim_T5_CS.mat'); 
Sim5_C_CS = Sim5_C_CS.Sim;

Sim_C_CS = [Sim1_C_CS;Sim5_C_CS]; 

Sim1_C_DDIa = load('Data/Hybrid_Simulations_new/Treatments12h_Cip_CS_DDI_fast/Sim_T1_DDIa.mat'); 
Sim1_C_DDIa = Sim1_C_DDIa.Sim;
Sim5_C_DDIa = load('Data/Hybrid_Simulations_new/Treatments12h_Cip_CS_DDI_fast/Sim_T5_DDIa.mat'); 
Sim5_C_DDIa = Sim5_C_DDIa.Sim;

Sim_C_DDIa = [Sim1_C_DDIa;Sim5_C_DDIa];

Sim1_C_DDIs = load('Data/Hybrid_Simulations_new/Treatments12h_Cip_CS_DDI_fast/Sim_T1_DDIs.mat'); 
Sim1_C_DDIs = Sim1_C_DDIs.Sim;
Sim5_C_DDIs = load('Data/Hybrid_Simulations_new/Treatments12h_Cip_CS_DDI_fast/Sim_T5_DDIs.mat'); 
Sim5_C_DDIs = Sim5_C_DDIs.Sim;

Sim_C_DDIs = [Sim1_C_DDIs;Sim5_C_DDIs];

% Bottleneck with CS
Sim1_C_CS_BN = load('Data/Hybrid_Simulations_new/Treatments12h_Cip_CS_DDI_fast/Sim_Lab_T1_BN.mat'); 
Sim1_C_CS_BN = Sim1_C_CS_BN.Sim;
Sim5_C_CS_BN = load('Data/Hybrid_Simulations_new/Treatments12h_Cip_CS_DDI_fast/Sim_Lab_T5_BN.mat'); 
Sim5_C_CS_BN = Sim5_C_CS_BN.Sim;

Sim_C_CS_BN = [Sim1_C_CS_BN;Sim5_C_CS_BN]; 
%%

D_sim = 0:0.01:1;
ind_sub = [1,3,5];
% Calculate mean values 
n_sim = 100; 
for i = 1:5
    for j = 1:length(D_sim)
        Sim_t_erad_C_BN(1,j) = mean(Sim_C_BN{i,j}); 
        Sim_t_erad_std_C_BN(1,j) = std(Sim_C_BN{i,j}); 
        Sim_t_erad_F2_BN(1,j) = mean(Sim_F2_BN{i,j}); 
        Sim_t_erad_std_F2_BN(1,j) = std(Sim_F2_BN{i,j}); 
    end
    Sim_mean_C_BN{i} = Sim_t_erad_C_BN; 
    Sim_std_C_BN{i} = Sim_t_erad_std_C_BN;
    Sim_sem_C_BN{i} = Sim_t_erad_std_C_BN/sqrt(n_sim);
    
    Sim_mean_F2_BN{i} = Sim_t_erad_F2_BN; 
    Sim_std_F2_BN{i} = Sim_t_erad_std_F2_BN;
    Sim_sem_F2_BN{i} = Sim_t_erad_std_F2_BN/sqrt(n_sim);
end 


for i = 1:5
    for j = 1:length(D_sim)
        Sim_t_erad_C(1,j) = mean(Sim_C{i,j}); 
        Sim_t_erad_std_C(1,j) = std(Sim_C{i,j}); 
        Sim_t_erad_F2(1,j) = mean(Sim_F2{i,j}); 
        Sim_t_erad_std_F2(1,j) = std(Sim_F2{i,j}); 
    end
    Sim_mean_C{i} = Sim_t_erad_C; 
    Sim_std_C{i} = Sim_t_erad_std_C;
    Sim_sem_C{i} = Sim_t_erad_std_C/sqrt(n_sim);
    
    Sim_mean_F2{i} = Sim_t_erad_F2; 
    Sim_std_F2{i} = Sim_t_erad_std_F2;
    Sim_sem_F2{i} = Sim_t_erad_std_F2/sqrt(n_sim);
end 

for i = 1:2
    for j = 1:length(D_sim)
        Sim_t_erad_C_CS(1,j) = mean(Sim_C_CS{i,j}); 
        Sim_t_erad_std_C_CS(1,j) = std(Sim_C_CS{i,j});  
        
        Sim_t_erad_C_DDIa(1,j) = mean(Sim_C_DDIa{i,j}); 
        Sim_t_erad_std_C_DDIa(1,j) = std(Sim_C_DDIa{i,j});
        
        Sim_t_erad_C_DDIs(1,j) = mean(Sim_C_DDIs{i,j}); 
        Sim_t_erad_std_C_DDIs(1,j) = std(Sim_C_DDIs{i,j});
        
        Sim_t_erad_C_CS_BN(1,j) = mean(Sim_C_CS_BN{i,j}); 
        Sim_t_erad_std_C_CS_BN(1,j) = std(Sim_C_CS_BN{i,j});
    end
    Sim_mean_C_CS{i} = Sim_t_erad_C_CS; 
    Sim_std_C_CS{i} = Sim_t_erad_std_C_CS;
    Sim_sem_C_CS{i} = Sim_t_erad_std_C_CS/sqrt(n_sim);
    
    Sim_mean_C_DDIa{i} = Sim_t_erad_C_DDIa; 
    Sim_std_C_DDIa{i} = Sim_t_erad_std_C_DDIa;
    Sim_sem_C_DDIa{i} = Sim_t_erad_std_C_DDIa/sqrt(n_sim);
    
    Sim_mean_C_DDIs{i} = Sim_t_erad_C_DDIs;
    Sim_std_C_DDIs{i} = Sim_t_erad_std_C_DDIs;
    Sim_sem_C_DDIs{i} = Sim_t_erad_std_C_DDIs/sqrt(n_sim);
    
    Sim_mean_C_CS_BN{i} = Sim_t_erad_C_CS_BN; 
    Sim_std_C_CS_BN{i} = Sim_t_erad_std_C_CS_BN;
    Sim_sem_C_CS_BN{i} = Sim_t_erad_std_C_CS_BN/sqrt(n_sim);
    
end 
%% Delete entry for D = 0.8 (ind 81) (ODE system was not solvable - division by zero)

for i = 1:9 
    temp = [P_C(i).t_erad(1:80),P_C(i).t_erad(82:end)];
    P_C(i).t_erad = temp; 
    
    temp2 = [P_F2(i).t_erad(1:80),P_F2(i).t_erad(82:end)];
    P_F2(i).t_erad = temp2; 
end

D_sub = [D(1,1:80),D(1,82:end)]; 


%% S17: Pat and Lab 
% Set the positions of the different subplots 
% length and height of sub-plots: 
sp_length = 0.35; 
sp_height = 0.22;
% position of subplots in x-direction 
x_pos1 = 0.08; 
x_pos2 = 0.5; 

% Position of the Subplots 
Position = [x_pos1,0.7,sp_length,sp_height;...
            x_pos2,0.7,sp_length,sp_height;...
            x_pos1,0.4,sp_length,sp_height;...
            x_pos2,0.4,sp_length,sp_height;...
            x_pos1,0.1,sp_length,sp_height;...
            x_pos2,0.1,sp_length,sp_height];
        
% New color vector: 
grey_mat = flipud(gray(14)); 
        
% Selection of Colors
ind_col = [3,6,9,12,16];

% Plot figure 
fig1=figure(1);
% Set size of the figure 
fig1.Units = 'centimeters';
fig1.Position=[10,2,15.5,15]; 

subplot('Position',Position(1,:))
for j = 1:5
    % Plot t_erad 
    plots(j) = plot(D_sub(1,:),P_C(j).t_erad,'Color', mymap(ind_col(j),:),'Linewidth',1.5);
    hold on 
end
for j = 6:9
    % Plot t_erad 
    plots(j) = plot(D_sub(1,:),P_C(j).t_erad,'--','Color', grey_mat(j-3,:),'Linewidth',1.5);
    hold on 
end
% Set fontsize for axis
set(gca,'FontSize',font2);
% set axis propeties 
ylim([0,300])
yticks([0,50,150,250]);
title('$\kappa_A = \kappa_B = 1$','FontSize',font);

subplot('Position',Position(2,:))
for j = 1:5
    % Plot t_erad 
    plots(j) = plot(D_sub(1,:),P_F2(j).t_erad,'Color', mymap(ind_col(j),:),'Linewidth',1.5);
    hold on 
end
for j = 6:9
    % Plot t_erad 
    plots(j) = plot(D_sub(1,:),P_F2(j).t_erad,'--','Color', grey_mat(j-3,:),'Linewidth',1.5);
    hold on 
end
% Set fontsize for axis
set(gca,'FontSize',font2);
% set axis propeties 
ylim([0,300])
yticks([0,50,150,250]);
title('$\kappa_A = \kappa_B = 2$','FontSize',font);

subplot('Position',Position(3,:))
for j = 1:5
    % Plot t_erad 
    plots(j) = plot(D(1,:),L_C(j).t_erad,'Color', mymap(ind_col(j),:),'Linewidth',1.5);
    hold on 
end
for j = 6:9
    % Plot t_erad 
    plots(j) = plot(D(1,:),L_C(j).t_erad,'--','Color', grey_mat(j-3,:),'Linewidth',1.5);
    hold on 
end
% Set fontsize for axis
set(gca,'FontSize',font2);
% set axis propeties 
ylim([0,300])
yticks([0,50,150,250]);
% title('Lab, \kappa = 1','FontSize',font);
ylabel('Time until threshold $N_c$ in hours','FontSize',font);

subplot('Position',Position(4,:))
for j = 1:5
    % Plot t_erad 
    plots(j) = plot(D(1,:),L_F2(j).t_erad,'Color', mymap(ind_col(j),:),'Linewidth',1.5);
    hold on 
end
for j = 6:9
    % Plot t_erad 
    plots(j) = plot(D(1,:),L_F2(j).t_erad,'--','Color', grey_mat(j-3,:),'Linewidth',1.5);
    hold on 
end
% Set fontsize for axis
set(gca,'FontSize',font2);
% set axis propeties 
ylim([0,300])
yticks([0,50,150,250]);
% title('Lab, \kappa = 2','FontSize',font);


h1 = subplot('Position',Position(5,:));
for j = 1:5
    % Plot t_erad 
    plots(j) = plot(D(1,:),L_C_Bot(j).t_erad,'Color', mymap(ind_col(j),:),'Linewidth',1.5);
    hold on 
end
for j = 6:9
    % Plot t_erad 
    plots(j) = plot(D(1,:),L_C_Bot(j).t_erad,'--','Color', grey_mat(j-3,:),'Linewidth',1.5);
    hold on 
end
% Set fontsize for axis
set(gca,'FontSize',font2);
% set axis propeties 
ylim([0,300])
yticks([0,50,150,250]);
% title('Lab, \kappa = 1, with Bottelneck','FontSize',font);

h2 = subplot('Position',Position(6,:));
for j = 1:5
    % Plot t_erad 
    plots(j) = plot(D(1,:),L_F2_Bot(j).t_erad,'Color', mymap(ind_col(j),:),'Linewidth',1.5);
    hold on 
end
for j = 6:9
    % Plot t_erad 
    plots(j) = plot(D(1,:),L_F2_Bot(j).t_erad,'--','Color', grey_mat(j-3,:),'Linewidth',1.5);
    hold on 
end
% Set fontsize for axis
set(gca,'FontSize',font2);
% set axis propeties 
ylim([0,300])
yticks([0,50,150,250]);
% title('$\kappa_A = \kappa_B = 2','FontSize',font);
l1=legend(plots(9:-1:1),treatments(9:-1:1),'FontSize',font2,'Location','best');
l1.Position = [0.875,0.745,0.1,0.1];
l1.ItemTokenSize = [15 18];
title(l1,'Switch after');

% Commen X label 
p3=get(h1,'position');
p4=get(h2,'position');
width = p4(1)+p4(3)-p3(1); 
axes('position',[p3(1) p4(2) width p3(4)],'visible','off');
xlabel('Drug dose $D$ in multiples of zMIC$_W$','FontSize',font','visible','on','Fontsize',font,'Interpreter','latex');


%Annotate 'A' and 'B' 
dimA = [0.07,0.96,0,0];
annotation('textbox',dimA,'String','A','FitBoxToText','on','EdgeColor','none','Fontsize',font3);
dimC = [0.07,0.66,0,0];
annotation('textbox',dimC,'String','B','FitBoxToText','on','EdgeColor','none','Fontsize',font3);
dimE = [0.07,0.36,0,0];
annotation('textbox',dimE,'String','C','FitBoxToText','on','EdgeColor','none','Fontsize',font3);
dimG = [0.81,0.96,0,0];
annotation('textbox',dimG,'String','D','FitBoxToText','on','EdgeColor','none','Fontsize',font3);

dimB = [0.81,0.66,0,0];
annotation('textbox',dimB,'String','E','FitBoxToText','on','EdgeColor','none','Fontsize',font3);
dimD = [0.81,0.36,0,0];
annotation('textbox',dimD,'String','F','FitBoxToText','on','EdgeColor','none','Fontsize',font3);

dim5 = [0.42,0.97,0,0];
str5 = 'Patient';
annotation('textbox',dim5,'String',str5,'FitBoxToText','on','EdgeColor','none','Fontsize',font,'fontweight','bold');

dim6 = [0.34,0.67,0,0];
str6 = 'Lab without bottleneck';
annotation('textbox',dim6,'String',str6,'FitBoxToText','on','EdgeColor','none','Fontsize',font,'fontweight','bold');

dim7 = [0.35,0.37,0,0];
str7 = 'Lab with bottleneck';
annotation('textbox',dim7,'String',str7,'FitBoxToText','on','EdgeColor','none','Fontsize',font,'fontweight','bold');


set(findall(fig1,'-property','Interpreter'),'Interpreter','latex');
set(findall(fig1,'-property','TickLabelInterpreter'),'TickLabelInterpreter','latex');
set(findall(fig1,'-property','YLim'),'YLim',[0,150]);
set(findall(fig1,'-property','YTick'),'YTick',[0,50,100,150]);
%% S23: Effect of CS
set_ind = [1,5];

% Set the positions of the different subplots 
% length and height of sub-plots: 
sp_length = 0.35; 
sp_height = 0.22;
% position of subplots in x-direction 
x_pos1 = 0.08; 
x_pos2 = 0.5; 

% Position of the Subplots 
Position = [x_pos1,0.7,sp_length,sp_height;...
            x_pos2,0.7,sp_length,sp_height;...
            x_pos1,0.4,sp_length,sp_height;...
            x_pos2,0.4,sp_length,sp_height;...
            x_pos1,0.1,sp_length,sp_height;...
            x_pos2,0.1,sp_length,sp_height];

fig2 = figure(2); 
fig2.Units = 'centimeters';
fig2.Position=[10,5,15.5,15];

subplot('Position',Position(1,:))
% Plot Basic, wCS and sCS for the four different Treatments 
plot(D(1,:),P_C_sCS(set_ind(1)).t_erad,'-.','Color', mymap(12,:),'Linewidth',2);
hold on    
plot(D_sub(1,:),P_C(set_ind(1)).t_erad,'Color', mymap(15,:),'Linewidth',2);
hold on
set(gca,'FontSize',font2)
str = strcat('Switch after',{' '},treatments(set_ind(1)));
title(str,'FontSize',font)
% Set fontsize for axis
set(gca,'FontSize',font2);
% set axis propeties 
ylim([0,300])
yticks([0,50,150,250]);

subplot('Position',Position(2,:))
% Plot Basic, wCS and sCS for the four different Treatments 
p_CS(2)=plot(D(1,:),P_C_sCS(set_ind(2)).t_erad,'-.','Color', mymap(12,:),'Linewidth',2);
hold on    
p_CS(1)=plot(D_sub(1,:),P_C(set_ind(2)).t_erad,'Color', mymap(15,:),'Linewidth',2);
hold on
str = strcat('Switch after',{' '},treatments(set_ind(2)));
title(str,'FontSize',font)
% Set fontsize for axis
set(gca,'FontSize',font2);
% set axis propeties 
ylim([0,300])
yticks([0,50,150,250]);
l2=legend(p_CS,{'No collateral sensitivity','Collateral sensitivity'},'FontSize',font2,'Location','southwest');
l2.ItemTokenSize = [15 18];

subplot('Position',Position(3,:))
% Plot Basic, wCS and sCS for the four different Treatments 
plot(D(1,:),L_C_sCS(set_ind(1)).t_erad,'-.','Color', mymap(12,:),'Linewidth',2);
hold on    
plot(D(1,:),L_C(set_ind(1)).t_erad,'Color', mymap(15,:),'Linewidth',2);
hold on
% Set fontsize for axis
set(gca,'FontSize',font2);
% set axis propeties 
ylim([0,300])
yticks([0,50,150,250]);
% title('Lab, \kappa = 1','FontSize',font);
ylabel('Time until threshold $N_c$ in hours','FontSize',font);

subplot('Position',Position(4,:))
% Plot Basic, wCS and sCS for the four different Treatments 
plot(D(1,:),L_C_sCS(set_ind(2)).t_erad,'-.','Color', mymap(12,:),'Linewidth',2);
hold on   
plot(D(1,:),L_C(set_ind(2)).t_erad,'Color', mymap(15,:),'Linewidth',2);
hold on
% Set fontsize for axis
set(gca,'FontSize',font2);
% set axis propeties 
ylim([0,300])
yticks([0,50,150,250]);
% title('Lab, \kappa = 2','FontSize',font);


h1 = subplot('Position',Position(5,:));
plot(D(1,:),L_C_Bot_sCS(set_ind(1)).t_erad,'-.','Color', mymap(12,:),'Linewidth',2);
hold on    
plot(D(1,:),L_C_Bot(set_ind(1)).t_erad,'Color', mymap(15,:),'Linewidth',2);
hold on
% Set fontsize for axis
set(gca,'FontSize',font2);
% set axis propeties 
ylim([0,300])
yticks([0,50,150,250]);
% title('Lab, \kappa = 1, with Bottelneck','FontSize',font);

h2 = subplot('Position',Position(6,:));
plot(D(1,:),L_C_Bot_sCS(set_ind(2)).t_erad,'-.','Color', mymap(12,:),'Linewidth',2);
hold on    
plot(D(1,:),L_C_Bot(set_ind(2)).t_erad,'Color', mymap(15,:),'Linewidth',2);
hold on
% Set fontsize for axis
set(gca,'FontSize',font2);
% set axis propeties 
ylim([0,300])
yticks([0,50,150,250]);
% title('$\kappa_A = \kappa_B = 2','FontSize',font);


%Commen X label 
p3=get(h1,'position');
p4=get(h2,'position');
width = p4(1)+p4(3)-p3(1); 
axes('position',[p3(1) p4(2) width p3(4)],'visible','off');
xlabel('Drug dose $D$ in multiples of zMIC$_W$','FontSize',font','visible','on','Fontsize',font,'Interpreter','latex');


%Annotate 'A' and 'B' 
dimA = [0.07,0.96,0,0];
annotation('textbox',dimA,'String','A','FitBoxToText','on','EdgeColor','none','Fontsize',font3);
dimC = [0.07,0.66,0,0];
annotation('textbox',dimC,'String','B','FitBoxToText','on','EdgeColor','none','Fontsize',font3);
dimE = [0.07,0.36,0,0];
annotation('textbox',dimE,'String','C','FitBoxToText','on','EdgeColor','none','Fontsize',font3);
dimG = [0.81,0.96,0,0];
annotation('textbox',dimG,'String','D','FitBoxToText','on','EdgeColor','none','Fontsize',font3);

dimB = [0.81,0.66,0,0];
annotation('textbox',dimB,'String','E','FitBoxToText','on','EdgeColor','none','Fontsize',font3);
dimD = [0.81,0.36,0,0];
annotation('textbox',dimD,'String','F','FitBoxToText','on','EdgeColor','none','Fontsize',font3);

dim5 = [0.42,0.97,0,0];
str5 = 'Patient';
annotation('textbox',dim5,'String',str5,'FitBoxToText','on','EdgeColor','none','Fontsize',font,'fontweight','bold');

dim6 = [0.34,0.67,0,0];
str6 = 'Lab without bottleneck';
annotation('textbox',dim6,'String',str6,'FitBoxToText','on','EdgeColor','none','Fontsize',font,'fontweight','bold');

dim7 = [0.35,0.37,0,0];
str7 = 'Lab with bottleneck';
annotation('textbox',dim7,'String',str7,'FitBoxToText','on','EdgeColor','none','Fontsize',font,'fontweight','bold');


set(findall(fig2,'-property','Interpreter'),'Interpreter','latex');
set(findall(fig2,'-property','TickLabelInterpreter'),'TickLabelInterpreter','latex');
set(findall(fig2,'-property','YLim'),'YLim',[0,75]);
set(findall(fig2,'-property','XLim'),'XLim',[0,60]);
set(findall(fig2,'-property','YTick'),'YTick',[0,20,40,60]);
%% S28: Drug-Drug Interaction:

set_ind = [1,5];

% Set the positions of the different subplots 
% length and height of sub-plots: 
sp_length = 0.4; 
sp_height = 0.7;
% position of subplots in x-direction 
x_pos1 = 0.1; 
x_pos2 = 0.55; 

% Position of the Subplots 
Position = [x_pos1,0.15,sp_length,sp_height;...
            x_pos2,0.15,sp_length,sp_height];

fig3 = figure(3); 
fig3.Units = 'centimeters';
fig3.Position=[10,10,15.5,6];

h1 = subplot('Position',Position(1,:));
% Plot Basic, Anta and Syn for the four different Treatments 
p_DDI(1)=plot(D(1,:),P_C_DDIa(set_ind(1)).t_erad,'Color', col(2,:),'Linewidth',2);
hold on 
p_DDI(3)=plot(D(1,:),P_C_DDIs(set_ind(1)).t_erad,'Color', col(3,:),'Linewidth',2);
hold on    
p_DDI(2)=plot(D_sub(1,:),P_C(set_ind(1)).t_erad,'Color', col(1,:),'Linewidth',2);
hold on 
set(gca,'FontSize',font2)
ylim([0,70]);
str = strcat('Switch after',{' '},treatments(set_ind(1)));
title(str,'FontSize',font)
l3=legend(p_DDI,{'Antagonism','Bliss Indipendence','Synergism'},'FontSize',font2,'Location','northeast');
l3.ItemTokenSize = [15 18];
ylabel({'Time until threshold'; '$N_c$ in hours'},'visible','on','Fontsize',font,'Interpreter','latex');


h2 = subplot('Position',Position(2,:));
% Plot Basic, Anta and Syn for the four different Treatments 
plot(D(1,:),P_C_DDIa(set_ind(2)).t_erad,'Color', col(2,:),'Linewidth',2);
hold on 
plot(D(1,:),P_C_DDIs(set_ind(2)).t_erad,'Color', col(3,:),'Linewidth',2);
hold on    
plot(D_sub(1,:),P_C(set_ind(2)).t_erad,'Color', col(1,:),'Linewidth',2);
hold on 
set(gca,'FontSize',font2)
set(gca,'YTickLabels',[]);
set(gca,'FontSize',font2)
str = strcat('Switch after',{' '},treatments(set_ind(2)));
title(str,'FontSize',font)
ylim([0,70]);



% Commen X label 
p3=get(h1,'position');
p4=get(h2,'position');
width = p4(1)+p4(3)-p3(1); 
axes('position',[p3(1) p3(2) width p3(4)],'visible','off');
xlabel('Drug dose $D$ in multiples of zMIC$_W$','FontSize',font','visible','on','Fontsize',font,'Interpreter','latex');

dimA = [0.09,0.96,0,0];
annotation('textbox',dimA,'String','A','FitBoxToText','on','EdgeColor','none','Fontsize',font3);
dimB = [0.54,0.96,0,0];
annotation('textbox',dimB,'String','B','FitBoxToText','on','EdgeColor','none','Fontsize',font3);




set(findall(fig3,'-property','Interpreter'),'Interpreter','latex');
set(findall(fig3,'-property','TickLabelInterpreter'),'TickLabelInterpreter','latex');
set(findall(fig3,'-property','XLim'),'XLim',[0,60])

%% S30: Treatment comparison drug-drug-interaction
fig4 = figure(4); 
fig4.Units = 'centimeters';
fig4.Position=[10,10,15.5,6];

for j = 9:-1:6
    % Plot t_erad 
    plots(j) = plot(D(1,:),P_C_DDIa(j).t_erad,'--','Color',grey_mat(j-3,:),'Linewidth',1.5);
    hold on 
end
for j = 5:-1:1
    % Plot t_erad 
    plots(j) = plot(D(1,:),P_C_DDIa(j).t_erad,'Color', mymap(ind_col(j),:),'Linewidth',1.5);
    hold on 
end
% Set fontsize for axis
set(gca,'FontSize',font2);
% set axis propeties 
ylim([0,300])
yticks([0,50,150,250]);
% title('$\kappa_A = \kappa_B = 2','FontSize',font);
l1=legend(plots(9:-1:1),treatments(9:-1:1),'FontSize',font2,'Location','northeastoutside');
l1.ItemTokenSize = [15 18];
title(l1,'Switch after');
xlabel('Drug dose $D$ in multiples of zMIC$_W$','FontSize',font','visible','on','Fontsize',font,'Interpreter','latex');
ylabel({'Time until threshold'; '$N_c=1$ in hours'},'visible','on','Fontsize',font,'Interpreter','latex');
title('Antagonistic drug-drug-interactions','FontSize',font);

set(findall(fig4,'-property','Interpreter'),'Interpreter','latex');
set(findall(fig4,'-property','TickLabelInterpreter'),'TickLabelInterpreter','latex');
set(findall(fig4,'-property','YLim'),'YLim',[0,100]);
set(findall(fig4,'-property','XLim'),'XLim',[0,60])
set(findall(fig4,'-property','YTick'),'YTick',[0,20,40,60,80]);


%% S18: Simulation results 

Pos = [0.1,0.2,0.35,0.7;
        0.55,0.2,0.35,0.7];
    
fig1 = figure(1);  
fig1.Units = 'centimeter';
fig1.Position = [10,2,15.5,6]; 
n_err = 8; 

s1 = subplot('Position',Pos(1,:));
for i = 5:-1:1
%     errorbar(D_sim(1:n_err:end)/0.017,Sim_mean_C{i}(1:n_err:end),Sim_std_C{i}(1:n_err:end),'.','Color',mymap(ind_col(i),:),'linewidth',1,'CapSize',4);
%     hold on 
    plot(D_sim([17:80,82:101])/0.017,Sim_mean_C{i}([17:80,82:101]),'Color', mymap(ind_col(i),:),'Linewidth',1.5);
    hold on 
end

% Set fontsize for axis
set(gca,'FontSize',font2);
% set axis propeties 
ylim([0,90])
% title('Lab, \kappa = 1, with Bottelneck','FontSize',font);
ylabel({'Time until threshold'; '$N_c$ in hours'},'visible','on','Fontsize',font,'Interpreter','latex');
title('$\kappa_A = \kappa_B = 1$','FontSize',font); 

s2 = subplot('Position',Pos(2,:));
for i = 5:-1:1
%     errorbar(D_sim(1:n_err:end)/0.017,Sim_mean_F2{i}(1:n_err:end),Sim_std_F2{i}(1:n_err:end),'.','Color',mymap(ind_col(i),:),'linewidth',1,'CapSize',4);
%     hold on
     plots(i)=plot(D_sim([1:80,82:101])/0.017,Sim_mean_F2{i}([1:80,82:101]),'Color', mymap(ind_col(i),:),'Linewidth',1.5);
    hold on 
end
% Set fontsize for axis
set(gca,'FontSize',font2);
% set axis propeties 
ylim([0,90])
% title('Lab, \kappa = 1, with Bottelneck','FontSize',font);
leg = legend(plots(5:-1:1),treatments(5:-1:1)); 
title(leg,'Switch after'); 
leg.ItemTokenSize = [15 18]; 
title('$\kappa_A = \kappa_B = 2$','FontSize',font); 


% Commen X label 
p3=get(s1,'position');
p4=get(s2,'position');
width = p4(1)+p4(3)-p3(1); 
axes('position',[p3(1) p4(2) width p3(4)],'visible','off');
xlabel('Drug dose $D$ in multiples of zMIC$_W$','FontSize',font','visible','on','Fontsize',font,'Interpreter','latex');

dimA = [0.09,1,0,0];
annotation('textbox',dimA,'String','A','FitBoxToText','on','EdgeColor','none','Fontsize',font3);
dimC = [0.54,1,0,0];
annotation('textbox',dimC,'String','B','FitBoxToText','on','EdgeColor','none','Fontsize',font3);


set(findall(fig1,'-property','Interpreter'),'Interpreter','latex');
set(findall(fig1,'-property','TickLabelInterpreter'),'TickLabelInterpreter','latex');
set(findall(fig1,'-property','XLim'),'XLim',[0,60]);
set(findall(fig1,'-property','YLim'),'YLim',[0,40]);
%% S19: Simulation of Bottlenecks 

Pos = [0.1,0.2,0.35,0.7;
        0.55,0.2,0.35,0.7];
    
fig1 = figure(2);  
fig1.Units = 'centimeter';
fig1.Position = [10,2,15.5,6]; 
n_err = 8; 

s1 = subplot('Position',Pos(1,:));
for i = 5:-1:1
%     errorbar(D_sim(1:n_err:end)/0.017,Sim_mean_C{i}(1:n_err:end),Sim_std_C{i}(1:n_err:end),'.','Color',mymap(ind_col(i),:),'linewidth',1,'CapSize',4);
%     hold on 
    if i > 1
        plot(D_sim/0.017,Sim_mean_C_BN{i},'Color', mymap(ind_col(i),:),'Linewidth',1.5);
        hold on 
    else 
        plot(D_sim(8:end)/0.017,Sim_mean_C_BN{i}(8:end),'Color', mymap(ind_col(i),:),'Linewidth',1.5);
        hold on 
    end
end

% Set fontsize for axis
set(gca,'FontSize',font2);
% set axis propeties 
ylim([0,90])
% title('Lab, \kappa = 1, with Bottelneck','FontSize',font);
ylabel({'Time until threshold'; '$N_c$ in hours'},'visible','on','Fontsize',font,'Interpreter','latex');
title('$\kappa_A = \kappa_B = 1$','FontSize',font); 

s2 = subplot('Position',Pos(2,:));
for i = 5:-1:1
%     errorbar(D_sim(1:n_err:end)/0.017,Sim_mean_F2{i}(1:n_err:end),Sim_std_F2{i}(1:n_err:end),'.','Color',mymap(ind_col(i),:),'linewidth',1,'CapSize',4);
%     hold on
     plots(i)=plot(D_sim/0.017,Sim_mean_F2_BN{i},'Color', mymap(ind_col(i),:),'Linewidth',1.5);
    hold on 
end
% Set fontsize for axis
set(gca,'FontSize',font2);
% set axis propeties 
ylim([0,90])
% title('Lab, \kappa = 1, with Bottelneck','FontSize',font);
leg = legend(plots(5:-1:1),treatments(5:-1:1)); 
title(leg,'Switch after'); 
leg.ItemTokenSize = [15 18]; 
title('$\kappa_A = \kappa_B = 2$','FontSize',font); 

% Commen X label 
p3=get(s1,'position');
p4=get(s2,'position');
width = p4(1)+p4(3)-p3(1); 
axes('position',[p3(1) p4(2) width p3(4)],'visible','off');
xlabel('Drug dose $D$ in multiples of zMIC$_W$','FontSize',font','visible','on','Fontsize',font,'Interpreter','latex');

dimA = [0.09,1,0,0];
annotation('textbox',dimA,'String','A','FitBoxToText','on','EdgeColor','none','Fontsize',font3);
dimC = [0.54,1,0,0];
annotation('textbox',dimC,'String','B','FitBoxToText','on','EdgeColor','none','Fontsize',font3);


set(findall(fig1,'-property','Interpreter'),'Interpreter','latex');
set(findall(fig1,'-property','TickLabelInterpreter'),'TickLabelInterpreter','latex');

%% S24: CS and DDI

Pos = [0.1,0.2,0.35,0.7;
        0.55,0.2,0.35,0.7];
    
fig1 = figure(3);  
fig1.Units = 'centimeter';
fig1.Position = [10,2,15.5,6]; 
n_err = 8; 
int_B = [1,5];

s(1) = subplot('Position',Pos(1,:));
plot(D_sim([17:80,82:101])/0.017,Sim_mean_C{1}([17:80,82:101]),'Color', mymap(15,:),'Linewidth',1.5);
hold on 
plot(D_sim([1:80,82:101])/0.017,Sim_mean_C_CS{1}([1:80,82:101]),'Color', mymap(12,:),'Linewidth',1.5);
hold on 
% Set fontsize for axis
set(gca,'FontSize',font2);
% set axis propeties 
ylim([0,90])
% title('Lab, \kappa = 1, with Bottelneck','FontSize',font);
ylabel({'Time until threshold'; '$N_c$ in hours'},'visible','on','Fontsize',font,'Interpreter','latex');
title('Collateral sensitivity','FontSize',font); 
legCS = legend('Without collateral sensitivity','With collateral sensitivity'); 
legCS.ItemTokenSize = [15,18]; 


s(2) = subplot('Position',Pos(2,:));
plot(D_sim/0.017,Sim_mean_C_DDIa{1},'Color', mymap(6,:),'Linewidth',1.5);
hold on
plot(D_sim([17:80,82:101])/0.017,Sim_mean_C{1}([17:80,82:101]),'Color', mymap(15,:),'Linewidth',1.5);
hold on 
plot(D_sim/0.017,Sim_mean_C_DDIs{1},'Color', mymap(18,:),'Linewidth',1.5);
hold on 
% Set fontsize for axis
set(gca,'FontSize',font2);
% set axis propeties 
ylim([0,90])
% title('Lab, \kappa = 1, with Bottelneck','FontSize',font);
title('Drug-drug-interactions','FontSize',font); 
legDDI = legend('Antagonism','Bliss independence','Synergism'); 
legDDI.ItemTokenSize = [15,18]; 


% Commen X label 
p3=get(s(1),'position');
p4=get(s(2),'position');
width = p4(1)+p4(3)-p3(1); 
axes('position',[p3(1) p4(2) width p3(4)],'visible','off');
xlabel('Drug dose $D$ in multiples of zMIC$_W$','FontSize',font','visible','on','Fontsize',font,'Interpreter','latex');

dimA = [0.09,1,0,0];
annotation('textbox',dimA,'String','A','FitBoxToText','on','EdgeColor','none','Fontsize',font3);
dimC = [0.54,1,0,0];
annotation('textbox',dimC,'String','B','FitBoxToText','on','EdgeColor','none','Fontsize',font3);


set(findall(fig1,'-property','Interpreter'),'Interpreter','latex');
set(findall(fig1,'-property','TickLabelInterpreter'),'TickLabelInterpreter','latex');
set(findall(fig1,'-property','XLim'),'XLim',[0,60]);
set(findall(fig1,'-property','YLim'),'YLim',[0,40]);

%% S25: Simulation of Bottlenecks with CS

Pos = [0.1,0.2,0.35,0.7;
        0.55,0.2,0.35,0.7];
    
fig1 = figure(2);  
fig1.Units = 'centimeter';
fig1.Position = [10,2,15.5,6]; 
n_err = 8; 

s1 = subplot('Position',Pos(1,:));
plot(D_sim(4:end)/0.017,Sim_mean_C_BN{1}(4:end),'Color', mymap(15,:),'Linewidth',1.5);
hold on 
plot(D_sim/0.017,Sim_mean_C_CS_BN{1},'Color', mymap(12,:),'Linewidth',1.5);
hold on 
% Set fontsize for axis
set(gca,'FontSize',font2);
% set axis propeties 
ylim([0,90])
% title('Lab, \kappa = 1, with Bottelneck','FontSize',font);
ylabel({'Time until threshold'; '$N_c$ in hours'},'visible','on','Fontsize',font,'Interpreter','latex');
title('Switch after 12h','FontSize',font); 
legCS = legend('Without collateral sensitivity','With collateral sensitivity'); 
legCS.ItemTokenSize = [15,18]; 

s2 = subplot('Position',Pos(2,:));
plot(D_sim/0.017,Sim_mean_C_BN{5},'Color', mymap(15,:),'Linewidth',1.5);
hold on 
plot(D_sim/0.017,Sim_mean_C_CS_BN{2},'Color', mymap(12,:),'Linewidth',1.5);
hold on 

% Set fontsize for axis
set(gca,'FontSize',font2);
% set axis propeties 
ylim([0,90])
% title('Lab, \kappa = 1, with Bottelneck','FontSize',font);
title('Switch after 2.5d','FontSize',font); 

% Commen X label 
p3=get(s1,'position');
p4=get(s2,'position');
width = p4(1)+p4(3)-p3(1); 
axes('position',[p3(1) p4(2) width p3(4)],'visible','off');
xlabel('Drug dose $D$ in multiples of zMIC$_W$','FontSize',font','visible','on','Fontsize',font,'Interpreter','latex');

dimA = [0.09,1,0,0];
annotation('textbox',dimA,'String','A','FitBoxToText','on','EdgeColor','none','Fontsize',font3);
dimC = [0.54,1,0,0];
annotation('textbox',dimC,'String','B','FitBoxToText','on','EdgeColor','none','Fontsize',font3);


set(findall(fig1,'-property','Interpreter'),'Interpreter','latex');
set(findall(fig1,'-property','TickLabelInterpreter'),'TickLabelInterpreter','latex');
set(findall(fig1,'-property','XLim'),'XLim',[0,60]);
set(findall(fig1,'-property','YLim'),'YLim',[0,60]);