%Skript to generate Figure 2 
% Initialize Matlab: 
clc
close all 
clear 

%% Plot Settings 

%Colors
dark = [2 93 114]/255;
pink = [178 93 141]/255;
light = [121 182 185]/255; 
occa = [215,200,100]/255;
black = [0 0 0];
% Define a color vector 
col = [dark;pink;light;occa];
% Define a colormap
col_n=4;
mymap1 = [linspace(black(1),pink(1),6)', linspace(black(2),pink(2),6)',linspace(black(3),pink(3),6)'];
mymap2 = [linspace(pink(1),occa(1),col_n)', linspace(pink(2),occa(2),col_n)',linspace(pink(3),occa(3),col_n)'];
mymap3 = [linspace(occa(1),dark(1),col_n)', linspace(occa(2),dark(2),col_n)',linspace(occa(3),dark(3),col_n)'];
mymap4 = [linspace(dark(1),light(1),col_n)', linspace(dark(2),light(2),col_n)',linspace(dark(3),light(3),col_n)'];
mymap5 = [linspace(light(1),light(1),col_n)', linspace(light(2),light(2),col_n)',linspace(light(3),light(3),col_n)'];
mymap = [mymap1;mymap2;mymap3;mymap4;mymap5];

% Define a marker array for the Treatment Comparison 
marker ={'-','--',':','-.','-','--',':','-.','-','--',':','-.','-','--',':','-.'};
% Linewidth
set(0, 'DefaultLineLineWidth', 1.5);
% Fontsizes
font = 10; 
font2 = 8;
font3 = 12;

% Treatment Name 
T ={'12h','24h','1.5D','2D','2.5D','3D','3.5D','4D','5D','6D','7D','8D','9D','14D','20D','never'};
% Size of the Marker 
m_Size = 5;


%% Load Data 

% First Effective Concentration for all cases 
EffectiveC_Cip = load('Data/First_Effective_Concentration/EffectiveC_Cip.mat');
EffectiveC_Cip = EffectiveC_Cip.Effective_C;
EffectiveC_lab_Cip = load('Data/First_Effective_Concentration/EffectiveC_Lab_Cip.mat');
EffectiveC_Lab_Cip = EffectiveC_lab_Cip.Effective_C_Lab;
EffectiveC_Fab2 = load('Data/First_Effective_Concentration/EffectiveC_Fab2.mat');
EffectiveC_Fab2 = EffectiveC_Fab2.Effective_C;
EffectiveC_lab_Fab2 = load('Data/First_Effective_Concentration/EffectiveC_Lab_Fab2.mat');
EffectiveC_Lab_Fab2 = EffectiveC_lab_Fab2.Effective_C_Lab;

% C_star 
C_star_Cip= load('Data/First_Effective_Concentration/C_star_Cip.mat');
C_star_Cip = C_star_Cip.C_star; 

C_star_Cip_Lab= load('Data/First_Effective_Concentration/C_star_Cip_Lab.mat');
C_star_Cip_Lab = C_star_Cip_Lab.C_star; 


n = 200;
D1 = [linspace(0,0.2,n);linspace(0,0.2,n)];
D2 = [linspace(0.2,0.6,n);linspace(0.2,0.6,n)]; 
D = [D1(:,1:end-1),D2];
% Scale Drug Concnetration in fold change of MIC
D = D./0.017; 


%% Figure 2  
% Positions for the subplots 
Position = [0.1,0.2,0.38,0.7;...
            0.55,0.2,0.38,0.7];
 % Treatment settings for x-axis
treatment = [0.5,1,1.5,2,2.5,3,3.5,4,5,6,7,8,9,14,20,25];
T_sub ={'','1','','2','','3','','4','5','6','7','8','9','14','20','mono-therapy'};

my_grey = mymap(15,:);

% Drug Range 

clear('p_sub')
set(0, 'DefaultLineLineWidth', 2);
fig1 = figure(1); 
fig1.Units = 'centimeters';
fig1.Position=[5,5,15.5,6];
% set(gcf,'renderer','Painters')
h12 = subplot('Position',Position(2,:));
p_sub(1) = plot(treatment(1:15),EffectiveC_Cip.Basic(1:15)/0.017,'-x','Color',my_grey,'MarkerSize',m_Size); 
hold on
plot(25,EffectiveC_Cip.Basic(16)/0.017,'x','Color',my_grey,'MarkerSize',m_Size); 
hold on
p_sub(2) = plot(treatment(1:15),C_star_Cip.Basic_W(1:15)/0.017,'-x','Color',mymap(9,:),'MarkerSize',m_Size); 
hold on
plot(25,C_star_Cip.Basic_W(16)/0.017,'x','Color',mymap(9,:),'MarkerSize',m_Size); 
hold on
p_sub(3) = plot(treatment(1:15),C_star_Cip.Basic_MAB(1:15)/0.017,'-x','Color',mymap(5,:),'MarkerSize',m_Size); 
hold on
plot(25,C_star_Cip.Basic_MAB(16)/0.017,'x','Color',mymap(5,:),'MarkerSize',m_Size); 
hold on
plot([0 26],[28 28],'--','Color',[0.2 0.2 0.2 0.2],'LineWidth',1); 
hold on 
set(gca,'Fontsize',font2); 
xlim([0,26]);
ax = gca;
ax.XTick = treatment;
ax.XTickLabel=T_sub;
ax.YTick = [1,10,100];
ax.YTickLabels ={'1','10','$10^2$'}; 
set(gca,'YScale','log')
ylim([1,7*10^2]);
title('Patient, $\kappa_A = \kappa_B = 1$'); 

h11 = subplot('Position',Position(1,:)); 
p_sub(1) = plot(treatment(1:15),EffectiveC_Lab_Cip.Basic(1:15)/0.017,'-x','Color',my_grey,'MarkerSize',m_Size); 
hold on
plot(25,EffectiveC_Lab_Cip.Basic(16)/0.017,'x','Color',my_grey,'MarkerSize',m_Size); 
hold on
p_sub(2) = plot(treatment(1:15),C_star_Cip_Lab.Basic_W(1:15)/0.017,'-x','Color',mymap(9,:),'MarkerSize',m_Size); 
hold on
plot(25,C_star_Cip_Lab.Basic_W(16)/0.017,'x','Color',mymap(9,:),'MarkerSize',m_Size); 
hold on
p_sub(3) = plot(treatment(1:15),C_star_Cip_Lab.Basic_MAB(1:15)/0.017,'-x','Color',mymap(5,:),'MarkerSize',m_Size); 
hold on
plot(25,C_star_Cip_Lab.Basic_MAB(16)/0.017,'x','Color',mymap(5,:),'MarkerSize',m_Size); 
hold on
p_sub(4) = plot([0 26],[28 28],'--','Color',[0.2 0.2 0.2 0.2],'LineWidth',1); 
hold on 
set(gca,'Fontsize',font2); 
xlim([0,26]);
ax = gca;
ax.XTick = treatment;
ax.XTickLabel=T_sub;
ax.YTick = [1,10,100];
ax.YTickLabels ={'1','10','$10^2$'}; 
set(gca,'YScale','log')
ylim([1,7*10^2]);
title('Lab, $\kappa_A = \kappa_B = 1$'); 
leg = legend(p_sub,'Full Model','$u_A=u_B=0$','$W=M_A=M_B=0,M_{AB}=N_0$','FontSize',font2,'Location','northwest');
leg.ItemTokenSize = [15,18];
ylabel({'First effective dose $D$'; 'in multiples of zMIC$_W$'},'visible','on','Fontsize',font);


annotation('textbox',[0.05,1,0,0],'String','A','FitBoxToText','on','EdgeColor','none','Fontsize',font3);
annotation('textbox',[0.50,1,0,0],'String','B','FitBoxToText','on','EdgeColor','none','Fontsize',font3);
annotation('textbox',[0.1,0.55,0,0],'String','$\beta_{\mathrm{res}}$ zMIC$_W$','FitBoxToText','on','EdgeColor','none','Fontsize',font2,'Color',[0.6,0.6,0.6]);
annotation('textbox',[0.55,0.55,0,0],'String','$\beta_{\mathrm{res}}$ zMIC$_W$','FitBoxToText','on','EdgeColor','none','Fontsize',font2,'Color',[0.6,0.6,0.6]);

% Commen y label 
p11=get(h11,'position');
p12=get(h12,'position');

% Commen X label 
width = p12(1)+p12(3)-p11(1); 
h6 = axes('position',[p11(1) p12(2) width p11(4)],'visible','off');
h_label=xlabel('Time between the switches in days','visible','on','Fontsize',font);

annotation('textbox', [0.43, 0.2, 0.01, 0.01], 'String', "[...]",'EdgeColor',...
    'white','BackgroundColor','white','FitBoxToText','on','Margin',0.001,...
    'VerticalAlignment','middle','HorizontalAlignment','center','FontSize',6)

annotation('textbox', [0.88, 0.2, 0.01, 0.01], 'String', "[...]",'EdgeColor',...
    'white','BackgroundColor','white','FitBoxToText','on','Margin',0.001,...
    'VerticalAlignment','middle','HorizontalAlignment','center','FontSize',6)

set(findall(fig1,'-property','Interpreter'),'Interpreter','latex');
set(findall(fig1,'-property','TickLabelInterpreter'),'TickLabelInterpreter','latex');


