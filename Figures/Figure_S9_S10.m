% SI Figures for Collateral Sensitivity in the Lab (S7): 

% Initialize 
clc 
clear 
close all 
%% Plot Settings 

% Label for Legend 
treatments = {'12h','24h','1.5d','2d','2.5d','3d','3.5d','4d','5d','6d','7d','8d','9d'};

% Set the positions of the different subplots 
% length and height of sub-plots: 
sp_length = 0.35; 
sp_height = 0.16;
% position of subplots in x-direction 
x_pos1 = 0.08; 
x_pos2 = 0.5; 

% Position of the Subplots 
Position = [x_pos1,0.795,sp_length,sp_height;...
            x_pos2,0.795,sp_length,sp_height;...
            x_pos1,0.55,sp_length,sp_height;...
            x_pos2,0.55,sp_length,sp_height;...
            x_pos1,0.315,sp_length,sp_height;...
            x_pos2,0.315,sp_length,sp_height;...
            x_pos1,0.075,sp_length,sp_height;...
            x_pos2,0.075,sp_length,sp_height];

%Colors
dark = [2 93 114]/255;
pink = [178 93 141]/255;
light = [121 182 185]/255; 
occa = [215,200,100]/255;
black = [0 0 0];
% Define a color vector 
col = [dark;pink;light;occa];
% Define a colormap
col_n=4;
mymap1 = [linspace(black(1),pink(1),6)', linspace(black(2),pink(2),6)',linspace(black(3),pink(3),6)'];
mymap2 = [linspace(pink(1),occa(1),col_n)', linspace(pink(2),occa(2),col_n)',linspace(pink(3),occa(3),col_n)'];
mymap3 = [linspace(occa(1),dark(1),col_n)', linspace(occa(2),dark(2),col_n)',linspace(occa(3),dark(3),col_n)'];
mymap4 = [linspace(dark(1),light(1),col_n)', linspace(dark(2),light(2),col_n)',linspace(dark(3),light(3),col_n)'];
mymap5 = [linspace(light(1),light(1),col_n)', linspace(light(2),light(2),col_n)',linspace(light(3),light(3),col_n)'];
mymap = [mymap1;mymap2;mymap3;mymap4;mymap5];

% Linewidth
set(0, 'DefaultLineLineWidth', 1.5);
% Fontsizes
font = 10; 
font2 = 8;
font3 = 12;

% Selection of Colors
ind_col = [3,6,9,12,16];


%% Load Data 

%---Deterministic---------------------------------------------------------------
% Define Data Path
dataPath = 'Data/Treatments12h_Cip';
L_C = load(strcat(dataPath,'/Data_L_C0_06.mat')); 
L_C  = L_C.P_data;
% CS
L_C_wCS = load(strcat(dataPath,'/Data_L_wCS_C0_06.mat'));
L_C_sCS = load(strcat(dataPath,'/Data_L_sCS_C0_06.mat')); 
L_C_wCS = L_C_wCS.P_data;
L_C_sCS = L_C_sCS.P_data;

dataPath = 'Data/Treatments12h_Fab2';
L_F = load(strcat(dataPath,'/Data_L_C0_06.mat')); 
L_F  = L_F.P_data;
% CS
L_F_wCS = load(strcat(dataPath,'/Data_L_wCS_C0_06.mat'));
L_F_sCS = load(strcat(dataPath,'/Data_L_sCS_C0_06.mat')); 
L_F_wCS = L_F_wCS.P_data;
L_F_sCS = L_F_sCS.P_data;

n = 200;
D1 = [linspace(0,0.2,n);linspace(0,0.2,n)];
D2 = [linspace(0.2,0.6,n);linspace(0.2,0.6,n)]; 
D = [D1(:,1:end-1),D2];
% Scale Drug Concnetration in fold change of MIC
D = D./0.017; 
%% Plot data 

% Position of the Subplots 
Position = [0.1,0.2,0.35,0.65;...
            0.46,0.2,0.35,0.65];

fig1 = figure(2);
fig1.Units = 'centimeters'; 
fig1.Position = [5,2,15.5,5];

% New color vector: 
grey_mat = flipud(gray(14)); 

h1 = subplot('Position',Position(1,:)); 
for j = 13:-1:6
    % Plot t_erad 
    plots(j) = plot(D(1,:),L_C_sCS(j).t_erad,'--','Color', grey_mat(j-3,:),'Linewidth',1);
    hold on 
end
for j = 5:-1:1
    % Plot t_erad 
    plots(j) = plot(D(1,:),L_C_sCS(j).t_erad,'Color', mymap(ind_col(j),:),'Linewidth',1.5);
    hold on 
end
% Set fontsize for axis
set(gca,'FontSize',font2);
% set axis propeties 
xlim([0,23.53])
ylim([0,300])
yticks([0,50,150,250]);
title('$\kappa_A = \kappa_B = 1$','FontSize',font,'Interpreter','latex');
ylabel({'Time until threshold'; '$N_C$ in hours'});

h2 = subplot('Position',Position(2,:)); 
for j = 13:-1:6
    % Plot t_erad 
    plots(j) = plot(D(1,:),L_F_sCS(j).t_erad,'--','Color', grey_mat(j-3,:),'Linewidth',1);
    hold on 
end
for j = 5:-1:1
    % Plot t_erad 
    plots(j) = plot(D(1,:),L_F_sCS(j).t_erad,'Color', mymap(ind_col(j),:),'Linewidth',1.5);
    hold on 
end
% Set fontsize for axis
set(gca,'FontSize',font2);
% set axis propeties 
xlim([0,23.53])
ylim([0,300])
yticks([0,50,150,250,350]);
yticks([0,50,150,250]);
l1=legend(plots(13:-1:1),treatments(13:-1:1),'FontSize',font2,'Location','best','NumColumns',2);
l1.Position = [0.855,0.5,0.1,0.1];
l1.ItemTokenSize = [15 18];
title(l1,'Switch after');
title('$\kappa_A = \kappa_B = 2$','FontSize',font,'Interpreter','latex');
yticklabels([]);
% Commen X label 
p3=get(h1,'position');
p4=get(h2,'position');
width = p4(1)+p4(3)-p3(1); 
axes('position',[p3(1) p4(2) width p3(4)],'visible','off');
xlabel('Drug dose $D$ in multiples of zMIC$_W$','FontSize',font','visible','on','Fontsize',font);
   

% Annotate 'A' and 'B' 
annotation('textbox',[0.1,0.99,0,0],'String','A','FitBoxToText','on','EdgeColor','none','Fontsize',font3);
annotation('textbox',[0.46,0.99,0,0],'String','B','FitBoxToText','on','EdgeColor','none','Fontsize',font3);

set(findall(fig1,'-property','Interpreter'),'Interpreter','latex');
set(findall(fig1,'-property','TickLabelInterpreter'),'TickLabelInterpreter','latex');

%% Direct Compariosn CS and no CS

% Set the positions of the different subplots 
% length and height of sub-plots: 
sp_length = 0.25; 
sp_height = 0.6;
% position of subplots in x-direction 
x_pos1 = 0.1; 
x_pos2 = 0.55; 

% Position of the Subplots 
Pos = [0.1,0.25,sp_length,sp_height;...
           0.4,0.25,sp_length,sp_height;...
            0.7,0.25,sp_length,sp_height];
        
set_ind = [1,8,12];

fig4 = figure(4); 
fig4.Units = 'centimeters';
fig4.Position=[10,10,15.5,4.45];

for i=1:3
    subplot('Position', Pos(i,:))
    % Plot Basic, wCS and sCS for the four different Treatments 
    plots(2) = plot(D(1,:),L_C_sCS(set_ind(i)).t_erad,'-.','Color', mymap(12,:),'Linewidth',2);
    hold on    
    plots(1) = plot(D(1,:),L_C(set_ind(i)).t_erad,'Color', mymap(15,:),'Linewidth',2);
    hold on
    set(gca,'FontSize',font2)
    str = strcat('Switch after',{' '},treatments(set_ind(i)));
    title(str,'FontSize',font)
    xlim([0 36]); 
    ylim([0,300]);
    ax1 = gca;
    if i == 1
        set(gca,'YTick',[50,150,250])
        set(gca,'YTickLabels',[50,150,250])
        ylabel({'Time until threshold';'$N_c$ in hours'},'FontSize',font)

    else 
        set(gca,'YTickLabels',[])
        
    end
    
    if i == 2 
      xlabel('Drug concentration in multiples of the zMIC','FontSize',font)
    end
    xlim([0 19])
end

l1_L=legend(plots,'Without CS','With CS','Position',[0.7,0.25,0.224649676912234,0.358333328792027],'FontSize',font2);
title(l1_L,'Collateral Sensitivity (CS)')

dimA = [0.09,0.98,0,0];
annotation('textbox',dimA,'String','A','FitBoxToText','on','EdgeColor','none','Fontsize',font3);
dimC = [0.39,0.98,0,0];
annotation('textbox',dimC,'String','B','FitBoxToText','on','EdgeColor','none','Fontsize',font3);
dimE = [0.69,0.98,0,0];
annotation('textbox',dimE,'String','C','FitBoxToText','on','EdgeColor','none','Fontsize',font3);

set(findall(fig4,'-property','Interpreter'),'Interpreter','latex');
set(findall(fig4,'-property','TickLabelInterpreter'),'TickLabelInterpreter','latex');

