%Skript to Generate Figure 4

% Inintialize Matlab
clc
clear 
close all

%% Plot Settings 

%Colors
dark = [2 93 114]/255;
pink = [178 93 141]/255;
light = [121 182 185]/255; 
occa = [215,200,100]/255;
black = [0 0 0];
% Define a color vector 
col = [dark;pink;light;occa];
% Define a colormap
col_n=4;
mymap1 = [linspace(black(1),pink(1),6)', linspace(black(2),pink(2),6)',linspace(black(3),pink(3),6)'];
mymap2 = [linspace(pink(1),occa(1),col_n)', linspace(pink(2),occa(2),col_n)',linspace(pink(3),occa(3),col_n)'];
mymap3 = [linspace(occa(1),dark(1),col_n)', linspace(occa(2),dark(2),col_n)',linspace(occa(3),dark(3),col_n)'];
mymap4 = [linspace(dark(1),light(1),col_n)', linspace(dark(2),light(2),col_n)',linspace(dark(3),light(3),col_n)'];
mymap5 = [linspace(light(1),light(1),col_n)', linspace(light(2),light(2),col_n)',linspace(light(3),light(3),col_n)'];
mymap = [mymap1;mymap2;mymap3;mymap4;mymap5];

% Linewidth
set(0, 'DefaultLineLineWidth', 1.5);
% Fontsizes
font = 10; 
font2 = 8;
font3 = 12;

% Treatment Name 
T ={'12 hours','24h','1.5D','2 days','2.5D','3D','3.5D','4 days','5D','6D','7D','8 days','9D','14D','20D','never'};


%% Load Data 

%---Deterministic---------------------------------------------------------------
% Define Data Path
dataPath = 'Data/Treatments12h_Cip';
P_Cip = load(strcat(dataPath,'/Data_P_C0_06.mat')); 
P_Cip = P_Cip.P_data;
L_Cip = load(strcat(dataPath,'/Data_L_C0_06.mat')); 
L_Cip  = L_Cip .P_data;

% CS
P_Cip_wCS = load(strcat(dataPath,'/Data_P_wCS_C0_06.mat')); 
L_Cip_wCS = load(strcat(dataPath,'/Data_L_wCS_C0_06.mat'));
P_Cip_sCS = load(strcat(dataPath,'/Data_P_sCS_C0_06.mat')); 
L_Cip_sCS = load(strcat(dataPath,'/Data_L_sCS_C0_06.mat')); 
P_Cip_wCS = P_Cip_wCS.P_data;
L_Cip_wCS = L_Cip_wCS.P_data;
P_Cip_sCS = P_Cip_sCS.P_data;
L_Cip_sCS = L_Cip_sCS.P_data;

% DDI
P_Cip_DDIa = load(strcat(dataPath,'/Data_P_DDIa_C0_06.mat')); 
P_Cip_DDIs = load(strcat(dataPath,'/Data_P_DDIs_C0_06.mat')); 
P_Cip_DDIa = P_Cip_DDIa.P_data;
P_Cip_DDIs = P_Cip_DDIs.P_data;

% DDI and sCS
P_Cip_sCS_DDIa = load(strcat(dataPath,'/Data_P_sCS_DDIa_C0_06.mat')); 
P_Cip_sCS_DDIs = load(strcat(dataPath,'/Data_P_sCS_DDIs_C0_06.mat'));  
P_Cip_sCS_DDIa = P_Cip_sCS_DDIa.P_data;
P_Cip_sCS_DDIs = P_Cip_sCS_DDIs.P_data;

% Drug Concentration
n = 200;
D1 = [linspace(0,0.2,n);linspace(0,0.2,n)];
D2 = [linspace(0.2,0.6,n);linspace(0.2,0.6,n)]; 
D = [D1(:,1:end-1),D2];
% Scale Drug Concnetration in fold change of MIC
D = D./0.017; 

dataPath = 'Data/Hybrid_Simulations/Treatments12h_Cip';
Sim_Cip_Basic = load(strcat(dataPath,'/Sim_T1_2.mat')); 
Sim_Cip_Basic_T1 = Sim_Cip_Basic.Sim;
Sim_Cip_Basic = load(strcat(dataPath,'/Sim_T8.mat'));  

Sim_Cip_Basic_T1_HC = load(strcat(dataPath,'/Sim_T1_2_HC.mat'));
Sim_Cip_Basic_T1_HC = Sim_Cip_Basic_T1_HC.Sim;
Sim_Cip_Basic_1 = [Sim_Cip_Basic_T1,Sim_Cip_Basic_T1_HC];
Sim_Cip_Basic_T8 = Sim_Cip_Basic.Sim;
Sim_Cip_Basic_T8{2,1}(Sim_Cip_Basic_T8{2,1} < 0 ) = nan;
Sim_Cip_Basic = [Sim_Cip_Basic_1(1,:);Sim_Cip_Basic_T8(2,:)];

dataPath = 'Data/Hybrid_Simulations/Treatments12h_Cip_CS_DDI';
Sim_Cip_CS1 = load(strcat(dataPath,'/Sim_T1_CS.mat')); 
Sim_Cip_CS1 = Sim_Cip_CS1.Sim;
Sim_Cip_CS8 = load(strcat(dataPath,'/Sim_T8_CS.mat')); 
Sim_Cip_CS8 = Sim_Cip_CS8.Sim;
Sim_Cip_CS = [Sim_Cip_CS1;Sim_Cip_CS8(2,:)];

Sim_Cip_DDIa = load(strcat(dataPath,'/Sim_T1_8_DDIa.mat')); 
Sim_Cip_DDIa = Sim_Cip_DDIa.Sim;

Sim_Cip_DDIs = load(strcat(dataPath,'/Sim_T1_8_DDIs.mat')); 
Sim_Cip_DDIs = Sim_Cip_DDIs.Sim;

Sim_Cip_CS_DDIa = load(strcat(dataPath,'/Sim_T1_8_CS_DDIa.mat')); 
Sim_Cip_CS_DDIa = Sim_Cip_CS_DDIa.Sim;

Sim_Cip_CS_DDIs = load(strcat(dataPath,'/Sim_T1_8_CS_DDIs.mat')); 
Sim_Cip_CS_DDIs = Sim_Cip_CS_DDIs.Sim;


D_Basic = 0.08:0.01:0.6; 
D_Char = 0.136:0.01:0.6; 
D_Char2 = 0.136:0.01:0.51; 

for i = 1:2
    for j = 1:53

        Sim_t_erad_C_Basic(1,j) = mean(Sim_Cip_Basic{i,j},'omitnan'); 
        Sim_t_erad_std_C_Basic(1,j) = std(Sim_Cip_Basic{i,j},'omitnan'); 
        
        
        if j < 48
        Sim_t_erad_C_CS(1,j) = mean(Sim_Cip_CS{i,j},'omitnan'); 
        Sim_t_erad_std_C_CS(1,j) = std(Sim_Cip_CS{i,j},'omitnan');
        
        Sim_t_erad_C_DDIa(1,j) = mean(Sim_Cip_DDIa{i,j},'omitnan'); 
        Sim_t_erad_std_C_DDIa(1,j) = std(Sim_Cip_DDIa{i,j},'omitnan');
        
        Sim_t_erad_C_DDIs(1,j) = mean(Sim_Cip_DDIs{i,j},'omitnan'); 
        Sim_t_erad_std_C_DDIs(1,j) = std(Sim_Cip_DDIs{i,j},'omitnan');

        Sim_t_erad_C_CS_DDIa(1,j) = mean(Sim_Cip_CS_DDIa{i,j},'omitnan'); 
        Sim_t_erad_std_C_CS_DDIa(1,j) = std(Sim_Cip_CS_DDIa{i,j},'omitnan');
        
        Sim_t_erad_C_CS_DDIs(1,j) = mean(Sim_Cip_CS_DDIs{i,j},'omitnan'); 
        Sim_t_erad_std_C_CS_DDIs(1,j) = std(Sim_Cip_CS_DDIs{i,j},'omitnan');
        end
    end
    
    Sim_mean_C_CS{i} = Sim_t_erad_C_CS; 
    Sim_std_C_CS{i} = Sim_t_erad_std_C_CS;
    Sim_sem_C_CS{i} = Sim_t_erad_std_C_CS/sqrt(100);
    
    
    Sim_mean_C_Basic{i} = Sim_t_erad_C_Basic; 
    Sim_std_C_Basic{i} = Sim_t_erad_std_C_Basic;
    Sim_sem_C_Basic{i} = Sim_t_erad_std_C_Basic/sqrt(100);
    
    Sim_mean_C_DDIa{i} = Sim_t_erad_C_DDIa; 
    Sim_std_C_DDIa{i} = Sim_t_erad_std_C_DDIa;
    Sim_sem_C_DDIa{i} = Sim_t_erad_std_C_DDIa/sqrt(100);
    
    Sim_mean_C_DDIs{i} = Sim_t_erad_C_DDIs; 
    Sim_std_C_DDIs{i} = Sim_t_erad_std_C_DDIs;
    Sim_sem_C_DDIs{i} = Sim_t_erad_std_C_DDIs/sqrt(100);

    Sim_mean_C_CS_DDIa{i} = Sim_t_erad_C_CS_DDIa; 
    Sim_std_C_CS_DDIa{i} = Sim_t_erad_std_C_CS_DDIa;
    Sim_sem_C_CS_DDIa{i} = Sim_t_erad_std_C_CS_DDIa/sqrt(100);
    
    Sim_mean_C_CS_DDIs{i} = Sim_t_erad_C_CS_DDIs; 
    Sim_std_C_CS_DDIs{i} = Sim_t_erad_std_C_CS_DDIs;
    Sim_sem_C_CS_DDIs{i} = Sim_t_erad_std_C_CS_DDIs/sqrt(100);
end

%% Plot deterministic & stochastic 
x_length = 0.22;
y_height = 0.13;
set_ind = [1,8,12];
n_err = 3;

% index for simulations which need to be excluded since they do not always
% lead to extinction 
ind_B = [6,8]; 
ind_C = [1,2]; 

% Pos = [0.31 0.82 x_length y_height;...
%       0.54 0.82 x_length y_height; ...
%       0.77 0.82 x_length y_height;...
%       0.31 0.676 x_length y_height;...
%       0.54 0.676 x_length y_height; ...
%       0.31 0.532 x_length y_height;...
%       0.54 0.532 x_length y_height;...
%       0.77 0.532 x_length y_height;...
%       0.31 0.388 x_length y_height;...
%       0.54 0.388 x_length y_height;...
%       0.31 0.244 x_length y_height;...
%       0.54 0.244 x_length y_height;...
%       0.77 0.244 x_length y_height;...
%       0.31 0.1 x_length y_height;...
%       0.54 0.1 x_length y_height];
  
Pos = [ 0.31 0.82 x_length y_height;...
      0.54 0.82 x_length y_height; ...
      0.77 0.82 x_length y_height;...
      0.31 0.676 x_length y_height;...
      0.54 0.676 x_length y_height; ...
      0.31 0.532 x_length y_height;...
      0.54 0.532 x_length y_height;...     
      0.77 0.532 x_length y_height;... 
      0.31 0.388 x_length y_height;...
      0.54 0.388 x_length y_height;...
      0.31 0.244 x_length y_height;...
      0.54 0.244 x_length y_height;...  
      0.77 0.244 x_length y_height;...
      0.31 0.1 x_length y_height;...
      0.54 0.1 x_length y_height;....
      0.77 0.1 x_length y_height];

fig1 = figure(1); 
fig1.Units = 'centimeters';
fig1.Position=[10,2,15.5,18];
for i=1:3
    p_sub(i) = subplot('Position', Pos(i,:));
    % Plot Basic, wCS and sCS for the four different Treatments 
    p_CS(2)=plot(D(1,:),P_Cip_wCS(set_ind(i)).t_erad,'--','Color', mymap(13,:),'Linewidth',2);
    hold on 
    p_CS(3)=plot(D(1,:),P_Cip_sCS(set_ind(i)).t_erad,'-.','Color', mymap(12,:),'Linewidth',2);
    hold on    
    p_CS(1)=plot(D(1,:),P_Cip(set_ind(i)).t_erad,'Color', mymap(15,:),'Linewidth',2);
    hold on
    set(gca,'FontSize',font2)
    str = strcat('Switch after',{' '},T(set_ind(i)));
    title(str,'FontSize',font)
    xlim([0 36]); 
    ylim([0,400]);
    ax1 = gca;
    if i == 1
        set(gca,'YTick',[50,150,250,350])
        set(gca,'YTickLabels',[50,150,250])
    else 
        set(gca,'YTickLabels',[])
        set(gca,'YTick',[50,150,250,350])
    end
    if i <3
        set(gca,'XTickLabels',[]);
    end
end
l1=legend(p_CS,{'Without CS','Weak CS','Strong CS'},'Position',[0.007023545019804,0.861470589357264,0.224649676912234,0.088529410642736],'FontSize',font2);
title(l1,'Collateral Sensitivity (CS)')
l1.ItemTokenSize = [15,18];

for i=1:2
    p_sub2(i) = subplot('Position', Pos(3+i,:));
   % Plot Basic, wCS and sCS for the four different Treatments   
    plot1(1)=plot(D_Char(ind_C(i):end)/0.017,Sim_mean_C_CS{i}(ind_C(i):end),'Color',mymap(12,:),'linewidth',1);
    hold on
    errorbar(D_Char(ind_C(i):n_err:end)/0.017,Sim_mean_C_CS{i}(ind_C(i):n_err:end),Sim_sem_C_CS{i}(ind_C(i):n_err:end),'.','Color',mymap(12,:),'linewidth',1,'CapSize',4);
    hold on 
    plot1(2)=plot(D_Basic(ind_B(i):end)/0.017,Sim_mean_C_Basic{i}(ind_B(i):end),'Color',mymap(15,:),'linewidth',1);
    hold on
    errorbar(D_Basic(ind_B(i):n_err:end)/0.017,Sim_mean_C_Basic{i}(ind_B(i):n_err:end),Sim_sem_C_Basic{i}(ind_B(i):n_err:end),'.','Color',mymap(15,:),'linewidth',1,'CapSize',4);
    hold on
 
    set(gca,'FontSize',font2)
    xlim([0 36]); 
    ylim([0,400]);
    ax1 = gca;
    if i == 1
        set(gca,'YTick',[50,150,250,350])
    else 
        set(gca,'YTick',[50,150,250,350])
        set(gca,'YTickLabels',[])
        
    end
    set(gca,'XTickLabel',[]);
    
end

for i=1:3
    p_sub(i+3) = subplot('Position', Pos(5+i,:));
    % Plot Basic, Anta and Syn for the four different Treatments 
    p_DDI(1)=plot(D(1,:),P_Cip_DDIa(set_ind(i)).t_erad,'Color', col(2,:),'Linewidth',2);
    hold on 
    p_DDI(3)=plot(D(1,:),P_Cip_DDIs(set_ind(i)).t_erad,'Color', col(3,:),'Linewidth',2);
    hold on    
    p_DDI(2)=plot(D(1,:),P_Cip(set_ind(i)).t_erad,'Color', col(1,:),'Linewidth',2);
    hold on 
    set(gca,'FontSize',font2)
    if i == 1
        set(gca,'YTick',[50,150,250])
        set(gca,'YTick',[50,150,250,350])
    else 
        set(gca,'YTick',[50,150,250,350])
        set(gca,'YTickLabels',[])
    end
    xlim([0 36]); 
    ylim([0,400]);
    if i <3
        set(gca,'XTickLabels',[]);
    end
    
end
l2 = legend(p_DDI,{'Antagonism','Bliss independence','Synergism'},'Position',[0.007023545019804,0.573470589357264,0.203021373766552,0.088529410642736],'FontSize',font2);
title(l2,'Drug-Drug Interactions','Fontsize',font2)
l2.ItemTokenSize = [15,18];

for i=1:2
    p_sub2(2+i) = subplot('Position', Pos(i+8,:));
    % Plot Basic, Anta and Syn for the four different Treatments 
    plot2(1)=plot(D_Char(ind_C(i):end)/0.017,Sim_mean_C_DDIa{i}(ind_C(i):end),'Color',col(2,:),'linewidth',1);
    hold on
    errorbar(D_Char(ind_C(i):n_err:end)/0.017,Sim_mean_C_DDIa{i}(ind_C(i):n_err:end),Sim_sem_C_DDIa{i}(ind_C(i):n_err:end),'.','Color',col(2,:),'linewidth',1,'CapSize',4);
    hold on 
    plot2(2)=plot(D_Char(ind_C(i):end)/0.017,Sim_mean_C_DDIs{i}(ind_C(i):end),'Color',col(3,:),'linewidth',1);
    hold on
    errorbar(D_Char(ind_C(i):n_err:end)/0.017,Sim_mean_C_DDIs{i}(ind_C(i):n_err:end),Sim_sem_C_DDIs{i}(ind_C(i):n_err:end),'.','Color',col(3,:),'linewidth',1,'CapSize',4);
    hold on   
    plot2(3)=plot(D_Basic(ind_B(i):end)/0.017,Sim_mean_C_Basic{i}(ind_B(i):end),'Color',mymap(15,:),'linewidth',1);
    hold on
    errorbar(D_Basic(ind_B(i):n_err:end)/0.017,Sim_mean_C_Basic{i}(ind_B(i):n_err:end),Sim_sem_C_Basic{i}(ind_B(i):n_err:end),'.','Color',mymap(15,:),'linewidth',1,'CapSize',4);
    hold on
    set(gca,'FontSize',font2)
    if i == 1
        set(gca,'YTick',[50,150,250,350])
    else 
        set(gca,'YTick',[50,150,250,350])
        set(gca,'YTickLabels',[])
    end
    xlim([0 36]); 
    ylim([0,400]);
    set(gca,'XTickLabel', []);
    
    if i == 2
      %Inlcude Zoomin 
        axes('position',[0.65 0.45 0.1 0.04])
        box on
        plot2(1)=plot(D_Char(ind_C(i):end)/0.017,Sim_mean_C_DDIa{i}(ind_C(i):end),'Color',col(2,:),'linewidth',1);
        hold on      
        plot2(2)=plot(D_Char(ind_C(i):end)/0.017,Sim_mean_C_DDIs{i}(ind_C(i):end),'Color',col(3,:),'linewidth',1);
        hold on 
        plot2(3)=plot(D_Basic(ind_B(i):end)/0.017,Sim_mean_C_Basic{i}(ind_B(i):end),'Color',mymap(15,:),'linewidth',1);
        hold on  
            %axis properties 
                ylim([85,160])
                xlim([8 15])
                yticks([100,140]);
                xticks([9,11,13]);
     end  
    
end

for i=1:3
    p_sub(i+6) = subplot('Position', Pos(10+i,:));
    % Plot Anta, Syn, Anta+sCS and Syn+sCS for the four different Treatments  
    plot(D(1,:),P_Cip_DDIa(set_ind(i)).t_erad,'Color', col(2,:),'Linewidth',2);
    hold on 
    plot(D(1,:),P_Cip_DDIs(set_ind(i)).t_erad,'Color', col(3,:),'Linewidth',2);
    hold on 
    plot(D(1,:),P_Cip_sCS_DDIa(set_ind(i)).t_erad,'-.','Color', mymap(5,:),'Linewidth',2);
    hold on 
    plot(D(1,:),P_Cip_sCS_DDIs(set_ind(i)).t_erad,'-.','Color', mymap(17,:),'Linewidth',2);
    hold on 
    set(gca,'FontSize',font2)
    xlim([0 36]); 
    ylim([0,400]);
    ax3 = gca;
    if i == 1
        set(gca,'YTick',[50,150,250,350])
    else 
        set(gca,'YTick',[50,150,250,350])
        set(gca,'YTickLabels',[])
    end
    if i <3
        set(gca,'XTickLabels',[]);
    end
end
l3 = legend({'Antagonism','Synergism','Antagonism \& strong CS','Synergism \& strong CS'},'Position',[0.007023545019804,0.249545725188225,0.253381781738685,0.124454274811775],'FontSize',font2);
title(l3, {'Drug-Drug Interactions'; '\& Collateral Sensitivity (CS)'},'Interpreter','latex')
l3.ItemTokenSize = [15,18];
for i=1:2
    p_sub2(4+i) = subplot('Position', Pos(13+i,:));
    if i < 3    
        % Plot Anta, Syn, Anta+sCS and Syn+sCS for the four different Treatments  
        plot3(1)=plot(D_Char(ind_C(i):end)/0.017,Sim_mean_C_DDIa{i}(ind_C(i):end),'Color',col(2,:),'linewidth',1);
        hold on
        errorbar(D_Char(ind_C(i):n_err:end)/0.017,Sim_mean_C_DDIa{i}(ind_C(i):n_err:end),Sim_sem_C_DDIa{i}(ind_C(i):n_err:end),'.','Color',col(2,:),'linewidth',1,'CapSize',4);
        hold on 
        plot3(2)=plot(D_Char(ind_C(i):end)/0.017,Sim_mean_C_DDIs{i}(ind_C(i):end),'Color',col(3,:),'linewidth',1);
        hold on
        errorbar(D_Char(ind_C(i):n_err:end)/0.017,Sim_mean_C_DDIs{i}(ind_C(i):n_err:end),Sim_sem_C_DDIs{i}(ind_C(i):n_err:end),'.','Color',col(3,:),'linewidth',1,'CapSize',4);
        hold on    
        plot3(3)=plot(D_Char(ind_C(i):end)/0.017,Sim_mean_C_CS_DDIa{i}(ind_C(i):end),'-.','Color',mymap(5,:),'linewidth',1);
        hold on
        errorbar(D_Char(ind_C(i):n_err:end)/0.017,Sim_mean_C_CS_DDIa{i}(ind_C(i):n_err:end),Sim_sem_C_CS_DDIa{i}(ind_C(i):n_err:end),'.','Color',mymap(5,:),'linewidth',1,'CapSize',4);
        hold on 
        plot3(4)=plot(D_Char(ind_C(i):end)/0.017,Sim_mean_C_CS_DDIs{i}(ind_C(i):end),'-.','Color',mymap(17,:),'linewidth',1);
        hold on
        errorbar(D_Char(ind_C(i):n_err:end)/0.017,Sim_mean_C_CS_DDIs{i}(ind_C(i):n_err:end),Sim_sem_C_CS_DDIs{i}(ind_C(i):n_err:end),'.','Color',mymap(17,:),'linewidth',1,'CapSize',4);
        hold on    
        else 
        set(gca,'YColor','none')
    end
    set(gca,'FontSize',font2)
    if i == 1
       set(gca,'YTick',[50,150,250,350])
    else 
        set(gca,'YTick',[50,150,250,350])
        set(gca,'YTickLabels',[])
    end
    xlim([0 36]); 
    ylim([0,400]);
     if i == 2
        xlabel('Drug dose $D$ in multiples of zMIC$_W$','FontSize',font, 'Interpreter','latex')
     end
    
     if i == 2
      %Inlcude Zoomin 
        axes('position',[0.65 0.162 0.1 0.04])
        box on
        plot3(1)=plot(D_Char(ind_C(i):end)/0.017,Sim_mean_C_DDIa{i}(ind_C(i):end),'Color',col(2,:),'linewidth',1);
        hold on
        plot3(2)=plot(D_Char(ind_C(i):end)/0.017,Sim_mean_C_DDIs{i}(ind_C(i):end),'Color',col(3,:),'linewidth',1);
        hold on  
        plot3(3)=plot(D_Char(ind_C(i):end)/0.017,Sim_mean_C_CS_DDIa{i}(ind_C(i):end),'-.','Color',mymap(5,:),'linewidth',1);
        hold on
        plot3(4)=plot(D_Char(ind_C(i):end)/0.017,Sim_mean_C_CS_DDIs{i}(ind_C(i):end),'-.','Color',mymap(17,:),'linewidth',1);
        hold on 
            %axis properties 
                ylim([75,160])
                xlim([8 15])
                yticks([100,140]);
                xticks([9,11,13]);
     end  

end

% Commen Y label
p2=get(p_sub2(3),'position');
p1=get(p_sub(4),'position');
height=p1(2)+p1(4)-p2(2);
h15=axes('position',[p1(1)-0.02 p2(2) p2(3) height],'visible','off');
ylabel('Time until threshold $N_c$ in hours','visible','on','Fontsize',font,'Interpreter','latex');

annotation('textbox',[0.305,0.955,0,0],'String','A','FitBoxToText','on','EdgeColor','none','Fontsize',font3);
annotation('textbox',[0.305,0.811,0,0],'String','D','FitBoxToText','on','EdgeColor','none','Fontsize',font3);
annotation('textbox',[0.305,0.667,0,0],'String','F','FitBoxToText','on','EdgeColor','none','Fontsize',font3);
annotation('textbox',[0.305,0.523,0,0],'String','I','FitBoxToText','on','EdgeColor','none','Fontsize',font3);
annotation('textbox',[0.305,0.379,0,0],'String','K','FitBoxToText','on','EdgeColor','none','Fontsize',font3);
annotation('textbox',[0.305,0.235,0,0],'String','N','FitBoxToText','on','EdgeColor','none','Fontsize',font3);

annotation('textbox',[0.535,0.955,0,0],'String','B','FitBoxToText','on','EdgeColor','none','Fontsize',font3);
annotation('textbox',[0.535,0.811,0,0],'String','E','FitBoxToText','on','EdgeColor','none','Fontsize',font3);
annotation('textbox',[0.535,0.667,0,0],'String','G','FitBoxToText','on','EdgeColor','none','Fontsize',font3);
annotation('textbox',[0.535,0.523,0,0],'String','J','FitBoxToText','on','EdgeColor','none','Fontsize',font3);
annotation('textbox',[0.535,0.379,0,0],'String','L','FitBoxToText','on','EdgeColor','none','Fontsize',font3);
annotation('textbox',[0.535,0.235,0,0],'String','O','FitBoxToText','on','EdgeColor','none','Fontsize',font3);

annotation('textbox',[0.765,0.955,0,0],'String','C','FitBoxToText','on','EdgeColor','none','Fontsize',font3);
annotation('textbox',[0.765,0.667,0,0],'String','H','FitBoxToText','on','EdgeColor','none','Fontsize',font3);
annotation('textbox',[0.765,0.379,0,0],'String','M','FitBoxToText','on','EdgeColor','none','Fontsize',font3);

% Label deterministic and stochastic
annotation('textbox',[0.405,0.95,0,0],'String','Deterministic','FitBoxToText','on','EdgeColor','none','Fontsize',font2);
annotation('textbox',[0.3875,0.806,0,0],'String','Semi-Stochastic','FitBoxToText','on','EdgeColor','none','Fontsize',font2);
annotation('textbox',[0.405,0.662,0,0],'String','Deterministic','FitBoxToText','on','EdgeColor','none','Fontsize',font2);
annotation('textbox',[0.3875,0.518,0,0],'String','Semi-Stochastic','FitBoxToText','on','EdgeColor','none','Fontsize',font2);
annotation('textbox',[0.405,0.374,0,0],'String','Deterministic','FitBoxToText','on','EdgeColor','none','Fontsize',font2);
annotation('textbox',[0.3875,0.23,0,0],'String','Semi-Stochastic','FitBoxToText','on','EdgeColor','none','Fontsize',font2);

annotation('textbox',[0.635,0.95,0,0],'String','Deterministic','FitBoxToText','on','EdgeColor','none','Fontsize',font2);
annotation('textbox',[0.6175,0.806,0,0],'String','Semi-Stochastic','FitBoxToText','on','EdgeColor','none','Fontsize',font2);
annotation('textbox',[0.635,0.662,0,0],'String','Deterministic','FitBoxToText','on','EdgeColor','none','Fontsize',font2);
annotation('textbox',[0.6175,0.518,0,0],'String','Semi-Stochastic','FitBoxToText','on','EdgeColor','none','Fontsize',font2);
annotation('textbox',[0.635,0.374,0,0],'String','Deterministic','FitBoxToText','on','EdgeColor','none','Fontsize',font2);
annotation('textbox',[0.6175,0.23,0,0],'String','Semi-Stochastic','FitBoxToText','on','EdgeColor','none','Fontsize',font2);

annotation('textbox',[0.865,0.95,0,0],'String','Deterministic','FitBoxToText','on','EdgeColor','none','Fontsize',font2);
annotation('textbox',[0.865,0.662,0,0],'String','Deterministic','FitBoxToText','on','EdgeColor','none','Fontsize',font2);
annotation('textbox',[0.865,0.374,0,0],'String','Deterministic','FitBoxToText','on','EdgeColor','none','Fontsize',font2);


set(findall(fig1,'-property','Interpreter'),'Interpreter','latex');
set(findall(fig1,'-property','TickLabelInterpreter'),'TickLabelInterpreter','latex');

