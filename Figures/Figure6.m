% Skritp to generate figure 6 

%Initialize
clc 
clear 
close all 

%% Plot Settings 

%Colors
dark = [2 93 114]/255;
pink = [178 93 141]/255;
light = [121 182 185]/255; 
occa = [215,200,100]/255;
black = [0 0 0];
% Define a color vector 
col = [dark;pink;light;occa];
% Define a colormap
col_n=4;
mymap1 = [linspace(black(1),pink(1),6)', linspace(black(2),pink(2),6)',linspace(black(3),pink(3),6)'];
mymap2 = [linspace(pink(1),occa(1),col_n)', linspace(pink(2),occa(2),col_n)',linspace(pink(3),occa(3),col_n)'];
mymap3 = [linspace(occa(1),dark(1),col_n)', linspace(occa(2),dark(2),col_n)',linspace(occa(3),dark(3),col_n)'];
mymap4 = [linspace(dark(1),light(1),col_n)', linspace(dark(2),light(2),col_n)',linspace(dark(3),light(3),col_n)'];
mymap5 = [linspace(light(1),light(1),col_n)', linspace(light(2),light(2),col_n)',linspace(light(3),light(3),col_n)'];
mymap = [mymap1;mymap2;mymap3;mymap4;mymap5];

% Define a marker array for the Treatment Comparison 
marker ={'-','--',':','-.','-','--',':','-.','-','--',':','-.','-','--',':','-.'};
% Linewidth
set(0, 'DefaultLineLineWidth', 1.5);
% Fontsizes
font = 10; 
font2 = 8;
font3 = 12;

% Treatment Name 
T ={'12h','24h','1.5d','2d','2.5d','3d','3.5d','4d','5d','6d','7d','8d','9d','14d','20d','never'};
% Size of the Marker 
m_Size = 5;

%% Load Data 
dec = 2; 
%---Kappa = 1---------------------------------------------------------------
% Define Data Path
dataPath = 'Data/Treatments12h_Cip';
P = load(strcat(dataPath,'/Data_P_C0_06.mat')); 
P = P.P_data;
L = load(strcat(dataPath,'/Data_L_C0_06.mat')); 
L  = L .P_data;

% CS
P_wCS = load(strcat(dataPath,'/Data_P_wCS_C0_06.mat')); 
L_wCS = load(strcat(dataPath,'/Data_L_wCS_C0_06.mat'));
P_sCS = load(strcat(dataPath,'/Data_P_sCS_C0_06.mat')); 
L_sCS = load(strcat(dataPath,'/Data_L_sCS_C0_06.mat')); 
P_wCS = P_wCS.P_data;
L_wCS = L_wCS.P_data;
P_sCS = P_sCS.P_data;
L_sCS = L_sCS.P_data;


%---Kappa = 2---------------------------------------------------------------
% Define Data Path
dataPath = 'Data/Treatments12h_Fab2';
P_F = load(strcat(dataPath,'/Data_P_C0_06.mat')); 
P_F = P_F.P_data;
L_F = load(strcat(dataPath,'/Data_L_C0_06.mat')); 
L_F  = L_F.P_data;

% CS
P_wCS_F = load(strcat(dataPath,'/Data_P_wCS_C0_06.mat')); 
L_wCS_F = load(strcat(dataPath,'/Data_L_wCS_C0_06.mat'));
P_sCS_F = load(strcat(dataPath,'/Data_P_sCS_C0_06.mat')); 
L_sCS_F = load(strcat(dataPath,'/Data_L_sCS_C0_06.mat')); 
P_wCS_F = P_wCS_F.P_data;
L_wCS_F = L_wCS_F.P_data;
P_sCS_F = P_sCS_F.P_data;
L_sCS_F= L_sCS_F.P_data;


%--SGV---------------------------------------------------------------------

%---Kappa = 1---------------------------------------------------------------
% Define Data Path
dataPath = 'Data/Treatments12h_Cip_sgv';
P_sgv = load(strcat(dataPath,'/Data_P_C0_06.mat')); 
P_sgv = P_sgv.P_data;
L_sgv = load(strcat(dataPath,'/Data_L_C0_06.mat')); 
L_sgv  = L_sgv .P_data;

% CS
P_wCS_sgv = load(strcat(dataPath,'/Data_P_wCS_C0_06.mat')); 
L_wCS_sgv = load(strcat(dataPath,'/Data_L_wCS_C0_06.mat'));
P_sCS_sgv = load(strcat(dataPath,'/Data_P_sCS_C0_06.mat')); 
L_sCS_sgv = load(strcat(dataPath,'/Data_L_sCS_C0_06.mat')); 
P_wCS_sgv = P_wCS_sgv.P_data;
L_wCS_sgv = L_wCS_sgv.P_data;
P_sCS_sgv = P_sCS_sgv.P_data;
L_sCS_sgv = L_sCS_sgv.P_data;

%---Kappa = 2---------------------------------------------------------------
% Define Data Path
dataPath = 'Data/Treatments12h_Fab2_sgv';
P_F_sgv = load(strcat(dataPath,'/Data_P_C0_06.mat')); 
P_F_sgv = P_F_sgv.P_data;
L_F_sgv = load(strcat(dataPath,'/Data_L_C0_06.mat')); 
L_F_sgv  = L_F_sgv.P_data;

% CS
P_wCS_F_sgv = load(strcat(dataPath,'/Data_P_wCS_C0_06.mat')); 
L_wCS_F_sgv = load(strcat(dataPath,'/Data_L_wCS_C0_06.mat'));
P_sCS_F_sgv = load(strcat(dataPath,'/Data_P_sCS_C0_06.mat')); 
L_sCS_F_sgv= load(strcat(dataPath,'/Data_L_sCS_C0_06.mat')); 
P_wCS_F_sgv = P_wCS_F_sgv.P_data;
L_wCS_F_sgv = L_wCS_F_sgv.P_data;
P_sCS_F_sgv = P_sCS_F_sgv.P_data;
L_sCS_F_sgv= L_sCS_F_sgv.P_data;

%--SGV2--------------------------------------------------------------------

%---Kappa = 1---------------------------------------------------------------
% Define Data Path
dataPath = 'Data/Treatments12h_Cip_sgv2';
P_sgv2 = load(strcat(dataPath,'/Data_P_C0_06.mat')); 
P_sgv2 = P_sgv2.P_data;
L_sgv2 = load(strcat(dataPath,'/Data_L_C0_06.mat')); 
L_sgv2  = L_sgv2 .P_data;

% CS
P_wCS_sgv2 = load(strcat(dataPath,'/Data_P_wCS_C0_06.mat')); 
L_wCS_sgv2 = load(strcat(dataPath,'/Data_L_wCS_C0_06.mat'));
P_sCS_sgv2 = load(strcat(dataPath,'/Data_P_sCS_C0_06.mat')); 
L_sCS_sgv2 = load(strcat(dataPath,'/Data_L_sCS_C0_06.mat')); 
P_wCS_sgv2 = P_wCS_sgv2.P_data;
L_wCS_sgv2 = L_wCS_sgv2.P_data;
P_sCS_sgv2 = P_sCS_sgv2.P_data;
L_sCS_sgv2 = L_sCS_sgv2.P_data;

%---Kappa = 2---------------------------------------------------------------
% Define Data Path
dataPath = 'Data/Treatments12h_Fab2_sgv2';
P_F_sgv2 = load(strcat(dataPath,'/Data_P_C0_06.mat')); 
P_F_sgv2 = P_F_sgv2.P_data;
L_F_sgv2 = load(strcat(dataPath,'/Data_L_C0_06.mat')); 
L_F_sgv2  = L_F_sgv2.P_data;

% CS
P_wCS_F_sgv2 = load(strcat(dataPath,'/Data_P_wCS_C0_06.mat')); 
L_wCS_F_sgv2 = load(strcat(dataPath,'/Data_L_wCS_C0_06.mat'));
P_sCS_F_sgv2 = load(strcat(dataPath,'/Data_P_sCS_C0_06.mat')); 
L_sCS_F_sgv2 = load(strcat(dataPath,'/Data_L_sCS_C0_06.mat')); 
P_wCS_F_sgv2 = P_wCS_F_sgv2.P_data;
L_wCS_F_sgv2 = L_wCS_F_sgv2.P_data;
P_sCS_F_sgv2 = P_sCS_F_sgv2.P_data;
L_sCS_F_sgv2= L_sCS_F_sgv2.P_data;




n = 200;
D1 = [linspace(0,0.2,n);linspace(0,0.2,n)];
D2 = [linspace(0.2,0.6,n);linspace(0.2,0.6,n)]; 
D = [D1(:,1:end-1),D2];
% Scale Drug Concnetration in fold change of MIC
D = D./0.017; 



%% Figure 3: CS and sgv exact same ratio as figure 4 


% Set the positions of the different subplots 
% length and height of sub-plots: 
sp_length = 0.25; 
sp_height = 0.25;
% position of subplots in x-direction 
x_pos1 = 0.1; 
x_pos2 = 0.55; 

% Position of the Subplots 
Position = [0.1,0.6,sp_length,sp_height;...
            0.4,0.6,sp_length,sp_height;...
            0.7,0.6,sp_length,sp_height;...
            0.1,0.15,sp_length,sp_height;...
            0.4,0.15,sp_length,sp_height;...
            0.7,0.15,sp_length,sp_height];
        
% Treatment 
ind = 1; 

fig3 = figure(3);  
fig3.Units = 'centimeters';
fig3.Position=[5,5,15.5,8.4]; 


sub(1) = subplot('Position',Position(1,:));
plot(D(1,:),P_sCS(ind).t_erad,'Color',mymap(12,:),'Linewidth',1.5)
hold on 
plot(D(1,:),P(ind).t_erad,'Color',dark,'Linewidth',1.5)
hold on 
set(gca,'FontSize',font2); 
ylim([0,300]);
title('No pre-existing resistance');
xlim([0,36]);

sub(2) = subplot('Position',Position(2,:));
plot(D(1,:),P_sCS_sgv(ind).t_erad,'Color',mymap(12,:),'Linewidth',1.5)
hold on 
plot(D(1,:),P_sgv(ind).t_erad,'Color',dark,'Linewidth',1.5)
hold on 
set(gca,'FontSize',font2); 
ylim([0,300]);
title('$92$ single mutants each');
xlim([0,36]);

sub(3) = subplot('Position',Position(3,:));
plots_cs(2)=plot(D(1,:),P_sCS_sgv2(ind).t_erad,'Color',mymap(12,:),'Linewidth',1.5);
hold on 
plots_cs(1)=plot(D(1,:),P_sgv2(ind).t_erad,'Color',dark,'Linewidth',1.5);
hold on 
set(gca,'FontSize',font2); 
ylim([0,300]);
title('$1000$ single mutants each');
xlim([0,36]);
l1=legend(plots_cs,'without CS','with CS','Position',[0.768834160078263,0.702050474991182,0.181165839921737,0.147949525008818],'FontSize',font2);
title(l1,'Collateral Sensitivity')
l1.ItemTokenSize = [15,18];

sub(4) = subplot('Position',Position(4,:));
plot(D(1,:),P_F(ind).t_erad,'Color',dark)
hold on 
plot(D(1,:),P_wCS_F(ind).t_erad,'-.','Color',mymap(12,:))
hold on 
plot(D(1,:),P_sCS_F(ind).t_erad,'--','Color',mymap(13,:))
hold on 
set(gca,'FontSize',font2); 
title('No pre-existing resistance');
ylim([0,300]);
xlim([0,36]);

sub(5) = subplot('Position',Position(5,:));
plot(D(1,:),P_F_sgv(ind).t_erad,'Color',dark)
hold on 
plot(D(1,:),P_sCS_F_sgv(ind).t_erad,'--','Color',mymap(13,:))
hold on 
set(gca,'FontSize',font2); 
ylim([0,300]);
xlim([0,36]);
title('$92$ single mutants each');
xlabel('Drug dose $D$ in multiples of MIC$_W$','visible','on','Fontsize',font);


sub(6) = subplot('Position',Position(6,:));
plot(D(1,:),P_F_sgv2(ind).t_erad,'Color',dark)
hold on 
plot(D(1,:),P_sCS_F_sgv2(ind).t_erad,'--','Color',mymap(13,:))
hold on 
set(gca,'FontSize',font2); 
title('$1000$ single mutants each');
ylim([0,300]);
xlim([0,36]);

% % Commen Y label
p1=get(sub(1),'position');
p2=get(sub(4),'position');
height=p1(2)+p1(4)-p2(2);
h15=axes('position',[p1(1)-0.03 p2(2) p2(3) height],'visible','off');
ylabel('Time until threshold $N_c$ in hours','visible','on','Fontsize',font,'Interpreter','latex');



dimA = [0.07,0.92,0,0];
annotation('textbox',dimA,'String','A','FitBoxToText','on','EdgeColor','none','Fontsize',font3);
dimB = [0.37,0.92,0,0];
annotation('textbox',dimB,'String','B','FitBoxToText','on','EdgeColor','none','Fontsize',font3);
dimD = [0.67,0.92,0,0];
annotation('textbox',dimD,'String','C','FitBoxToText','on','EdgeColor','none','Fontsize',font3);

dimD = [0.07,0.47,0,0];
annotation('textbox',dimD,'String','D','FitBoxToText','on','EdgeColor','none','Fontsize',font3);
dimE = [0.37,0.47,0,0];
annotation('textbox',dimE,'String','E','FitBoxToText','on','EdgeColor','none','Fontsize',font3);
dimF = [0.67,0.47,0,0];
annotation('textbox',dimF,'String','F','FitBoxToText','on','EdgeColor','none','Fontsize',font3);

dim1 = [0.429868626490133,0.887886436233761,0.190262747019734,0.092113563766239];
annotation('textbox',dim1,'String','$\kappa_A = \kappa_B = 1$','FitBoxToText','on','EdgeColor','none','Fontsize',font3);
dim2 = [0.429868626490133,0.437886436233761,0.190262747019734,0.092113563766239];
annotation('textbox',dim2,'String','$\kappa_A = \kappa_B = 2$','FitBoxToText','on','EdgeColor','none','Fontsize',font3);


set(findall(fig3,'-property','Interpreter'),'Interpreter','latex');
set(findall(fig3,'-property','TickLabelInterpreter'),'TickLabelInterpreter','latex');
set(findall(fig3,'-property','YTick'),'YTick',[50,150,250]);

