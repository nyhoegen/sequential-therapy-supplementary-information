% Skript to generate Figure S5:

%Initialize matlab 
clc 
clear 
close all 

%% Plot Settings 

%Colors
dark = [2 93 114]/255;
pink = [178 93 141]/255;
light = [121 182 185]/255; 
occa = [215,200,100]/255;
black = [0 0 0];
% Define a color vector 
col = [dark;pink;light;occa];
% Define a colormap
col_n=4;
mymap1 = [linspace(black(1),pink(1),6)', linspace(black(2),pink(2),6)',linspace(black(3),pink(3),6)'];
mymap2 = [linspace(pink(1),occa(1),col_n)', linspace(pink(2),occa(2),col_n)',linspace(pink(3),occa(3),col_n)'];
mymap3 = [linspace(occa(1),dark(1),col_n)', linspace(occa(2),dark(2),col_n)',linspace(occa(3),dark(3),col_n)'];
mymap4 = [linspace(dark(1),light(1),col_n)', linspace(dark(2),light(2),col_n)',linspace(dark(3),light(3),col_n)'];
mymap5 = [linspace(light(1),light(1),col_n)', linspace(light(2),light(2),col_n)',linspace(light(3),light(3),col_n)'];
mymap = [mymap1;mymap2;mymap3;mymap4;mymap5];

% Define a marker array for the Treatment Comparison 
marker ={'-','--',':','-.','-','--',':','-.','-','--',':','-.','-','--',':','-.'};
% Linewidth
set(0, 'DefaultLineLineWidth', 1.5);
% Fontsizes
font = 10; 
font2 = 8;
font3 = 12;

% Treatment Name 
T ={'12h','24h','1.5D','2D','2.5D','3D','3.5D','4D','5D','6D','7D','8D','9D','14D','20D','never'};
% Size of the Marker 
m_Size = 5;


%% Function handles

% function handles for the psi_functions  
mu_fab =@(c,zMIC)  ((0.088+6.5)*(c/zMIC).^2./((c/zMIC).^2+6.5/0.088));
mu_cip =@(c,zMIC)  ((0.088+6.5)*(c/zMIC).^1./((c/zMIC).^1+6.5/0.088));

mu_fab_W =@(c) mu_fab(c,0.017);
mu_fab_M =@(c) mu_fab(c,28*0.017);

% Drug Range 
c_vec = linspace(0,10,6000);

% Calculate the psi value for different drug concentrations 
[X,Y] = meshgrid(c_vec,c_vec); 

Z_fab_W1 =  mu_fab_W(X+Y); 
Z_fab_W2 =  mu_fab_W(X) + mu_fab_W(Y); 

Z_fab_MA1 = mu_fab_M(X+Y); 
Z_fab_MA2 =  mu_fab_M(X)+ mu_fab_W(Y); 

Z_fab_MB1 =  mu_fab_W(X+Y); 
Z_fab_MB2 =  mu_fab_M(X) + mu_fab_W(Y); 

Z_fab_MAB1 =  mu_fab_M(X+Y); 
Z_fab_MAB2 =  mu_fab_M(X) + mu_fab_M(Y); 

%% Plot 
% Position of the subfigures 
Position = [0.06,0.55,0.24,0.35;...
            0.06,0.12,0.24,0.35;...
            0.4,0.55,0.25,0.35;...
            0.4,0.12,0.25,0.35;...
            0.7,0.55,0.25,0.35;...
            0.7,0.12,0.25,0.35];

% Plot psi function 
fig2 = figure(2); 
fig2.Units = 'centimeters';
fig2.Position=[10,10,15.5,10];
% set(gcf,'renderer','Painters')
h11 = subplot('Position',Position(1,:));
plot(c_vec/0.017,mu_cip(2*c_vec,0.017),'Color',[2 93 114]/255)
hold on 
plot(c_vec/0.017,2*mu_cip(c_vec,0.017),'Color',[dark 0.5])
hold on 
set(gca,'FontSize',font2)
ylim([0,7]); 
xlim([0,40]);
yticks([0,2,4,6]);
leg = legend('$\mu_W(c_A = 2c,c_B = 0)$','$\mu_W(c_A = c,c_B = c)$','Location','northwest'); 
leg.ItemTokenSize = [15 18];
title('Susceptible wild type')

h12 = subplot('Position',Position(2,:)); 
plot(c_vec/0.017,mu_fab(2*c_vec,0.017),'Color',mymap(5,:))
hold on 
plot(c_vec/0.017,2*mu_fab(c_vec,0.017),'Color',[mymap(5,:),0.6])
hold on 
set(gca,'FontSize',font2)
xlabel('c in multiples of zMIC$_W$', 'FontSize',font); 
ylim([0,18]);
xlim([0 40]);
leg = legend('$\mu_W(c_A = 2c,c_B = 0)$','$\mu_W(c_A = c,c_B = c)$','Location','northwest'); 
leg.ItemTokenSize = [15 18];

% Commen y label 
p11=get(h11,'position');
p12=get(h12,'position');
height=p11(2)+p11(4)-p12(2);
h15=axes('position',[p11(1)+0.01 p12(2) p12(3) height],'visible','off');
ylabel('Antibiotic induced kill rate $\mu_W(c_A,c_B)$','visible','on','Fontsize',font);

% Define colors for the countourf plot 
mymap_small = [0.95,0.95,0.95;mymap(4,:)];
n_con = [-10,0,10];

h1 = subplot('Position',Position(3,:));
contourf(X/0.017,Y/0.017,Z_fab_W1-Z_fab_W2,n_con,'linewidth',1.5) 
hold on 
set(gca,'FontSize',font2);
zlabel('0.088-\mu(C_1+C_2)')
title('Susceptible wild type')
xlim([0,1/0.017])
ylim([0,1/0.017])
colormap(mymap_small)

subplot('Position',Position(5,:));
contourf(X/0.017,Y/0.017,Z_fab_MA1-Z_fab_MA2,n_con,'linewidth',1.5) 
hold on 
set(gca,'FontSize',font2);
zlabel('0.088-\mu(C_1+C_2)')
title('Resistant to drug A')
xlim([0,2/0.017])
ylim([0,2/0.017])
colormap(mymap_small)

h3 = subplot('Position',Position(4,:));
contourf(X/0.017,Y/0.017,Z_fab_MB1-Z_fab_MB2,n_con,'linewidth',1.5) 
hold on 
set(gca,'FontSize',font2);
zlabel('0.088-\mu(C_1+C_2)')
title('Resistant to drug B')
xlim([0,3/0.017])
ylim([0,3/0.017])
colormap(mymap_small)

h4 = subplot('Position',Position(6,:));
contourf(X/0.017,Y/0.017,Z_fab_MAB1-Z_fab_MAB2,n_con,'linewidth',1.5) 
hold on 
set(gca,'FontSize',font2);
zlabel('0.088-\mu(C_1+C_2)')
title('Resistant to both drugs')
xlim([0,8/0.017])
ylim([0,8/0.017])
colormap(mymap_small)

% Commen y label 
p1=get(h1,'position');
% p2=get(h2,'position');
p3=get(h3,'position');
p4=get(h4,'position');
height=p1(2)+p1(4)-p3(2);
axes('position',[p1(1) p3(2) p3(3) height],'visible','off');
ylabel('$c_2$ in multiples of zMIC$_W$','visible','on','Fontsize',font);

% Commen X label 
width = p4(1)+p4(3)-p3(1); 
axes('position',[p3(1) p4(2) width p3(4)],'visible','off');
xlabel('$c_1$ in multiples of zMIC$_W$','visible','on','Fontsize',font);

% Annotate text 
dim = [0.69,0.045,0.35,0.1575];
str = '$\mu_X(c_A = c_1,c_B = c_2)$';
annotation('textbox',dim,'String',str,'FitBoxToText','on','EdgeColor','none','Color',[1,1,1],'Fontsize',font2);
dim = [0.7,0.01,0.35,0.1575];
str = '$<\mu_X(c_A = c_1+c_2,c_B =0)$';
annotation('textbox',dim,'String',str,'FitBoxToText','on','EdgeColor','none','Color',[1,1,1],'Fontsize',font2);

dim2 = [0.72,0.33,0.35,0.15];
str2 = '$\mu_X(c_A = c_1,c_B = c_2)$';
annotation('textbox',dim2,'String',str2,'FitBoxToText','on','EdgeColor','none','Fontsize',font2);
dim2 = [0.725,0.295,0.35,0.15];
str2 = '$>\mu_X(c_A = c_1+c_2,c_B =0)$';

annotation('textbox',dim2,'String',str2,'FitBoxToText','on','EdgeColor','none','Fontsize',font2);
dim2 = [0.18,0.45,0.35,0.15];
str2 = '$\kappa_A = \kappa_B = 1$';
annotation('textbox',dim2,'String',str2,'FitBoxToText','on','EdgeColor','none','Fontsize',font2);
dim2 = [0.18,0.02,0.35,0.15];
str2 = '$\kappa_A = \kappa_B = 2$';
annotation('textbox',dim2,'String',str2,'FitBoxToText','on','EdgeColor','none','Fontsize',font2);
dim2 = [0.53,0.75,0.35,0.15];
str2 = '$\kappa_A = \kappa_B = 2$';
annotation('textbox',dim2,'String',str2,'FitBoxToText','on','EdgeColor','none','Fontsize',font2);
% Annotate 'A' and 'B' 
annotation('textbox',[0.05,0.96,0,0],'String','A','FitBoxToText','on','EdgeColor','none','Fontsize',font3);
annotation('textbox',[0.39,0.96,0,0],'String','C','FitBoxToText','on','EdgeColor','none','Fontsize',font3);
annotation('textbox',[0.69,0.96,0,0],'String','E','FitBoxToText','on','EdgeColor','none','Fontsize',font3);
annotation('textbox',[0.05,0.53,0,0],'String','B','FitBoxToText','on','EdgeColor','none','Fontsize',font3);
annotation('textbox',[0.39,0.53,0,0],'String','D','FitBoxToText','on','EdgeColor','none','Fontsize',font3);
annotation('textbox',[0.69,0.53,0,0],'String','F','FitBoxToText','on','EdgeColor','none','Fontsize',font3);

set(findall(fig2,'-property','Interpreter'),'Interpreter','latex');
set(findall(fig2,'-property','TickLabelInterpreter'),'TickLabelInterpreter','latex');

