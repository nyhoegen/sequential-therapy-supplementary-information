% Skript to generate Figure 6
% 02.16.22
% 
% The data for this figure can be found in 'Hybrid_Simulation_2'. The date
% was not generated with the latest version of the simulation code. All changes 
% in the code where minor and did not impact the results, but could of course 
% leed to slight variation in the data.   
%
% Initialize Matlab:
clc
clear
close all
%% Load Simulation Data 

dataPath = 'Data/Hybrid_Simulations_2/Treatments12h_Cip';
Sim_C_Basic = load(strcat(dataPath,'/Sim_T1_2.mat')); 
Sim_C_Basic_T1 = Sim_C_Basic.Sim;
Sim_C_Basic = load(strcat(dataPath,'/Sim_T3_4.mat')); 
Sim_C_Basic_T4 = Sim_C_Basic.Sim;
Sim_C_Basic = load(strcat(dataPath,'/Sim_T8.mat')); 
Sim_C_Basic_T8 = Sim_C_Basic.Sim;
Sim_C_Basic = [Sim_C_Basic_T1(1,:);Sim_C_Basic_T4(2,:);Sim_C_Basic_T8(2,:)]; 

dataPath = 'Data/Hybrid_Simulations_2/Treatments12h_Fab2';
Sim_F2_Basic = load(strcat(dataPath,'/Sim_T1_2.mat')); 
Sim_F2_Basic_T1 = Sim_F2_Basic.Sim;
Sim_F2_Basic = load(strcat(dataPath,'/Sim_T3_4.mat')); 
Sim_F2_Basic_T4 = Sim_F2_Basic.Sim;
Sim_F2_Basic = load(strcat(dataPath,'/Sim_T8.mat')); 
Sim_F2_Basic_T8 = Sim_F2_Basic.Sim;
Sim_F2_Basic = [Sim_F2_Basic_T1(1,:);Sim_F2_Basic_T4(2,:);Sim_F2_Basic_T8(2,:)]; 


dataPath = 'Data/Hybrid_Simulations_2/Treatments12h_CipFab2';
Sim_CF2_Basic = load(strcat(dataPath,'/Sim_T1_4.mat')); 
Sim_CF2_Basic_T1 = Sim_CF2_Basic.Sim;
Sim_CF2_Basic = load(strcat(dataPath,'/Sim_T8.mat')); 
Sim_CF2_Basic_T8 = Sim_CF2_Basic.Sim;
Sim_CF2_Basic = [Sim_CF2_Basic_T1;Sim_CF2_Basic_T8(2,:)]; 

dataPath = 'Data/Hybrid_Simulations_2/Treatments12h_Fab2Cip';
Sim_F2C_Basic = load(strcat(dataPath,'/Sim_T1_4.mat')); 
Sim_F2C_Basic_T1 = Sim_F2C_Basic.Sim;
Sim_F2C_Basic = load(strcat(dataPath,'/Sim_T8.mat')); 
Sim_F2C_Basic_T8 = Sim_F2C_Basic.Sim;
Sim_F2C_Basic = [Sim_F2C_Basic_T1;Sim_F2C_Basic_T8(2,:)]; 

D_Sim = (0.08:0.01:0.4)/0.017; 
D_F2 = (0.054:0.01:0.4)/0.017;

for i = 1:3
    for j = 1:length(D_Sim)

        Sim_t_erad_C(1,j) = mean(Sim_C_Basic{i,j},'omitnan'); 
        Sim_t_erad_std_C(1,j) = std(Sim_C_Basic{i,j},'omitnan'); 

        Sim_t_erad_CF2(1,j) = mean(Sim_CF2_Basic{i,j},'omitnan'); 
        Sim_t_erad_std_CF2(1,j) = std(Sim_CF2_Basic{i,j},'omitnan'); 

        Sim_t_erad_F2C(1,j) = mean(Sim_F2C_Basic{i,j},'omitnan'); 
        Sim_t_erad_std_F2C(1,j) = std(Sim_F2C_Basic{i,j},'omitnan'); 
        
       
    end

    Sim_mean_C{i} = Sim_t_erad_C; 
    Sim_std_C{i} = Sim_t_erad_std_C;
    Sim_sem_C{i} = Sim_t_erad_std_C/sqrt(100);

    Sim_mean_CF2{i} = Sim_t_erad_CF2; 
    Sim_std_CF2{i} = Sim_t_erad_std_CF2;
    Sim_sem_CF2{i} = Sim_t_erad_std_CF2/sqrt(100);

    Sim_mean_F2C{i} = Sim_t_erad_F2C; 
    Sim_std_F2C{i} = Sim_t_erad_std_F2C;
    Sim_sem_F2C{i} = Sim_t_erad_std_F2C/sqrt(100);
end
    

for i = 1:3
    for j = 1:length(D_F2)
        Sim_t_erad_F2(1,j) = mean(Sim_F2_Basic{i,j},'omitnan'); 
        Sim_t_erad_std_F2(1,j) = std(Sim_F2_Basic{i,j},'omitnan'); 
    end

    Sim_mean_F2{i} = Sim_t_erad_F2; 
    Sim_std_F2{i} = Sim_t_erad_std_F2;
    Sim_sem_F2{i} = Sim_t_erad_std_F2/sqrt(100);

end


%% Plot Settings 

% Label for Legend 
treatments = {'12h','24h','2d','4d'};
ind_treat =[1,2,4,8]; 
% Selection of Colors
ind_col = [16,9,12,6];

% Set the positions of the different subplots 
% length and height of sub-plots: 
sp_length = 0.375; 
sp_height = 0.35;
% position of subplots in x-direction 
x_pos1 = 0.075; 
x_pos2 = 0.5; 

% Position of the Subplots 
Position = [x_pos1,0.575,sp_length,sp_height;...
            x_pos2,0.575,sp_length,sp_height;...
            x_pos1,0.1,sp_length,sp_height;...
            x_pos2,0.1,sp_length,sp_height];

%Colors
dark = [2 93 114]/255;
pink = [178 93 141]/255;
light = [121 182 185]/255; 
occa = [215,200,100]/255;
black = [0 0 0];
% Define a color vector 
col = [dark;pink;light;occa];
% Define a colormap
col_n=4;
mymap1 = [linspace(black(1),pink(1),6)', linspace(black(2),pink(2),6)',linspace(black(3),pink(3),6)'];
mymap2 = [linspace(pink(1),occa(1),col_n)', linspace(pink(2),occa(2),col_n)',linspace(pink(3),occa(3),col_n)'];
mymap3 = [linspace(occa(1),dark(1),col_n)', linspace(occa(2),dark(2),col_n)',linspace(occa(3),dark(3),col_n)'];
mymap4 = [linspace(dark(1),light(1),col_n)', linspace(dark(2),light(2),col_n)',linspace(dark(3),light(3),col_n)'];
mymap5 = [linspace(light(1),light(1),col_n)', linspace(light(2),light(2),col_n)',linspace(light(3),light(3),col_n)'];
mymap = [mymap1;mymap2;mymap3;mymap4;mymap5];

% Linewidth
set(0, 'DefaultLineLineWidth', 1.5);
% Fontsizes
font = 10; 
font2 = 8;
font3 = 12;



%% Plot figure Version 2 Simulation

% Label for Legend 
treatments = {'12 hours','2 days','4 days'};

% vector with with foirst drug concnetration for which the results are
% included (exclude all treatments for which p_ext < 1
d_ind = [2,2,8];

% Set the positions of the different subplots 
% length and height of sub-plots: 
sp_length = 0.7; 
sp_height = 0.225;
% position of subplots in x-direction 
x_pos1 = 0.15; 

% Position of the Subplots 
Position = [x_pos1,0.7,sp_length,sp_height;...
            x_pos1,0.4,sp_length,sp_height;...
            x_pos1,0.1,sp_length,sp_height];

fig1=figure(1);
% Set size of the figure 
fig1.Units = 'centimeters';
fig1.Position=[10,2,7.75,12]; 

for i = 1:3
    h(i) = subplot('Position',Position(i,:)); 
    plot1(1)=plot(D_Sim(d_ind(i):end),Sim_mean_C{i}(d_ind(i):end),'Color',mymap(ind_col(1),:),'linewidth',1.5);
    hold on
    errorbar(D_Sim(d_ind(i):3:end),Sim_mean_C{i}(d_ind(i):3:end),Sim_sem_C{i}(d_ind(i):3:end),'.','Color',mymap(ind_col(1),:),'linewidth',1,'CapSize',4);
    hold on 
    plot1(2)=plot(D_Sim(d_ind(i):end),Sim_mean_CF2{i}(d_ind(i):end),'Color',mymap(ind_col(2),:),'linewidth',1.5);
    hold on
    errorbar(D_Sim(d_ind(i):3:end),Sim_mean_CF2{i}(d_ind(i):3:end),Sim_sem_CF2{i}(d_ind(i):3:end),'.','Color',mymap(ind_col(2),:),'linewidth',1,'CapSize',4);
    hold on 
    plot1(3)=plot(D_Sim(d_ind(i):end),Sim_mean_F2C{i}(d_ind(i):end),'Color',mymap(ind_col(3),:),'linewidth',1.5);
    hold on
    errorbar(D_Sim(d_ind(i):3:end),Sim_mean_F2C{i}(d_ind(i):3:end),Sim_sem_F2C{i}(d_ind(i):3:end),'.','Color',mymap(ind_col(3),:),'linewidth',1,'CapSize',4);
    hold on 
    plot1(4)=plot(D_F2(d_ind(i)+3:end),Sim_mean_F2{i}(d_ind(i)+3:end),'Color',mymap(ind_col(4),:),'linewidth',1.5);
    hold on
    errorbar(D_F2(d_ind(i)+3:3:end),Sim_mean_F2{i}(d_ind(i)+3:3:end),Sim_sem_F2{i}(d_ind(i)+3:3:end),'.','Color',mymap(ind_col(4),:),'linewidth',1,'CapSize',4);
    hold on 
    
    if i == 3
        xlabel('Drug dose $D$ in multiples of zMIC$_W$','FontSize',font','visible','on','Fontsize',font,'Interpreter','latex');
    elseif i==1
        leg = legend(plot1,{'$\kappa_A = 1, \kappa_B = 1$','$\kappa_A = 1, \kappa_B = 2$','$\kappa_A = 2, \kappa_B = 1$','$\kappa_A = 2, \kappa_B = 2$'},'Location','northeast');
        leg.ItemTokenSize = [15 18]; 
    end
 
% Set fontsize for axis
set(gca,'FontSize',font2);
% set axis propeties 
xlim([4,23.53])
ylim([0,350])
str = strcat('Switch after', {' '},treatments{i});
title(str,'FontSize',font)
end
%l1 = legend(plot1,{'\kappa = 1, \kappa = 1','\kappa = 1, \kappa = 2','\kappa = 2, \kappa = 1','\kappa = 2, \kappa = 2'});
% l1.Position = [0.8625,0.7925,0.1,0.1];




% Commen Y label
p1=get(h(1),'position');
p2=get(h(3),'position');
height=p1(2)+p1(4)-p2(2);
h15=axes('position',[p1(1) p2(2) p2(3) height],'visible','off');
ylabel('Time until threshold $N_c$ in hours','visible','on','Fontsize',font,'Interpreter','latex');

% Annotate 'A' and 'B' 
dim3 = [0.13,0.96,0,0.01];
str3 = 'A';
annotation('textbox',dim3,'String',str3,'FitBoxToText','on','EdgeColor','none','Fontsize',font3);
dim4 = [0.13,0.66,0,0.01];
str4= 'B';
annotation('textbox',dim4,'String',str4,'FitBoxToText','on','EdgeColor','none','Fontsize',font3);
dim1 = [0.13,0.37,0,0];
str1 = 'C';
annotation('textbox',dim1,'String',str1,'FitBoxToText','on','EdgeColor','none','Fontsize',font3);

set(findall(fig1,'-property','Interpreter'),'Interpreter','latex');
set(findall(fig1,'-property','TickLabelInterpreter'),'TickLabelInterpreter','latex');


