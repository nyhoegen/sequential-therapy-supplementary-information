% SI Figure for the STD of Simulations (S3):
%
% Initializy matlab 
clc 
clear all 
close all 

%% Load Data: 

%---Simulation Data: kappa = 1 ------------------------------------------- 
Sim1_C = load('Data/Hybrid_Simulations/Treatments12h_Cip/Sim_T1_2.mat'); 
Sim1_C = Sim1_C.Sim;
Sim1_C_L = load('Data/Hybrid_Simulations/Treatments12h_Cip/Sim_Lab_T1_2.mat'); 
Sim1_C_L = Sim1_C_L.Sim;

%T3 and T4
Sim2_C = load('Data/Hybrid_Simulations/Treatments12h_Cip/Sim_T3_4.mat'); 
Sim2_C = Sim2_C.Sim;
Sim2_C_L = load('Data/Hybrid_Simulations/Treatments12h_Cip/Sim_Lab_T3_4.mat'); 
Sim2_C_L = Sim2_C_L.Sim;

%T5 
Sim3_C = load('Data/Hybrid_Simulations/Treatments12h_Cip/Sim_T5.mat'); 
Sim3_C = Sim3_C.Sim;
Sim3_C_L = load('Data/Hybrid_Simulations/Treatments12h_Cip/Sim_Lab_T5.mat'); 
Sim3_C_L = Sim3_C_L.Sim;

% Combine:  
Sim_C = [Sim1_C;Sim2_C;Sim3_C]; 
Sim_C_L = [Sim1_C_L;Sim2_C_L;Sim3_C_L]; 
D_C = (0.08:0.01:0.4)/0.017;
D_C_L = (0.05:0.01:0.4)/0.017;

%---Simulation Data: kappa = 2 ------------------------------------------- 
Sim1_F2 = load('Data/Hybrid_Simulations/Treatments12h_Fab2/Sim_T1_2.mat'); 
Sim1_F2 = Sim1_F2.Sim;
Sim1_F2_L = load('Data/Hybrid_Simulations/Treatments12h_Fab2/Sim_Lab_T1_2.mat'); 
Sim1_F2_L = Sim1_F2_L.Sim;

%T3 and T4
Sim2_F2 = load('Data/Hybrid_Simulations/Treatments12h_Fab2/Sim_T3_4.mat'); 
Sim2_F2 = Sim2_F2.Sim;
Sim2_F2_L = load('Data/Hybrid_Simulations/Treatments12h_Fab2/Sim_Lab_T3_4.mat'); 
Sim2_F2_L = Sim2_F2_L.Sim;

%T5 
Sim3_F2 = load('Data/Hybrid_Simulations/Treatments12h_Fab2/Sim_T5.mat'); 
Sim3_F2 = Sim3_F2.Sim;
Sim3_F2_L = load('Data/Hybrid_Simulations/Treatments12h_Fab2/Sim_Lab_T5.mat'); 
Sim3_F2_L = Sim3_F2_L.Sim;

% Compbine:  
Sim_F2 = [Sim1_F2;Sim2_F2;Sim3_F2]; 
Sim_F2_L = [Sim1_F2_L;Sim2_F2_L;Sim3_F2_L]; 
D_F2 = (0.054:0.01:0.4)/0.017;
D_F2_L = (0.03:0.01:0.4)/0.017;

% Calculate mean values 
n_sim = 100; 
for i = 1:5
    for j = 1:length(D_C)
        Sim_t_erad_C(1,j) = mean(Sim_C{i,j},'omitnan'); 
        Sim_t_erad_std_C(1,j) = std(Sim_C{i,j},'omitnan'); 
    end
    Sim_mean_C{i} = Sim_t_erad_C; 
    Sim_std_C{i} = Sim_t_erad_std_C;
    Sim_sem_C{i} = Sim_t_erad_std_C/sqrt(n_sim);
end 

for i = 1:5
    for j = 1:length(D_C_L)
        Sim_t_erad_C_L(1,j) = mean(Sim_C_L{i,j},'omitnan'); 
        Sim_t_erad_std_C_L(1,j) = std(Sim_C_L{i,j},'omitnan'); 
    end
    Sim_mean_C_L{i} = Sim_t_erad_C_L; 
    Sim_std_C_L{i} = Sim_t_erad_std_C_L;
    Sim_sem_C_L{i} = Sim_t_erad_std_C_L/sqrt(n_sim);
end 

for i = 1:5
    for j = 1:length(D_F2)
        Sim_t_erad_F2(1,j) = mean(Sim_F2{i,j},'omitnan'); 
        Sim_t_erad_std_F2(1,j) = std(Sim_F2{i,j},'omitnan'); 
    end
    Sim_mean_F2{i} = Sim_t_erad_F2; 
    Sim_std_F2{i} = Sim_t_erad_std_F2;
    Sim_sem_F2{i} = Sim_t_erad_std_F2/sqrt(n_sim);
end

for i = 1:5
    for j = 1:length(D_F2_L)
        Sim_t_erad_F2_L(1,j) = mean(Sim_F2_L{i,j},'omitnan'); 
        Sim_t_erad_std_F2_L(1,j) = std(Sim_F2_L{i,j},'omitnan'); 
    end
    Sim_mean_F2_L{i} = Sim_t_erad_F2_L; 
    Sim_std_F2_L{i} = Sim_t_erad_std_F2_L;
    Sim_sem_F2_L{i} = Sim_t_erad_std_F2_L/sqrt(n_sim);
end

%% Plot Settings 

% Label for Legend 
treatments = {'12h','24h','1.5d','2d','2.5d','3d','3.5d','4d','5d','6d','7d','8d','9d'};

% Set the positions of the different subplots 
% length and height of sub-plots: 
sp_length = 0.35; 
sp_height = 0.16;
% position of subplots in x-direction 
x_pos1 = 0.08; 
x_pos2 = 0.5; 

%Colors
dark = [2 93 114]/255;
pink = [178 93 141]/255;
light = [121 182 185]/255; 
occa = [215,200,100]/255;
black = [0 0 0];
% Define a color vector 
col = [dark;pink;light;occa];
% Define a colormap
col_n=4;
mymap1 = [linspace(black(1),pink(1),6)', linspace(black(2),pink(2),6)',linspace(black(3),pink(3),6)'];
mymap2 = [linspace(pink(1),occa(1),col_n)', linspace(pink(2),occa(2),col_n)',linspace(pink(3),occa(3),col_n)'];
mymap3 = [linspace(occa(1),dark(1),col_n)', linspace(occa(2),dark(2),col_n)',linspace(occa(3),dark(3),col_n)'];
mymap4 = [linspace(dark(1),light(1),col_n)', linspace(dark(2),light(2),col_n)',linspace(dark(3),light(3),col_n)'];
mymap5 = [linspace(light(1),light(1),col_n)', linspace(light(2),light(2),col_n)',linspace(light(3),light(3),col_n)'];
mymap = [mymap1;mymap2;mymap3;mymap4;mymap5];

% Linewidth
set(0, 'DefaultLineLineWidth', 2);
% Fontsizes
font = 10; 
font2 = 8;
font3 = 12;

% Selection of Colors
ind_col = [3,6,9,12,16];
%% Figure 3: Standarddeviation as function of c 

treatments_sim = {'12h','24h','1.5D','2D','2.5D','3D','3.5D','4D','5D'};

% ind_col = [3,5,6,8,9,12,13,16,18];
ind_col = [3,6,9,12,16,18];
% Drug Concentration in which most treatments exept 12h switch is best for Fab2  

% Position of the Subplots 
Position = [0.1,0.575,0.35,0.35;...
            0.5,0.575,0.35,0.35;...
            0.1,0.125,0.35,0.35;...
            0.5,0.125,0.35,0.35];

% Plot figure 
fig1=figure(1); 
fig1.Units = 'centimeters'; 
fig1.Position = [5,2,15.5,8];

hs1=subplot('Position',Position(1,:));
for i = 1:5
    plot(D_C_L,Sim_std_C_L{i},'Color',mymap(ind_col(i),:),'linewidth',1.2)
    hold on  
end
set(gca,'FontSize',font2,'YScale','log'); 
title('Lab','FontSize',font)

hs2=subplot('Position',Position(2,:));
for i = 1:5
    plot(D_C,Sim_std_C{i},'Color',mymap(ind_col(i),:),'linewidth',1.2)
    hold on  
end
set(gca,'FontSize',font2,'YScale','log'); 
title('Patient','FontSize',font)
leg = legend(treatments_sim(1:5),'Location','best');
leg.ItemTokenSize = [15,18]; 
title(leg,'Switch after'); 
leg.Position = [0.875,0.74,0.1,0.1];

hs3=subplot('Position',Position(3,:));
for i = 1:5
    plot(D_F2_L,Sim_std_F2_L{i},'Color',mymap(ind_col(i),:),'linewidth',1.2)
    hold on  
end
set(gca,'FontSize',font2); 
set(gca,'FontSize',font2,'YScale','log'); 

hs4=subplot('Position',Position(4,:));
for i = 1:5
    plot(D_F2,Sim_std_F2{i},'Color',mymap(ind_col(i),:),'linewidth',1.2)
    hold on  
end
set(gca,'FontSize',font2); 
set(gca,'FontSize',font2,'YScale','log'); 


% Commen X label 
p3=get(hs3,'position');
p4=get(hs4,'position');
width = p4(1)+p4(3)-p3(1); 
axes('position',[p3(1) p4(2) width p3(4)],'visible','off');
xlabel('Drug dose $D$ in multiples of zMIC$_W$','FontSize',font','visible','on','Fontsize',font);

 % Commen Y label
p1=get(hs1,'position');
p2=get(hs3,'position');
height=p1(2)+p1(4)-p2(2);
h15=axes('position',[p1(1) p2(2) p2(3) height],'visible','off');
ylabel('Standard deviation in hours','visible','on','Fontsize',font,'Interpreter','latex');   

% Annotate 'A' and 'B' 
annotation('textbox',[0.1,1,0,0],'String','A','FitBoxToText','on','EdgeColor','none','Fontsize',font3);
annotation('textbox',[0.1,0.55,0,0],'String','C','FitBoxToText','on','EdgeColor','none','Fontsize',font3);
annotation('textbox',[0.5,1,0,0],'String','B','FitBoxToText','on','EdgeColor','none','Fontsize',font3);
annotation('textbox',[0.5,0.55,0,0],'String','D','FitBoxToText','on','EdgeColor','none','Fontsize',font3);

% Annotate kappa values 
annotation('textbox',[0.32,0.925,0,0],'String','$\kappa_A = \kappa_B =1$','FitBoxToText','on','EdgeColor','none','Fontsize',font2);
annotation('textbox',[0.32,0.475,0,0],'String','$\kappa_A = \kappa_B =2$','FitBoxToText','on','EdgeColor','none','Fontsize',font2);
annotation('textbox',[0.72,0.925,0,0],'String','$\kappa_A = \kappa_B =1$','FitBoxToText','on','EdgeColor','none','Fontsize',font2);
annotation('textbox',[0.72,0.475,0,0],'String','$\kappa_A = \kappa_B =2$','FitBoxToText','on','EdgeColor','none','Fontsize',font2);

set(findall(fig1,'-property','Interpreter'),'Interpreter','latex');
set(findall(fig1,'-property','TickLabelInterpreter'),'TickLabelInterpreter','latex');
set(findall(fig1,'-property','YLim'),'YLim',[0.2,200]);
set(findall(fig1,'-property','XLim'),'XLim',[0,23.3]);
set(findall(fig1,'-property','YTick'),'YTick',[1,10,10^2]);