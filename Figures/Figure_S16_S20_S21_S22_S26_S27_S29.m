% SI Sktipt for the Pharamcodynamics of different characteristics and
% Example Figures 

clc 
clear all 
close all 

%% Plot Settings
treatments = {'12h','24h','1.5d','2d','2.5d','3d','3.5d','4d','5d'};

%Colors
dark = [2 93 114]/255;
pink = [178 93 141]/255;
light = [121 182 185]/255; 
occa = [215,200,100]/255;
black = [0 0 0];
% Define a color vector 
col = [dark;pink;light;occa];
% Define a colormap
col_n=4;
mymap1 = [linspace(black(1),pink(1),6)', linspace(black(2),pink(2),6)',linspace(black(3),pink(3),6)'];
mymap2 = [linspace(pink(1),occa(1),col_n)', linspace(pink(2),occa(2),col_n)',linspace(pink(3),occa(3),col_n)'];
mymap3 = [linspace(occa(1),dark(1),col_n)', linspace(occa(2),dark(2),col_n)',linspace(occa(3),dark(3),col_n)'];
mymap4 = [linspace(dark(1),light(1),col_n)', linspace(dark(2),light(2),col_n)',linspace(dark(3),light(3),col_n)'];
mymap5 = [linspace(light(1),light(1),col_n)', linspace(light(2),light(2),col_n)',linspace(light(3),light(3),col_n)'];
mymap = [mymap1;mymap2;mymap3;mymap4;mymap5];

% Linewidth
set(0, 'DefaultLineLineWidth', 1.5);
% Fontsizes
font = 10; 
font2 = 8;
font3 = 12;

% Selection of Colors
ind_col = [1,4,6,8,9,12,13,16,18];


%% S29: Growth rates

% Function handle for the pharmacodynamic function:  
% Kill rate 
mu = @(c,p_max)  (p_max+6.5)*(c./0.017).^2./((c./0.017).^2+6.5/p_max);
% PD-function: 
psi = @(c,p_max) p_max - mu(c,p_max);

mu2 = @(c,p_max)  (p_max+50)*(c./0.017).^2./((c./0.017).^2+50/p_max);
psi2 = @(c,p_max) p_max - mu2(c,p_max);

% DDI term: 
INT = @(c,I) 1 + (I*c./(0.05+c));
% Kill rate 
mu_int = @(cA,cB,p_max,I)  ((p_max+6.5)*(cA./(INT(cB,I)*0.017)).^2./((cA./(INT(cB,I)*0.017)).^2+6.5/p_max));
% PD-function with DDI 
psi_int = @(cA,cB,pmax,I,k) pmax-mu_int(cA,cB,pmax,I);

% define a vector of concnetrations 
c_vec = linspace(0,0.5,1000);


Pos = [0.1,0.2,0.35,0.7;
        0.55,0.2,0.35,0.7];
fig1 = figure(1);  
fig1.Units = 'centimeter';
fig1.Position = [10,2,15.5,6]; 

s1=subplot('position',Pos(1,:)); 
plot(c_vec/0.017,arrayfun(@(c) psi(c,0.088),c_vec),'Color',dark); 
hold on 
plot(c_vec/0.017,arrayfun(@(c) psi_int(c,0.05,0.088,5),c_vec),'Color',pink); 
hold on 
plot(c_vec/0.017,arrayfun(@(c) psi_int(c,0.05,0.088,-0.9),c_vec),'Color',light); 
hold on 
plot([0,30],[0 0],'--','Color',[0.2 0.2 0.2 0.2]);
set(gca,'FontSize',font2); 
ylabel('Growth rate','FontSize',font);
title('Slow replication: $\psi_{\mathrm{max}} = 0.088$','FontSize',font); 

s2=subplot('position',Pos(2,:));
plot([0,30],[0 0],'--','Color',[0.2 0.2 0.2 0.2]);
hold on
plots(2)=plot(c_vec/0.017,arrayfun(@(c) psi(c,0.88),c_vec),'Color',dark); 
hold on 
plots(1)=plot(c_vec/0.017,arrayfun(@(c) psi_int(c,0.05,0.88,5),c_vec),'Color',pink); 
hold on 
plots(3)=plot(c_vec/0.017,arrayfun(@(c) psi_int(c,0.05,0.88,-0.9),c_vec),'Color',light); 
hold on 
title('Fast replication: $\psi_{\mathrm{max}} = 0.88$','FontSize',font); 
l1 = legend(plots,'Antagonism','Bliss Independence','Synergism'); 
l1.ItemTokenSize = [15 18]; 
% Commen X label 
p3=get(s1,'position');
p4=get(s2,'position');
width = p4(1)+p4(3)-p3(1); 
axes('position',[p3(1) p4(2) width p3(4)],'visible','off');
xlabel('Drug dose $D$ in multiples of zMIC$_W$','FontSize',font','visible','on','Fontsize',font);

dimA = [0.09,1,0,0];
annotation('textbox',dimA,'String','A','FitBoxToText','on','EdgeColor','none','Fontsize',font3);
dimB = [0.54,1,0,0];
annotation('textbox',dimB,'String','B','FitBoxToText','on','EdgeColor','none','Fontsize',font3);

set(findall(fig1,'-property','Interpreter'),'Interpreter','latex');
set(findall(fig1,'-property','TickLabelInterpreter'),'TickLabelInterpreter','latex');
set(findall(fig1,'-property','YLim'),'YLim',[-7,2]);


%% S16: Comparing fast and slow growth curves for different types 
clear('plots');
Pos = [0.1,0.2,0.35,0.7;
        0.55,0.2,0.35,0.7];
    
mu_val = @(c,p_max,zmic)  (p_max+6.5)*(c./(zmic)).^2./((c./(zmic)).^2+6.5/p_max);
% PD-function: 
psi_val = @(c,p_max,zmic) p_max - mu_val(c,p_max,zmic);

% define a vector of concnetrations 
c_vec = linspace(0,0.6,1000);

fig1 = figure(2);  
fig1.Units = 'centimeter';
fig1.Position = [10,2,15.5,6]; 

s1=subplot('position',Pos(1,:)); 
plot(c_vec/0.017,arrayfun(@(c) psi_val(c,0.088,0.017),c_vec),'Color',dark); 
hold on 
plot(c_vec/0.017,arrayfun(@(c) psi_val(c,0.88,0.017),c_vec),'Color',pink); 
hold on 
plot([0,40],[0 0],'--','Color',[0.2 0.2 0.2 0.2]);
set(gca,'FontSize',font2); 
ylabel('Growth rate','FontSize',font);
title('Susceptible type','FontSize',font); 


s2=subplot('position',Pos(2,:));
plot([0,30],[0 0],'--','Color',[0.2 0.2 0.2 0.2]);
hold on
plots(2)=plot(c_vec/0.017,arrayfun(@(c) psi_val(c,0.0782,28*0.017),c_vec),'Color',dark); 
hold on 
plots(1)=plot(c_vec/0.017,arrayfun(@(c) psi_val(c,0.782,28*0.017),c_vec),'Color',pink); 
hold on 
plot([0,40],[0 0],'--','Color',[0.2 0.2 0.2 0.2]);
hold on 
title('Resistant type' ,'FontSize',font); 
leg= legend(plots,'$\psi_{\mathrm{max}} = 0.88$','$\psi_{\mathrm{max}} = 0.088$','Location','best');
leg.ItemTokenSize = [15 18];
%Commen X label 
p3=get(s1,'position');
p4=get(s2,'position');
width = p4(1)+p4(3)-p3(1); 
axes('position',[p3(1) p4(2) width p3(4)],'visible','off');
xlabel('Drug dose $D$ in multiples of zMIC$_W$','FontSize',font','visible','on','Fontsize',font);

dimA = [0.09,1,0,0];
annotation('textbox',dimA,'String','A','FitBoxToText','on','EdgeColor','none','Fontsize',font3);
dimB = [0.54,1,0,0];
annotation('textbox',dimB,'String','B','FitBoxToText','on','EdgeColor','none','Fontsize',font3);

set(findall(fig1,'-property','Interpreter'),'Interpreter','latex');
set(findall(fig1,'-property','TickLabelInterpreter'),'TickLabelInterpreter','latex');
set(findall(fig1,'-property','YLim'),'YLim',[-7,2]);
set(findall(fig1,'-property','XLim'),'XLim',[0,36]);

%% S27: With CS
Pos = [0.1,0.2,0.35,0.7;
        0.55,0.2,0.35,0.7];
    
clear('plots');

    mu_val1 = @(c,p_max,zmic)  (p_max+6.5)*(c./(zmic)).^1./((c./(zmic)).^1+6.5/p_max);
% PD-function: 
psi_val1 = @(c,p_max,zmic) p_max - mu_val1(c,p_max,zmic);

mu_val = @(c,p_max,zmic)  (p_max+6.5)*(c./(zmic)).^2./((c./(zmic)).^2+6.5/p_max);
% PD-function: 
psi_val = @(c,p_max,zmic) p_max - mu_val(c,p_max,zmic);

% define a vector of concnetrations 
c_vec = linspace(0,0.6,1000);

fig1 = figure(2);  
fig1.Units = 'centimeter';
fig1.Position = [10,2,15.5,6]; 

s1=subplot('position',Pos(1,:)); 
plots(1)=plot(c_vec/0.017,arrayfun(@(c) psi_val(c,0.0782,0.017),c_vec),'Color',dark); 
hold on 
plots(2)=plot(c_vec/0.017,arrayfun(@(c) psi_val(c,0.0782,0.007),c_vec),'--','Color',dark); 
hold on 
plot([0,40],[0 0],'--','Color',[0.2 0.2 0.2 0.2]);
set(gca,'FontSize',font2); 
ylabel('Growth rate','FontSize',font);
title('Slow replication: $\psi_{\mathrm{max}} = 0.088$','FontSize',font); 
leg= legend(plots,'Without collateral sensitivity','With collateral sensitivity','Location','best');
leg.ItemTokenSize = [15 18];


s2=subplot('position',Pos(2,:));
plot([0,30],[0 0],'--','Color',[0.2 0.2 0.2 0.2]);
hold on
plots(1)=plot(c_vec/0.017,arrayfun(@(c) psi_val(c,0.782,0.017),c_vec),'Color',pink); 
hold on 
plots(2)=plot(c_vec/0.017,arrayfun(@(c) psi_val(c,0.782,0.007),c_vec),'--','Color',pink); 
hold on 
plot([0,40],[0 0],'--','Color',[0.2 0.2 0.2 0.2]);
hold on 
title('Fast replication: $\psi_{\mathrm{max}} = 0.88$','FontSize',font); 
leg= legend(plots,'Without collateral sensitivity','With collateral sensitivity','Location','best');
leg.ItemTokenSize = [15 18];%Commen X label 
p3=get(s1,'position');
p4=get(s2,'position');
width = p4(1)+p4(3)-p3(1); 
axes('position',[p3(1) p4(2) width p3(4)],'visible','off');
xlabel('Drug dose $D$ in multiples of zMIC$_W$','FontSize',font','visible','on','Fontsize',font);

dimA = [0.09,1,0,0];
annotation('textbox',dimA,'String','A','FitBoxToText','on','EdgeColor','none','Fontsize',font3);
dimB = [0.54,1,0,0];
annotation('textbox',dimB,'String','B','FitBoxToText','on','EdgeColor','none','Fontsize',font3);

set(findall(fig1,'-property','Interpreter'),'Interpreter','latex');
set(findall(fig1,'-property','TickLabelInterpreter'),'TickLabelInterpreter','latex');
set(findall(fig1,'-property','YLim'),'YLim',[-7,1]);
set(findall(fig1,'-property','XLim'),'XLim',[0,20]);


%% S22: Comparing fast and slow growth curves for different types 
Pos = [0.1,0.2,0.35,0.7;
        0.55,0.2,0.35,0.7];
    
clear('plots');

    mu_val1 = @(c,p_max,zmic)  (p_max+6.5)*(c./(zmic)).^1./((c./(zmic)).^1+6.5/p_max);
% PD-function: 
psi_val1 = @(c,p_max,zmic) p_max - mu_val1(c,p_max,zmic);

mu_val = @(c,p_max,zmic)  (p_max+6.5)*(c./(zmic)).^2./((c./(zmic)).^2+6.5/p_max);
% PD-function: 
psi_val = @(c,p_max,zmic) p_max - mu_val(c,p_max,zmic);

% define a vector of concnetrations 
c_vec = linspace(0,1.5,1000);

fig1 = figure(2);  
fig1.Units = 'centimeter';
fig1.Position = [10,2,15.5,6]; 

s1=subplot('position',Pos(1,:)); 
plots(1)=plot(c_vec/0.017,arrayfun(@(c) psi_val(c,0.0782,28*0.017),c_vec),'Color',dark); 
hold on 
plots(2)=plot(c_vec/0.017,arrayfun(@(c) psi_val1(c,0.0782,28*0.017),c_vec),'--','Color',dark); 
hold on 
plot([0,40],[0 0],'--','Color',[0.2 0.2 0.2 0.2]);
set(gca,'FontSize',font2); 
ylabel('Growth rate','FontSize',font);
title('Slow replication: $\psi_{\mathrm{max}} = 0.088$','FontSize',font); 
leg= legend(plots,'$\kappa_A = \kappa_B = 2$','$\kappa_A = \kappa_B = 1$','Location','best');
leg.ItemTokenSize = [15 18];


s2=subplot('position',Pos(2,:));
plot([0,30],[0 0],'--','Color',[0.2 0.2 0.2 0.2]);
hold on
plots(1)=plot(c_vec/0.017,arrayfun(@(c) psi_val(c,0.782,28*0.017),c_vec),'Color',pink); 
hold on 
plots(2)=plot(c_vec/0.017,arrayfun(@(c) psi_val1(c,0.782,28*0.017),c_vec),'--','Color',pink); 
hold on 
plot([0,40],[0 0],'--','Color',[0.2 0.2 0.2 0.2]);
hold on 
title('Fast replication: $\psi_{\mathrm{max}} = 0.88$','FontSize',font); 
leg= legend(plots,'$\kappa_A = \kappa_B = 2$','$\kappa_A = \kappa_B = 1$','Location','best');
leg.ItemTokenSize = [15 18];
%Commen X label 
p3=get(s1,'position');
p4=get(s2,'position');
width = p4(1)+p4(3)-p3(1); 
axes('position',[p3(1) p4(2) width p3(4)],'visible','off');
xlabel('Drug dose $D$ in multiples of zMIC$_W$','FontSize',font','visible','on','Fontsize',font);

dimA = [0.09,1,0,0];
annotation('textbox',dimA,'String','A','FitBoxToText','on','EdgeColor','none','Fontsize',font3);
dimB = [0.54,1,0,0];
annotation('textbox',dimB,'String','B','FitBoxToText','on','EdgeColor','none','Fontsize',font3);

set(findall(fig1,'-property','Interpreter'),'Interpreter','latex');
set(findall(fig1,'-property','TickLabelInterpreter'),'TickLabelInterpreter','latex');
set(findall(fig1,'-property','YLim'),'YLim',[-7,1]);
set(findall(fig1,'-property','XLim'),'XLim',[0,80]);

%% Load datasets 
Cip_fast = load('Cip_fast_growth.mat'); 
Cip_slow = load('Cip_growth.mat'); 
t_val = 12;
% set TA, TB: 12
P_f = Cip_fast.P; 
P_f.PK(1).Tthis = t_val; 
P_f.PK(2).Tthis = t_val; 
P_f.PK(1).Tother = t_val; 
P_f.PK(2).Tother = t_val; 

P_s = Cip_slow.P; 
P_s.PK(1).Tthis = t_val; 
P_s.PK(2).Tthis = t_val; 
P_s.PK(1).Tother = t_val; 
P_s.PK(2).Tother = t_val; 


t1 = repmat([1 0], [1 40]); 
t2 = repmat([1 1 0 0], [1 20]); 
t3 = repmat([ones(1,3),zeros(1,3)], [1 14]);
t4 = repmat([ones(1,4),zeros(1,4)], [1 10]);
t5 = repmat([ones(1,5),zeros(1,5)], [1 8]); 
t6 = repmat([ones(1,6),zeros(1,6)], [1 7]);
t7 = repmat([ones(1,7),zeros(1,7)], [1 6]);
t8 = repmat([ones(1,8),zeros(1,8)], [1 5]);
t9 = repmat([ones(1,10),zeros(1,10)], [1 4]);
t10 = repmat([ones(1,12),zeros(1,12)], [1 4]);
t11 = repmat([ones(1,14),zeros(1,14)], [1 3]);
t12 = repmat([ones(1,16),zeros(1,16)], [1 3]);
t13 = repmat([ones(1,18),zeros(1,18)], [1 3]);
t14 = repmat([ones(1,28),zeros(1,28)], [1 2]);
t15 = [ones(1,40),zeros(1,40)];
t16 = ones(1,80); 
%% S20: Fast vs. Slow growth Bilss T1

% Treatment T1: 
P_f.PK(1).cycle = t1(1:20); 
P_f.PK(2).cycle = ~t1(1:20); 

P_s.PK(1).cycle = t1(1:20); 
P_s.PK(2).cycle = ~t1(1:20); 

c_val = 6*0.017;
P_f.PK(1).D = c_val; 
P_f.PK(2).D = c_val; 
P_s.PK(1).D = c_val; 
P_s.PK(2).D = c_val; 
[dyn1,data1] = Two_Drugs_Cycling_CC_u(P_f.G,P_f.INT,P_f.PD,P_f.PK,P_f.CC,P_f.M); 
[dyn2,data2] = Two_Drugs_Cycling_CC_u(P_s.G,P_s.INT,P_s.PD,P_s.PK,P_s.CC,P_s.M); 

c_val = 8*0.017;
P_f.PK(1).D = c_val; 
P_f.PK(2).D = c_val; 
P_s.PK(1).D = c_val; 
P_s.PK(2).D = c_val; 
[dyn3,data3] = Two_Drugs_Cycling_CC_u(P_f.G,P_f.INT,P_f.PD,P_f.PK,P_f.CC,P_f.M); 
[dyn4,data4] = Two_Drugs_Cycling_CC_u(P_s.G,P_s.INT,P_s.PD,P_s.PK,P_s.CC,P_s.M); 

% T3

% Treatment T1: 
P_f.PK(1).cycle = t3(1:20); 
P_f.PK(2).cycle = ~t3(1:20); 

P_s.PK(1).cycle = t3(1:20); 
P_s.PK(2).cycle = ~t3(1:20); 

c_val = 10*0.017;
P_f.PK(1).D = c_val; 
P_f.PK(2).D = c_val; 
P_s.PK(1).D = c_val; 
P_s.PK(2).D = c_val; 
[dyn2_1,data2_1] = Two_Drugs_Cycling_CC_u(P_f.G,P_f.INT,P_f.PD,P_f.PK,P_f.CC,P_f.M); 
[dyn2_2,data2_2] = Two_Drugs_Cycling_CC_u(P_s.G,P_s.INT,P_s.PD,P_s.PK,P_s.CC,P_s.M); 

c_val = 30*0.017;
P_f.PK(1).D = c_val; 
P_f.PK(2).D = c_val; 
P_s.PK(1).D = c_val; 
P_s.PK(2).D = c_val; 
[dyn2_3,data2_3] = Two_Drugs_Cycling_CC_u(P_f.G,P_f.INT,P_f.PD,P_f.PK,P_f.CC,P_f.M); 
[dyn2_4,data2_4] = Two_Drugs_Cycling_CC_u(P_s.G,P_s.INT,P_s.PD,P_s.PK,P_s.CC,P_s.M); 

% Figure

x_length = 0.35;
y_length = 0.15;
Pos = [0.1,0.74,x_length,y_length;...
         0.55,0.74,x_length,y_length;...
         0.1,0.525,x_length,y_length;...
         0.55,0.525,x_length,y_length;...
         0.1,0.265,x_length,y_length;...
         0.55,0.265,x_length,y_length;...
         0.1,0.05,x_length,y_length;...
         0.55,0.05,x_length,y_length];
     
fig1 = figure(1);  
fig1.Units = 'centimeter';
fig1.Position = [10,1,15.5,18]; 

s2 = subplot('Position',Pos(2,:)); 
plot(dyn1.time,dyn1.W,'Color',occa); 
hold on 
plot(dyn1.time,dyn1.MA,'Color',dark); 
hold on 
plot(dyn1.time,dyn1.MB,'Color',light); 
hold on 
plot(dyn1.time,dyn1.MAB,'Color',pink); 
hold on 
title('$\psi^W_{\mathrm{max}} = 0.88$, $D$ = 6 zMIC$_W$'); 
leg1 = legend('Wildtyp','Mutant A','Mutant B','Mutant AB'); 
leg1.ItemTokenSize = [15 18];

s1 = subplot('Position',Pos(1,:)); 
plot(dyn2.time,dyn2.W,'Color',occa); 
hold on 
plot(dyn2.time,dyn2.MA,'Color',dark); 
hold on 
plot(dyn2.time,dyn2.MB,'Color',light); 
hold on 
plot(dyn2.time,dyn2.MAB,'Color',pink); 
hold on 
title('$\psi^W_{\mathrm{max}} = 0.088$, $D$ = 6 zMIC$_W$'); 


s4 = subplot('Position',Pos(4,:)); 
plot(dyn3.time,dyn3.W,'Color',occa); 
hold on 
plot(dyn3.time,dyn3.MA,'Color',dark); 
hold on 
plot(dyn3.time,dyn3.MB,'Color',light); 
hold on 
plot(dyn3.time,dyn3.MAB,'Color',pink); 
hold on 
title('$\psi^W_{\mathrm{max}} = 0.88$, $D$ = 8 zMIC$_W$'); 

s3 = subplot('Position',Pos(3,:)); 
plot(dyn4.time,dyn4.W,'Color',occa); 
hold on 
plot(dyn4.time,dyn4.MA,'Color',dark); 
hold on 
plot(dyn4.time,dyn4.MB,'Color',light); 
hold on 
plot(dyn4.time,dyn4.MAB,'Color',pink); 
hold on 
title('$\psi^W_{\mathrm{max}} = 0.088$, $D$ = 8 zMIC$_W$'); 

s6 = subplot('Position',Pos(6,:)); 
plot(dyn2_1.time,dyn2_1.W,'Color',occa); 
hold on 
plot(dyn2_1.time,dyn2_1.MA,'Color',dark); 
hold on 
plot(dyn2_1.time,dyn2_1.MB,'Color',light); 
hold on 
plot(dyn2_1.time,dyn2_1.MAB,'Color',pink); 
hold on 
title('$\psi^W_{\mathrm{max}} = 0.88$, $D$ = 10 zMIC$_W$'); 
leg1 = legend('Wildtyp','Mutant A','Mutant B','Mutant AB'); 
leg1.ItemTokenSize = [15 18];

s5 = subplot('Position',Pos(5,:)); 
plot(dyn2_2.time,dyn2_2.W,'Color',occa); 
hold on 
plot(dyn2_2.time,dyn2_2.MA,'Color',dark); 
hold on 
plot(dyn2_2.time,dyn2_2.MB,'Color',light); 
hold on 
plot(dyn2_2.time,dyn2_2.MAB,'Color',pink); 
hold on 
title('$\psi^W_{\mathrm{max}} = 0.088$, $D$ = 10 zMIC$_W$'); 


s8 = subplot('Position',Pos(8,:)); 
plot(dyn2_3.time,dyn2_3.W,'Color',occa); 
hold on 
plot(dyn2_3.time,dyn2_3.MA,'Color',dark); 
hold on 
plot(dyn2_3.time,dyn2_3.MB,'Color',light); 
hold on 
plot(dyn2_3.time,dyn2_3.MAB,'Color',pink); 
hold on 
title('$\psi^W_{\mathrm{max}} = 0.88$, $D$ = 30 zMIC$_W$'); 

s7 = subplot('Position',Pos(7,:)); 
plot(dyn2_4.time,dyn2_4.W,'Color',occa); 
hold on 
plot(dyn2_4.time,dyn2_4.MA,'Color',dark); 
hold on 
plot(dyn2_4.time,dyn2_4.MB,'Color',light); 
hold on 
plot(dyn2_4.time,dyn2_4.MAB,'Color',pink); 
hold on 
title('$\psi^W_{\mathrm{max}} = 0.088$, $D$ = 30 zMIC$_W$'); 


% % Commen Y label
p1=get(s3,'position');
p2=get(s5,'position');
height=p1(2)+p1(4)-p2(2);
h15=axes('position',[p1(1) p2(2) p2(3) height],'visible','off');
ylabel('Cell numbers','visible','on','Fontsize',font,'Interpreter','latex');

% Commen X label 
p3=get(s7,'position');
p4=get(s8,'position');
width = p4(1)+p4(3)-p3(1); 
axes('position',[p3(1) p4(2) width p3(4)],'visible','off');
xlabel('Time in hours','FontSize',font','visible','on','Fontsize',font,'Interpreter','latex');

dimA = [0.1,0.92,0,0];
annotation('textbox',dimA,'String','A','FitBoxToText','on','EdgeColor','none','Fontsize',font3);
dimB = [0.55,0.92,0,0];
annotation('textbox',dimB,'String','B','FitBoxToText','on','EdgeColor','none','Fontsize',font3);
dimC = [0.1,0.705,0,0];
annotation('textbox',dimC,'String','C','FitBoxToText','on','EdgeColor','none','Fontsize',font3);
dimD = [0.55,0.705,0,0];
annotation('textbox',dimD,'String','D','FitBoxToText','on','EdgeColor','none','Fontsize',font3);

dimA2 = [0.1,0.445,0,0];
annotation('textbox',dimA2,'String','E','FitBoxToText','on','EdgeColor','none','Fontsize',font3);
dimB2 = [0.55,0.445,0,0];
annotation('textbox',dimB2,'String','F','FitBoxToText','on','EdgeColor','none','Fontsize',font3);
dimC2 = [0.1,0.23,0,0];
annotation('textbox',dimC2,'String','G','FitBoxToText','on','EdgeColor','none','Fontsize',font3);
dimD2 = [0.55,0.23,0,0];
annotation('textbox',dimD2,'String','H','FitBoxToText','on','EdgeColor','none','Fontsize',font3);



dimE = [0.15983490636163,0.922741667423646,0.23033018727674,0.077258332576354];
annotation('textbox',dimE,'String','Slow replication','FitBoxToText','on','EdgeColor','none','Fontsize',font3);
dimF = [0.612224687329657,0.922741667423646,0.225550625340686,0.077258332576354];
annotation('textbox',dimF,'String','Rapid replication','FitBoxToText','on','EdgeColor','none','Fontsize',font3);

dimG = [0.373553001228586,0.917078703030834,0.252894004947817,0.042921296969167];
annotation('textbox',dimG,'String','Switch every 12 $h$','FitBoxToText','on','EdgeColor','none','Fontsize',font3);
dimH = [0.370519927977088,0.442,0.258960151450812,0.042921296969167];
annotation('textbox',dimH,'String','Switch every 1.5 $d$','FitBoxToText','on','EdgeColor','none','Fontsize',font3);


set(findall(fig1,'-property','Interpreter'),'Interpreter','latex');
set(findall(fig1,'-property','TickLabelInterpreter'),'TickLabelInterpreter','latex');
set(findall(fig1,'-property','YScale'),'YScale','log');
set(findall(fig1,'-property','YLim'),'YLim',[1,10^15]);
set(findall(fig1,'-property','XLim'),'XLim',[0,200]);

%% S21: Fast gworth effect kappa 

P_f.INT(1).I = 0; 
P_f.INT(1).EC50 = 0.05; 
P_f.INT(2).I = 0; 
P_f.INT(2).EC50 = 0.05; 

P_f.PK(1).cycle = t3(1:20); 
P_f.PK(2).cycle = ~t3(1:20); 

c_val = 30*0.017;
P_f.PK(1).D = c_val; 
P_f.PK(2).D = c_val; 
P_s.PK(1).D = c_val; 
P_s.PK(2).D = c_val;

P_f2 = P_f; 
P_s2 = P_s; 
for i = 1:4
    P_f2.PD(i).kA = 2;
    P_f2.PD(i).kB = 2; 
    P_s2.PD(i).kA = 2;
    P_s2.PD(i).kB = 2; 
end


[dyn1,data1] = Two_Drugs_Cycling_CC_u(P_f.G,P_f.INT,P_f.PD,P_f.PK,P_f.CC,P_f.M); 
[dyn2,data2] = Two_Drugs_Cycling_CC_u(P_s.G,P_s.INT,P_s.PD,P_s.PK,P_s.CC,P_s.M); 

[dyn3,data3] = Two_Drugs_Cycling_CC_u(P_f2.G,P_f2.INT,P_f2.PD,P_f2.PK,P_f2.CC,P_f2.M); 
[dyn4,data4] = Two_Drugs_Cycling_CC_u(P_s2.G,P_s2.INT,P_s2.PD,P_s2.PK,P_s2.CC,P_s2.M); 


%%     

x_length = 0.35; 
y_length = 0.27; 

Pos = [0.1,0.55,x_length,y_length;...
         0.55,0.55,x_length,y_length;...
         0.1,0.1,x_length,y_length;...
         0.55,0.1,x_length,y_length];
     
fig1 = figure(1);  
fig1.Units = 'centimeter';
fig1.Position = [10,2,15.5,10]; 

s2 = subplot('Position',Pos(2,:)); 
plot(dyn1.time,dyn1.W,'Color',occa); 
hold on 
plot(dyn1.time,dyn1.MA,'Color',dark); 
hold on 
plot(dyn1.time,dyn1.MB,'Color',light); 
hold on 
plot(dyn1.time,dyn1.MAB,'Color',pink); 
hold on 
title('$\psi^W_{\mathrm{max}} = 0.88$'); 
leg1 = legend('Wildtyp','Mutant A','Mutant B','Mutant AB'); 
leg1.ItemTokenSize = [15 18];

s1 = subplot('Position',Pos(1,:)); 
plot(dyn2.time,dyn2.W,'Color',occa); 
hold on 
plot(dyn2.time,dyn2.MA,'Color',dark); 
hold on 
plot(dyn2.time,dyn2.MB,'Color',light); 
hold on 
plot(dyn2.time,dyn2.MAB,'Color',pink); 
hold on 
title('$\psi^W_{\mathrm{max}} = 0.088$'); 


s4 = subplot('Position',Pos(4,:)); 
plot(dyn3.time,dyn3.W,'Color',occa); 
hold on 
plot(dyn3.time,dyn3.MA,'Color',dark); 
hold on 
plot(dyn3.time,dyn3.MB,'Color',light); 
hold on 
plot(dyn3.time,dyn3.MAB,'Color',pink); 
hold on 
title('$\psi^W_{\mathrm{max}} = 0.88$'); 

s3 = subplot('Position',Pos(3,:)); 
plot(dyn4.time,dyn4.W,'Color',occa); 
hold on 
plot(dyn4.time,dyn4.MA,'Color',dark); 
hold on 
plot(dyn4.time,dyn4.MB,'Color',light); 
hold on 
plot(dyn4.time,dyn4.MAB,'Color',pink); 
hold on 
title('$\psi^W_{\mathrm{max}} = 0.088$'); 


% % Commen Y label
p1=get(s1,'position');
p2=get(s3,'position');
height=p1(2)+p1(4)-p2(2);
h15=axes('position',[p1(1) p2(2) p2(3) height],'visible','off');
ylabel('Cell numbers','visible','on','Fontsize',font,'Interpreter','latex');

% Commen X label 
p3=get(s3,'position');
p4=get(s4,'position');
width = p4(1)+p4(3)-p3(1); 
axes('position',[p3(1) p4(2) width p3(4)],'visible','off');
xlabel('Time in hours','FontSize',font','visible','on','Fontsize',font,'Interpreter','latex');

dimA = [0.1,0.88,0,0];
annotation('textbox',dimA,'String','A','FitBoxToText','on','EdgeColor','none','Fontsize',font3);
dimB = [0.55,0.88,0,0];
annotation('textbox',dimB,'String','B','FitBoxToText','on','EdgeColor','none','Fontsize',font3);
dimC = [0.1,0.43,0,0];
annotation('textbox',dimC,'String','C','FitBoxToText','on','EdgeColor','none','Fontsize',font3);
dimD = [0.55,0.43,0,0];
annotation('textbox',dimD,'String','D','FitBoxToText','on','EdgeColor','none','Fontsize',font3);

dimE = [0.15983490636163,0.922741667423646,0.23033018727674,0.077258332576354];
annotation('textbox',dimE,'String','Slow replication','FitBoxToText','on','EdgeColor','none','Fontsize',font3);
dimF = [0.612224687329657,0.922741667423646,0.225550625340686,0.077258332576354];
annotation('textbox',dimF,'String','Rapid replication','FitBoxToText','on','EdgeColor','none','Fontsize',font3);


dimG = [0.410113213199853,0.8627416654555,0.190319011593934,0.0772583345445];
annotation('textbox',dimG,'String','$\kappa_A = \kappa_B = 1$','FitBoxToText','on','EdgeColor','none','Fontsize',font3);
dimH = [0.410113213199853,0.4127,0.225550625340686,0.077258332576354];
annotation('textbox',dimH,'String','$\kappa_A = \kappa_B = 2$','FitBoxToText','on','EdgeColor','none','Fontsize',font3);


set(findall(fig1,'-property','Interpreter'),'Interpreter','latex');
set(findall(fig1,'-property','TickLabelInterpreter'),'TickLabelInterpreter','latex');
set(findall(fig1,'-property','YScale'),'YScale','log');
set(findall(fig1,'-property','YLim'),'YLim',[1,10^15]);
set(findall(fig1,'-property','XLim'),'XLim',[0,150]);
%% S26: Fast vs. Slow growth CS

% Treatment T1: 
P_f.PK(1).cycle = t1(1:20); 
P_f.PK(2).cycle = ~t1(1:20); 

P_s.PK(1).cycle = t1(1:20); 
P_s.PK(2).cycle = ~t1(1:20); 

P_f.PD(2).zMICB = 0.017; 
P_f.PD(3).zMICA = 0.017; 
P_s.PD(2).zMICB = 0.017; 
P_s.PD(3).zMICA = 0.017; 

c_val = 6.5*0.017;
P_f.PK(1).D = c_val; 
P_f.PK(2).D = c_val; 
P_s.PK(1).D = c_val; 
P_s.PK(2).D = c_val; 


[dyn1,data1] = Two_Drugs_Cycling_CC_u(P_f.G,P_f.INT,P_f.PD,P_f.PK,P_f.CC,P_f.M); 
[dyn2,data2] = Two_Drugs_Cycling_CC_u(P_s.G,P_s.INT,P_s.PD,P_s.PK,P_s.CC,P_s.M); 

P_f.PD(2).zMICB = 0.007; 
P_f.PD(3).zMICA = 0.007; 
P_s.PD(2).zMICB = 0.007; 
P_s.PD(3).zMICA = 0.007; 

[dyn3,data3] = Two_Drugs_Cycling_CC_u(P_f.G,P_f.INT,P_f.PD,P_f.PK,P_f.CC,P_f.M); 
[dyn4,data4] = Two_Drugs_Cycling_CC_u(P_s.G,P_s.INT,P_s.PD,P_s.PK,P_s.CC,P_s.M); 

 %%
fig1 = figure(1);  
fig1.Units = 'centimeter';
fig1.Position = [10,2,15.5,10]; 

s2 = subplot('Position',Pos(2,:)); 
plot(dyn1.time,dyn1.W,'Color',occa); 
hold on 
plot(dyn1.time,dyn1.MA,'Color',dark); 
hold on 
plot(dyn1.time,dyn1.MB,'Color',light); 
hold on 
plot(dyn1.time,dyn1.MAB,'Color',pink); 
hold on 
title('$\psi^W_{\mathrm{max}} = 0.88$'); 
leg1 = legend('Wildtyp','Mutant A','Mutant B','Mutant AB'); 
leg1.ItemTokenSize = [15 18];

s1 = subplot('Position',Pos(1,:)); 
plot(dyn2.time,dyn2.W,'Color',occa); 
hold on 
plot(dyn2.time,dyn2.MA,'Color',dark); 
hold on 
plot(dyn2.time,dyn2.MB,'Color',light); 
hold on 
plot(dyn2.time,dyn2.MAB,'Color',pink); 
hold on 
title('$\psi^W_{\mathrm{max}} = 0.088$'); 


s4 = subplot('Position',Pos(4,:)); 
plot(dyn3.time,dyn3.W,'Color',occa); 
hold on 
plot(dyn3.time,dyn3.MA,'Color',dark); 
hold on 
plot(dyn3.time,dyn3.MB,'Color',light); 
hold on 
plot(dyn3.time,dyn3.MAB,'Color',pink); 
hold on 
title('$\psi^W_{\mathrm{max}} = 0.88$'); 

s3 = subplot('Position',Pos(3,:)); 
plot(dyn4.time,dyn4.W,'Color',occa); 
hold on 
plot(dyn4.time,dyn4.MA,'Color',dark); 
hold on 
plot(dyn4.time,dyn4.MB,'Color',light); 
hold on 
plot(dyn4.time,dyn4.MAB,'Color',pink); 
hold on 
title('$\psi^W_{\mathrm{max}} = 0.088$'); 


% % Commen Y label
p1=get(s1,'position');
p2=get(s3,'position');
height=p1(2)+p1(4)-p2(2);
h15=axes('position',[p1(1) p2(2) p2(3) height],'visible','off');
ylabel('Cell numbers','visible','on','Fontsize',font,'Interpreter','latex');

% Commen X label 
p3=get(s3,'position');
p4=get(s4,'position');
width = p4(1)+p4(3)-p3(1); 
axes('position',[p3(1) p4(2) width p3(4)],'visible','off');
xlabel('Time in hours','FontSize',font','visible','on','Fontsize',font,'Interpreter','latex');

dimA = [0.09,0.88,0,0];
annotation('textbox',dimA,'String','A','FitBoxToText','on','EdgeColor','none','Fontsize',font3);
dimB = [0.54,0.88,0,0];
annotation('textbox',dimB,'String','B','FitBoxToText','on','EdgeColor','none','Fontsize',font3);
dimC = [0.09,0.43,0,0];
annotation('textbox',dimC,'String','C','FitBoxToText','on','EdgeColor','none','Fontsize',font3);
dimD = [0.54,0.43,0,0];
annotation('textbox',dimD,'String','D','FitBoxToText','on','EdgeColor','none','Fontsize',font3);

dimE = [0.15983490636163,0.922741667423646,0.23033018727674,0.077258332576354];
annotation('textbox',dimE,'String','Slow replication','FitBoxToText','on','EdgeColor','none','Fontsize',font3);
dimF = [0.612224687329657,0.922741667423646,0.225550625340686,0.077258332576354];
annotation('textbox',dimF,'String','Rapid replication','FitBoxToText','on','EdgeColor','none','Fontsize',font3);

dimG = [0.31067552362178,0.8627416654555,0.38919439075008,0.0772583345445];
annotation('textbox',dimG,'String','Without collateral sensitivity','FitBoxToText','on','EdgeColor','none','Fontsize',font3);
dimH = [0.330400793368676,0.412699998031854,0.349743851256288,0.0772583345445];
annotation('textbox',dimH,'String','With collateral sensitivity','FitBoxToText','on','EdgeColor','none','Fontsize',font3);


set(findall(fig1,'-property','Interpreter'),'Interpreter','latex');
set(findall(fig1,'-property','TickLabelInterpreter'),'TickLabelInterpreter','latex');
set(findall(fig1,'-property','YScale'),'YScale','log');
set(findall(fig1,'-property','YLim'),'YLim',[1,10^15]);
set(findall(fig1,'-property','XLim'),'XLim',[0,150]);
