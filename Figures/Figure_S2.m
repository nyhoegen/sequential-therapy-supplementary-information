% Skript to generate Figure 3
% Initialize Matlab:
clc
clear
close all
%% Load Data: 

%---Simulation Data: kappa = 1 ------------------------------------------- 
Sim1_C = load('Data/Hybrid_Simulations/Treatments12h_Cip/Sim_T1_2.mat'); 
Sim1_C = Sim1_C.Sim;
Dyn1_C = load('Data/Hybrid_Simulations/Treatments12h_Cip/Dyn_T1_2.mat'); 
Dyn1_C = Dyn1_C.Dyn; 

%T3 and T4
Sim2_C = load('Data/Hybrid_Simulations/Treatments12h_Cip/Sim_T3_4.mat'); 
Sim2_C = Sim2_C.Sim;
Dyn2_C = load('Data/Hybrid_Simulations/Treatments12h_Cip/Dyn_T3_4.mat'); 
Dyn2_C = Dyn2_C.Dyn; 

%T5 
Sim3_C = load('Data/Hybrid_Simulations/Treatments12h_Cip/Sim_T5.mat'); 
Sim3_C = Sim3_C.Sim;
Dyn3_C = load('Data/Hybrid_Simulations/Treatments12h_Cip/Dyn_T5.mat'); 
Dyn3_C = Dyn3_C.Dyn; 

% Combine:  
Sim_C = [Sim1_C;Sim2_C;Sim3_C]; 
Dyn_C = [Dyn1_C,Dyn2_C,Dyn3_C]; 
D_C = (0.08:0.01:0.4)/0.017;

%---Simulation Data: kappa = 2 ------------------------------------------- 
Sim1_F2 = load('Data/Hybrid_Simulations/Treatments12h_Fab2/Sim_T1_2.mat'); 
Sim1_F2 = Sim1_F2.Sim;
Dyn1_F2 = load('Data/Hybrid_Simulations/Treatments12h_Fab2/Dyn_T1_2.mat'); 
Dyn1_F2 = Dyn1_F2.Dyn; 

%T3 and T4
Sim2_F2 = load('Data/Hybrid_Simulations/Treatments12h_Fab2/Sim_T3_4.mat'); 
Sim2_F2 = Sim2_F2.Sim;
Dyn2_F2 = load('Data/Hybrid_Simulations/Treatments12h_Fab2/Dyn_T3_4.mat'); 
Dyn2_F2 = Dyn2_F2.Dyn; 

%T5 
Sim3_F2 = load('Data/Hybrid_Simulations/Treatments12h_Fab2/Sim_T5.mat'); 
Sim3_F2 = Sim3_F2.Sim;
Dyn3_F2 = load('Data/Hybrid_Simulations/Treatments12h_Fab2/Dyn_T5.mat'); 
Dyn3_F2 = Dyn3_F2.Dyn; 

% Compbine:  
Sim_F2 = [Sim1_F2;Sim2_F2;Sim3_F2]; 
Dyn_F2 = [Dyn1_F2,Dyn2_F2,Dyn3_F2]; 
D_F2 = (0.054:0.01:0.4)/0.017;

% Calculate mean values 
n_sim = 100; 
for i = 1:5
    for j = 1:length(D_C)
        Sim_t_erad_C(1,j) = mean(Sim_C{i,j},'omitnan'); 
        Sim_t_erad_std_C(1,j) = std(Sim_C{i,j},'omitnan'); 
    end
    Sim_mean_C{i} = Sim_t_erad_C; 
    Sim_std_C{i} = Sim_t_erad_std_C;
    Sim_sem_C{i} = Sim_t_erad_std_C/sqrt(n_sim);
end 
 

for i = 1:5
    for j = 1:length(D_F2)
        Sim_t_erad_F2(1,j) = mean(Sim_F2{i,j},'omitnan'); 
        Sim_t_erad_std_F2(1,j) = std(Sim_F2{i,j},'omitnan'); 
    end
    Sim_mean_F2{i} = Sim_t_erad_F2; 
    Sim_std_F2{i} = Sim_t_erad_std_F2;
    Sim_sem_F2{i} = Sim_t_erad_std_F2/sqrt(n_sim);
end

%% Plot Settings 

% Label for Legend 
treatments_sim = {'12 hours','24 hours','1.5 days','2 days','2.5 days','3 days','3.5 days','4 days','5 days'};

% Set the positions of the different subplots 
% length and height of sub-plots: 
sp_length = 0.32; 
sp_height = 0.14;
% position of subplots in x-direction 
x_pos1 = 0.15; 
x_pos2 = 0.55; 

% Position of the Subplots 
Position = [x_pos1,0.80,sp_length,sp_height;...
            x_pos2,0.80,sp_length,sp_height;...
            x_pos1,0.6125,sp_length,sp_height;...
            x_pos2,0.6125,sp_length,sp_height;...
            x_pos1,0.425,sp_length,sp_height;...
            x_pos2,0.425,sp_length,sp_height;...
            x_pos1,0.2375,sp_length,sp_height;...
            x_pos2,0.2375,sp_length,sp_height;...
            x_pos1,0.05,sp_length,sp_height;...
            x_pos2,0.05,sp_length,sp_height];

%Colors
dark = [2 93 114]/255;
pink = [178 93 141]/255;
light = [121 182 185]/255; 
occa = [215,200,100]/255;
black = [0 0 0];
% Define a color vector 
col = [dark;pink;light;occa];
% Define a colormap
col_n=4;
mymap1 = [linspace(black(1),pink(1),6)', linspace(black(2),pink(2),6)',linspace(black(3),pink(3),6)'];
mymap2 = [linspace(pink(1),occa(1),col_n)', linspace(pink(2),occa(2),col_n)',linspace(pink(3),occa(3),col_n)'];
mymap3 = [linspace(occa(1),dark(1),col_n)', linspace(occa(2),dark(2),col_n)',linspace(occa(3),dark(3),col_n)'];
mymap4 = [linspace(dark(1),light(1),col_n)', linspace(dark(2),light(2),col_n)',linspace(dark(3),light(3),col_n)'];
mymap5 = [linspace(light(1),light(1),col_n)', linspace(light(2),light(2),col_n)',linspace(light(3),light(3),col_n)'];
mymap = [mymap1;mymap2;mymap3;mymap4;mymap5];

% Linewidth
set(0, 'DefaultLineLineWidth', 1.5);
% Fontsizes
font = 10; 
font2 = 8;
font3 = 12;

% Selection of Colors
ind_col = [3,6,9,12,16,18];
%% Plot Figure 

fig2 = figure(2);
% Set size of the figure 
fig2.Units = 'centimeters';
fig2.Position=[10,2,15.5,18];

for i = 1:5
    h1(i)=subplot('Position',Position(i*2-1,:));
    plot(D_C,Sim_mean_C{i},':','Color',mymap(ind_col(i),:),'linewidth',1)
    hold on
    errorbar(D_C(1:2:end),Sim_mean_C{i}(1:2:end),Sim_std_C{i}(1:2:end),'.','Color',mymap(ind_col(i),:),'linewidth',1,'CapSize',4);
    hold on 
     plot(D_C,Dyn_C{i},'Color',[0.2,0.2,0.2,0.7]);
    hold on 
    set(gca,'FontSize',font2); 
%     set(gca,'YScale','log','YTick',10.^(0:2))
    str = strcat('Switch after',{' '},treatments_sim{i});
    title(str,'FontSize',font)
    ylim([0,630]); 
    xlim([3,24])
    if i<5
        set(gca,'XTickLabel',[]);
    end
    if i == 3
    ylabel('Time until threshold $N_c$ in hours','visible','on','Fontsize',font,'Interpreter','latex');
    end
end

for i = 1:5
    h2(i)=subplot('Position',Position(i*2,:));
    plot(D_F2,Sim_mean_F2{i},':','Color',mymap(ind_col(i),:),'linewidth',1)
    hold on 
    errorbar(D_F2(1:2:end),Sim_mean_F2{i}(1:2:end),Sim_std_F2{i}(1:2:end),'.','Color',mymap(ind_col(i),:),'linewidth',1,'CapSize',4);
    hold on 
    plot(D_F2,Dyn_F2{i},'Color',[0.2,0.2,0.2,0.7])
    hold on 
    set(gca,'FontSize',font2);
%     set(gca,'YScale','log')
    str = strcat('Switch after',{' '},treatments_sim{i});
    title(str,'FontSize',font)
    ylim([0,460]);
    xlim([2.5,24])
    if i < 5
        set(gca,'XTickLabel',[]);
    end
end


% Commen X label 
p3=get(h1(5),'position');
p4=get(h2(5),'position');
width = p4(1)+p4(3)-p3(1); 
axes('position',[p3(1) p4(2) width p3(4)],'visible','off');
xlabel('Drug dose $D$ in multiples of zMIC$_W$','FontSize',font','visible','on','Fontsize',font,'Interpreter','latex');

% Annotate 'A','B' etc.
annotation('textbox',[0.14,0.97,0,0],'String','A','FitBoxToText','on','EdgeColor','none','Fontsize',font3);
annotation('textbox',[0.14,0.7825,0,0],'String','B','FitBoxToText','on','EdgeColor','none','Fontsize',font3);
annotation('textbox',[0.14,0.595,0,0],'String','C','FitBoxToText','on','EdgeColor','none','Fontsize',font3);
annotation('textbox',[0.14,0.4075,0,0],'String','D','FitBoxToText','on','EdgeColor','none','Fontsize',font3);
annotation('textbox',[0.14,0.22,0,0],'String','E','FitBoxToText','on','EdgeColor','none','Fontsize',font3);
annotation('textbox',[0.54,0.97,0,0],'String','F','FitBoxToText','on','EdgeColor','none','Fontsize',font3);
annotation('textbox',[0.54,0.7825,0,0],'String','G','FitBoxToText','on','EdgeColor','none','Fontsize',font3);
annotation('textbox',[0.54,0.595,0,0],'String','H','FitBoxToText','on','EdgeColor','none','Fontsize',font3);
annotation('textbox',[0.54,0.4075,0,0],'String','I','FitBoxToText','on','EdgeColor','none','Fontsize',font3);
annotation('textbox',[0.54,0.22,0,0],'String','J','FitBoxToText','on','EdgeColor','none','Fontsize',font3);

annotation('textbox',[0.22,1,0,0],'String','$\kappa_A = \kappa_B = 1$','FitBoxToText','on','EdgeColor','none','Fontsize',font3);
annotation('textbox',[0.62,1,0,0],'String','$\kappa_A = \kappa_B = 2$','FitBoxToText','on','EdgeColor','none','Fontsize',font3);

set(findall(fig2,'-property','Interpreter'),'Interpreter','latex');
set(findall(fig2,'-property','TickLabelInterpreter'),'TickLabelInterpreter','latex');

