% Skript to generate Figure 6 
% Initialize Matlab 
close all 
clear  

%% Plot Settings 

% Label for Legend 
treatments = {'12h','24h','1.5d','2d','2.5d','3d','3.5d','4d','5d','6d','7d','8d','9d'};
% Set the positions of the different subplots 
% length and height of sub-plots: 
sp_length = 0.35; 
sp_height = 0.2;
% position of subplots in x-direction 
x_pos1 = 0.08; 
x_pos2 = 0.5; 

% Position of the Subplots 
Position = [x_pos1,0.7,sp_length,sp_height;...
            x_pos2,0.7,sp_length,sp_height;...
            x_pos1,0.4,sp_length,sp_height;...
            x_pos2,0.4,sp_length,sp_height;...
            x_pos1,0.1,sp_length,sp_height;...
            x_pos2,0.1,sp_length,sp_height];

        
%Colors
dark = [2 93 114]/255;
pink = [178 93 141]/255;
light = [121 182 185]/255; 
occa = [215,200,100]/255;
black = [0 0 0];
% Define a color vector 
col = [dark;pink;light;occa];
% Define a colormap
col_n=4;
mymap1 = [linspace(black(1),pink(1),6)', linspace(black(2),pink(2),6)',linspace(black(3),pink(3),6)'];
mymap2 = [linspace(pink(1),occa(1),col_n)', linspace(pink(2),occa(2),col_n)',linspace(pink(3),occa(3),col_n)'];
mymap3 = [linspace(occa(1),dark(1),col_n)', linspace(occa(2),dark(2),col_n)',linspace(occa(3),dark(3),col_n)'];
mymap4 = [linspace(dark(1),light(1),col_n)', linspace(dark(2),light(2),col_n)',linspace(dark(3),light(3),col_n)'];
mymap5 = [linspace(light(1),light(1),col_n)', linspace(light(2),light(2),col_n)',linspace(light(3),light(3),col_n)'];
mymap = [mymap1;mymap2;mymap3;mymap4;mymap5];

% Linewidth
set(0, 'DefaultLineLineWidth', 1.5);
% Fontsizes
font = 10; 
font2 = 8;
font3 = 12;

% Selection of Colors
ind_col = [3,6,9,12,16];
grey_mat = flipud(gray(14)); 
%% Load Data 
dec = 2; 
%---Kappa = 1---------------------------------------------------------------
% Define Data Path
dataPath = 'Data/Treatments12h_Cip';
P = load(strcat(dataPath,'/Data_P_C0_06.mat')); 
P = P.P_data;
L = load(strcat(dataPath,'/Data_L_C0_06.mat')); 
L  = L .P_data;

% CS
P_wCS = load(strcat(dataPath,'/Data_P_wCS_C0_06.mat')); 
L_wCS = load(strcat(dataPath,'/Data_L_wCS_C0_06.mat'));
P_sCS = load(strcat(dataPath,'/Data_P_sCS_C0_06.mat')); 
L_sCS = load(strcat(dataPath,'/Data_L_sCS_C0_06.mat')); 
P_wCS = P_wCS.P_data;
L_wCS = L_wCS.P_data;
P_sCS = P_sCS.P_data;
L_sCS = L_sCS.P_data;

% DDI
P_DDIa = load(strcat(dataPath,'/Data_P_DDIa_C0_06.mat')); 
P_DDIs = load(strcat(dataPath,'/Data_P_DDIs_C0_06.mat')); 
P_DDIa = P_DDIa.P_data;
P_DDIs = P_DDIs.P_data;

% DDI and sCS
P_sCS_DDIa = load(strcat(dataPath,'/Data_P_sCS_DDIa_C0_06.mat')); 
P_sCS_DDIs = load(strcat(dataPath,'/Data_P_sCS_DDIs_C0_06.mat'));  
P_sCS_DDIa = P_sCS_DDIa.P_data;
P_sCS_DDIs = P_sCS_DDIs.P_data;


%---Kappa = 1---------------------------------------------------------------
% Define Data Path
dataPath = 'Data/Treatments12h_Cip_sgv';
P_sgv = load(strcat(dataPath,'/Data_P_C0_06.mat')); 
P_sgv = P_sgv.P_data;
L_sgv = load(strcat(dataPath,'/Data_L_C0_06.mat')); 
L_sgv  = L_sgv .P_data;

% CS
P_wCS_sgv = load(strcat(dataPath,'/Data_P_wCS_C0_06.mat')); 
L_wCS_sgv = load(strcat(dataPath,'/Data_L_wCS_C0_06.mat'));
P_sCS_sgv = load(strcat(dataPath,'/Data_P_sCS_C0_06.mat')); 
L_sCS_sgv = load(strcat(dataPath,'/Data_L_sCS_C0_06.mat')); 
P_wCS_sgv = P_wCS_sgv.P_data;
L_wCS_sgv = L_wCS_sgv.P_data;
P_sCS_sgv = P_sCS_sgv.P_data;
L_sCS_sgv = L_sCS_sgv.P_data;

% DDI
P_DDIa_sgv = load(strcat(dataPath,'/Data_P_DDIa_C0_06.mat')); 
P_DDIs_sgv = load(strcat(dataPath,'/Data_P_DDIs_C0_06.mat')); 
P_DDIa_sgv = P_DDIa_sgv.P_data;
P_DDIs_sgv = P_DDIs_sgv.P_data;

%---Kappa = 1---------------------------------------------------------------
% Define Data Path
dataPath = 'Data/Treatments12h_Cip_sgv2';
P_sgv2 = load(strcat(dataPath,'/Data_P_C0_06.mat')); 
P_sgv2 = P_sgv2.P_data;
L_sgv2 = load(strcat(dataPath,'/Data_L_C0_06.mat')); 
L_sgv2  = L_sgv2.P_data;

% CS
P_wCS_sgv2 = load(strcat(dataPath,'/Data_P_wCS_C0_06.mat')); 
L_wCS_sgv2 = load(strcat(dataPath,'/Data_L_wCS_C0_06.mat'));
P_sCS_sgv2 = load(strcat(dataPath,'/Data_P_sCS_C0_06.mat')); 
L_sCS_sgv2 = load(strcat(dataPath,'/Data_L_sCS_C0_06.mat')); 
P_wCS_sgv2 = P_wCS_sgv2.P_data;
L_wCS_sgv2 = L_wCS_sgv2.P_data;
P_sCS_sgv2 = P_sCS_sgv2.P_data;
L_sCS_sgv2 = L_sCS_sgv2.P_data;

% DDI
P_DDIa_sgv2 = load(strcat(dataPath,'/Data_P_DDIa_C0_06.mat')); 
P_DDIs_sgv2 = load(strcat(dataPath,'/Data_P_DDIs_C0_06.mat')); 
P_DDIa_sgv2 = P_DDIa_sgv2.P_data;
P_DDIs_sgv2 = P_DDIs_sgv2.P_data;
%---Kappa = 2---------------------------------------------------------------
% Define Data Path
dataPath = 'Data/Treatments12h_Fab2';
P_F = load(strcat(dataPath,'/Data_P_C0_06.mat')); 
P_F = P_F.P_data;
L_F = load(strcat(dataPath,'/Data_L_C0_06.mat')); 
L_F  = L_F.P_data;

% CS
P_wCS_F = load(strcat(dataPath,'/Data_P_wCS_C0_06.mat')); 
L_wCS_F = load(strcat(dataPath,'/Data_L_wCS_C0_06.mat'));
P_sCS_F = load(strcat(dataPath,'/Data_P_sCS_C0_06.mat')); 
L_sCS_F = load(strcat(dataPath,'/Data_L_sCS_C0_06.mat')); 
P_wCS_F = P_wCS_F.P_data;
L_wCS_F = L_wCS_F.P_data;
P_sCS_F = P_sCS_F.P_data;
L_sCS_F= L_sCS_F.P_data;

% DDI
P_DDIa_F = load(strcat(dataPath,'/Data_P_DDIa_C0_06.mat')); 
P_DDIs_F = load(strcat(dataPath,'/Data_P_DDIs_C0_06.mat')); 
P_DDIa_F = P_DDIa_F.P_data;
P_DDIs_F= P_DDIs_F.P_data;

% DDI and sCS
P_sCS_DDIa_F = load(strcat(dataPath,'/Data_P_sCS_DDIa_C0_06.mat')); 
P_sCS_DDIs_F = load(strcat(dataPath,'/Data_P_sCS_DDIs_C0_06.mat'));  
P_sCS_DDIa_F = P_sCS_DDIa_F.P_data;
P_sCS_DDIs_F = P_sCS_DDIs_F.P_data;

%---Kappa = 2---------------------------------------------------------------
% Define Data Path
dataPath = 'Data/Treatments12h_Fab2_sgv';
P_F_sgv = load(strcat(dataPath,'/Data_P_C0_06.mat')); 
P_F_sgv = P_F_sgv.P_data;
L_F_sgv = load(strcat(dataPath,'/Data_L_C0_06.mat')); 
L_F_sgv  = L_F_sgv.P_data;

% CS
P_wCS_F_sgv = load(strcat(dataPath,'/Data_P_wCS_C0_06.mat')); 
L_wCS_F_sgv = load(strcat(dataPath,'/Data_L_wCS_C0_06.mat'));
P_sCS_F_sgv = load(strcat(dataPath,'/Data_P_sCS_C0_06.mat')); 
L_sCS_F_sgv= load(strcat(dataPath,'/Data_L_sCS_C0_06.mat')); 
P_wCS_F_sgv = P_wCS_F_sgv.P_data;
L_wCS_F_sgv = L_wCS_F_sgv.P_data;
P_sCS_F_sgv = P_sCS_F_sgv.P_data;
L_sCS_F_sgv= L_sCS_F_sgv.P_data;

% DDI
P_DDIa_F_sgv = load(strcat(dataPath,'/Data_P_DDIa_C0_06.mat')); 
P_DDIs_F_sgv = load(strcat(dataPath,'/Data_P_DDIs_C0_06.mat')); 
P_DDIa_F_sgv = P_DDIa_F_sgv.P_data;
P_DDIs_F_sgv= P_DDIs_F_sgv.P_data;

% Define Data Path
dataPath = 'Data/Treatments12h_Fab2_sgv2';
P_F_sgv2 = load(strcat(dataPath,'/Data_P_C0_06.mat')); 
P_F_sgv2 = P_F_sgv2.P_data;
L_F_sgv2 = load(strcat(dataPath,'/Data_L_C0_06.mat')); 
L_F_sgv2  = L_F_sgv2.P_data;

% CS
P_wCS_F_sgv2 = load(strcat(dataPath,'/Data_P_wCS_C0_06.mat')); 
L_wCS_F_sgv2 = load(strcat(dataPath,'/Data_L_wCS_C0_06.mat'));
P_sCS_F_sgv2 = load(strcat(dataPath,'/Data_P_sCS_C0_06.mat')); 
L_sCS_F_sgv2= load(strcat(dataPath,'/Data_L_sCS_C0_06.mat')); 
P_wCS_F_sgv2 = P_wCS_F_sgv2.P_data;
L_wCS_F_sgv2 = L_wCS_F_sgv2.P_data;
P_sCS_F_sgv2 = P_sCS_F_sgv2.P_data;
L_sCS_F_sgv2= L_sCS_F_sgv2.P_data;

% DDI
P_DDIa_F_sgv2 = load(strcat(dataPath,'/Data_P_DDIa_C0_06.mat')); 
P_DDIs_F_sgv2 = load(strcat(dataPath,'/Data_P_DDIs_C0_06.mat')); 
P_DDIa_F_sgv2 = P_DDIa_F_sgv2.P_data;
P_DDIs_F_sgv2= P_DDIs_F_sgv2.P_data;

n = 200;
D1 = [linspace(0,0.2,n);linspace(0,0.2,n)];
D2 = [linspace(0.2,0.6,n);linspace(0.2,0.6,n)]; 
D = [D1(:,1:end-1),D2];
% Scale Drug Concnetration in fold change of MIC
D = D./0.017; 

%% Figure 1 - with right kappa 2
ind = [15,12,6,18];
marker = {'o','x','s','*'};
marker2 = {'^','<','>','v'};

% Set the positions of the different subplots 
% length and height of sub-plots: 
sp_length = 0.4; 
sp_height = 0.7;
% position of subplots in x-direction 
x_pos1 = 0.08; 
x_pos2 = 0.54; 

% Position of the Subplots 
Position = [x_pos1,0.15,sp_length,sp_height;...
            x_pos2,0.15,sp_length,sp_height];

% Concentartions: 
c1_ind = 141; 
c3_ind = 71; 

fig1=figure(1); 
% Set size of the figure 
fig1.Units = 'centimeters';
fig1.Position=[10,2,15.5,8]; 

treat = [0.5:0.5:3.5,4:9];
% Subplot 1 
for i = 1:13
    y1(1,i) = P(i).t_erad(c1_ind); 
    y1(2,i) = P_sCS(i).t_erad(c1_ind); 
    y1(3,i) = P_DDIa(i).t_erad(c1_ind); 
    y1(4,i) = P_DDIs(i).t_erad(c1_ind); 
    
    y2(1,i) = P_F(i).t_erad(c3_ind); 
    y2(2,i) = P_sCS_F(i).t_erad(c3_ind); 
    y2(3,i) = P_DDIa_F(i).t_erad(c3_ind); 
    y2(4,i) = P_DDIs_F(i).t_erad(c3_ind); 
end


for i = 1:4
    [~,temp1] = min(y1(i,:)); 
    y1_opt(i) = treat(temp1);
    [~,temp2] = min(y2(i,:)); 
    y2_opt(i) = treat(temp2);
end

s1=subplot('Position',Position(1,:));
for i = 1:4 
    plot(treat,y1(i,:),'--','Color',[mymap(ind(i),:),0.4])
    hold on 
end
for i = 1:4 
    plots(i) = plot(treat,y1(i,:),marker{i},'Color',mymap(ind(i),:)); 
    hold on 
end


ylabel('Time until threshold $N_c=1$ in hours');
title('$\kappa_A=\kappa_B = 1, \ D = 8.27 \cdot$ zMIC$_{W}$'); 
l1 = legend(plots,'Bliss Independence','Collateral Sensitivity','Antagonsim','Synergism','Location','southeast');
l1.ItemTokenSize = [15 18];


x_opt = [10,10,20,10];
s2 = subplot('Position',Position(2,:));
for i = 1:4 
    plot(treat,y2(i,:),'--','Color',[mymap(ind(i),:),0.4])
    hold on 
end
for i = 1:4 
     plot(treat,y2(i,:),marker{i},'Color',mymap(ind(i),:));
    hold on 
end
title('$\kappa_A=\kappa_B = 2, \ D = 4.14 \cdot$ zMIC$_{W}$'); 
% Commen X label 
p3=get(s1,'position');
p4=get(s2,'position');
width = p4(1)+p4(3)-p3(1); 
axes('position',[p3(1) p4(2) width p3(4)],'visible','off');
xlabel('Time between switches in days','FontSize',font','visible','on','Fontsize',font,'Interpreter','latex');


set(findall(fig1,'-property','YLim'),'YLim',[0,275]);
set(findall(fig1,'-property','FontSize'),'FontSize',font);

%Annotate 'A' and 'B' 
annotation('textbox',[0.07,0.93,0,0],'String','A','FitBoxToText','on','EdgeColor','none','Fontsize',font3);
annotation('textbox',[0.53,0.93,0,0],'String','B','FitBoxToText','on','EdgeColor','none','Fontsize',font3);
y_pos1 = [0.12,0.14;0.09,0.11;0.12,0.14;0.06,0.08];
y_pos2 = [0.12,0.14;0.12,0.14;0.09,0.11;0.12,0.14];
for i = 1:4 
    x_pos1 = y1_opt(i)*0.04+0.08; 
    x_pos2 = y2_opt(i)*0.04+0.54; 
    annotation('arrow',[x_pos1,x_pos1],y_pos1(i,:),'Color',mymap(ind(i),:),'HeadLength',7,'HeadWidth',7);
    annotation('arrow',[x_pos2,x_pos2],y_pos2(i,:),'Color',mymap(ind(i),:),'HeadLength',7,'HeadWidth',7);
end
set(findall(fig1,'-property','Interpreter'),'Interpreter','latex');
set(findall(fig1,'-property','TickLabelInterpreter'),'TickLabelInterpreter','latex');
