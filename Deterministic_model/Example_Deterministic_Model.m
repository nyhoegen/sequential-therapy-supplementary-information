% Example Skript how to run the Code 

% Initialize Matlab 
clear all 
close all 

%% Load Parameter 

% Dataset for the Drug Cip: 
Cip = load('Cip.mat'); 

% Contains Patient and Lab Data
P = Cip.P; 
L = Cip.L;

% Change the Drug concnetration in Patient and Lab 
P.PK(1).D = 0.1; 
P.PK(2).D = 0.1; 
L.Lab.cA = 0.1;
L.Lab.cB = 0.1;

% Change the Time between administrations (default 24h)
P.PK(1).Tthis = 12; 
P.PK(1).Tother = 12; 
P.PK(2).Tthis = 12; 
P.PK(2).Tother = 12; 
L.Lab.TA = 12;
L.Lab.TB = 12;

% Define a treatment (the two drugs are encoded with 1 and 0) 
t1 = repmat([ones(1,1) zeros(1,1)],[1 10]);
t2 = repmat([1 1 1 1 0 0 0 0],[1 5]);

% Change the treatment in the patient 
P.PK(1).cycle = t1;
P.PK(2).cycle = ~t1; %IMPORTANT change the numbering here. 1 is always the drug of that repective PK set


%% Deterministic Model 

% Patient Example 
[dyn,data] = Two_Drugs_Cycling_CC_u(P.G,P.INT,P.PD,P.PK,P.CC,P.M); 
% Plot the dynamics 
Plot_Cycling_Data(dyn,data,1,'Pat')

% Lab Example 
[dyn2,data2] = Lab_Cycling_NumCC_u(L.G,L.PD,L.Lab,L.CC,L.M); 
% Plot the dynamics 
Plot_Cycling_Data(dyn2,data2,2,'Lab')

