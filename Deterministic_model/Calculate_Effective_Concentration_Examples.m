% Generate First effective Concentration for all Datasets
% Need to be run with the data folder present

%Initialize Matlab 
clc 
clear all 
close all 
%% Load Data: Cip
% Define Data Path
dataPath = 'Data/Treatments12h_Cip';

% Basic
P = load(strcat(dataPath,'/Data_P_C0_06.mat')); 
P =P.P_data;
L = load(strcat(dataPath,'/Data_L_C0_06.mat')); 
L = L.P_data;

% CS
P_wCS = load(strcat(dataPath,'/Data_P_wCS_C0_06.mat')); 
L_wCS = load(strcat(dataPath,'/Data_L_wCS_C0_06.mat'));
P_sCS = load(strcat(dataPath,'/Data_P_sCS_C0_06.mat')); 
L_sCS = load(strcat(dataPath,'/Data_L_sCS_C0_06.mat')); 
P_wCS = P_wCS.P_data;
L_wCS = L_wCS.P_data;
P_sCS = P_sCS.P_data;
L_sCS = L_sCS.P_data;
P_DDIa = load(strcat(dataPath,'/Data_P_DDIa_C0_06.mat')); 
P_DDIs = load(strcat(dataPath,'/Data_P_DDIs_C0_06.mat')); 
P_DDIa = P_DDIa.P_data;
P_DDIs = P_DDIs.P_data;

n = 200;
D1 = [linspace(0,0.2,n);linspace(0,0.2,n)];
D2 = [linspace(0.2,0.6,n);linspace(0.2,0.6,n)]; 
D = [D1(:,1:end-1),D2];


%% Effective Drug Concentration 

for j = 1:16
        % Basic Patient 
        if j < 16
            temp_t_erad = P(j).t_erad;
            last_nan = find(isnan(temp_t_erad)==1,1,'last');
            if last_nan == length(P(j).t_erad)
                last_nan = last_nan-1;
            end
            Effective_C.Basic(1,j) = D(1,last_nan+1);
       
            % sCS Patient 
            temp_t_erad = P_sCS(j).t_erad;
            last_nan = find(isnan(temp_t_erad)==1,1,'last');
            if last_nan == length(P_sCS(j).t_erad)
                last_nan = last_nan-1;
            end
            Effective_C.sCS(1,j) = D(1,last_nan+1);

            if j < 15
                % DDIa Patient 
                temp_t_erad = P_DDIa(j).t_erad;
                last_nan = find(isnan(temp_t_erad)==1,1,'last');
                if last_nan == length(P_DDIa(j).t_erad)
                    last_nan = last_nan-1;
                end
                Effective_C.DDIa(1,j) = D(1,last_nan+1);
            else 
                Effective_C.DDIa(1,j) = 0.6486;
            end

            % DDIs Patient 
            temp_t_erad = P_DDIs(j).t_erad;
            last_nan = find(isnan(temp_t_erad)==1,1,'last');
            if last_nan == length(P_DDIs(j).t_erad)
                last_nan = last_nan-1;
            end
            Effective_C.DDIs(1,j) = D(1,last_nan+1);
        else
            % For j = 16 not within the drug range! 
                Effective_C.Basic(1,j) = 0.6571;
                Effective_C.sCS(1,j) = 0.6571; 
                Effective_C.DDIa(1,j) = 0.6571; 
                Effective_C.DDIs(1,j) = 0.6571; 
        end 
        
        % Basic Lab
        temp_t_erad = L(j).t_erad;
        last_nan = find(isnan(temp_t_erad)==1,1,'last');
        if last_nan == length(P(j).t_erad)
            last_nan = last_nan-1;
        end
        Effective_C_Lab.Basic(1,j) = D(1,last_nan+1);
        
        % sCS Lab 
        temp_t_erad = L_sCS(j).t_erad;
        last_nan = find(isnan(temp_t_erad)==1,1,'last');
        if last_nan == length(P_sCS(j).t_erad)
            last_nan = last_nan-1;
        end
        Effective_C_Lab.sCS(1,j) = D(1,last_nan+1);
end
    

%% SAVE DATA 
 path = 'Data_Cluster/Final_Data/EffectiveC_Cip.mat';
 save(path,'Effective_C');

 path = 'Data_Cluster/Final_Data/EffectiveC_Lab_Cip.mat';
 save(path,'Effective_C_Lab');
 
%% 
% ==========================================================================
%        For the type specific data (no mutations)
% ==========================================================================
clc
clear
%% Load Data CIP PAT
dataPath = 'Data/Treatments12h_Cip_Types';

% Basic Data 
basic_W = load(strcat(dataPath,'/Data_P_W.mat')); 
basic_W =basic_W.P_data;

basic_MAB = load(strcat(dataPath,'/Data_P_MAB.mat')); 
basic_MAB =basic_MAB.P_data;


%% Get C* values: 
% 1. Drug Concnetration one D = [0.017:0.001:0.2;0.017:0.001:0.2];
% BASIC W,BASIC MA, BASIC MB, CS MA, CS MB, SYN MA, SYN MB 
D1 = [0.017:0.001:0.2;0.017:0.001:0.2];
D2 = [1.5:0.001:2;1.5:0.001:2];

for i = 1:16
    % Basic W
    ind_basic_W = find(isnan(basic_W(i).t_erad),1,'last')+1;
    C_star.Basic_W(1,i) = D1(1,ind_basic_W);   
    
    % Basic MAB
    ind_basic_MAB = find(basic_MAB(i).N_end<1,1,'first');
    C_star.Basic_MAB(1,i) = D2(1,ind_basic_MAB); 
   
end


%% Scale and Plot data 

scale_MIC = @(x) x./0.017; 
C_star2 = structfun(@(s) scale_MIC(s),C_star,'UniformOutput',false);

%% SAVE DATA 
 path1 = 'Data_Cluster/Final_Data/C_star_Cip.mat';
 save(path1,'C_star');
 path2 = 'Data_Cluster/Final_Data/C_star_MIC_Cip.mat';
 save(path2,'C_star2');
 
 
