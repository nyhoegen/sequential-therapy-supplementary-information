function [] = Plot_Cycling_Data(dyn,data,f,id)
% 
% Function to plot data of Two_Drugs_Cycling function
%
% INPUT
% 
% dyn  = 1x1 struct array with cell numbers for the three types 
% data = 1x1 struct array 
%        containing concntrations of the drugs and growth rates depending
%        on each drug an the combined growth rate 
% f    = value for figure number
% 
% OUTPUT 
% fig
% 
%==========================================================================

%==========================================================================
% COLORS
%==========================================================================
dark = [2 93 114]/255;
pink = [178 93 141]/255;
light = [121 182 185]/255; 
blue2 = [3,137,169]/255;
occa = [215,200,100]/255;
skin = [210,140,140]/255;
mymap1 = [linspace(light(1),pink(1),20)', linspace(light(2),pink(2),20)',linspace(light(3),pink(3),20)'];

%==========================================================================
% FIGURE SETTINGS
%==========================================================================

% Linewidth
set(0, 'DefaultLineLineWidth', 1.25);
% Fontsize
font = 8; 
font2 = 8;

% length of xaxis
cyc_A = data.TA*data.cycle;
cyc_B = data.TB*~data.cycle;
x_ax = sum([cyc_A,cyc_B]);


% Xticks
cycle_img = repelem(~data.cycle,cyc_A+cyc_B);

tri_mat = triu(ones(length(data.cycle)),1);
% Calculate at which time the considered drug is aministered during cycle
ad_times = (data.cycle*data.TA+(~data.cycle)*data.TB)*tri_mat;
tick_val = ad_times+data.cycle*data.TA/2+~data.cycle*data.TB/2;

for i =1:length(data.cycle)
    if data.cycle(i)
        cyc{i} = 'A';
    else
        cyc{i} = 'B';
    end
end 
% 


%==========================================================================
% FIGURE
%==========================================================================

n = length(data.c_A);
figure(f) 
% --------- Subplot 1----------------
subplot('Position',[0.15,0.90,0.7,0.025])
imagesc(cycle_img)
title('Cycling Schedule', 'FontSize', font)
set(gca,'Fontsize', font2);
set(gca,'YTickLabel',[]);
xticks(tick_val);
xticklabels(cyc);
colormap(mymap1)
% --------- Subplot 2----------------
subplot('Position',[0.15,0.725,0.7,0.1])
plot(1:n,data.c_A, 'Color',light)
hold on 
plot(1:n,data.c_B, 'Color', pink)
title('Drug Concentrations','FontSize', font)
legend('Drug A','Drug B','Location','best','FontSize', font2); 
ylabel('\mug/ml', 'FontSize', font)
set(gca,'xticklabel', {[]});
xlim([0 x_ax])
set(gca,'Fontsize', font2);
% --------- Subplot 3----------------
if strcmp(id,'LAB')
    n = length(data.psi_W);
    subplot('Position',[0.15,0.475,0.7,0.2])
    set(gca,'Fontsize', font2);
    plot(dyn.time,data.psi_W, 'Color', occa)
    hold on 
    plot(dyn.time,data.psi_MA, 'Color', dark)
    hold on 
    plot(dyn.time,data.psi_MB, 'Color', light)
    hold on 
    plot(dyn.time,data.psi_MAB, 'Color', pink)
    hold on 
    set(gca,'xticklabel', {[]});
    ylabel('\psi(t)', 'FontSize',font)
    title('Per Capita Growth Rate', 'FontSize', font)
    legend('W','MA','MB','MAB','FontSize', font2);
    xlim([0 x_ax])
     set(gca,'Fontsize', font2);
else 
    subplot('Position',[0.15,0.475,0.7,0.2])
    set(gca,'Fontsize', font2);
    plot(dyn.time,data.psi_W, 'Color', occa)
    hold on 
    plot(dyn.time,data.psi_MA, 'Color', dark)
    hold on 
    plot(dyn.time,data.psi_MB, 'Color', light)
    hold on 
    plot(dyn.time,data.psi_MAB, 'Color', pink)
    hold on 
    set(gca,'xticklabel', {[]});
    ylabel('\psi(t)', 'FontSize',font)
    title('Per Capita Growth Rate', 'FontSize', font)
    legend('W','MA','MB','MAB','FontSize', font2);
    xlim([0 x_ax])
     set(gca,'Fontsize', font2);
end

% --------- Subplot 4---------------- 
subplot('Position',[0.15,0.1,0.7,0.325])
plot(dyn.time,dyn.W,'Color',occa)
hold on 
plot(dyn.time,dyn.MA,'Color',dark)
hold on
plot(dyn.time,dyn.MB,'Color',light)
hold on
plot(dyn.time,dyn.MAB,'Color',pink)
hold on
% plot(dyn.time,dyn.W+dyn.MA+dyn.MB+dyn.MAB,'--','Color', blue2);
% hold on
legend('Wildetype','Mutant A','Mutant B','Mutant AB','Location','best','FontSize', font2);
xlabel('Time in h','FontSize', font)
ylabel('Cell Numbers','FontSize', font)
title('Dynamics during Cycling Treatment','FontSize', font)
%ylim([0,10^6])
set(gca, 'YScale', 'log')






end
