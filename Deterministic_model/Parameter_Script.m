% Skipt with a list of all Parameter and instruction to save the struct
% array as .mat file
% Paramter and their origin are described in the main Text of the
% publication 

%==========================================================================
% LOAD PARAMETER: CIP 
%==========================================================================

% General values 
% struct 1x1 containing N0 the fractions p0 and mutation probabilities u 

G.N0 = 10^10;            %Size of initial Population 
G.p0A = 0;               %Fraction of Mutants MA in intial Population
G.p0B = 0;               %Fraction of Mutants MB in intial Population
G.p0AB = 0;              %Fraction of Mutants MAB in intial Population
G.uA = 10^-9;            %Mutation probability of resistance to Drug A 
G.uB = 10^-9;            %Mutation probability of resistance to Drug B 
 

%--------------------------------------------------------------------------
% IND parameter values: 
% 1x2 struct, one for each Drugs (A and B) 

% Effect of B on A (values for Psi_A): 
INT(1).I = 0 ;          %Maximum interaction of Drug B on A
INT(1).EC50 = -0.8;     %Concentration to achieve 50% of i
INT(1).k = 1;           %Hill coefficient

% Effect of A on B (values for Psi_B): 
INT(2).I = 0;           %Maximum interaction of Drug A on B
INT(2).EC50 = -0.8;     %Concentration to achieve 50% of i
INT(2).k = 1;           %Hill coefficient

%--------------------------------------------------------------------------
% PD values: 
% 1x4 struct, one for each type (W,MA,MB,MAB)

% Data for Wildetype for Ciprofloxacin und Streptomycin (REGOES et al)
% Wildeptype: 
PD(1).Psi_max = 0.088;  %Maximum growth rate in absence of Antibiotic 
PD(1).mu_0 = 0.01;      %Intrinsic death rate of the bacteria 
PD(1).Psi_minA = -6.5;  %Maximum kill rate of drug A 
PD(1).Psi_minB = -6.5;  %Maximum kill rate of drug B 
PD(1).zMICA = 0.017;    %Zentral MIC of Bacteria for drug A
PD(1).zMICB = 0.017;    %Zentral MIC of Bacteria for drug B
PD(1).kA = 1;           %Hill coefficient of drug A
PD(1).kB = 1;           %Hill coefficient of drug B

% Mutant MA: 
cA = 0.1; 
bA = 28; 
PD(2).Psi_max = (1-cA)*(PD(1).Psi_max+PD(1).mu_0)-PD(1).mu_0;  
                        %Maximum growth rate in absence of Antibiotic 
PD(2).mu_0 = 0.01;      %Intrinsic death rate of the bacteria 
PD(2).Psi_minA = -6.5;  %Maximum kill rate of drug A 
PD(2).Psi_minB = -6.5;  %Maximum kill rate of drug B 
PD(2).zMICA = bA* PD(1).zMICA;    %Zentral MIC of Bacteria for drug A
PD(2).zMICB = 0.017;    %Zentral MIC of Bacteria for drug B
PD(2).kA = 1;           %Hill coefficient of drug A
PD(2).kB = 1;           %Hill coefficient of drug B


% Mutant MB: 
cB = 0.1; 
bB = 28; 
PD(3).Psi_max = (1-cB)*(PD(1).Psi_max+PD(1).mu_0)-PD(1).mu_0;  
                        %Maximum growth rate in absence of Antibiotic 
PD(3).mu_0 = 0.01;      %Intrinsic death rate of the bacteria 
PD(3).Psi_minA = -6.5;  %Maximum kill rate of drug A 
PD(3).Psi_minB = -6.5;  %Maximum kill rate of drug B 
PD(3).zMICA = 0.017;    %Zentral MIC of Bacteria for drug A
PD(3).zMICB = bB*PD(1).zMICB;    %Zentral MIC of Bacteria for drug B
PD(3).kA = 1;           %Hill coefficient of drug A
PD(3).kB = 1;           %Hill coefficient of drug B


% Mutant MAB: 
PD(4).Psi_max = (1-cB-cA+cA*cB)*(PD(1).Psi_max+PD(1).mu_0)-PD(1).mu_0;  
                        %Maximum growth rate in absence of Antibiotic 
PD(4).mu_0 = 0.01;      %Intrinsic death rate of the bacteria 
PD(4).Psi_minA = -6.5;  %Maximum kill rate of drug A 
PD(4).Psi_minB = -6.5;  %Maximum kill rate of drug B 
PD(4).zMICA = bA*PD(1).zMICA;    %Zentral MIC of Bacteria for drug A
PD(4).zMICB = bB*PD(1).zMICB;    %Zentral MIC of Bacteria for drug B
PD(4).kA = 1;           %Hill coefficient of drug A
PD(4).kB = 1;           %Hill coefficient of drug B

%--------------------------------------------------------------------------
% PK parameter values: 
% 1x2 struct, one for each drug (A,B)
% Defining the treatment Schedule: 
Cycling_schedule = repmat([1 0],[1 10]); %Cycling schedule with 1 = A, 0 = B
TA = 24;                %Lenght for one part of the treatment of drug A
TB = 24;                %Lenght for one part of the treatment of drug B

% Drug A
PK(1).Kct = 0.4;       %Distribution rate from central to tissue compartment
PK(1).Ktc = 0.4;       %Distribution rate from tissue to central compartment
PK(1).Kel = 0.322;        %Elimination rate from central compartment
PK(1).D = 0.3;           %Dose of Drug 
PK(1).tau = 4;        %Length of Infusion (for ad == 'I')
PK(1).half = 3.5;        % Half life in hours 
PK(1).ad = 'E';        %Treatment type('B' = Bolus, 'I'=Infusion, 
%                                      'E' = Exponential depending on t1/2)
% Rest defined above
PK(1).Tthis = TA;       %Length of 'Treatment-part' for drug A
PK(1).Tother = TB;      %Length of 'Treatment-part' for drug B
PK(1).cycle = Cycling_schedule;

% Drug B
PK(2).Kct = 0.4;       %Distribution rate from central to tissue compartment
PK(2).Ktc = 0.4;       %Distribution rate from tissue to central compartment
PK(2).Kel = 0.322;       %Elimination rate from central compartment
PK(2).D = 0.3;         %Dodrug_Cse of Drug 
PK(2).tau =4;        %Length of Infusion (for ad == 'I')
PK(2).half = 3.5;        %Half life in hours 
PK(2).ad = 'E';        %Treatment type('B' = Bolus, 'I'=Infusion, 
%                                      'E' = Exponential depending on t1/2)
% Rest defined above
PK(2).Tthis = TB;       %Length of 'Treatment-part' for drug B
PK(2).Tother = TA;      %Length of 'Treatment-part' for drug A
PK(2).cycle = ~Cycling_schedule; % invert for B 

%--------------------------------------------------------------------------

% We implemented different version to include the carrying capacity 
% these are the standard settings used for the data generation 
% the type can be 'logit' or sigmoidal' as well 
% the link can be 'pmax' as well  
CC.type =  'lin';
CC.link =  'growth'; 
CC.Nmax =  10^15;
CC.k    =  6;
CC.N50  =  2;

% We implemented different version - if cut=1: whenever a type drops
% below one, it will be set to zero. If show=1 all cell number below one
% will be saved otherwise these were set to zero. 
% M.dil is a dilution factor for the lab treatment to account for the
% reduction of cell numbers during serial transfers. 
M.cut = 0; 
M.show = 0; 
M.dil = 0; 
%--------------------------------------------------------------------------

Lab.TA = TA; 
Lab.TB = TB; 
Lab.cA = PK(1).D;
Lab.cB = PK(2).D; 
Lab.cycle = Cycling_schedule; 

%--------------------------------------------------------------------------

P.G = G; 
P.INT = INT; 
P.PD = PD; 
P.PK = PK; 
P.CC = CC; 
P.M = M;

L.G = G; 
L.CC = CC; 
L.PD = PD; 
L.Lab = Lab; 
L.M = M;

%% Instructions to save a file:

%1. Generate struct with P and L struct fields 

Save.P = P; 
Save.L = L; 

%2. Save the struct array under new name:
%   save('newName','-struct','oldName')
save('Cip','-struct','Save'); 

%3. Load data with as a new variable: 
%   newVar = load('newName'); 
%   newVar replaces first struct: P = newVar.P

