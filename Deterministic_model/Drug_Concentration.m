function [c_t] = Drug_Concentration(PK,t)
% 
% Calculates drug concentration over treatment period of cycling treatment 
% Either for infusion treatment or Bolus treatment 
% 
% INPUT: 
% PK    = 1x1 struct of PK parameter for considered drug
%         Kct    - transition rate central to tissue compartment
%         Ktc    - transition rate tissue to central compartment 
%         Kel    - elimination rate from centram compartment
%         Kr     - receptor binding in tissue compartment
%         D      - Dosis of the drug
%         tau    - lenght of infusion (if ad = 'I') (dtau <= Tthis !!!)
%         ad     - administration type (Bolus : 'B', Infusion 'I')
%         Tthis  - Time of one part (one admin.) in cycle for considered drug
%         Tother - Time of one part (one admin.) in cycle for other drug
%         cycle  - Schedule for the cycling indicating with '1' when the
%                  considered drug is administered 
% 
% t     = Point in time 
% 
% OUTPUT: 
% c_t = drug concentration over time of treatment for one drug
%
%==========================================================================

% Define Treatment Schedule: 

% Number of Administartions durng the cycle 
n_ad = length(PK.cycle); 
tri_mat = triu(ones(n_ad),1);
% Calculate at which time the considered drug is aministered during cycle
ad_times = (PK.cycle*PK.Tthis+(~PK.cycle)*PK.Tother)*tri_mat;
ad_this = ad_times(PK.cycle == 1); 

if isempty(ad_this) 
    c_t = 0; 
else
% Implement as coulm vector so that integral works on this function 
for i=1:length(ad_this)
t_times(i,:) = t-ad_this(i); 
end

% Call function for the specific administration type 

%================BOLUS=====================================================
if PK.ad == 'B' 
    % Calculate the concentrations for each administration
    for i= 1:length(ad_this)
    c_vec(i,:) = arrayfun(@(t) Bolus_Cycle(PK,t),t_times(i,:));
    end
    % Sum over all administrations 
    c_t = sum(c_vec,1);
%================INFUSION==================================================
elseif PK.ad == 'I'
    % Calculate the concentrations for each administration
     for i= 1:length(ad_this)
    c_vec(i,:) = arrayfun(@(t) Infusion_Cycle(PK,t),t_times(i,:));
     end
    % Sum over all administrations 
    c_t = sum(c_vec,1);
%================EXPONENTIAL===============================================
elseif PK.ad == 'E'
    % Calculate the concentrations for each administration
     for i= 1:length(ad_this)
    c_vec(i,:) = arrayfun(@(t) Exp_Cycle(PK,t),t_times(i,:));
     end
    % Sum over all administrations 
    c_t = sum(c_vec,1);

end
end
end

