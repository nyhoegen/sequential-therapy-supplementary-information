function [dyn, data] = Two_Drugs_Cycling_CC_u(G,INT,PD,PK,CC,M)
%
% Calculates the cell numbers of the four different types under cycling
% treatment with two drugs (cycle defined in PK struct array (PK.cycle))
% Growth rate calculated with PKPD Model with Drug Interaction. 
% 
% INPUT: 
% G     = 1x1 struct array containing genral parameter 
%         N0  - number of bacteria in initial population 
%         p0i - fraction of mutatnts in initial population (i in {a,b,ab})
%         u0i - mutation rates of mutation i (i in {a,b,ab})
% 
% INT   = 1x2 struct array of interaction parameter (one struct for each drug) 
%         (struct 1: interaction B on A, struct 2: interaction A on B)
%         I    - maximum effect of interaction (I in [-1,inf))
%         EC50 - concentration to reach 50% of I 
%         k    - hill coefficient 
%
% PD    = 1x4 struct array of PD parameter (one struct for each type)
%         (struct 1: W, struct 2: MA, struct 3: MB, struct 4: MAB)
%         Psi_max  - maximum growth rate in absense of AB
%         mu_0     - intrinsic death rate 
%         Psi_minA - maximum kill rate of drug A
%         Psi_minB - maximum kill rate of drug B
%         zMICA    - MIC for drug A
%         zMICB    - MIC for drug B
%         KA       - hill coefficient regarding drug A
%         KB       - hill coefficient regarding drug B
%
% PK    = 1x2 struct of PK parameter (one struct for each drug)
%         Kct    - transition rate central to tissue compartment
%         Ktc    - transition rate tissue to central compartment 
%         Kel    - elimination rate from centram compartment
%         Kr     - receptor binding in tissue compartment
%         D      - Dosis of the drug
%         tau    - lenght of infusion (if ad = 'I')
%         ad     - admission type (Bolus : 'B', Infusion 'I')
%         Tthis  - Time of one part (one admin.) in cycle for considered drug
%         Tother - Time of one part (one admin.) in cycle for other drug
%         cycle  - Schedule for the cycling indicating with '1' when the
%                  considered drug is administered 
% 
% CC    = 1x1 struct array of CC paramter
%         type - defines the type of function (sig,logit,lin) 
%         link - defines the link (pmax, growth) 
%         Nmax - carrying limit 
%         k    - Hill coefficient, in case of sigmoidal function 
%         N50  - Cell number for 50% inhibition (sig) or n50 (logit)
% 
% M     = 1x1 struct array that defines the model 
%         cut  - model with or without cutoff (binary variable)
%         show - show data below one or not 
%         dil  - model with or without dilution (binary variable)
% 
% OUTPUT: 
% W   - cell numbers of wildetype population over time
% MA  - cell numbers of mutant (MA) population over time
% MB  - cell numbers of mutant (MB) population over time
% MAB - cell numbers of mutant (MAB) population over time
%
%==========================================================================

%==========================================================================
% GENERAL PARAMETER
%==========================================================================

% Calculate the end of the treatment: 
t_end = sum((PK(1).Tthis*PK(1).cycle)+(PK(2).Tthis*PK(2).cycle));
% Time Interval for the ode Solver
times =[0,t_end];

% C values 
C1 = G.N0*(1-G.p0A-G.p0B-G.p0AB); 
C2 = G.N0*G.p0A;
C3 = G.N0*G.p0B;
C4 = G.N0*G.p0AB;

% Initial condition for the solver: 
init = [C1,C2,C3,C4];

%==========================================================================
% SOLVER ODE45 
%==========================================================================
% Sum of all mutation rates: 
u_all = G.uA*(1-G.uB)+G.uB*(1-G.uA)+G.uA*G.uB; 
% Function hanlde for per capita growth rate
psi = @(N_t,t,pd,u) Psi_with_Int_CC_and_u(INT,CC,PD(pd),PK,N_t,t,u);


% Define the ODE System: 
odeSys = @(t,dyn) [psi(sum(dyn),t,1,u_all)*dyn(1);...
                   psi(sum(dyn),t,2,G.uB)*dyn(2) + Mutation_Rate(CC,PD(1),sum(dyn),G.uA*(1-G.uB))* dyn(1);...
                   psi(sum(dyn),t,3,G.uA)*dyn(3) + Mutation_Rate(CC,PD(1),sum(dyn),G.uB*(1-G.uA))* dyn(1);...
                   psi(sum(dyn),t,4,0)*dyn(4) + Mutation_Rate(CC,PD(1),sum(dyn),G.uA*G.uB)*dyn(1)+...
                   Mutation_Rate(CC,PD(2),sum(dyn),G.uB)*dyn(2)+ Mutation_Rate(CC,PD(3),sum(dyn),G.uA)*dyn(3)];
                                                                                                                                                                                       
% define tolerance in options and events
% Solve the first ODE System depending on cut
if M.cut 
    options = odeset('AbsTol',1e-10,'RelTol',1e-10, 'Events', @MyEvents);
    [tau_part,DYN1,te,ye,ie] = ode45(odeSys,times,init, options);
else 
     options = odeset('AbsTol',1e-10,'RelTol',1e-10);
    [tau_part,DYN1] = ode45(odeSys,times,init, options);
    disp(PK(1).cycle);
    disp(PK(1).D);
end
    
DYN = DYN1; 
tau = tau_part; 
num_times = length(tau_part)-1; 
% set current time point
curr_time = tau(end); 
% Check if the current time point is still below t_end
% While is only entered when cut is true 
while curr_time < t_end 
    ye(ie)=0; 
    [tau_part,DYN_part,te,ye,ie] = ode45(odeSys,[te(1,1),t_end],ye(1,:),options);
    % In case if a mutant < 1 it can still happen that the number increases
    % everything below 0 will be cut odd if it is increases higher than 1
    % it will be by de novo mutation 
    DYN = [DYN(1:num_times,:);DYN_part];
    tau = [tau(1:num_times,:);tau_part];
    % update data 
    num_times = length(tau)-1;
    % update current time point
    curr_time = tau(end);   
end 
%==========================================================================
%   MANIPULATE DATA IF SHOW IS TRUE
%==========================================================================

% In Case that all Types (except MAB) are zero (here MAB can still be > 0
% or even >1) Set al that values to zero because the Infection was
% eradicated 
if ~M.show
    if any(sum((DYN(:,1:4)<1),2)==4)
        % Point in time at which the infection was eradicated 
        ind_erad = find(sum((DYN(:,1:3)<1),2)==3,1,'first');
        % Save values for MAB
        dyn.MABcell = DYN(:,4);
        dyn.MAcell = DYN(:,2);
        dyn.MBcell = DYN(:,3);
        % Set all values lower one to zero
        DYN(DYN<1) = 0;
        % Set all MAB values bigger one after eradication to zero 
        DYN(ind_erad:end,:) = 0; 
    else
        % Save values for MAB
        dyn.MABcell = DYN(:,4);
        dyn.MAcell = DYN(:,2);
        dyn.MBcell = DYN(:,3);
        % Set all values lower than one to zero 
        DYN(DYN<1) = 0;
    end

end 

%Shape Data 
dyn.W = DYN(:,1);
dyn.MA = DYN(:,2);
dyn.MB = DYN(:,3);
dyn.MAB = DYN(:,4);
All_cells = dyn.W+dyn.MA+dyn.MB+dyn.MAB;
dyn.All = All_cells;
dyn.time = tau;


%==========================================================================
% CALCULATE INFOS FOR DATA
%==========================================================================
n = length(dyn.time);
% concentrations 
data.c_A = arrayfun(@(t) Drug_Concentration(PK(1),t), 0:t_end);
data.c_B = arrayfun(@(t) Drug_Concentration(PK(2),t), 0:t_end); 
[data.psi_W,data.CC] = arrayfun(@(i) Psi_with_Int_CC_and_u(INT,CC,PD(1),PK,All_cells(i),dyn.time(i),u_all),1:n);
data.psi_MA = arrayfun(@(i) Psi_with_Int_CC_and_u(INT,CC,PD(2),PK,All_cells(i),dyn.time(i),G.uB),1:n);
data.psi_MB = arrayfun(@(i) Psi_with_Int_CC_and_u(INT,CC,PD(3),PK,All_cells(i),dyn.time(i),G.uA),1:n);
data.psi_MAB = arrayfun(@(i) Psi_with_Int_CC_and_u(INT,CC,PD(4),PK,All_cells(i),dyn.time(i),0),1:n);
data.cycle = PK(1).cycle; 
data.TA = PK(1).Tthis;
data.TB = PK(2).Tthis;
end

