function [dyn,data] = Lab_Cycling_NumCC_u(G,PD,Lab,CC,M)
% Calculates the cell numbers of the four different types under cycling
% treatment with two drugs in the lab evironment 
% Growth rate calculated with PKPD Model
% 
% INPUT: 
% G     = 1x1 struct array containing genral parameter 
%         N0  - number of bacteria in initial population 
%         p0i - fraction of mutatnts in initial population (i in {a,b,ab})
%         u0i - mutation rates of mutation i (i in {a,b,ab})
%
% PD    = 1x4 struct array of PD parameter (one struct for each type)
%         (struct 1: W, struct 2: MA, struct 3: MB, struct 4: MAB)
%         Psi_max  - maximum growth rate in absense of AB
%         mu_0     - intrinsic death rate 
%         Psi_minA - maximum kill rate of drug A
%         Psi_minB - maximum kill rate of drug B
%         zMICA    - MIC for drug A
%         zMICB    - MIC for drug B
%         KA       - hill coefficient regarding drug A
%         KB       - hill coefficient regarding drug B
%
% Lab   = 1x1 struct array of drug related parameter 
%         Lab.TA    - Lenght of treatment with drug A (each switch)
%         Lab.TB    - Lenght of treatment with drug B (each switch)
%         Lab.cA    - Cocnetration of drug A
%         Lab.cB    - Concentration of drug B
%         Lab.cycle - Cycling schdule of the sequential treatment 
% 
% Nmax  = Carrying Limit 
% 
% M     = 1x1 struct array that defines the model 
%         cut  - model with or without cutoff (binary variable)
%         show - show data below one or not 
%         dil  - model with or without dilution (binary variable)
% 
% OUTPUT 
% dyn   = Cell numbers over the time curse of treatment 
% 

%==========================================================================
%==========================================================================
% GENERAL PARAMETER
%==========================================================================
% Calculate the end of the treatment: 
t_end = sum((Lab.TA*Lab.cycle)+(Lab.TB*(~Lab.cycle)));

% Calculate the times of Administration (for Dilution)
n_ad = length(Lab.cycle); 
tri_mat = triu(ones(n_ad));
ad_times = (Lab.cycle*Lab.TA+(~Lab.cycle)*Lab.TB)*tri_mat;
% Define first time interval depending in dil 
if M.dil
    % First time Interval for the ode Solver
    times =[0,ad_times(1)-1];
else 
    % Time Interval for the ode Solver
    times =[0,t_end-1];
end

% C values 
C1 = G.N0*(1-G.p0A-G.p0B-G.p0AB); 
C2 = G.N0*G.p0A;
C3 = G.N0*G.p0B;
C4 = G.N0*G.p0AB;
% Initial condition for the solver: 
init = [C1,C2,C3,C4];

%==========================================================================
% SOLVER ODE45 
%==========================================================================
u_all = G.uA*(1-G.uB)+G.uB*(1-G.uA)+G.uA*G.uB;
psi = @(N,t,pd,u) Psi_of_c_CC_u(PD(pd),Lab,t,N,CC,u);

% Define the ODE System: 
odeSys = @(t,dyn) [psi(sum(dyn),t,1,u_all)*dyn(1);...
                   psi(sum(dyn),t,2,G.uB)*dyn(2) + Mutation_Rate(CC,PD(1),sum(dyn),G.uA*(1-G.uB))* dyn(1);...
                   psi(sum(dyn),t,3,G.uA)*dyn(3) + Mutation_Rate(CC,PD(1),sum(dyn),G.uB*(1-G.uA))* dyn(1);...
                   psi(sum(dyn),t,4,0)*dyn(4) + Mutation_Rate(CC,PD(1),sum(dyn),G.uA*G.uB)*dyn(1)+...
                   Mutation_Rate(CC,PD(2),sum(dyn),G.uB)*dyn(2)+ Mutation_Rate(CC,PD(3),sum(dyn),G.uA)*dyn(3)];

%------With Dilution ------------------------------------------------------
if M.dil 
    % Counter for the cycles 
    cyc_count = 1; 
    % Calculat the Cell numbers for each cycle: 
    while cyc_count < n_ad+1
        % Solve the first ODE System depending on cut
        if M.cut 
            options = odeset('AbsTol',1e-10,'RelTol',1e-10, 'Events', @MyEvents);
            [tau_part,DYN1,te,ye,ie] = ode45(odeSys,times,init, options);
        else 
            options = odeset('AbsTol',1e-10,'RelTol',1e-10);
            [tau_part,DYN1] = ode45(odeSys,times,init, options);
        end
        
        DYN_run = DYN1; 
        tau_run = tau_part; 
        num_times = length(tau_part)-1; 
        % set current time point
        curr_time = tau_part(end); 
        % Check if the current time point is still below t_end
        counter = 1;
        % While is only entered for the case if cut is true 
        while curr_time < ad_times(cyc_count)-1 
            ye(ie)=0; 
            [tau_part,DYN_part,te,ye,ie] = ode45(odeSys,[te,ad_times(cyc_count)],ye,options);
            % In case if a mutant < 1 it can still happen that the number increases
            % everything below 0 will be cut odd if it is increases higher than 1
            % it will be by de novo mutation 
            DYN_run = [DYN_run(1:num_times,:);DYN_part];
            tau_run = [tau_run(1:num_times,:);tau_part];
            % update data 
            num_times = length(tau_run)-1;
            % update current time point
            curr_time = tau_run(end); 
            counter = counter +1;

        end 
            % When one cycle is done, upgrade the parameter
        % Save the current cellnumbers 
        if cyc_count == 1
            DYN = DYN_run;
            tau = tau_run;
        else
            DYN = [DYN;DYN_run];
            tau = [tau;tau_run];
        end
        % New time Vector
        if cyc_count<n_ad
             times = [ad_times(cyc_count),ad_times(cyc_count+1)-1];
        end
        % Sample the cells: 1/100 dilution 
        init = 1/100*DYN(end,:);
            % If cut is true - cell count of types that have less than one cell
    % after diluting will be set to zero 
    if M.cut 
        if any ((DYN(end,:)>=1).*(init<1))
            ind_val = find((DYN(end,:)>=1)&(init<1));
            init(ind_val)=0;
        end
    end

        % Increase counter variable 
        cyc_count = cyc_count+1;
    end
else               
    %------- Without Dilution -------------------------------------------------
    % define tolerance in options and events
    % Solve the first ODE System depending on cut
    if M.cut 
        options = odeset('AbsTol',1e-12,'RelTol',1e-12, 'Events', @MyEvents);
        [tau_part,DYN1,te,ye,ie] = ode45(odeSys,times,init, options);
    else 
         options = odeset('AbsTol',1e-12,'RelTol',1e-12);
        [tau_part,DYN1] = ode45(odeSys,times,init, options);
    end


    DYN = DYN1; 
    tau = tau_part; 
    num_times = length(tau_part)-1; 
    % set current time point
    curr_time = tau(end); 
    % Check if the current time point is still below t_end
    counter = 1;
    % While is only entered when cut is true 
    while curr_time < t_end-1 
        ye(ie)=0; 
        [tau_part,DYN_part,te,ye,ie] = ode45(odeSys,[te,t_end],ye,options);
        % In case if a mutant < 1 it can still happen that the number increases
        % everything below 0 will be cut odd if it is increases higher than 1
        % it will be by de novo mutation 
        DYN = [DYN(1:num_times,:);DYN_part];
        tau = [tau(1:num_times,:);tau_part];
        % update data 
        num_times = length(tau)-1;
        % update current time point
        curr_time = tau(end); 
        counter = counter +1;

    end 
end

%==========================================================================
%   MANIPULATE DATA IF SHOW IS TRUE
%==========================================================================
% In Case that all Types (except MAB) are zero (here MAB can still be > 0
% or even >1) Set al that values to zero because the Infection was
% eradicated 
if ~M.show
    if any(sum((DYN(:,1:4)<1),2)==4)
        % Point in time at which the infection was eradicated 
        ind_erad = find(sum((DYN(:,1:3)<1),2)==3,1,'first');
        % Save values for MAB,MA,MB
        dyn.MABcell = DYN(:,4); 
        dyn.MAcell = DYN(:,2); 
        dyn.MBcell = DYN(:,3); 
        % Set all values lower one to zero
        DYN(DYN<1) = 0;
        % Set all MAB values bigger one after eradication to zero 
        DYN(ind_erad:end,:) = 0; 
    else
        % Set all values lower than one to zero
        % Save values for MAB,MA,MB
        dyn.MABcell = DYN(:,4);
        dyn.MAcell = DYN(:,2); 
        dyn.MBcell = DYN(:,3);
        DYN(DYN<1) = 0;
    end
end 

%Shape Data 
dyn.W = DYN(:,1);
dyn.MA = DYN(:,2);
dyn.MB = DYN(:,3);
dyn.MAB = DYN(:,4);
All_cells = dyn.W+dyn.MA+dyn.MB+dyn.MAB;
dyn.All = All_cells;
dyn.time = tau;

%==========================================================================
% CALCULATE INFOS FOR DATA
%==========================================================================
n = length(dyn.time);
% concentrations 
data.c_A = repelem(Lab.cA*Lab.cycle,Lab.cycle*Lab.TA+(~Lab.cycle)*Lab.TB);
data.c_B = repelem(Lab.cB*(~Lab.cycle),Lab.cycle*Lab.TA+(~Lab.cycle)*Lab.TB); 
data.psi_W = arrayfun(@(i) Psi_of_c_CC_u(PD(1),Lab,dyn.time(i), dyn.All(i),CC,u_all),1:n);
data.psi_MA = arrayfun(@(i) Psi_of_c_CC_u(PD(2),Lab,dyn.time(i),dyn.All(i),CC,G.uB),1:n);
data.psi_MB = arrayfun(@(i) Psi_of_c_CC_u(PD(3),Lab,dyn.time(i),dyn.All(i),CC,G.uA),1:n);
data.psi_MAB = arrayfun(@(i) Psi_of_c_CC_u(PD(4),Lab,dyn.time(i),dyn.All(i),CC,0),1:n);
data.cycle = Lab.cycle; 
data.TA = Lab.TA;
data.TB = Lab.TB; 

end