function [P_imat_best,P_imat_set,P_last_nan,P_imat_best_mAB] = Get_Best_Regimen(P,dec,n_treat)
% Function to calculate the best treatment strategy for each drug
% concnetration among the different cycling regimens based on the time
% until the threshold is reached 

% INPUT 
% P       = Datset constructed with 'Treatment_Comparison_Drug_Dose_Data.m'
% dec     = number of decimal points the tiem until threshold is rounded to
% n_treat = Number of Treatments that are considered 
% 
% OUTPUT 
% P_imat_best = matrix in which all treatments (by index) are ordered according
%               to the time until threshold 
% P_imat_set  = matrix indicating with '1' the best treatment, multiple
%               treatments can be the best, depending on the dec rounded to
% P_last_nan  = last drug concentration for which no treatment reached the
%               threshold 

%==========================================================================

% Initialize matrix to save t_threshold for each treatment 
P_erad = zeros(n_treat,399); 

% Save the times until threshold in matrix
for i = 1:n_treat
 P_erad(i,:) = P(i).t_erad;
end

% Sort the matrix to get the order of the treatments (best in row 1) 
[~,P_imat_best] = sort(round(P_erad,dec)); 

% Save the last time point no treatment reaches the threshold 
P_last_nan = find(sum(isnan(P_erad))==n_treat,1,'last');

% Initialize the Matrix with the set of best treatments 
P_imat_set = zeros(n_treat,399);
for i=1:399
    P_imat_set(:,i) =  (round(P_erad(:,i),dec) == round(P_erad(P_imat_best(1,i),i),dec)); 
end

% Best Treatment regarding MAB 
% Save the times until threshold in matrix
for i = 1:n_treat
 P_mAB(i,:) = P(i).t_mAB;
end

% Sort the matrix to get the order of the treatments (best in row 1) 
[~,P_imat_best_mAB] = sort(round(P_mAB,dec),'descend'); 
end

