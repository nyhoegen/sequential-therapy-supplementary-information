function [eval_sum] = Evaluate_Treatment(P,model,varargin)
% 
% Function to calculate a summary of measures to evaluate the treatment
% efficiancy for later comparison between treatments 
% Based on the Cycling function: Two_Drugs_Cycling3_CC
%
% INPUT: 
% P     = struct array depending on the model choice
%         always in the arry: 
%    G     = 1x1 struct array containing genral parameter 
%             N0  - number of bacteria in initial population 
%             p0i - fraction of mutatnts in initial population (i in {a,b,ab})
%             u0i - mutation rates of mutation i (i in {a,b,ab})
% 
%    PD    = 1x4 struct array of PD parameter (one struct for each type)
%             (struct 1: W, struct 2: MA, struct 3: MB, struct 4: MAB)
%             Psi_max  - maximum growth rate in absense of AB
%             mu_0     - intrinsic death rate 
%             Psi_minA - maximum kill rate of drug A
%             Psi_minB - maximum kill rate of drug B
%             zMICA    - MIC for drug A
%             zMICB    - MIC for drug B
%             KA       - hill coefficient regarding drug A
%             KB       - hill coefficient regarding drug B
%
%    CC    = 1x1 struct array of CC paramter
%             type - defines the type of function (sig,logit,lin) 
%             link - defines the link (pmax, growth) 
%             Nmax - carrying limit 
%             k    - Hill coefficient, in case of sigmoidal function 
%             N50  - Cell number for 50% inhibition (sig) or n50 (logit)
% 
% M        = 1x1 struct array that defines the model 
%             cut  - model with or without cutoff (binary variable)
%             show - show data below one or not 
%             dil  - model with or without dilution (binary variable)
% 
% PATEIENT:
%
%    INT   = 1x2 struct array of interaction parameter (one struct for each drug) 
%             (struct 1: interaction B on A, struct 2: interaction A on B)
%             I    - maximum effect of interaction (I in [-1,inf))
%             EC50 - concentration to reach 50% of I 
%             k    - hill coefficient 
%

%
%   PK    = 1x2 struct of PK parameter (one struct for each drug)
%            Kct    - transition rate central to tissue compartment
%            Ktc    - transition rate tissue to central compartment 
%            Kel    - elimination rate from centram compartment
%            Kr     - receptor binding in tissue compartment
%            D      - Dosis of the drug
%            tau    - lenght of infusion (if ad = 'I')
%            ad     - admission type (Bolus : 'B', Infusion 'I')
%            Tthis  - Time of one part (one admin.) in cycle for considered drug
%            Tother - Time of one part (one admin.) in cycle for other drug
%            cycle  - Schedule for the cycling indicating with '1' when the
%                     considered drug is administered 
%
% LAB
%   Lab   = 1x1 struct array of drug related parameter 
%             Lab.TA    - Lenght of treatment with drug A (each switch)
%             Lab.TB    - Lenght of treatment with drug B (each switch)
%             Lab.cA    - Cocnetration of drug A
%             Lab.cB    - Concentration of drug B
%             Lab.cycle - Cycling schdule of the sequential treatment 
%
% 
% Model    = LAB or PATIENT 
%
% varargin = Variable input to change drug concnetration
% 
% OUTPUT: 
% eval_sum = summary of evalutation measures 
%            1. Endpoint measures 
%               N_end     = cell numbers after the last cycle of the treatment
%               Nw_end    = number of wildtypes      - " -
%               Nm_end    = number of mutants        - " -
%               NmA_end   = number of mutants MA     - " -
%               NmB_end   = number of mutants MB     - " -
%               NmAB_end  = number of mutants MAB    - " -
%               Pm_end    = fraction of mutants        - " -
%               PmA_end   = fraction of mutants MA     - " -
%               PmB_end   = fraction of mutants MB     - " -
%               PmAB_end  = fraction of mutants MAB    - " -
% 
%            2. Cycle measures (each a vector) 
%               N_cyc     = cell numbers after the every cycle
%               Nw_cyc    = number of Wildtypes      - " -
%               Nm_cyc    = number of mutants        - " -
%               Nm_cyc    = number of mutants        - " -
%               NmA_cyc   = number of mutants MA     - " -
%               NmB_cyc   = number of mutants MB     - " -
%               NmAB_cyc  = number of mutants MAB    - " -
%               Pm_cyc    = fraction of mutants        - " -
%               PmA_cyc   = fraction of mutants MA     - " -
%               PmB_cyc   = fraction of mutants MB     - " -
%               PmAB_cyc  = fraction of mutants MAB    - " -
%
%            3. time points
%               t_erad    = time of eradication (if happend treatment was
%                                                sucessful, otherwise 'NA')
%               n_mAB     = Cell count of double mutants at time point of
%                           eradication 
%               t_m_50    =  time until mutatnt population takes over 
%                                                ('NA' if not) 
%               t_MAB     = time point until MAB mutant appears (reaches
%                           frequency above one 
%  
%==========================================================================
% If the function got additnional concnetration values, set the
% concnetration in P.PK/P.Lab to the according value: 
if nargin == 3 
    if strcmp(model,'LAB')
        P.Lab.cA = varargin{1}(1,1); 
        P.Lab.cB = varargin{1}(2,1); 
    else 
        P.PK(1).D = varargin{1}(1,1);
        P.PK(2).D = varargin{1}(2,1);
    end
end 

% Calculate the Cell numbers during the time course of the chosen
% treatement




% Calculate the cell numebrs depending in the model choice 
if strcmp(model,'LAB')
        [dyn,~] = Lab_Cycling_NumCC_u(P.G,P.PD,P.Lab,P.CC,P.M);
        %[dyn,~] = Lab_Cycling_NumCC(P.G,P.PD,P.Lab,P.CC.Nmax,P.M); 
        % Calculate the time points after each cycle
        % Safe this variable for later
        n_ad = length(P.Lab.cycle); 
        % Number of Administartions durng the cycle 
        tri_mat = triu(ones(n_ad));
        % Calculate at which time the considered drug is aministered during cycle
ad_times = ((P.Lab.cycle*P.Lab.TA+(~P.Lab.cycle)*P.Lab.TB)*tri_mat)-1;
elseif strcmp(model,'PATIENT')
        [dyn,~] = Two_Drugs_Cycling_CC_u(P.G,P.INT,P.PD,P.PK,P.CC,P.M);
        %[dyn,~] = Two_Drugs_Cycling_CC(P.G,P.INT,P.PD,P.PK,P.CC,P.M);
        % Calculate the time points after each cycle 
        % Safe this variable for later
        n_ad = length(P.PK(1).cycle); 
        % Number of Administartions durng the cycle 
        tri_mat = triu(ones(n_ad));
        % Calculate at which time the considered drug is aministered during cycle
        ad_times = ((P.PK(1).cycle*P.PK(1).Tthis+(~P.PK(1).cycle)*P.PK(1).Tother)*tri_mat)-1;
end

%==========================================================================
%       ENDPOINT MEASURES
%==========================================================================

% Total cell number at the end of the treatement
eval_sum.N_end = dyn.All(end);
% Total Cell number of types: 
eval_sum.Nw_end = dyn.W(end);
eval_sum.Nm_end = dyn.MA(end)+dyn.MB(end)+dyn.MAB(end); 
eval_sum.NmA_end = dyn.MA(end); 
eval_sum.NmB_end = dyn.MB(end);
eval_sum.NmAB_end = dyn.MAB(end);
% overall cell count is zero, set all fractions to zero (avoid division by
% zero) 
if eval_sum.N_end == 0
    % Set all values to zero: 
    
    % Fraction of mutants at the end of the treatment 
    eval_sum.Pm_end = 0;
    % Fraction of mutantsb MA at the end of the treatment 
    eval_sum.PmA_end = 0;
    % Fraction of mutantsb MB at the end of the treatment
    eval_sum.PmB_end = 0;
    % Fraction of mutantsb MAB at the end of the treatment
    eval_sum.PmAB_end = 0;
else 
    % Calculate the fractions: 
    
    % Fraction of mutants at the end of the treatment 
    eval_sum.Pm_end = (dyn.MA(end)+dyn.MB(end)+dyn.MAB(end))/eval_sum.N_end;
    % Fraction of mutantsb MA at the end of the treatment 
    eval_sum.PmA_end = (dyn.MA(end))/eval_sum.N_end;
    % Fraction of mutantsb MB at the end of the treatment
    eval_sum.PmB_end = (dyn.MB(end))/eval_sum.N_end;
    % Fraction of mutantsb MAB at the end of the treatment
    eval_sum.PmAB_end = (dyn.MAB(end))/eval_sum.N_end;
end

%==========================================================================
%       CYCLE MEASURES 
%==========================================================================


% Calculate the indices of the vec dyn.time for the time pionts: 
for i = 1:length(ad_times) 
    ind = find(dyn.time>=ad_times(i),1,'first');
    
     % Total cell number at the end of the treatement
    eval_sum.N_cyc(1,i) = dyn.All(ind);
    % Total Cell number of types: 
    eval_sum.Nw_cyc(1,i) = dyn.W(ind);
    eval_sum.Nm_cyc(1,i) = dyn.MA(ind)+dyn.MB(ind)+dyn.MAB(ind); 
    eval_sum.NmA_cyc(1,i) = dyn.MA(ind); 
    eval_sum.NmB_cyc(1,i) = dyn.MB(ind);
    eval_sum.NmAB_cyc(1,i) = dyn.MAB(ind);
    
    % overall cell count is zero, set all fractions to zero (avoid division by
    % zero) 
 
    if eval_sum.N_cyc(1,i) == 0 
        % Set all values to zero:
        
        % Fraction of mutants at the end of the treatment 
        eval_sum.Pm_cyc(1,i) = 0;
        % Fraction of mutantsb MA at the end of the treatment 
        eval_sum.PmA_cyc(1,i) = 0;
        % Fraction of mutantsb MB at the end of the treatment
        eval_sum.PmB_cyc(1,i) = 0;
        % Fraction of mutantsb MAB at the end of the treatment
        eval_sum.PmAB_cyc(1,i) = 0;
    else
        % Calculate the fractions: 
        
        % Fraction of mutants at the end of the treatment 
        eval_sum.Pm_cyc(1,i) = (dyn.MA(ind)+dyn.MB(ind)+dyn.MAB(ind))/eval_sum.N_cyc(1,i);
        % Fraction of mutantsb MA at the end of the treatment 
        eval_sum.PmA_cyc(1,i) = (dyn.MA(ind))/eval_sum.N_cyc(1,i);
        % Fraction of mutantsb MB at the end of the treatment
        eval_sum.PmB_cyc(1,i) = (dyn.MB(ind))/eval_sum.N_cyc(1,i);
        % Fraction of mutantsb MAB at the end of the treatment
        eval_sum.PmAB_cyc(1,i) = (dyn.MAB(ind))/eval_sum.N_cyc(1,i);
    end
    
end


%==========================================================================
%       TIME POINTS
%==========================================================================

% Caculate the point in time where the whole population is eradicated 
t_erad = find(dyn.All < 1,1,'first'); 
% If there wasn't an eradication, the value will be 'NA' otherwise the time
% point will be safes 
if isempty(t_erad) 
    % safe as not-a-number 
    eval_sum.t_erad = nan;
    eval_sum.erad_mAB = nan; 
else
    % safe time point 
    eval_sum.t_erad = dyn.time(t_erad);
    if P.M.show == 0 
        eval_sum.erad_mAB = dyn.MABcell(t_erad); 
    else 
        eval_sum.erad_mAB = dyn.MAB(t_erad);
    end
end



% Calculate the time point where the mutant population takes over (fraction
% bigger than 50% 
frac = (dyn.MA+dyn.MB+dyn.MAB)./dyn.All; 
t_m_50 = find(frac >= 0.5,1,'first'); 
% If ther wasn't the case in which the mutant population reached 50%
% frequency, set the value to 'NA' otherwise safe timpe point. 
if isempty(t_m_50)
    % safe as not-a-number
    eval_sum.t_m_50 = nan; 
else
    % Safe as time point 
    eval_sum.t_m_50 = dyn.time(t_m_50); 
end

% Time point of appearance of MAB: 
% Depending on data shown below one or not caclulate the time point 
if P.M.show == 0
    t_mAB = find(dyn.MABcell >= 1,1,'first'); 
else 
    t_mAB = find(dyn.MAB >= 1,1,'first'); 
end 
% Safe the time point if the 
if isempty(t_mAB)
    eval_sum.t_mAB = nan; 
else 
    eval_sum.t_mAB = dyn.time(t_mAB); 
end

% Time when the Wildtype is eradicated 
% Caculate the point in time where the whole population is eradicated 
t_erad_W = find(dyn.W < 1,1,'first'); 
% If there wasn't an eradication, the value will be 'NA' otherwise the time
% point will be safes 
if isempty(t_erad_W) 
    % safe as not-a-number 
    eval_sum.t_erad_W = nan;
    eval_sum.Nm_t_erad_W = nan; 
else
    % safe time point 
    eval_sum.t_erad_W = dyn.time(t_erad_W);
    if P.M.show == 0 
        eval_sum.Nm_t_erad_W = dyn.MABcell(t_erad_W)+ dyn.MAcell(t_erad_W) +dyn.MBcell(t_erad_W); 
    else 
        eval_sum.Nm_t_erad_W = dyn.MAB(t_erad_W)+ dyn.MA(t_erad_W) +dyn.MB(t_erad_W);
    end
end
end

