function [psi,d,CC] = Psi_of_c_CC_u(PD,Lab,t,N_t,CC,u)
% 
% Skript to calculate the growth rate for a given type at a certain
% concentration and time point
% 
% INPUT: 
% PD    = 1x4 struct array of PD parameter (one struct for each type)
%         (struct 1: W, struct 2: MA, struct 3: MB, struct 4: MAB)
%         Psi_max  - maximum growth rate in absense of AB
%         mu_0     - intrinsic death rate 
%         Psi_minA - maximum kill rate of drug A
%         Psi_minB - maximum kill rate of drug B
%         zMICA    - MIC for drug A
%         zMICB    - MIC for drug B
%         KA       - hill coefficient regarding drug A
%         KB       - hill coefficient regarding drug B
%
% Lab   = 1x1 struct array of drug related parameter 
%         Lab.TA    - Lenght of treatment with drug A (each switch)
%         Lab.TB    - Lenght of treatment with drug B (each switch)
%         Lab.cA    - Cocnetration of drug A
%         Lab.cB    - Concentration of drug B
%         Lab.cycle - Cycling schdule of the sequential treatment 
% 
% OUTPUT 
% psi   = growth rate for either drug A or B depending on t
% 

%==========================================================================
%==========================================================================
%       CHOOSE FUCNTION FOR CC
%==========================================================================

% linear CC
if strcmp(CC.type,'lin')
   CC_t = (1-(N_t/CC.Nmax)); 
%     if CC_t<0
%         CC_t = 0;
%     end
% sigmoid 
elseif strcmp(CC.type,'sig')
    CC_t = 1-(N_t^CC.k/(N_t^CC.k+CC.N50^CC.k));
elseif strcmp(CC.type,'logit')
    CC_t = 1 -(1/(1+exp(-5*CC.N50*(N_t-CC.Nmax/CC.N50)/CC.Nmax)));
end 

if strcmp(CC.link,'pmax')
    
    psi_c = @(psi_max,mu_0,psi_min,zmic,k,c) CC_t*(1-u)*(psi_max+mu_0) - mu_0...
        - (((CC_t*(psi_max+mu_0)-mu_0-psi_min)*(c/zmic)^k)/((c/zmic)^k-(psi_min/(CC_t*(psi_max+mu_0)-mu_0)))); 
        
elseif strcmp(CC.link,'growth')
    psi_c = @(psi_max,mu_0,psi_min,zmic,k,c) CC_t*(1-u)*(psi_max+mu_0) - mu_0...
        - (((psi_max-psi_min)*(c/zmic)^k)/((c/zmic)^k-(psi_min/(psi_max))));
end 
    
% Calculate which drug is present at t: 
% Number of Administartions durng the cycle 
n_ad = length(Lab.cycle); 
tri_mat = triu(ones(n_ad),1);
% Calculate at which time the considered drug is aministered during cycle
ad_times = (Lab.cycle*Lab.TA+(~Lab.cycle)*Lab.TB)*tri_mat;

% check in which interval t is: 
ind = sum(t >= ad_times);

% check which treatment is used: 
drug = Lab.cycle(ind); 

% Calculate the growth rate depending on this value: 
if drug 
    psi = psi_c(PD.Psi_max,PD.mu_0,PD.Psi_minA,PD.zMICA,PD.kA,Lab.cA); 
    d = 'A';
else  
    psi = psi_c(PD.Psi_max,PD.mu_0,PD.Psi_minB,PD.zMICB,PD.kB,Lab.cB); 
    d = 'B';
end 

end