function [c_t] = Infusion_Cycle(PK,t)
%
% Drug concentration for one administration at continous rate
% 
% INPUT:  
% PK    = 1x1 struct of PK parameter for considered drug
%         PK.Kct    - transition rate central to tissue compartment
%         PK.Ktc    - transition rate tissue to central compartment 
%         PK.Kel    - elimination rate from centram compartment
%         PK.Kr     - receptor binding in tissue compartment
%         D      - Dosis of the drug
%         tau   - lenght of infusion (if ad = 'I') (tau <= Tthis !!!)
%         ad     - administration type (Bolus : 'B', Infusion 'I')
%         Tthis  - Time of one part (one admin.) in cycle for considered drug
%         Tother - Time of one part (one admin.) in cycle for other drug
%         cycle  - Schedule for the cycling indicating with '1' when the
%                  considered drug is administered 
% 
% t     = Point in time 
%
% OUTPUT
% c_t = Drug concentration over time
%==========================================================================
% Check whether t is negative
if t < 0 
    c_t = 0;
else 
   
% Calculation of eigenvalues (lambda_1,2) and first entries of eigenvectors
% (v11,v21):
lambda_1 =  - PK.Kct/2 - PK.Kel/2 - PK.Ktc/2 - (PK.Kct^2 + 2*PK.Kct*PK.Kel ...
            + 2*PK.Kct*PK.Ktc + PK.Kel^2 - 2*PK.Kel*PK.Ktc ...
            + PK.Ktc^2)^(1/2)/2;
        
lambda_2 = - PK.Kct/2 - PK.Kel/2 - PK.Ktc/2 + (PK.Kct^2 + 2*PK.Kct*PK.Kel ...
            + 2*PK.Kct*PK.Ktc + PK.Kel^2 - 2*PK.Kel*PK.Ktc ...
            + PK.Ktc^2)^(1/2)/2;
        
v11 = 1/PK.Kct*(PK.Ktc+lambda_1); 

   
v21 = 1/PK.Kct*(PK.Ktc+lambda_2); 

% Function Handle for the parts during and after infusion administration

% During Infusion: 
C1 = PK.D*PK.Kct/((lambda_1-lambda_2)*lambda_1); 
C2 = PK.D*PK.Kct/((lambda_2-lambda_1)*lambda_2); 

c_t_1 =@(time) C1 * exp(lambda_1*time)+ C2 * exp(lambda_2*time) + PK.D*PK.Kct/(lambda_1*lambda_2);
    
% After Infusion: 
% Calculate initial values for phase of exponential decline
Vt0 = c_t_1(PK.tau);
Vc0 = v11 * C1 * exp(lambda_1*PK.tau)+ v21 * C2 * exp(lambda_2*PK.tau) ...
      + PK.D * PK.Kct / (lambda_1-lambda_2) *(v21/lambda_2 - v11/lambda_1);
    
C12 = (Vc0-Vt0*v21)/(v11-v21);
C22 = Vt0-C12; 

% Calculation of function value
c_t_2 =@(time) C12*exp(lambda_1.*(time-PK.tau))+C22*exp(lambda_2.*(time-PK.tau));

% Piecewise function 
  if t <= PK.tau 
      c_t = c_t_1(t);
  else 
      c_t = c_t_2(t); 
  end
end
end

