function [value,isterminal,direction,ye,ie] = MyEvents(t,dyn)
% Function to define Events for the ODE function
% Namely stop when population size drops below one
value = dyn-1;
isterminal = ones(4,1);
direction = -1*ones(4,1);
end

