function [c_t] = Exp_Cycle(PK,t)
% One Compartment Model (Exponential Decrease) 
% INPUT:  
% PK    = 1x1 struct of PK parameter for considered drug
%         Kct    - transition rate central to tissue compartment
%         Ktc    - transition rate tissue to central compartment 
%         Kel    - elimination rate from centram compartment
%         Kr     - receptor binding in tissue compartment
%         D      - Dosis of the drug
%         dtau   - lenght of infusion (if ad = 'I') (dtau <= Tthis !!!)
%         half   - half life for exponential decrease
%         ad     - administration type (Bolus : 'B', Infusion 'I')
%         Tthis  - Time of one part (one admin.) in cycle for considered drug
%         Tother - Time of one part (one admin.) in cycle for other drug
%         cycle  - Schedule for the cycling indicating with '1' when the
%                  considered drug is administered 
% 
% t     = Point in time 
%
% OUTPUT
% c_t = Drug concentration over time
%==========================================================================
% Check whether t is negative 
if t < 0 
    c_t = 0; 
else 
% Calculate the exponential decrease: 
% Elimination constant Kel 
Kel = log(2)/PK.half;
c_t = PK.D * exp(-Kel*t); 
end 
end

