function [c_t] = Bolus_Cycle(PK,t)
% Drug concentration for one administration 
%
% INPUT:  
% PK    = 1x1 struct of PK parameter for considered drug
%         Kct    - transition rate central to tissue compartment
%         Ktc    - transition rate tissue to central compartment 
%         Kel    - elimination rate from centram compartment
%         Kr     - receptor binding in tissue compartment
%         D      - Dosis of the drug
%         dtau   - lenght of infusion (if ad = 'I') (dtau <= Tthis !!!)
%         ad     - administration type (Bolus : 'B', Infusion 'I')
%         Tthis  - Time of one part (one admin.) in cycle for considered drug
%         Tother - Time of one part (one admin.) in cycle for other drug
%         cycle  - Schedule for the cycling indicating with '1' when the
%                  considered drug is administered 
% 
% t     = Point in time 
%
% OUTPUT
% c_t = Drug concentration over time
%==========================================================================
% Check whether t is negative 
if t < 0 
    c_t = 0; 
else 
% Calculation of eigenvalues (lambda_1,2) and first entries of eigenvectors
% (v11,v21):
lambda_1 =  - PK.Kct/2 - PK.Kel/2 - PK.Ktc/2 - (PK.Kct^2 + 2*PK.Kct*PK.Kel ...
            + 2*PK.Kct*PK.Ktc + PK.Kel^2 - 2*PK.Kel*PK.Ktc ...
            + PK.Ktc^2)^(1/2)/2;
lambda_2 = - PK.Kct/2 - PK.Kel/2 - PK.Ktc/2 + (PK.Kct^2 + 2*PK.Kct*PK.Kel ...
            + 2*PK.Kct*PK.Ktc + PK.Kel^2 - 2*PK.Kel*PK.Ktc ...
            + PK.Ktc^2)^(1/2)/2;
       
  
% Calculation of the multiplication factor of the SUM
C = PK.D*PK.Kct/(lambda_1-lambda_2);

% Calcultae the drug concentration for one administartion at t
c_t = C * (exp(lambda_1*t)-exp(lambda_2*t));
end 
end

