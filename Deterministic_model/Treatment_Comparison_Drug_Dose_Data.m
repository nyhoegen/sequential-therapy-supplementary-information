function [] = Treatment_Comparison_Drug_Dose_Data(P,id,T,D,export)
% 
% Function to calculate and visualize the measures of Treatment efficancy
% for different Treatment regimens and different Drug Concentrations. 

% INPUT: 
% P/L     = struct array depending on the model choice (P = Patient,L = Lab)
%         always in the arry: 
%    G     = 1x1 struct array containing genral parameter 
%             N0  - number of bacteria in initial population 
%             p0i - fraction of mutatnts in initial population (i in {a,b,ab})
%             u0i - mutation rates of mutation i (i in {a,b,ab})
% 
%    PD    = 1x4 struct array of PD parameter (one struct for each type)
%             (struct 1: W, struct 2: MA, struct 3: MB, struct 4: MAB)
%             Psi_max  - maximum growth rate in absense of AB
%             Psi_minA - maximum kill rate of drug A
%             Psi_minB - maximum kill rate of drug B
%             zMICA    - MIC for drug A
%             zMICB    - MIC for drug B
%             KA       - hill coefficient regarding drug A
%             KB       - hill coefficient regarding drug B
%
%    CC    = 1x1 struct array of CC paramter
%             type - defines the type of function (sig,logit,lin) 
%             link - defines the link (pmax, growth) 
%             Nmax - carrying limit 
%             k    - Hill coefficient, in case of sigmoidal function 
%             N50  - Cell number for 50% inhibition (sig) or n50 (logit)
%
% M        = 1x1 struct array that defines the model 
%            cut  - model with or without cutoff (binary variable)
%            show - show data below one or not 
%            dil  - model with or without dilution (binary variable)
% 
%  -- PATEIENT:
%
%    INT   = 1x2 struct array of interaction parameter (one struct for each drug) 
%             (struct 1: interaction B on A, struct 2: interaction A on B)
%             I    - maximum effect of interaction (I in [-1,inf))
%             EC50 - concentration to reach 50% of I 
%             k    - hill coefficient 
%
%
%   PK    = 1x2 struct of PK parameter (one struct for each drug)
%            Kct    - transition rate central to tissue compartment
%            Ktc    - transition rate tissue to central compartment 
%            Kel    - elimination rate from centram compartment
%            Kr     - receptor binding in tissue compartment
%            D      - Dosis of the drug
%            tau    - lenght of infusion (if ad = 'I')
%            ad     - admission type (Bolus : 'B', Infusion 'I')
%            Tthis  - Time of one part (one admin.) in cycle for considered drug
%            Tother - Time of one part (one admin.) in cycle for other drug
%            cycle  - Schedule for the cycling indicating with '1' when the
%                     considered drug is administered 
%
%  -- LAB
%   Lab   = 1x1 struct array of drug related parameter 
%             Lab.TA    - Lenght of treatment with drug A (each switch)
%             Lab.TB    - Lenght of treatment with drug B (each switch)
%             Lab.cA    - Cocnetration of drug A
%             Lab.cB    - Concentration of drug B
%             Lab.cycle - Cycling schdule of the sequential treatment
% 
% id        = 'P' or 'L' dependning on the model choice in P
%
% T          = Matrix with different Treatment regimens (e.g. [0,1,0,1,..]) 
%              in the rows of the matrix 
%             (the regimens need to have the same length)
% 
% D          = 2xX matrix of X drug concnetrations for each drug
%
% export     = 1x2 cell array :
%              {1}: binary variable indicating whether data
%                   should be saved
%              {2}: Subfolder name 
%              {3}: Id of the data for safing 
%              {4}: binary variable indicating wether fig should be
%                   produced (only saved if exp{1} is true
% 
% OUTPUT 
% fig1       = End-Point measures depending on the drug dose 
% 
% fig2,3       = Cycle-Measures depending on drug dose 
% 
% fig4       = Time-points 
%              
% =========================================================================

% Define general Parameter 
% Get number of different treatments 
[m,n] = size(T);
[~,d] = size(D);


%==========================================================================
%       GENERATE DATA
%==========================================================================

% Loop over the possible treatments: 
for i = 1:m 
    % Safe the Treatment in the according fields of the struct arrays with
    % the parameter values 
    % Differentiate between PKPD model or LAB : 
    if id == 'P'
        % Patient
        P.PK(1).cycle = T(i,:);
        P.PK(2).cycle = ~T(i,:);
    else 
        % Lab
        P.Lab.cycle = T(i,:); 
    end
    % Loop over the different drug concentrations
    parfor j = 1:d 
        % Safe the concnetration in the according fields of the struct
        % arrays with parameter values 
        % Differentiate between PKPD model or LAB : 
        
        % Sometimes the ODE is not solvable for every D 
        % use try and catch to prevent for loop from stopping
        Drug_solve = 1; 
        try 
            if id == 'P'
                % Patient
                E = Evaluate_Treatment(P,'PATIENT',D(:,j)); 
            else 
                %Lab 
                E = Evaluate_Treatment(P,'LAB',D(:,j)); 
            end
        catch 
            % Write Error message 
            warning('ODE not solvable for concentration with index %d:\n',j); 
            
            % Set drug variable to zero at this point (to exclude this
            % concentrations in the plot later
            Drug_solve = 0; 
            
            % Set all values form E to 'nan' 
            E.N_end = nan; 
            E.Nm_end = nan;
            E.NmA_end = nan; 
            E.NmB_end = nan;
            E.NmAB_end = nan;
            E.Nw_end = nan;
            E.N_cyc = nan*ones(1,n); 
            E.Nw_cyc = nan*ones(1,n); 
            E.Nm_cyc = nan*ones(1,n); 
            E.NmA_cyc = nan*ones(1,n); 
            E.NmB_cyc = nan*ones(1,n);
            E.NmAB_cyc = nan*ones(1,n);
            E.Pm_end = nan;
            E.PmA_end = nan; 
            E.PmB_end = nan;
            E.PmAB_end = nan;
            E.Pm_cyc = nan*ones(1,n);
            E.PmA_cyc = nan*ones(1,n); 
            E.PmB_cyc = nan*ones(1,n);
            E.PmAB_cyc = nan*ones(1,n);
            E.t_erad = nan;
            E.t_m_50 = nan;  
            E.t_mAB = nan; 
            E.erad_mAB = nan;
            E.t_erad_W = nan; 
            E.Nm_t_erad_W = nan;
            
        end 

        % Save the data in a struc arrays: 
        P_N_end(1,j)    = E.N_end; 
        P_Nm_end(1,j)   = E.Nm_end; 
        P_NmA_end(1,j)  = E.NmA_end; 
        P_NmB_end(1,j)  = E.NmB_end;
        P_NmAB_end(1,j) = E.NmAB_end;
        P_Nw_end(1,j)   = E.Nw_end;
        P_N_cyc(j,:)    = E.N_cyc; 
        P_Nw_cyc(j,:)   = E.Nw_cyc; 
        P_Nm_cyc(j,:)   = E.Nm_cyc; 
        P_NmA_cyc(j,:)  = E.NmA_cyc; 
        P_NmB_cyc(j,:)  = E.NmB_cyc;
        P_NmAB_cyc(j,:) = E.NmAB_cyc;
        P_Pm_end(1,j)   = E.Pm_end;
        P_PmA_end(1,j)  = E.PmA_end; 
        P_PmB_end(1,j)  = E.PmB_end;
        P_PmAB_end(1,j) = E.PmAB_end;
        P_Pm_cyc(j,:)   = E.Pm_cyc; 
        P_PmA_cyc(j,:)  = E.PmA_cyc; 
        P_PmB_cyc(j,:)  = E.PmB_cyc;
        P_PmAB_cyc(j,:) = E.PmAB_cyc;
        P_t_erad(1,j)   = E.t_erad;
        P_t_m_50(1,j)   = E.t_m_50; 
        P_t_mAB(1,j)    = E.t_mAB; 
        P_erad_mAB(1,j) = E.erad_mAB;
        P_t_erad_W(1,j) = E.t_erad_W; 
        P_erad_W_mut(1,j) = E.Nm_t_erad_W;
        P_D_solve(1,j) = Drug_solve; 
        
 
    end
        P_data(i).N_end    = P_N_end;
        P_data(i).Nm_end   = P_Nm_end; 
        P_data(i).Nw_end   = P_Nw_end; 
        P_data(i).NmA_end  = P_NmA_end; 
        P_data(i).NmB_end  = P_NmB_end;
        P_data(i).NmAB_end = P_NmAB_end;
        P_data(i).N_cyc    = P_N_cyc; 
        P_data(i).Nw_cyc   = P_Nw_cyc;
        P_data(i).Nm_cyc   = P_Nm_cyc; 
        P_data(i).NmA_cyc  = P_NmA_cyc; 
        P_data(i).NmB_cyc  = P_NmB_cyc;
        P_data(i).NmAB_cyc = P_NmAB_cyc;
        P_data(i).Pm_end   = P_Pm_end; 
        P_data(i).PmA_end  = P_PmA_end; 
        P_data(i).PmB_end  = P_PmB_end;
        P_data(i).PmAB_end = P_PmAB_end;
        P_data(i).Pm_cyc   = P_Pm_cyc; 
        P_data(i).PmA_cyc  = P_PmA_cyc; 
        P_data(i).PmB_cyc  = P_PmB_cyc;
        P_data(i).PmAB_cyc = P_PmAB_cyc;
        P_data(i).t_erad   = P_t_erad;
        P_data(i).t_m_50   = P_t_m_50; 
        P_data(i).t_mAB    = P_t_mAB;
        P_data(i).erad_mAB = P_erad_mAB; 
        P_data(i).t_erad_W = P_t_erad_W; 
        P_data(i).Nm_t_erad_W = P_erad_W_mut; 
        P_data(i).D_solve = P_D_solve; 

end
if export{1}
    % Save generated data as. mat file  
    name =  append(pwd,'/',export{2},'/Data_',export{3},'.mat');
    save(name,'P_data');
end

end

