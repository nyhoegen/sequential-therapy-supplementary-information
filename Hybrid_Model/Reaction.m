function [Xw,XmA,XmB,XmAB] = Reaction(state,mu_reac,Xw,XmA,XmB,XmAB)
% Function to update the cell numbers based on the current state and the
% random event 
%
% INPUT 
% state  = 1x4 vector indicating with 1 if the type should be handeld
%          deterministically or stochastically with 0 
%          order of types [W,MA,MB,MAB] 
% mu_reac  = random value that decides which random event is choosen 
% Xw    = old cell count of WT 
% XmA   = old cell count of MA
% XmB   = old cell count of MB 
% XmAB  = old cell count of MAB 
% 
% OUTPUT
% Xw_t   = new cell count of WT 
% XmA_t  = new cell count of MA
% XmB_t  = new cell count of MB 
% XmAB_t = new cell count of MAB
%==========================================================================

% Calculate ci_max_t based on the state vector 
if sum(state==[1,0,0,0])==4 
    
    % W DETERMINISTSICH
    % Carry out reaction 
             % Carry out reaction 
                if mu_reac == 1
                    XmA = XmA +1;
                elseif mu_reac == 2 
                    XmB = XmB +1;
                elseif mu_reac == 3 
                    XmAB = XmAB +1;
                elseif mu_reac == 4
                    XmA = XmA +1;
                elseif mu_reac == 5 
                    XmA = XmA -1; 
                elseif mu_reac == 6
                    XmAB = XmAB +1;
                elseif mu_reac == 7
                    XmB = XmB +1; 
                elseif mu_reac == 8
                    XmB = XmB -1; 
                elseif mu_reac == 9
                    XmAB = XmAB +1;
                elseif mu_reac == 10
                    XmAB = XmAB +1;
                else 
                    XmAB = XmAB -1; 
                end 
          
elseif sum(state==[1,1,0,0])==4
    % W,MA DETERMINISTSICH
      % Carry out reaction 
                if mu_reac == 1
                    XmA = XmA +1;
                elseif mu_reac == 2 
                    XmB = XmB +1;
                elseif mu_reac == 3 
                    XmAB = XmAB +1; 
                elseif mu_reac == 4
                    XmAB = XmAB +1;
                elseif mu_reac == 5
                    XmB = XmB +1; 
                elseif mu_reac == 6
                    XmB = XmB -1; 
                elseif mu_reac == 7
                    XmAB = XmAB +1;
                elseif mu_reac == 8
                    XmAB = XmAB +1;
                else 
                    XmAB = XmAB -1; 
                end 

    
elseif sum(state==[1,0,1,0])==4
    % W,MB DETERMINISTSICH
      % Carry out reaction 
                if mu_reac == 1
                    XmA = XmA +1;
                elseif mu_reac == 2 
                    XmB = XmB +1;
                elseif mu_reac == 3 
                    XmAB = XmAB +1;
                elseif mu_reac == 4
                    XmA = XmA +1;
                elseif mu_reac == 5 
                    XmA = XmA -1; 
                elseif mu_reac == 6
                    XmAB = XmAB +1;
                elseif mu_reac == 7
                    XmAB = XmAB +1;
                elseif mu_reac == 8
                    XmAB = XmAB +1;
                else 
                    XmAB = XmAB -1; 
                end 

    
elseif sum(state==[1,0,0,1])==4
    % W,MAB DETERMINISTSICH
     % Carry out reaction 
                if mu_reac == 1
                    XmA = XmA +1;
                elseif mu_reac == 2 
                    XmB = XmB +1;
                elseif mu_reac == 3 
                    XmAB = XmAB +1;
                elseif mu_reac == 4
                    XmA = XmA +1;
                elseif mu_reac == 5 
                    XmA = XmA -1; 
                elseif mu_reac == 6
                    XmAB = XmAB +1;
                elseif mu_reac == 7
                    XmB = XmB +1; 
                elseif mu_reac == 8
                    XmB = XmB -1; 
                elseif mu_reac == 9
                    XmAB = XmAB +1;
                end 

    
elseif sum(state==[1,1,1,0])==4
    % W,MA,MB DETERMINISTSICH
     % Carry out reaction 
                if mu_reac == 1
                    XmA = XmA +1;
                elseif mu_reac == 2 
                    XmB = XmB +1;
                elseif mu_reac == 3 
                    XmAB = XmAB +1;
                elseif mu_reac == 4
                    XmAB = XmAB +1;
                elseif mu_reac == 5
                    XmAB = XmAB +1;
                elseif mu_reac == 6
                    XmAB = XmAB +1;
                else 
                    XmAB = XmAB -1; 
                end 

    
elseif sum(state==[1,1,0,1])==4
    % W,MA,MAB DETERMINISTSICH
     % Carry out reaction 
                if mu_reac == 1
                    XmA = XmA +1;
                elseif mu_reac == 2 
                    XmB = XmB +1;
                elseif mu_reac == 3 
                    XmAB = XmAB +1;
                elseif mu_reac == 4
                    XmAB = XmAB +1;
                elseif mu_reac == 5
                    XmB = XmB +1; 
                elseif mu_reac == 6
                    XmB = XmB -1; 
                elseif mu_reac == 7
                    XmAB = XmAB +1; 
                end 

elseif sum(state==[1,0,1,1])==4
    % W,MB,MAB DETERMINISTSICH
      % Carry out reaction 
                if mu_reac == 1
                    XmA = XmA +1;
                elseif mu_reac == 2 
                    XmB = XmB +1;
                elseif mu_reac == 3 
                    XmAB = XmAB +1;
                elseif mu_reac == 4
                    XmA = XmA +1;
                elseif mu_reac == 5 
                    XmA = XmA -1; 
                elseif mu_reac == 6
                    XmAB = XmAB +1;
                elseif mu_reac == 7
                    XmAB = XmAB +1; 
                end 

    
elseif sum(state==[1,1,1,1])==4
    % W,MA,MB,MAB DETERMINISTSICH
                if mu_reac == 1
                    XmA = XmA +1;
                elseif mu_reac == 2 
                    XmB = XmB +1;
                elseif mu_reac == 3 
                    XmAB = XmAB +1; 
                elseif mu_reac == 4
                    XmAB = XmAB +1;
                elseif mu_reac == 5
                    XmAB = XmAB +1; 
                end 

elseif sum(state==[0,1,0,0])==4
    % MA DETERMINISTSICH
                if mu_reac == 1
                Xw = Xw+1;
                 elseif mu_reac == 2
                Xw = Xw-1; 
                 elseif mu_reac == 3
                    XmA = XmA +1;
                elseif mu_reac == 4
                    XmB = XmB +1;
                elseif mu_reac == 5 
                    XmAB = XmAB +1;
                elseif mu_reac == 6
                    XmAB = XmAB +1;
                elseif mu_reac == 7
                    XmB = XmB +1; 
                elseif mu_reac == 8
                    XmB = XmB -1; 
                elseif mu_reac == 9
                    XmAB = XmAB +1;
                elseif mu_reac == 10
                    XmAB = XmAB +1;
                else 
                    XmAB = XmAB -1; 
                end 
    
elseif sum(state==[0,0,1,0])==4
    % MB DETERMINISTSICH
                if mu_reac == 1
                Xw = Xw+1;
                 elseif mu_reac == 2
                Xw = Xw-1; 
                 elseif mu_reac == 3
                    XmA = XmA +1;
                elseif mu_reac == 4
                    XmB = XmB +1;
                elseif mu_reac == 5 
                    XmAB = XmAB +1;
                elseif mu_reac == 6
                    XmA = XmA +1;
                elseif mu_reac == 7 
                    XmA = XmA -1; 
                elseif mu_reac == 8
                    XmAB = XmAB +1;
                elseif mu_reac == 9
                    XmAB = XmAB +1;
                elseif mu_reac == 10
                    XmAB = XmAB +1;
                else 
                    XmAB = XmAB -1; 
                end 
    
    
elseif sum(state==[0,0,0,1])==4
    % MAB DETERMINISTSICH
                if mu_reac == 1
                Xw = Xw+1;
                 elseif mu_reac == 2
                Xw = Xw-1; 
                 elseif mu_reac == 3
                    XmA = XmA +1;
                elseif mu_reac == 4
                    XmB = XmB +1;
                elseif mu_reac == 5 
                    XmAB = XmAB +1;
                elseif mu_reac == 6
                    XmA = XmA +1;
                elseif mu_reac == 7 
                    XmA = XmA -1; 
                elseif mu_reac == 8
                    XmAB = XmAB +1;
                elseif mu_reac == 9
                    XmB = XmB +1; 
                elseif mu_reac == 10
                    XmB = XmB -1; 
                elseif mu_reac == 11
                    XmAB = XmAB +1;
                end 
    
elseif sum(state==[0,1,1,0])==4
    % MA,MB DETERMINISTSICH
                if mu_reac == 1
                Xw = Xw+1;
                 elseif mu_reac == 2
                Xw = Xw-1; 
                 elseif mu_reac == 3
                    XmA = XmA +1;
                elseif mu_reac == 4
                    XmB = XmB +1;
                elseif mu_reac == 5 
                    XmAB = XmAB +1;
                elseif mu_reac == 6
                    XmAB = XmAB +1;
                elseif mu_reac == 7
                    XmAB = XmAB +1;
                elseif mu_reac == 8
                    XmAB = XmAB +1;
                else 
                    XmAB = XmAB -1; 
                end 
    
elseif sum(state==[0,1,0,1])==4
    % MA,MAB DETERMINISTSICH
                if mu_reac == 1
                Xw = Xw+1;
                 elseif mu_reac == 2
                Xw = Xw-1; 
                 elseif mu_reac == 3
                    XmA = XmA +1;
                elseif mu_reac == 4
                    XmB = XmB +1;
                elseif mu_reac == 5 
                    XmAB = XmAB +1; 
                elseif mu_reac == 6
                    XmAB = XmAB +1;
                elseif mu_reac == 7
                    XmB = XmB +1; 
                elseif mu_reac == 8
                    XmB = XmB -1; 
                elseif mu_reac == 9
                    XmAB = XmAB +1;
                end 
    
    
elseif sum(state==[0,0,1,1])==4
    % MB,MAB DETERMINISTSICH
                if mu_reac == 1
                Xw = Xw+1;
                 elseif mu_reac == 2
                Xw = Xw-1; 
                 elseif mu_reac == 3
                    XmA = XmA +1;
                elseif mu_reac == 4
                    XmB = XmB +1;
                elseif mu_reac == 5 
                    XmAB = XmAB +1;
                elseif mu_reac == 6
                    XmA = XmA +1;
                elseif mu_reac == 7 
                    XmA = XmA -1; 
                elseif mu_reac == 8
                    XmAB = XmAB +1;
                elseif mu_reac == 9
                    XmAB = XmAB +1;
                end 
    
    
elseif sum(state==[0,1,1,1])==4
    % MA,MB,MAB DETERMINISTSICH
                if mu_reac == 1
                Xw = Xw+1;
                 elseif mu_reac == 2
                Xw = Xw-1; 
                 elseif mu_reac == 3
                    XmA = XmA +1;
                elseif mu_reac == 4
                    XmB = XmB +1;
                elseif mu_reac == 5 
                    XmAB = XmAB +1;
                elseif mu_reac == 6
                    XmAB = XmAB +1;
                elseif mu_reac == 7 
                elseif mu_reac == 8
                    XmAB = XmAB +1;
                end 
    
end

end

