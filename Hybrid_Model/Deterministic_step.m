function [Xw_t,XmA_t,XmB_t,XmAB_t] = Deterministic_step(state,P,model,times,Xw,XmA,XmB,XmAB)
% Function to solve the ODE system for all subpopulations that behave
% deterministically based on the state vector 
% INPUT 
% state  = 1x4 vector indicating with 1 if the type should be handeld
%          deterministically or stochastically with 0 
%          order of types [W,MA,MB,MAB] 
% P      = Model Parameters 
% model  = indicates weather the simulation is run for the Lab or the Pat
%          environment 
% times  = time step 
% Xw    = old cell count of WT 
% XmA   = old cell count of MA
% XmB   = old cell count of MB 
% XmAB  = old cell count of MAB 
% 
% OUTPUT
% Xw_t  = new cell count of WT 
% XmA_t  = new cell count of MA
% XmB_t  = new cell count of MB 
% XmAB_t = new cell count of MAB 

%==========================================================================

% Function hanlde for per capita growth rate
if strcmp(model,'LAB')
    psi = @(N,t,pd,u) Psi_of_c_CC_u(P.PD(pd),P.Lab,t,N,P.CC,u);
else
    psi = @(N_t,t,pd,u) Psi_with_Int_CC_and_u(P.INT,P.CC,P.PD(pd),P.PK,N_t,t,u);
end

%Optiond for each ODE Sys: tolerance 
options = odeset('AbsTol',1e-10,'RelTol',1e-10);

        
%==========================================================================

% Calculate new cell numbers through deterministic step based on the state vector 
if sum(state==[1,0,0,0])==4 
    
    % W DETERMINISTSICH
    % Define the ODE System: 
    odeSys = @(t,dyn) psi(dyn+XmA+XmB+XmAB,t,1,0)*dyn(1);
    % Solve ODE System 
    % Init value: current value for Xw 
    [~,W_det] = ode45(odeSys,times,Xw, options);
    
    %Update cell numbers 
    Xw_t = W_det(end); 
    XmA_t = XmA;
    XmB_t = XmB; 
    XmAB_t = XmAB; 
          
elseif sum(state==[1,1,0,0])==4
    % W,MA DETERMINISTSICH
    % Define the ODE System: 
    odeSys = @(t,dyn) [psi(sum(dyn)+XmB+XmAB,t,1,0)*dyn(1);...
                       psi(sum(dyn)+XmB+XmAB,t,2,0)*dyn(2)];
    % Initial conditions 
    init = [Xw,XmA];
    % Solve ODE System 
    % Init value: current value for Xw 
    [~,Dyn] = ode45(odeSys,times,init, options);
    
    %Update cell numbers 
    Xw_t = Dyn(end,1); 
    XmA_t = Dyn(end,2);
    XmB_t = XmB; 
    XmAB_t = XmAB; 
    
elseif sum(state==[1,0,1,0])==4
    % W,MB DETERMINISTSICH
    % Define the ODE System: 
    odeSys = @(t,dyn) [psi(sum(dyn)+XmA+XmAB,t,1,0)*dyn(1);...
                       psi(sum(dyn)+XmA+XmAB,t,3,0)*dyn(2)];
    % Initial conditions 
    init = [Xw,XmB];
    % Solve ODE System 
    % Init value: current value for Xw 
    [~,Dyn] = ode45(odeSys,times,init, options);
    
    %Update cell numbers 
    Xw_t = Dyn(end,1); 
    XmA_t = XmA;
    XmB_t = Dyn(end,2); 
    XmAB_t = XmAB; 
    
elseif sum(state==[1,0,0,1])==4
    % W,MAB DETERMINISTSICH
    % Define the ODE System: 
    odeSys = @(t,dyn) [psi(sum(dyn)+XmA+XmB,t,1,0)*dyn(1);...
                       psi(sum(dyn)+XmA+XmB,t,4,0)*dyn(2)];
    % Initial conditions 
    init = [Xw,XmAB];
    % Solve ODE System 
    % Init value: current value for Xw 
    [~,Dyn] = ode45(odeSys,times,init, options);
    
    %Update cell numbers 
    Xw_t = Dyn(end,1); 
    XmA_t = XmA;
    XmB_t = XmB; 
    XmAB_t = Dyn(end,2);
    
elseif sum(state==[1,1,1,0])==4
    % W,MA,MB DETERMINISTSICH
    % Define the ODE System: 
    odeSys = @(t,dyn) [psi(sum(dyn)+XmAB,t,1,0)*dyn(1);...
                       psi(sum(dyn)+XmAB,t,2,0)*dyn(2);...
                       psi(sum(dyn)+XmAB,t,3,0)*dyn(3)];
    % Initial conditions 
    init = [Xw,XmA,XmB];
    % Solve ODE System 
    % Init value: current value for Xw 
    [~,Dyn] = ode45(odeSys,times,init, options);
    
    %Update cell numbers 
    Xw_t = Dyn(end,1); 
    XmA_t = Dyn(end,2);
    XmB_t = Dyn(end,3); 
    XmAB_t = XmAB;
    
elseif sum(state==[1,1,0,1])==4
    % W,MA,MAB DETERMINISTSICH
    % Define the ODE System: 
    odeSys = @(t,dyn) [psi(sum(dyn)+XmB,t,1,0)*dyn(1);...
                       psi(sum(dyn)+XmB,t,2,0)*dyn(2);...
                       psi(sum(dyn)+XmB,t,4,0)*dyn(3)];
    % Initial conditions 
    init = [Xw,XmA,XmAB];
    % Solve ODE System 
    % Init value: current value for Xw 
    [~,Dyn] = ode45(odeSys,times,init, options);
    
    %Update cell numbers 
    Xw_t = Dyn(end,1); 
    XmA_t = Dyn(end,2);
    XmB_t = XmB; 
    XmAB_t = Dyn(end,3);
    
elseif sum(state==[1,0,1,1])==4
    % W,MB,MAB DETERMINISTSICH
    % Define the ODE System: 
    odeSys = @(t,dyn) [psi(sum(dyn)+XmA,t,1,0)*dyn(1);...
                       psi(sum(dyn)+XmA,t,3,0)*dyn(2);...
                       psi(sum(dyn)+XmA,t,4,0)*dyn(3)];
    % Initial conditions 
    init = [Xw,XmB,XmAB];
    % Solve ODE System 
    % Init value: current value for Xw 
    [~,Dyn] = ode45(odeSys,times,init, options);
    
    %Update cell numbers 
    Xw_t = Dyn(end,1); 
    XmA_t = XmA;
    XmB_t = Dyn(end,2); 
    XmAB_t = Dyn(end,3);
    
    
elseif sum(state==[1,1,1,1])==4
    % W,MA,MB,MAB DETERMINISTSICH
    % Define the ODE System: 
    odeSys = @(t,dyn) [psi(sum(dyn),t,1,0)*dyn(1);...
                       psi(sum(dyn),t,2,0)*dyn(2);...
                       psi(sum(dyn),t,3,0)*dyn(3);...
                       psi(sum(dyn),t,4,0)*dyn(4)];
    % Initial conditions 
    init = [Xw,XmA,XmB,XmAB];
    % Solve ODE System 
    % Init value: current value for Xw 
    [~,Dyn] = ode45(odeSys,times,init, options);
    
    %Update cell numbers 
    Xw_t = Dyn(end,1); 
    XmA_t = Dyn(end,2);
    XmB_t = Dyn(end,3); 
    XmAB_t = Dyn(end,4);

elseif sum(state==[0,1,0,0])==4
    % MA DETERMINISTSICH
      % Define the ODE System: 
    odeSys = @(t,dyn) psi(dyn+Xw+XmB+XmAB,t,2,0)*dyn(1);
    % Solve ODE System 
    % Init value: current value for Xw 
    [~,Dyn] = ode45(odeSys,times,XmA, options);
    
    %Update cell numbers 
    Xw_t = Xw; 
    XmA_t = Dyn(end,1);
    XmB_t = XmB; 
    XmAB_t = XmAB;
    
elseif sum(state==[0,0,1,0])==4
    % MB DETERMINISTSICH
    % Define the ODE System: 
    odeSys = @(t,dyn) psi(dyn+Xw+XmA+XmAB,t,3,0)*dyn(1);
    % Solve ODE System 
    % Init value: current value for Xw 
    [~,Dyn] = ode45(odeSys,times,XmB, options);
    
    %Update cell numbers 
    Xw_t = Xw; 
    XmA_t = XmA;
    XmB_t = Dyn(end,1); 
    XmAB_t = XmAB;
    
elseif sum(state==[0,0,0,1])==4
    % MAB DETERMINISTSICH
    % Define the ODE System: 
    odeSys = @(t,dyn) psi(dyn+Xw+XmB+XmA,t,4,0)*dyn(1);
    % Solve ODE System 
    % Init value: current value for Xw 
    [~,Dyn] = ode45(odeSys,times,XmA, options);
    
    %Update cell numbers 
    Xw_t = Xw; 
    XmA_t = XmA;
    XmB_t = XmB; 
    XmAB_t = Dyn(end,1);
    
elseif sum(state==[0,1,1,0])==4
    % MA,MB DETERMINISTSICH
    % Define the ODE System: 
    odeSys = @(t,dyn) [psi(sum(dyn)+Xw+XmAB,t,2,0)*dyn(1);...
                       psi(sum(dyn)+Xw+XmAB,t,3,0)*dyn(2)];
    % Initial conditions 
    init = [XmA,XmB];
    % Solve ODE System 
    % Init value: current value for Xw 
    [~,Dyn] = ode45(odeSys,times,init, options);
    
    %Update cell numbers 
    Xw_t = Xw; 
    XmA_t = Dyn(end,1);
    XmB_t = Dyn(end,2); 
    XmAB_t = XmAB; 
    
elseif sum(state==[0,1,0,1])==4
    % MA,MAB DETERMINISTSICH
    % Define the ODE System: 
    odeSys = @(t,dyn) [psi(sum(dyn)+Xw+XmB,t,2,0)*dyn(1);...
                       psi(sum(dyn)+Xw+XmB,t,4,0)*dyn(2)];
    % Initial conditions 
    init = [XmA,XmAB];
    % Solve ODE System 
    % Init value: current value for Xw 
    [~,Dyn] = ode45(odeSys,times,init, options);
    
    %Update cell numbers 
    Xw_t = Xw; 
    XmA_t = Dyn(end,1);
    XmB_t = XmB; 
    XmAB_t = Dyn(end,2); 
    
elseif sum(state==[0,0,1,1])==4
    % MB,MAB DETERMINISTSICH
    % Define the ODE System: 
    odeSys = @(t,dyn) [psi(sum(dyn)+Xw+XmA,t,3,0)*dyn(1);...
                       psi(sum(dyn)+Xw+XmA,t,4,0)*dyn(2)];
    % Initial conditions 
    init = [XmB,XmAB];
    % Solve ODE System 
    % Init value: current value for Xw 
    [~,Dyn] = ode45(odeSys,times,init, options);
    
    %Update cell numbers 
    Xw_t = Xw; 
    XmA_t = XmA;
    XmB_t = Dyn(end,1); 
    XmAB_t = Dyn(end,2); 
    
elseif sum(state==[0,1,1,1])==4
    % MA,MB,MAB DETERMINISTSICH
    % Define the ODE System: 
    odeSys = @(t,dyn) [psi(sum(dyn)+Xw,t,2,0)*dyn(1);...
                       psi(sum(dyn)+Xw,t,3,0)*dyn(2);...
                       psi(sum(dyn)+Xw,t,4,0)*dyn(3)];
    % Initial conditions 
    init = [XmA,XmB,XmAB];
    % Solve ODE System 
    % Init value: current value for Xw 
    [~,Dyn] = ode45(odeSys,times,init, options);
    
    %Update cell numbers 
    Xw_t = Xw; 
    XmA_t = Dyn(end,1);
    XmB_t = Dyn(end,2); 
    XmAB_t = Dyn(end,3);
    
end
end

