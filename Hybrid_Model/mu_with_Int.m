function [mu_t] = mu_with_Int(INT,PD,PK,t)
% Mutationrate on replicationrate: 17.12.20
% Psi with interaction and carrying capacity: 21.09.20
% Function to calculate the per captiva growth rate of the bacteria during
% antibiotic treatment for the PK/PD Model with drug interaction 
% Hill function of REGEOS et al. 2004 with applied drug interaction therm
% for the effect of each drug from Wicha et al 2017 and BLISS independency
% assumption. The function also includes a carrying capacity that is so far
% not fixed but can be chosen from sigmoidal, linear or logit function:
% 
% INPUT:
% INT   = 1x2 struct array of interaction parameter (one struct for each drug) 
%         (struct 1: interaction B on A, struct 2: interaction A on B)
%         I    - maximum effect of interaction (I in [-1,inf))
%         EC50 - concentration to reach 50% of I 
%         k    - hill coefficient 
%        
% 
%
% PD    = 1x1 struct array of PD parameter of the considered type
%         (struct 1: W, struct 2: MA, struct 3: MB, struct 4: MAB)
%         Psi_max  - maximum growth rate in absense of AB
%         mu_0     - intrinsic death rate 
%         Psi_minA - maximum kill rate of drug A
%         Psi_minB - maximum kill rate of drug B
%         zMICA    - MIC for drug A
%         zMICB    - MIC for drug B
%         KA       - hill coefficient regarding drug A
%         KB       - hill coefficient regarding drug B
%
% 
% PK    = 1x1 struct of PK parameter for considered drug
%         Kct    - transition rate central to tissue compartment
%         Ktc    - transition rate tissue to central compartment 
%         Kel    - elimination rate from centram compartment
%         Kr     - receptor binding in tissue compartment
%         D      - Dosis of the drug
%         tau    - lenght of infusion (if ad = 'I') (dtau <= Tthis !!!)
%         ad     - administration type (Bolus : 'B', Infusion 'I')
%         Tthis  - Time of one part (one admin.) in cycle for considered drug
%         Tother - Time of one part (one admin.) in cycle for other drug
%         cycle  - Schedule for the cycling indicating with '1' when the
%                  considered drug is administered 
%
% t     = Point in time 
% u     = mutationrate that needs to be considered for the specific type 

% OUTPUT: 
% psi_val = combined per capita growth rate
% =========================================================================

%==========================================================================
%       DRUG CONCNETRATIONS AND INTERACTION
%==========================================================================

% Calculate Drugconcentration at time t: 
c_A = Drug_Concentration(PK(1),t);
c_B = Drug_Concentration(PK(2),t); 

% Calculate interaction term for impact of B on A:
if INT(1).I ~= 0
    INT_BA = 1 + (INT(1).I.*c_B.^INT(1).k./(INT(1).EC50^INT(1).k+c_B.^INT(1).k));
else 
    INT_BA = 1;
end
    
% Calculate interaction term for impact of A on B:D
if INT(2).I ~= 0
    INT_AB = 1 + (INT(2).I.*c_A.^INT(2).k./(INT(2).EC50^INT(2).k+c_A.^INT(2).k));
else 
    INT_AB = 1; 
end
%==========================================================================
%       GROWTH RATE WITH CC
%==========================================================================

% Killrate mu depending on Drug A
mu_A =   ((PD.Psi_max-PD.Psi_minA)*((c_A./(INT_BA.*PD.zMICA)).^PD.kA)./...
        ((c_A./(INT_BA.*PD.zMICA)).^PD.kA -(PD.Psi_minA/PD.Psi_max)));
% Killrate mu depending on Drug B
mu_B =  ((PD.Psi_max-PD.Psi_minB)*((c_B./(INT_AB.*PD.zMICB)).^PD.kB)./...
        ((c_B./(INT_AB.*PD.zMICB)).^PD.kB -(PD.Psi_minB/PD.Psi_max)));    

% Calculate the combined effect (Bliss Independency on killrates based 
% on Baeder et al. 2016): 
mu_t = mu_A+mu_B;
 
end
