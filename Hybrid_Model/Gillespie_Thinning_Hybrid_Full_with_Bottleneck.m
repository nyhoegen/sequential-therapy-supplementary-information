function [Sym] = Gillespie_Thinning_Hybrid_Full_with_Bottleneck2(P,N_det,eps,seed)
% Function to simulate the cell dynamics during the sequential treatment in
% accordance with the deterministic model (e.g. Two_Drug_Cycling_CC.m,
% Lab_Cycling_NumCC_u.m) by the use of the Gilliespie algorithm. 
% To calculate the occurance of an event based on the time dependent rates 
% the thinning algorithm (to simulate a time inhomogenous point process) is
% applied. 
%
% INPUT
% P        - struct array depending on the model choice (P = Patient,L = Lab)
%         always in the arry: 
%    G     = 1x1 struct array containing genral parameter 
%             N0  - number of bacteria in initial population 
%             p0i - fraction of mutatnts in initial population (i in {a,b,ab})
%             u0i - mutation rates of mutation i (i in {a,b,ab})
% 
%    PD    = 1x4 struct array of PD parameter (one struct for each type)
%             (struct 1: W, struct 2: MA, struct 3: MB, struct 4: MAB)
%             Psi_max  - maximum growth rate in absense of AB
%             Psi_minA - maximum kill rate of drug A
%             Psi_minB - maximum kill rate of drug B
%             zMICA    - MIC for drug A
%             zMICB    - MIC for drug B
%             KA       - hill coefficient regarding drug A
%             KB       - hill coefficient regarding drug B
%
%    CC    = 1x1 struct array of CC paramter
%             type - defines the type of function (sig,logit,lin) 
%             link - defines the link (pmax, growth) 
%             Nmax - carrying limit 
%             k    - Hill coefficient, in case of sigmoidal function 
%             N50  - Cell number for 50% inhibition (sig) or n50 (logit)
%
%    M     = 1x1 struct array that defines the model 
%            cut  - model with or without cutoff (binary variable)
%            show - show data below one or not 
%            dil  - model with or without dilution (binary variable)
% N_det    - Threshold for Wildype cells 
% eps      - tolerance to adjust lambda_sup at the switches of the drugs
% seed     - seed for random generator 
% 
% OUTPUT 
% Sim      - 1x5 struct array:
%           tk: Vector with time points
%           W:  Cell numbers of wildtype for each time point  
%           MA: Cell numbers of mutant A for each time point
%           MB: Cell numbers of mutant B for each time point
%           MAB: Cell numbers of mutant AB for each time point
%--------------------------------------------------------------------------
% set the seed for the random generator 
rng(seed);
% Step size for deterministic setps 
step = 1; 
% Set Variables dependent on the struct array P: 
N0 = P.G.N0; 
uA = P.G.uA; 
uB = P.G.uB; 
Nmax = P.CC.Nmax; 
r0_W = P.PD(1).Psi_max+P.PD(1).mu_0; 
r0_MA = P.PD(2).Psi_max+P.PD(2).mu_0;
r0_MB = P.PD(3).Psi_max+P.PD(3).mu_0;
r0_MAB = P.PD(4).Psi_max+P.PD(4).mu_0;

% Calculate the time points at which the drugs are administered to adjust
% lambda_sup correctly at that time points
% Number of Administartions during the cycle 
% Lab 

% Calculate the end of the treatment: 
t_max = sum((P.Lab.TA*P.Lab.cycle)+(P.Lab.TB*(~P.Lab.cycle)));

% Calculate the times of Administration (for Dilution)
n_ad = length(P.Lab.cycle); 
tri_mat = triu(ones(n_ad));
ad_times = (P.Lab.cycle*P.Lab.TA+(~P.Lab.cycle)*P.Lab.TB)*tri_mat;



% Fundtion handle for the death rate function mu(c) 
mu = @(t,Nt,i) mu_of_c(P.PD(i),P.Lab,t);


% Initialize current subpopulation sizes (Xw,XmA,XmB,XmAB) 
Xw = N0; 
XmA = 0; 
XmB = 0; 
XmAB = 0; 
Xall = Xw+XmA+XmB+XmAB; 

% Initialize state vector 
state = [1,0,0,0]; 


% Initialize the current time point (s for homogen. and t for inhomo. process)
% and hi vector 
t = 0; 
s = 0; 

% Save all current data 
Sym.tk(1,1) = t;
Sym.sk(1,1) = s;
Sym.W(1,1) = Xw; 
Sym.MA(1,1) = XmA; 
Sym.MB(1,1) = XmB; 
Sym.MAB(1,1) = XmAB;

% Use counter to update Sym 
i = 2; 
j = 2;

% Initialize lambda_sup and lambda_s
lambda_s = 0; 


    while s < t_max && sum(state) > 0 && Xall > 0 
        % Calculate lambda_sup depending on the time point:
        % check in which cycle s is: 
        cycle = find(s<ad_times,1,'first'); 
        if (s+eps)>=ad_times(cycle)
            lambda_sup = sum(Ci_max(state,P,'LAB',ad_times(cycle)).*[Xw;XmA;XmB;XmAB]);
        else
            lambda_sup = sum(Ci_max(state,P,'LAB',s).*[Xw;XmA;XmB;XmAB]);
        end

        % Generate two random numbers r1,r2 and r3 (equvalent to D in Thinning)
        r1 = rand(1,1); 
        r2 = rand(1,1);
        r3 = rand(1,1);

        % Calculate the time point tau 
        tau = (1/lambda_sup)*log(1/r1);  
        
        % Bottleneck in case we are at the end of an growth period: 
        % check if within the time step tau the new administration
        % occured 
        min_step = min([tau,step]); 
            if (s+min_step)>(ad_times(cycle))
                
                % Calculate change in determinsitic cell numbers
                times = [s,ad_times(cycle)];
                [Xw,XmA,XmB,XmAB] = Deterministic_step(state,P,'LAB',times,Xw,XmA,XmB,XmAB);

                % sample cells before calculateing the new cell numbers
                % after crossing the time point of administration
                disp(cycle);
                Xw = round(Xw);
                XmA = round(XmA);
                XmB = round(XmB);
                XmAB = round(XmAB);

                num_Cells =sum([Xw,XmA,XmB,XmAB]);
                sample = round(num_Cells/100); 
                W = hygernd(num_Cells,Xw,sample); 
                mA = hygernd(num_Cells-Xw,XmA,sample-W);
                mB = hygernd(num_Cells-Xw-XmA,XmB,sample-W-mA);
                mAB = sample-W-mA-mB;

                % Save as new cell numbers:
                Xw = W;
                XmA = mA;
                XmB = mB;
                XmAB = mAB;
                
                % Set t and s to the start of next cycle 
                s = ad_times(cycle); 
                disp(s);
                t = s;
                
                % Save all current data 
                    Sym.tk(1,i) = t;
                    Sym.W(1,i) = Xw; 
                    Sym.MA(1,i) = XmA; 
                    Sym.MB(1,i) = XmB; 
                    Sym.MAB(1,i) = XmAB;
                
                    % increase counter 
                    i = i+1;
                % Calculate tau again 
                 % Calculate lambda_sup depending on the time point:
                % check in which cycle s is: 
                cycle = find(s<ad_times,1,'first'); 
                if (s+eps)>=ad_times(cycle)
                    lambda_sup = sum(Ci_max(state,P,'LAB',ad_times(cycle)).*[Xw;XmA;XmB;XmAB]);
                else
                    lambda_sup = sum(Ci_max(state,P,'LAB',s).*[Xw;XmA;XmB;XmAB]);
                end

                % Generate random number r1
                r1 = rand(1,1); 

                % Calculate the time point tau 
                tau = (1/lambda_sup)*log(1/r1); 
                disp([s,s+tau]);
            end
        % If time steps exceed step - Update the growth of the deterministic types 
        % and keep populationsize of other types as they are 
        % Otherwise use the step for next stochastic event 
        if tau > step 
            tau = step; 
            % Update the time 
            s = s+tau; 
            
            % update time 
                t = s;
            % Update Deterministic Types first
            %======================================================
            % SOLVER ODE45 
            %======================================================
            % Time step 
            times = [s-tau,s];
            % function to update deterministic types 
            [Xw,XmA,XmB,XmAB] = Deterministic_step(state,P,'LAB',times,Xw,XmA,XmB,XmAB); 

                 
                % Update Xall 
                Xall = Xw+XmA+XmB+XmAB;

                % Save all current data 
                Sym.tk(1,i) = t;
                Sym.W(1,i) = Xw; 
                Sym.MA(1,i) = XmA; 
                Sym.MB(1,i) = XmB; 
                Sym.MAB(1,i) = XmAB;

                % increase counter 
                i = i+1;

                Sym.sk(1,j) = s;
                Sym.W_sk(1,j) = Xw;
                Sym.lambda_sup(1,j-1) = lambda_sup;
                Sym.lambda_t(1,j-1) = lambda_s; 
                %Sym.c_i(j-1,:) = c_i_s';

                % increase other counter 
                j = j+1;
                % Check State change 
                state = [Xw,XmA,XmB,XmAB]> N_det; 
                
        else


            % Update the time 
            s = s+tau; 
            
            % Update Deterministic Types first
            %======================================================
            % SOLVER ODE45 
            %======================================================
            % Time step 
            times = [s-tau,s];
            % function to update deterministic types
            if tau < 0
                disp(tau);
            end
            [Xw,XmA,XmB,XmAB] = Deterministic_step(state,P,'LAB',times,Xw,XmA,XmB,XmAB);
                % Update Xall 
                Xall = Xw+XmA+XmB+XmAB;
                
                % Calculate labda (t - for the new t)  
                lambda_i = Lambda_i(state,P,'LAB',s,Xw,XmA,XmB,XmAB);
                lambda_s = sum(lambda_i); 

                % Decide if the point will be part of the inhomogeneous process: 
                if r3 <= lambda_s/lambda_sup

                    % update time 
                    t = s; 

                    % Calculate mu (the reaction that will occur at t+tau
                    alpha_sum = cumsum(lambda_i); 
                    mu_reac = find(alpha_sum >= r2*lambda_s,1,'first'); 

                    % Carry out reaction 
                    [Xw,XmA,XmB,XmAB] = Reaction(state,mu_reac,Xw,XmA,XmB,XmAB); 

                    % Update Xall 
                    Xall = Xw+XmA+XmB+XmAB;

                    % Save all current data 
                    Sym.tk(1,i) = t;
                    Sym.W(1,i) = Xw; 
                    Sym.MA(1,i) = XmA; 
                    Sym.MB(1,i) = XmB; 
                    Sym.MAB(1,i) = XmAB;

                    % increase counter 
                    i = i+1;

                end

                Sym.sk(1,j) = s;
                Sym.W_sk(1,j) = Xw;
                Sym.lambda_sup(1,j-1) = lambda_sup;
                Sym.lambda_t(1,j-1) = lambda_s; 

                % increase other counter 
                j = j+1;
                
                % Check State change 
                state = [Xw,XmA,XmB,XmAB]> N_det; 
             
        end
    end

    %==========================================================================
    % EVERYTHING STOCHASTIC 
    %==========================================================================

    % c_i with reactions for wildtype 
    % Function handle to calculate the vetcor ci for different time points and 
    % different current population size 
    c_i = @(t,Xall) [(1-Xall/Nmax)*r0_W*(1-uA-uB+uA*uB);... % growth of a wildtype 
                     P.PD(1).mu_0 + mu(t,Xall,1); ...       % death of a wildtype
                     (1-uB)*uA*(1-Xall/Nmax)*r0_W;...       % wildtype replicates and mutates to MA
                     (1-uA)*uB*(1-Xall/Nmax)*r0_W;...       % wildtype replicates and mutates to MB
                     uB*uA*(1-Xall/Nmax)*r0_W;...           % wildtype replicates and mutates to MAB
                     (1-uB)*(1-Xall/Nmax)*r0_MA;...         % growth of mutant MA
                     P.PD(2).mu_0 + mu(t,Xall,2);           % death of mutant MA
                     uB*(1-Xall/Nmax)*r0_MA;...             % mutant MA mutates to mutant MAB 
                     (1-uA)*(1-Xall/Nmax)*r0_MB;...         % growth of mutant MB
                     P.PD(3).mu_0 + mu(t,Xall,3);           % death of mutant MB
                     uA*(1-Xall/Nmax)*r0_MB;                % mutant MB mutates to mutant MAB        
                     (1-Xall/Nmax)*r0_MAB;...               % growth of mutant MAB
                     P.PD(4).mu_0 + mu(t,Xall,4)];          % death of mutant MAB

    % Define ci_max value as a proxi for lambda_sup (supremum of lambda(t))
    % Maximal possible values of c_i summed for the 4 types 
    ci_max =@(t) [r0_W+P.PD(1).mu_0+mu(t,0,1);...
              r0_MA+P.PD(2).mu_0+mu(t,0,2);... 
              r0_MB+P.PD(3).mu_0+mu(t,0,3);...   
              r0_MAB+P.PD(4).mu_0+mu(t,0,4)];  
    % Update h_i to correct format 
    h_i = [repmat(Xw,[1 5]),repmat(XmA,[1 3]),repmat(XmB,[1 3]),repmat(XmAB,[1 2])];
    % Round XW
    Xw = round(Xw); 
    % Update Xall 
    Xall = Xw+XmA+XmB+XmAB;

    while s < t_max && Xall > 0 && sum(state) == 0
        % Calculate lambda_sup depending on the time point:
        % check in which cycle s is: 
        cycle = find(s<ad_times,1,'first'); 
        if (s+eps)>=ad_times(cycle)
            lambda_sup = sum(ci_max(ad_times(cycle)).*[Xw;XmA;XmB;XmAB]);
        else
            lambda_sup = sum(ci_max(s).*[Xw;XmA;XmB;XmAB]);
        end

        % Generate two random numbers r1,r2 and r3 (equvalent to D in Thinning)
        r1 = rand(1,1); 
        r2 = rand(1,1);
        r3 = rand(1,1);

        % Calculate the time point tau 
        tau = (1/lambda_sup)*log(1/r1); 
        
        
        % Bottleneck: 
        % check if within the time step tau the new administration
        % occured 
        if (s+tau)>ad_times(cycle)
            disp('now');
            % sample cells before calculateing the new cell numbers
            % after crossing the time point of administration
            Xw = round(Xw);
            XmA = round(XmA);
            XmB = round(XmB);
            XmAB = round(XmAB);

            num_Cells =sum([Xw,XmA,XmB,XmAB]);
            sample = round(num_Cells/100); 
            W = hygernd(num_Cells,Xw,sample); 
            mA = hygernd(num_Cells-Xw,XmA,sample-W);
            mB = hygernd(num_Cells-Xw-XmA,XmB,sample-W-mA);
            mAB = sample-W-mA-mB;

            % Save as new cell numbers:
            Xw = W;
            XmA = mA;
            XmB = mB;
            XmAB = mAB;
        end
                
        % Update the time 
        s = s+tau;

        % Calculate labda (t - for the new t) 
        c_i_s = c_i(s,Xall); 
        lambda_i = c_i_s'.*h_i;
        lambda_s = sum(lambda_i); 

        % Decide if the point will be part of the inhomogeneous process: 
        if r3 <= lambda_s/lambda_sup

            % update time 
            t = s; 
            % Calculate mu (the reaction that will occur at t+tau
            alpha_sum = cumsum(lambda_i); 
            mu_reac = find(alpha_sum >= r2*lambda_s,1,'first'); 

            % Carry out reaction 
            if mu_reac == 1
                Xw = Xw+1;
            elseif mu_reac == 2
                Xw = Xw-1; 
            elseif ismember(mu_reac,[3,6]) 
                XmA = XmA +1; 
            elseif ismember(mu_reac,[4,9]) 
                XmB = XmB +1; 
            elseif ismember(mu_reac, [5,8,11,12]) 
                XmAB = XmAB +1; 
            elseif mu_reac == 7 
                XmA = XmA -1; 
            elseif mu_reac == 10
                XmB = XmB -1; 
            else 
                XmAB = XmAB -1; 
            end

            % Update Xall 
            Xall = Xw+XmA+XmB+XmAB;

            % Update hi accordingly 
            h_i = [repmat(Xw,[1 5]),repmat(XmA,[1 3]),repmat(XmB,[1 3]),repmat(XmAB,[1 2])];

            % Save all current data 
            Sym.tk(1,i) = t;
            Sym.W(1,i) = Xw; 
            Sym.MA(1,i) = XmA; 
            Sym.MB(1,i) = XmB; 
            Sym.MAB(1,i) = XmAB;

            % increase counter 
            i = i+1;

        end
        Sym.sk(1,j) = s;
        Sym.lambda_sup(1,j-1) = lambda_sup;
        Sym.lambda_t(1,j-1) = lambda_s; 
        %Sym.c_i(j-1,:) = c_i_s';
        Sym.W_sk(1,j) = Xw;
        % increase other counter 
        j = j+1;
    end

end

