function [lambda_i] = Lambda_i(state,P,model,t,Xw,XmA,XmB,XmAB)
% Function to calculate the c_i and ci_max functionhandle depending on the
% state of the system (e.g. what cell type should be handeled
% deterministically). Function is used in Giellespie_Thinning_Hybrid_Full.m
% 
% INPUT 
% state  = 1x4 vector indicating with 1 if the type should be handeld
%          deterministically or stochastically with 0 
%          order of types [W,MA,MB,MAB] 
% P      = Model Parameters 
% model  = indicates weather the simulation is run for the Lab or the Pat
%          environment 
% t      = current time point
% Xw     = old cell count of WT 
% XmA    = old cell count of MA
% XmB    = old cell count of MB 
% XmAB   = old cell count of MAB 
% 
% OUTPUT 
% lambda_i = vector that describes the rates of each stochastic events based 
%            on the state of the system 
% 
%==========================================================================
% Set Variables dependent on the struct array P: 
uA = P.G.uA; 
uB = P.G.uB; 
Nmax = P.CC.Nmax; 
r0_W = P.PD(1).Psi_max+P.PD(1).mu_0; 
r0_MA = P.PD(2).Psi_max+P.PD(2).mu_0;
r0_MB = P.PD(3).Psi_max+P.PD(3).mu_0;
r0_MAB = P.PD(4).Psi_max+P.PD(4).mu_0;

% Fundtion handle for the death rate function mu(c) 
if strcmp(model,'LAB')
    mu = @(t,i) mu_of_c(P.PD(i),P.Lab,t);
else
    mu =@(t,i) mu_with_Int(P.INT,P.PD(i),P.PK,t);
end
 
% Total Population size 
Xall = Xw+XmA+XmB+XmAB;

%==========================================================================

% Calculate Carrying Limit 
CC_t = (1-Xall/Nmax); 
%set to zero if it goes below one 
if CC_t < 0 
    CC_t = 0; 
end

% Calculate ci_max_t based on the state vector 
if sum(state==[1,0,0,0])==4 
    
    % W DETERMINISTSICH
    % Calculate the vector of probabilities 
    c_i =   [(1-uB)*uA*CC_t*r0_W;...       % wildtype replicates and mutates to MA
             (1-uA)*uB*CC_t*r0_W;...       % wildtype replicates and mutates to MB
             uB*uA*CC_t*r0_W;...           % wildtype replicates and mutates to MAB
             (1-uB)*CC_t*r0_MA;...         % growth of mutant MA
             P.PD(2).mu_0 + mu(t,2);           % death of mutant MA
             uB*CC_t*r0_MA;...             % mutant MA mutates to mutant MAB 
             (1-uA)*CC_t*r0_MB;...         % growth of mutant MB
             P.PD(3).mu_0 + mu(t,3);           % death of mutant MB
             uA*CC_t*r0_MB;                % mutant MB mutates to mutant MAB        
             CC_t*r0_MAB;...               % growth of mutant MAB
             P.PD(4).mu_0 + mu(t,4)];          % death of mutant MAB
     % Current cell numbers for each event     
     h_i = [repmat(Xw,[1 3]),repmat(XmA,[1 3]),repmat(XmB,[1 3]),repmat(XmAB,[1 2])];
     % Current rate vector 
     lambda_i = c_i'.*h_i;
          
elseif sum(state==[1,1,0,0])==4
    % W,MA DETERMINISTSICH
      % Calculate the vector of probabilities 
    c_i =   [(1-uB)*uA*CC_t*r0_W;...       % wildtype replicates and mutates to MA
             (1-uA)*uB*CC_t*r0_W;...       % wildtype replicates and mutates to MB
             uB*uA*CC_t*r0_W;...           % wildtype replicates and mutates to MAB
             uB*CC_t*r0_MA;...             % mutant MA mutates to mutant MAB 
             (1-uA)*CC_t*r0_MB;...         % growth of mutant MB
             P.PD(3).mu_0 + mu(t,3);           % death of mutant MB
             uA*CC_t*r0_MB;                % mutant MB mutates to mutant MAB        
             CC_t*r0_MAB;...               % growth of mutant MAB
             P.PD(4).mu_0 + mu(t,4)];          % death of mutant MAB
     % Current cell numbers for each event     
     h_i = [repmat(Xw,[1 3]),repmat(XmA,[1 1]),repmat(XmB,[1 3]),repmat(XmAB,[1 2])];
     % Current rate vector 
     lambda_i = c_i'.*h_i;
    
elseif sum(state==[1,0,1,0])==4
    % W,MB DETERMINISTSICH
     % Calculate the vector of probabilities 
    c_i =   [(1-uB)*uA*CC_t*r0_W;...       % wildtype replicates and mutates to MA
             (1-uA)*uB*CC_t*r0_W;...       % wildtype replicates and mutates to MB
             uB*uA*CC_t*r0_W;...           % wildtype replicates and mutates to MAB
             (1-uB)*CC_t*r0_MA;...         % growth of mutant MA
             P.PD(2).mu_0 + mu(t,2);           % death of mutant MA
             uB*CC_t*r0_MA;...             % mutant MA mutates to mutant MAB 
             uA*CC_t*r0_MB;                % mutant MB mutates to mutant MAB        
             CC_t*r0_MAB;...               % growth of mutant MAB
             P.PD(4).mu_0 + mu(t,4)];          % death of mutant MAB
     % Current cell numbers for each event     
     h_i = [repmat(Xw,[1 3]),repmat(XmA,[1 3]),repmat(XmB,[1 1]),repmat(XmAB,[1 2])];
     % Current rate vector 
     lambda_i = c_i'.*h_i;
    
elseif sum(state==[1,0,0,1])==4
    % W,MAB DETERMINISTSICH
    % Calculate the vector of probabilities 
    c_i =   [(1-uB)*uA*CC_t*r0_W;...       % wildtype replicates and mutates to MA
             (1-uA)*uB*CC_t*r0_W;...       % wildtype replicates and mutates to MB
             uB*uA*CC_t*r0_W;...           % wildtype replicates and mutates to MAB
             (1-uB)*CC_t*r0_MA;...         % growth of mutant MA
             P.PD(2).mu_0 + mu(t,2);           % death of mutant MA
             uB*CC_t*r0_MA;...             % mutant MA mutates to mutant MAB 
             (1-uA)*CC_t*r0_MB;...         % growth of mutant MB
             P.PD(3).mu_0 + mu(t,3);           % death of mutant MB
             uA*CC_t*r0_MB];                % mutant MB mutates to mutant MAB
     % Current cell numbers for each event     
     h_i = [repmat(Xw,[1 3]),repmat(XmA,[1 3]),repmat(XmB,[1 3])];
     % Current rate vector 
     lambda_i = c_i'.*h_i;
    
elseif sum(state==[1,1,1,0])==4
    % W,MA,MB DETERMINISTSICH
    % Calculate the vector of probabilities 
    c_i =   [(1-uB)*uA*CC_t*r0_W;...       % wildtype replicates and mutates to MA
             (1-uA)*uB*CC_t*r0_W;...       % wildtype replicates and mutates to MB
             uB*uA*CC_t*r0_W;...           % wildtype replicates and mutates to MAB
             uB*CC_t*r0_MA;...             % mutant MA mutates to mutant MAB 
             uA*CC_t*r0_MB;                % mutant MB mutates to mutant MAB        
             CC_t*r0_MAB;...               % growth of mutant MAB
             P.PD(4).mu_0 + mu(t,4)];          % death of mutant MAB
     % Current cell numbers for each event     
     h_i = [repmat(Xw,[1 3]),repmat(XmA,[1 1]),repmat(XmB,[1 1]),repmat(XmAB,[1 2])];
     % Current rate vector 
     lambda_i = c_i'.*h_i;
    
    
elseif sum(state==[1,1,0,1])==4
    % W,MA,MAB DETERMINISTSICH
    % Calculate the vector of probabilities 
    c_i =   [(1-uB)*uA*CC_t*r0_W;...       % wildtype replicates and mutates to MA
             (1-uA)*uB*CC_t*r0_W;...       % wildtype replicates and mutates to MB
             uB*uA*CC_t*r0_W;...           % wildtype replicates and mutates to MAB
             uB*CC_t*r0_MA;...             % mutant MA mutates to mutant MAB 
             (1-uA)*CC_t*r0_MB;...         % growth of mutant MB
             P.PD(3).mu_0 + mu(t,3);           % death of mutant MB
             uA*CC_t*r0_MB];                % mutant MB mutates to mutant MAB
     % Current cell numbers for each event     
     h_i = [repmat(Xw,[1 3]),repmat(XmA,[1 1]),repmat(XmB,[1 3])];
     % Current rate vector 
     lambda_i = c_i'.*h_i;
    
elseif sum(state==[1,0,1,1])==4
    % W,MB,MAB DETERMINISTSICH
    % Calculate the vector of probabilities 
    c_i =   [(1-uB)*uA*CC_t*r0_W;...       % wildtype replicates and mutates to MA
             (1-uA)*uB*CC_t*r0_W;...       % wildtype replicates and mutates to MB
             uB*uA*CC_t*r0_W;...           % wildtype replicates and mutates to MAB
             (1-uB)*CC_t*r0_MA;...         % growth of mutant MA
             P.PD(2).mu_0 + mu(t,2);           % death of mutant MA
             uB*CC_t*r0_MA;...             % mutant MA mutates to mutant MAB 
             uA*CC_t*r0_MB];                % mutant MB mutates to mutant MAB 
     % Current cell numbers for each event     
     h_i = [repmat(Xw,[1 3]),repmat(XmA,[1 3]),repmat(XmB,[1 1])];
     % Current rate vector 
     lambda_i = c_i'.*h_i;
    
elseif sum(state==[1,1,1,1])==4
    % W,MA,MB,MAB DETERMINISTSICH
     % Calculate the vector of probabilities 
    c_i =   [(1-uB)*uA*CC_t*r0_W;...       % wildtype replicates and mutates to MA
             (1-uA)*uB*CC_t*r0_W;...       % wildtype replicates and mutates to MB
             uB*uA*CC_t*r0_W;...           % wildtype replicates and mutates to MAB
             uB*CC_t*r0_MA;...             % mutant MA mutates to mutant MAB 
             uA*CC_t*r0_MB];                % mutant MB mutates to mutant MAB   
     % Current cell numbers for each event     
     h_i = [repmat(Xw,[1 3]),repmat(XmA,[1 1]),repmat(XmB,[1 1])];
     % Current rate vector 
     lambda_i = c_i'.*h_i;

elseif sum(state==[0,1,0,0])==4
    % MA DETERMINISTSICH
     c_i =  [CC_t*r0_W*(1-uA-uB+uA*uB);... % growth of a wildtype 
                     P.PD(1).mu_0 + mu(t,1); ...       % death of a wildtype
                     (1-uB)*uA*CC_t*r0_W;...       % wildtype replicates and mutates to MA
                     (1-uA)*uB*CC_t*r0_W;...       % wildtype replicates and mutates to MB
                     uB*uA*CC_t*r0_W;...           % wildtype replicates and mutates to MAB
                     uB*CC_t*r0_MA;...             % mutant MA mutates to mutant MAB 
                     (1-uA)*CC_t*r0_MB;...         % growth of mutant MB
                     P.PD(3).mu_0 + mu(t,3);           % death of mutant MB
                     uA*CC_t*r0_MB;                % mutant MB mutates to mutant MAB        
                     CC_t*r0_MAB;...               % growth of mutant MAB
                     P.PD(4).mu_0 + mu(t,4)];          % death of mutant MAB
    % Current cell numbers for each event     
     h_i = [repmat(Xw,[1 5]),repmat(XmA,[1 1]),repmat(XmB,[1 3]),repmat(XmAB,[1 2])];
     % Current rate vector 
     lambda_i = c_i'.*h_i;
     
elseif sum(state==[0,0,1,0])==4
    % MB DETERMINISTSICH
    c_i =  [CC_t*r0_W*(1-uA-uB+uA*uB);... % growth of a wildtype 
                     P.PD(1).mu_0 + mu(t,1); ...       % death of a wildtype
                     (1-uB)*uA*CC_t*r0_W;...       % wildtype replicates and mutates to MA
                     (1-uA)*uB*CC_t*r0_W;...       % wildtype replicates and mutates to MB
                     uB*uA*CC_t*r0_W;...           % wildtype replicates and mutates to MAB
                     (1-uB)*CC_t*r0_MA;...         % growth of mutant MA
                     P.PD(2).mu_0 + mu(t,2);           % death of mutant MA
                     uB*CC_t*r0_MA;...             % mutant MA mutates to mutant MAB 
                     uA*CC_t*r0_MB;                % mutant MB mutates to mutant MAB        
                     CC_t*r0_MAB;...               % growth of mutant MAB
                     P.PD(4).mu_0 + mu(t,4)];          % death of mutant MAB
    % Current cell numbers for each event     
     h_i = [repmat(Xw,[1 5]),repmat(XmA,[1 3]),repmat(XmB,[1 1]),repmat(XmAB,[1 2])];
     % Current rate vector 
     lambda_i = c_i'.*h_i;
     
elseif sum(state==[0,0,0,1])==4
    % MAB DETERMINISTSICH
    c_i =  [CC_t*r0_W*(1-uA-uB+uA*uB);... % growth of a wildtype 
                     P.PD(1).mu_0 + mu(t,1); ...       % death of a wildtype
                     (1-uB)*uA*CC_t*r0_W;...       % wildtype replicates and mutates to MA
                     (1-uA)*uB*CC_t*r0_W;...       % wildtype replicates and mutates to MB
                     uB*uA*CC_t*r0_W;...           % wildtype replicates and mutates to MAB
                     (1-uB)*CC_t*r0_MA;...         % growth of mutant MA
                     P.PD(2).mu_0 + mu(t,2);           % death of mutant MA
                     uB*CC_t*r0_MA;...             % mutant MA mutates to mutant MAB 
                     (1-uA)*CC_t*r0_MB;...         % growth of mutant MB
                     P.PD(3).mu_0 + mu(t,3);           % death of mutant MB
                     uA*CC_t*r0_MB];                % mutant MB mutates to mutant MAB   
    % Current cell numbers for each event     
     h_i = [repmat(Xw,[1 5]),repmat(XmA,[1 3]),repmat(XmB,[1 3])];
     % Current rate vector 
     lambda_i = c_i'.*h_i;
    
elseif sum(state==[0,1,1,0])==4
    % MA,MB DETERMINISTSICH
   c_i =  [CC_t*r0_W*(1-uA-uB+uA*uB);... % growth of a wildtype 
                     P.PD(1).mu_0 + mu(t,1); ...       % death of a wildtype
                     (1-uB)*uA*CC_t*r0_W;...       % wildtype replicates and mutates to MA
                     (1-uA)*uB*CC_t*r0_W;...       % wildtype replicates and mutates to MB
                     uB*uA*CC_t*r0_W;...           % wildtype replicates and mutates to MAB
                     uB*CC_t*r0_MA;...             % mutant MA mutates to mutant MAB 
                     uA*CC_t*r0_MB;                % mutant MB mutates to mutant MAB        
                     CC_t*r0_MAB;...               % growth of mutant MAB
                     P.PD(4).mu_0 + mu(t,4)];          % death of mutant MAB
    % Current cell numbers for each event     
     h_i = [repmat(Xw,[1 5]),repmat(XmA,[1 1]),repmat(XmB,[1 1]),repmat(XmAB,[1 2])];
     % Current rate vector 
     lambda_i = c_i'.*h_i;
    
elseif sum(state==[0,1,0,1])==4
    % MA,MAB DETERMINISTSICH 
    c_i =  [CC_t*r0_W*(1-uA-uB+uA*uB);... % growth of a wildtype 
                     P.PD(1).mu_0 + mu(t,1); ...       % death of a wildtype
                     (1-uB)*uA*CC_t*r0_W;...       % wildtype replicates and mutates to MA
                     (1-uA)*uB*CC_t*r0_W;...       % wildtype replicates and mutates to MB
                     uB*uA*CC_t*r0_W;...           % wildtype replicates and mutates to MAB
                     uB*CC_t*r0_MA;...             % mutant MA mutates to mutant MAB 
                     (1-uA)*CC_t*r0_MB;...         % growth of mutant MB
                     P.PD(3).mu_0 + mu(t,3);           % death of mutant MB
                     uA*CC_t*r0_MB];                % mutant MB mutates to mutant MAB   
    % Current cell numbers for each event     
     h_i = [repmat(Xw,[1 5]),repmat(XmA,[1 1]),repmat(XmB,[1 3])];
     % Current rate vector 
     lambda_i = c_i'.*h_i;
     
    
elseif sum(state==[0,0,1,1])==4
    % MB,MAB DETERMINISTSICH
    c_i =  [CC_t*r0_W*(1-uA-uB+uA*uB);... % growth of a wildtype 
                     P.PD(1).mu_0 + mu(t,1); ...       % death of a wildtype
                     (1-uB)*uA*CC_t*r0_W;...       % wildtype replicates and mutates to MA
                     (1-uA)*uB*CC_t*r0_W;...       % wildtype replicates and mutates to MB
                     uB*uA*CC_t*r0_W;...           % wildtype replicates and mutates to MAB
                     (1-uB)*CC_t*r0_MA;...         % growth of mutant MA
                     P.PD(2).mu_0 + mu(t,2);           % death of mutant MA
                     uB*CC_t*r0_MA;...             % mutant MA mutates to mutant MAB
                     uA*CC_t*r0_MB];                % mutant MB mutates to mutant MAB
    % Current cell numbers for each event     
     h_i = [repmat(Xw,[1 5]),repmat(XmA,[1 3]),repmat(XmB,[1 1])];
     % Current rate vector 
     lambda_i = c_i'.*h_i;
    
    
elseif sum(state==[0,1,1,1])==4
    % MA,MB,MAB DETERMINISTSICH
        c_i =  [CC_t*r0_W*(1-uA-uB+uA*uB);... % growth of a wildtype 
                     P.PD(1).mu_0 + mu(t,1); ...       % death of a wildtype
                     (1-uB)*uA*CC_t*r0_W;...       % wildtype replicates and mutates to MA
                     (1-uA)*uB*CC_t*r0_W;...       % wildtype replicates and mutates to MB
                     uB*uA*CC_t*r0_W;...           % wildtype replicates and mutates to MAB
                     uB*CC_t*r0_MA;...             % mutant MA mutates to mutant MAB 
                     uA*CC_t*r0_MB];                % mutant MB mutates to mutant MAB   
    % Current cell numbers for each event     
     h_i = [repmat(Xw,[1 5]),repmat(XmA,[1 1]),repmat(XmB,[1 1])];
     % Current rate vector 
     lambda_i = c_i'.*h_i;
    
end
end

