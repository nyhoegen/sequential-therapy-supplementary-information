% Example Skript how to run the Code 

% Initialize Matlab 
clear all 
close all 

%% Load Parameter 

% Dataset for the Drug Cip: 
Cip = load('Cip.mat'); 

% Contains Patient and Lab Data
P = Cip.P; 
L = Cip.L;

% Change the Drug concnetration in Patient and Lab 
P.PK(1).D = 0.1; 
P.PK(2).D = 0.1; 
L.Lab.cA = 0.1;
L.Lab.cB = 0.1;

% Change the Time between administrations (default 24h)
P.PK(1).Tthis = 12; 
P.PK(1).Tother = 12; 
P.PK(2).Tthis = 12; 
P.PK(2).Tother = 12; 
L.Lab.TA = 12;
L.Lab.TB = 12;

% Define a treatment (the two drugs are encoded with 1 and 0) 
t1 = repmat([ones(1,1) zeros(1,1)],[1 10]);
t2 = repmat([1 1 1 1 0 0 0 0],[1 5]);

% Change the treatment in the patient 
P.PK(1).cycle = t1;
P.PK(2).cycle = ~t1; %IMPORTANT change the numbering here. 1 is always the drug of that repective PK set


%% Deterministic Model 

% Patient Example 
[dyn,data] = Two_Drugs_Cycling_CC_u(P.G,P.INT,P.PD,P.PK,P.CC,P.M); 
% Plot the dynamics 
Plot_Cycling_Data(dyn,data,1,'Pat')

% Lab Example 
% [dyn,data] = Lab_Cycling_NumCC_u(L.G,L.PD,L.Lab,L.CC,L.M); 
% % Plot the dynamics 
% Plot_Cycling_Data(dyn,data,1,'Lab')

%% Hybrid Model 

% Number of Simulations and a seed for each run 
n_sim = 5;
rng(5); 
seed = datasample(1:100000,n_sim,'Replace',false);

% Run the Algorithm n_sim times 
for i = 1:n_sim
tic
% Patient 
Sim{i} = Gillespie_Thinning_Hybrid_Full(P,'PAT',1000,0.9,seed(i)); 
% Lab 
% Sim{i} = Gillespie_Thinning_Hybrid_Full(L,'LAB',1000,0.9,seed(i)); 
toc
end


%% Plot Settings 

%Colors
dark = [2 93 114]/255;
pink = [178 93 141]/255;
light = [121 182 185]/255; 
occa = [215,200,100]/255;
black = [0 0 0];
% Define a color vector 
col = [dark;pink;light;occa];
% Define a colormap
col_n=4;
mymap1 = [linspace(black(1),pink(1),6)', linspace(black(2),pink(2),6)',linspace(black(3),pink(3),6)'];
mymap2 = [linspace(pink(1),occa(1),col_n)', linspace(pink(2),occa(2),col_n)',linspace(pink(3),occa(3),col_n)'];
mymap3 = [linspace(occa(1),dark(1),col_n)', linspace(occa(2),dark(2),col_n)',linspace(occa(3),dark(3),col_n)'];
mymap4 = [linspace(dark(1),light(1),col_n)', linspace(dark(2),light(2),col_n)',linspace(dark(3),light(3),col_n)'];
mymap5 = [linspace(light(1),light(1),col_n)', linspace(light(2),light(2),col_n)',linspace(light(3),light(3),col_n)'];
mymap = [mymap1;mymap2;mymap3;mymap4;mymap5];


%% Plot the Dynamics of Simulation and Deterministic Model 
close all
figure(1) 
for i = 1:n_sim
    p1(5)=plot(Sim{i}.tk,Sim{i}.W,'Color',[mymap(9,:) 0.3]);
    hold on 
end
for i = 1:n_sim
    p1(6)=plot(Sim{i}.tk,Sim{i}.MA,'Color',[mymap(15,:) 0.3]);
    hold on 
end
for i = 1:n_sim
    p1(7)=plot(Sim{i}.tk,Sim{i}.MB,'Color',[mymap(18,:) 0.3]);
    hold on 
end
for i = 1:n_sim
    p1(8)=plot(Sim{i}.tk,Sim{i}.MAB,'Color',[mymap(6,:) 0.3]);
    hold on 
end
p1(1)=plot(dyn.time, dyn.W,'Color',mymap(9,:), 'Linewidth',3);
hold on 
p1(2)=plot(dyn.time, dyn.MA,'Color',mymap(15,:), 'Linewidth',3);
hold on 
p1(3)=plot(dyn.time, dyn.MB,'Color',mymap(18,:), 'Linewidth',3);
hold on 
p1(4)=plot(dyn.time, dyn.MAB,'Color',mymap(6,:), 'Linewidth',3);
hold on 
plot([0 300], [1000 1000], 'Color', [120 120 120]/255); 
hold on 
set(gca,'Fontsize',16)
xlim([0 300])
xlabel('Time in hours'); 
ylabel('Cell number'); 
set(gca,'Yscale','log'); 
set(gca,'FontSize',12)
legend(p1,'Deterministic W','Deterministic MA','Deterministic MB','Deterministic MAB',...
    'Simulation W','Simulation MA', 'Simulation MB','Simulation MAB'); 
title('Hybrid model vs. Deterministic model')

