function [mu] = mu_of_c(PD,Lab,t)
% 
% Function to calculate death rate to the Lab Model with
% carrying limit 
% 
% INPUT: 
% PD    = 1x4 struct array of PD parameter (one struct for each type)
%         (struct 1: W, struct 2: MA, struct 3: MB, struct 4: MAB)
%         Psi_max  - maximum growth rate in absense of AB
%         mu_0     - intrinsic death rate 
%         Psi_minA - maximum kill rate of drug A
%         Psi_minB - maximum kill rate of drug B
%         zMICA    - MIC for drug A
%         zMICB    - MIC for drug B
%         KA       - hill coefficient regarding drug A
%         KB       - hill coefficient regarding drug B
%
% Lab   = 1x1 struct array of drug related parameter 
%         Lab.TA    - Lenght of treatment with drug A (each switch)
%         Lab.TB    - Lenght of treatment with drug B (each switch)
%         Lab.cA    - Cocnetration of drug A
%         Lab.cB    - Concentration of drug B
%         Lab.cycle - Cycling schdule of the sequential treatment 
% 
% OUTPUT 
% psi   = growth rate for either drug A or B depending on t
% 

%==========================================================================
%==========================================================================
%       CHOOSE FUCNTION FOR CC
%==========================================================================

% Function handle for kill rate 
mu_c = @(psi_max,mu_0,psi_min,zmic,k,c) (((psi_max-psi_min)*(c/zmic)^k)/((c/zmic)^k-(psi_min/(psi_max))));
    
% Calculate which drug is present at t: 
% Number of Administartions durng the cycle 
n_ad = length(Lab.cycle); 
tri_mat = triu(ones(n_ad),1);
% Calculate at which time the considered drug is aministered during cycle
ad_times = (Lab.cycle*Lab.TA+(~Lab.cycle)*Lab.TB)*tri_mat;

% check in which interval t is: 
ind = sum(t >= ad_times);

% check which treatment is used: 
drug = Lab.cycle(ind); 

% Calculate the growth rate depending on this value: 
if drug 
    mu = mu_c(PD.Psi_max,PD.mu_0,PD.Psi_minA,PD.zMICA,PD.kA,Lab.cA); 
    d = 'A';
else  
    mu = mu_c(PD.Psi_max,PD.mu_0,PD.Psi_minB,PD.zMICB,PD.kB,Lab.cB); 
    d = 'B';
end 

end